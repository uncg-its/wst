<?php
use Composer\Script\Event;

/**
 * Installer to be used when composer is installing the base app.
 */
class Internal_Composer_Installer
{
	public static function setupArfDownloadsFolder(Event $event)
	{

		$paths = self::_helperDirectory($event);

		$io = $event->getIO();

		$arfDownloadDirectory = $paths['basePath'] . '/arf-downloads';

		$io->write('Creating ARF download folder');

		if (!file_exists($arfDownloadDirectory)) {
			mkdir($arfDownloadDirectory, 0776, true);
			$io->write('Created.');
		} else {
			$io->write('Already exists.');
		}


	}


	public static function setupWritableDirectories(Event $event)
	{
		$paths = self::_helperDirectory($event);

		$io = $event->getIO();

		$writable = array(
			'/arf-downloads',
		);

		foreach ($writable as $w) {
			self::_helperRecursiveChmod($paths['basePath'] . $w, 0757);
			$io->write('Making ' . $w . ' writable.');
		}
	}



	public static function _helperDirectory(Event $event)
	{
		return array(
			'basePath' => realpath($event->getComposer()->getConfig()->get('vendor-dir') . '/../'),
		);
	}

	public static function _helperRecursiveChmod($path, $permission)
	{
		// Check if the path exists
		if (!file_exists($path)) {
			return(false);
		}

		// See whether this is a file
		if (is_file($path)) {

			// Chmod the file with our given filepermissions
			chmod($path, $permission);

			// If this is a directory...
		} elseif (is_dir($path)) {

			// Then get an array of the contents
			$foldersAndFiles = scandir($path);

			// Remove "." and ".." from the list
			$entries = array_slice($foldersAndFiles, 2);

			// Parse every result...
			foreach ($entries as $entry) {

				// And call this function again recursively, with the same permissions
				self::_helperRecursiveChmod($path . "/" . $entry, $permission);

			}

			// When we are done with the contents of the directory, we chmod the directory itself
			chmod($path, $permission);
		}

		// Everything seemed to work out well, return TRUE
		return(true);
	}

}