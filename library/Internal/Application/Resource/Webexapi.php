<?php

class Internal_Application_Resource_Webexapi extends Zend_Application_Resource_ResourceAbstract
{
	protected $_xmlUrl = '';
	protected $_username = '';
	protected $_password = '';
	protected $_partnerId = '';
	protected $_siteId = '';
	protected $_siteName = '';
	protected $_newUserPlaceholderPassword = '';
	protected $_newUserMeetingTypes = '';
	protected $_nbrHost = '';

	/*
	 * resources.webexApi.xmlUrl = "https://uncg-test.webex.com"
resources.webexApi.username = "uncg-api-user"
resources.webexApi.password = "UA2HWghUDBCRiUFeQib7eNjUXn9j7g"
resources.webexApi.partnerId = "c7J4U418gpTZJe2V3tM4Bw"
resources.webexApi.siteId = "928667"
	 */

	// setters
	public function setXmlUrl($val)
	{
		$this->_xmlUrl = $val;
	}

	public function setUsername($val)
	{
		$this->_username = $val;
	}

	public function setPassword($val)
	{
		$this->_password = $val;
	}

	public function setPartnerId($val)
	{
		$this->_partnerId = $val;
	}

	public function setSiteId($val)
	{
		$this->_siteId = $val;
	}
	public function setSiteName($val)
	{
		$this->_siteName = $val;
	}

	public function setNewUserPlaceholderPassword($val) {
		$this->_newUserPlaceholderPassword = $val;
	}

	public function setNewUserMeetingTypes($val) {
		$this->_newUserMeetingTypes = $val;
	}

	public function setNbrHost($val) {
		$this->_nbrHost = $val;
	}

	public function init()
	{

		$resourceConfig = array(
			'xmlUrl'                     => $this->_xmlUrl,
			'username'                   => $this->_username,
			'password'                   => $this->_password,
			'partnerId'                  => $this->_partnerId,
			'siteId'                     => $this->_siteId,
			'siteName'                   => $this->_siteName,
			'newUserPlaceholderPassword' => $this->_newUserPlaceholderPassword,
			'newUserMeetingTypes'        => $this->_newUserMeetingTypes,
			'nbrHost'                    => $this->_nbrHost
		);

		Zend_Registry::set('webexApi', $resourceConfig);
	}
}