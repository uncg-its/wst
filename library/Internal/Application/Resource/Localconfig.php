<?php

class Internal_Application_Resource_Localconfig extends Zend_Application_Resource_ResourceAbstract {
	protected $_eligibilityList = '';
	protected $_emailSuffix = '';
	protected $_userDiffFilePath = '';
	protected $_queueProcessingLimit = '';
	protected $_userSourceType = '';
	protected $_webexLoginUrl = '';
	protected $_queueRetryAttempts = '';
	protected $_permittedApiCheckFailures = '';
	protected $_apiFailureCheckWindow = '';
	protected $_expectedSourceMinCount = '';
	protected $_apiQueueThreshold = '';
	protected $_maxDaysWithoutStaging = '';
	protected $_itemsPerPage = '';
	protected $_coreCronJobs = '';
	protected $_siteRecordingStorageLimit = '';
	protected $_siteActiveHostsLimit = '';
	protected $_siteTrashDaysBeforeDelete = '';
	protected $_localChecksumFields = '';
	protected $_appUrl = '';
	protected $_checkTeacherFeed = '';
	protected $_environmentName = '';

	// setters
	public function setEligibilityList($val) {
		$this->_eligibilityList = $val;
	}

	public function setEmailSuffix($val) {
		$this->_emailSuffix = $val;
	}

	public function setUserDiffFilePath($val) {
		$this->_userDiffFilePath = $val;
	}

	public function setQueueProcessingLimit($val) {
		$this->_queueProcessingLimit = $val;
	}

	public function setQueueRetryAttempts($val) {
	    $this->_queueRetryAttempts = $val;
	}

	public function setPermittedApiCheckFailures($val) {
		$this->_permittedApiCheckFailures = $val;
	}

	public function setApiFailureCheckWindow($val) {
		$this->_apiFailureCheckWindow = $val;
	}

	public function setUserSourceType($val) {
		$this->_userSourceType = $val;
	}

	public function setWebexLoginUrl($val) {
		$this->_webexLoginUrl = $val;
	}

	public function setExpectedSourceMinCount($val) {
		$this->_expectedSourceMinCount = $val;
	}

	public function setApiQueueThreshold($val) {
		$this->_apiQueueThreshold = $val;
	}

	public function setMaxDaysWithoutStaging($val) {
		$this->_maxDaysWithoutStaging = $val;
	}

	public function setItemsPerPage($val) {
		$this->_itemsPerPage = $val;
	}

	public function setCoreCronJobs($val) {
		$this->_coreCronJobs = $val;
	}

	public function setSiteRecordingStorageLimit($val) {
		$this->_siteRecordingStorageLimit = $val;
	}

	public function setSiteActiveHostsLimit($val) {
		$this->_siteActiveHostsLimit     = $val;
	}

	public function setSiteTrashDaysBeforeDelete($val) {
		$this->_siteTrashDaysBeforeDelete     = $val;
	}

	public function setLocalChecksumFields($val) {
		$this->_localChecksumFields     = $val;
	}

	public function setAppUrl($val) {
		$this->_appUrl = $val;
	}

	public function setCheckTeacherFeed($val) {
		$this->_checkTeacherFeed = $val == "true";
	}

	public function setEnvironmentName($val) {
		$this->_environmentName = $val;
	}

	public function init() {

		$resourceConfig = array(
			'webexLoginUrl'             => $this->_webexLoginUrl,
			'eligibilityList'           => $this->_eligibilityList,
			'emailSuffix'               => $this->_emailSuffix,
			'userDiffFilePath'          => $this->_userDiffFilePath,
			'queueProcessingLimit'      => $this->_queueProcessingLimit,
			'userSourceType'            => $this->_userSourceType,
			'queueRetryAttempts'        => $this->_queueRetryAttempts,
			'permittedApiCheckFailures' => $this->_permittedApiCheckFailures,
			'apiFailureCheckWindow'     => $this->_apiFailureCheckWindow,
			'expectedSourceMinCount'    => $this->_expectedSourceMinCount,
			'apiQueueThreshold'         => $this->_apiQueueThreshold,
			'maxDaysWithoutStaging'     => $this->_maxDaysWithoutStaging,
			'itemsPerPage'              => $this->_itemsPerPage,
			'coreCronJobs'              => $this->_coreCronJobs,
			'siteRecordingStorageLimit' => $this->_siteRecordingStorageLimit,
			'siteActiveHostsLimit'      => $this->_siteActiveHostsLimit,
			'siteTrashDaysBeforeDelete' => $this->_siteTrashDaysBeforeDelete,
			'localChecksumFields'       => $this->_localChecksumFields,
			'appUrl'                    => $this->_appUrl,
			'checkTeacherFeed'          => $this->_checkTeacherFeed,
			'environmentName'           => $this->_environmentName
		);

		Zend_Registry::set('localConfig', $resourceConfig);
	}
}