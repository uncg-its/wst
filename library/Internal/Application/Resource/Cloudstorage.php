<?php

class Internal_Application_Resource_Cloudstorage extends Zend_Application_Resource_ResourceAbstract {
	protected $_activeServices = '';

	// setters
	public function setActiveServices($val) {
		$this->_activeServices = $val;
	}

	public function init() {

		$resourceConfig = array(
			'activeServices'         => $this->_activeServices
		);

		Zend_Registry::set('cloudStorage', $resourceConfig);
	}
}