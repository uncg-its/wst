<?php

class Internal_Application_Resource_Boxapi extends Zend_Application_Resource_ResourceAbstract {
	protected $_enterpriseId = '';
	protected $_clientId = '';
	protected $_clientSecret = '';
	protected $_redirectUri = '';
	protected $_apiBaseUrl = '';
	protected $_apiBaseUploadUrl = '';
	protected $_csrfPreventionString = '';

	/*
	 * resources.boxApi.enterpriseId  = "ENTERPRISE_ID"
resources.boxApi.clientId      = "CLIENT_ID"
resources.boxApi.clientSecret  = "CLIENT_SECRET"
resources.boxApi.redirectUri   = "REDIRECT_URI"
resources.boxApi.apiBaseUrl    = "https://api.box.com/2.0/"
	 */

	// setters
	public function setEnterpriseId($val) {
		$this->_enterpriseId = $val;
	}

	public function setClientId($val) {
		$this->_clientId = $val;
	}

	public function setClientSecret($val) {
		$this->_clientSecret = $val;
	}

	public function setRedirectUri($val) {
		$this->_redirectUri = $val;
	}

	public function setApiBaseUrl($val) {
		$this->_apiBaseUrl = $val;
	}

	public function setApiBaseUploadUrl($val) {
		$this->_apiBaseUploadUrl = $val;
	}

	public function setCsrfPreventionString($val) {
		$this->_csrfPreventionString = $val;
	}

	public function init() {

		$resourceConfig = array(
			'enterpriseId'         => $this->_enterpriseId,
			'clientId'             => $this->_clientId,
			'clientSecret'         => $this->_clientSecret,
			'redirectUri'          => $this->_redirectUri,
			'apiBaseUrl'           => $this->_apiBaseUrl,
			'apiBaseUploadUrl'     => $this->_apiBaseUploadUrl,
			'csrfPreventionString' => $this->_csrfPreventionString
		);

		Zend_Registry::set('boxApi', $resourceConfig);
	}
}