<?php

class Internal_Application_Resource_Ldapreader extends Zend_Application_Resource_ResourceAbstract
{
    protected $_server = '';
    
    protected $_dn = '';

    protected $_password = '';
    
    protected $_port = '';
        
    public function setServer($val)
    {
        $this->_server = $val;
    }
    
    public function setDn($val)
    {
        $this->_dn = $val;
    }
    
    public function setPassword($val)
    {
        $this->_password = $val;
    }
    
    public function setPort($val)
    {
        $this->_port = $val;
    }
       
    public function init()
    {
                       
        $adminConfig = array(
            'server'                        => $this->_server,
            'dn'                            => $this->_dn,
            'password'                      => $this->_password,
            'port'                          => $this->_port
        );
        
        Zend_Registry::set('ldapreader', $adminConfig);
    }
}