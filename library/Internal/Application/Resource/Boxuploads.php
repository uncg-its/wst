<?php

class Internal_Application_Resource_Boxuploads extends Zend_Application_Resource_ResourceAbstract {
	protected $_recordingUploadBaseFolderName = '';

	// setters
	public function setRecordingUploadBaseFolderName($val) {
		$this->_recordingUploadBaseFolderName = $val;
	}

	public function init() {

		$resourceConfig = array(
			'recordingUploadBaseFolderName'         => $this->_recordingUploadBaseFolderName
		);

		Zend_Registry::set('boxUploads', $resourceConfig);
	}
}