<?php

class Internal_Application_Resource_Canvasteacherfeed extends Zend_Application_Resource_ResourceAbstract {
	protected $_endpoint = '';
	protected $_apiKey = '';

	// setters
	public function setEndpoint($val) {
		$this->_endpoint = $val;
	}

	public function setApiKey($val) {
		$this->_apiKey = $val;
	}

	public function init() {

		$resourceConfig = array(
			'endpoint' => $this->_endpoint,
			'apiKey'   => $this->_apiKey
		);

		Zend_Registry::set('canvasTeacherFeed', $resourceConfig);
	}
}