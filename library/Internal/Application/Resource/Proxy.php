<?php

class Internal_Application_Resource_Proxy extends Zend_Application_Resource_ResourceAbstract
{
    protected $_url = '';
    protected $_port = '';

    public function setUrl($val)
    {
        $this->_url = $val;
    }

    public function setPort($val)
    {
        $this->_port = $val;
    }
       
    public function init()
    {
                       
        $resourceConfig = array(
            'url'               => $this->_url,
            'port'               => $this->_port
        );
        
        Zend_Registry::set('proxy', $resourceConfig);
    }
}