<?php

class Internal_Application_Resource_Metricsdashboard extends Zend_Application_Resource_ResourceAbstract {
	protected $_endpoint = '';
	protected $_token = '';
	protected $_serviceId = '';
	protected $_sessionLengthThresholds = '';
	protected $_daysAgo = '';

	// setters
	public function setEndpoint($val) {
		$this->_endpoint = $val;
	}

	public function setToken($val) {
		$this->_token = $val;
	}

	public function setServiceId($val) {
		$this->_serviceId = $val;
	}

	public function setSessionLengthThresholds($val) {
		$this->_sessionLengthThresholds = $val;
	}

	public function setDaysAgo($val) {
		$this->_daysAgo = $val;
	}

	public function init() {

		$resourceConfig = array(
			'endpoint'                => $this->_endpoint,
			'token'                   => $this->_token,
			'serviceId'               => $this->_serviceId,
			'sessionLengthThresholds' => $this->_sessionLengthThresholds,
			'daysAgo'                 => $this->_daysAgo
		);

		Zend_Registry::set('metricsDashboard', $resourceConfig);
	}
}