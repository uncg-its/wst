<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/3/16
 * Time: 4:39 PM
 *
 * Testing custom form validators
 *
 */

class Custom_Validate_ExistingWebexUser extends Zend_Validate_Abstract {

	const MSG_USERNOTFOUND = "msgUserNotFound";

	protected $_messageTemplates = array(
		self::MSG_USERNOTFOUND => "This user was not found in WebEx."
	);

	function isValid($value) {

		$userListModel = new Wst_Model_Dbtable_Userlist();
		$user = $userListModel->getUserByWebExId($value);

		if (!$user) {
			$this->_error(self::MSG_USERNOTFOUND);
			return false;
		}

		return true;
	}
}