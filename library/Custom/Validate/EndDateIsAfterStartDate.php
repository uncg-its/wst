<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 4/22/16
 * Time: 11:36 AM
 */

class Custom_Validate_EndDateIsAfterStartDate extends Zend_Validate_Abstract {

	const MSG_BEFORE_STARTDATE = "beforeStartDate";
	const MSG_MISSING_CONTEXT = "missingContext";

	protected $_messageTemplates = array(
		self::MSG_BEFORE_STARTDATE => "end date must be after the start date",
		self::MSG_MISSING_CONTEXT => "missing context",
	);

	function isValid($value, $context = null) {

		//$this->_setValue($value);

		if (is_null($context)) {
			$this->_error(self::MSG_MISSING_CONTEXT);
			return false;
		}

		$startDate = $context["start_date"];
		$endDate = $value;

		$now = time();

		if ($startDate != "") {
			if (strtotime($endDate) < strtotime($startDate)) {
				$this->_error(self::MSG_BEFORE_STARTDATE);
				return false;
			}
		}

		return true;
	}
}