<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/3/16
 * Time: 4:39 PM
 *
 * Testing custom form validators
 *
 */

class Custom_Validate_UserDoesNotHaveTelephony extends Zend_Validate_Abstract {

	const MSG_USERALREADYHASTELEPHONY = "msgUserAlreadyHasTelephony";

	protected $_messageTemplates = array(
		self::MSG_USERALREADYHASTELEPHONY => "This user already has telephony. Edit or Remove priviliges instead."
	);

	function isValid($value) {

		$userListModel = new Wst_Model_Dbtable_Userlist();
		$user = $userListModel->getUserByWebExId($value);

		if ($user && !is_null($user["telephony_privileges"])) {
			$this->_error(self::MSG_USERALREADYHASTELEPHONY);
			return false;
		}

		return true;
	}
}