<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/3/16
 * Time: 4:39 PM
 *
 * Testing custom form validators
 *
 */

class Custom_Validate_ValidDayOfMonth extends Zend_Validate_Abstract {

	const MSG_VALIDDAYOFMONTH = "msgValidDayOfMonth";
	const MSG_ISINTEGER = "msgIsInteger";

	protected $_messageTemplates = array(
		self::MSG_VALIDDAYOFMONTH => "%value% is not a valid day of the month occurring every month (1-28)",
		self::MSG_ISINTEGER => "%value% is not a positive integer"
	);

	function isValid($value) {

		if (!ctype_digit($value)) { // not using is_int because the form field casts it as a string
			$this->_error(self::MSG_ISINTEGER);
			return false;
		}

		if ($value < 0 || $value > 28) {
			$this->_error(self::MSG_VALIDDAYOFMONTH);
			return false;
		}

		return true;
	}
}