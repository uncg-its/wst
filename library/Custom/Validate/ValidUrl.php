<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/3/16
 * Time: 4:39 PM
 *
 * Testing custom form validators
 *
 */

class Custom_Validate_ValidUrl extends Zend_Validate_Abstract {

	const MSG_VALIDURL = "msgValidUrl";

	protected $_messageTemplates = array(
		self::MSG_VALIDURL => "%value% is not a valid URL beginning in http:// or https://"
	);

	function isValid($value) {

		$validUrl = Zend_Uri::check($value);

		$urlBits = explode("://", $value);
		$prefix = $urlBits[0];
		$validPrefix = ($prefix == "http" || $prefix == "https");

		if (!$validUrl || !$validPrefix) {
			$this->_error(self::MSG_VALIDURL);
			return false;
		}

		return true;
	}
}