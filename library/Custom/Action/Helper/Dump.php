<?php

class Wst_Controller_Action_Helper_Dump extends Zend_Controller_Action_Helper_Abstract {

	public function dump($var, $die = false, $msg = "") {
		echo "<pre>";
		echo var_dump($var);
		echo "</pre>";

		if ($die) {
			die($msg);
		}
	}
}

?>