/**
 * Created by mattlibera on 5/26/16.
 */

var baseUrl;

$(document).ready(function() {

    baseUrl = $('#baseUrl').val();

    $('.ui-sortable th[data-sort]').click(function () {
        $('#sort').val($(this).attr('data-sort'));
        $('#direction').val($(this).attr('data-direction'));
        $('#searchForm').submit();
    });

    $('#sortTabs a').click(function() {
        $('#tab').val($(this).attr('data-tab'));
        $('#searchForm').submit();
        return false;
    });

});