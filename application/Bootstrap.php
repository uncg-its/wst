<?php

class Bootstrap extends Ot_Application_Bootstrap_Bootstrap {

	public function _initCron() {
		$cronjobs = array();

		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_Stageusers', 'User Feed STEP 1: Stage Users', 'Stages users from LDAP source file', '30 7,11,15,19 * * *', 'Wst_Cronjob_Stageusers');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_BuildApiQueue', 'User Feed STEP 2: Build API Queue', 'Generates API calls from eligible users in the staging table', '50 7,11,15,19 * * *', 'Wst_Cronjob_BuildApiQueue');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_ProcessApiQueue', 'User Feed STEP 3: Process API Queue', 'Executes queued API calls and updates local users database', '*/10 0,1,2,3,4,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23 * * *', 'Wst_Cronjob_ProcessApiQueue');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_RevertNamesToLdap', 'User Feed: Revert Names to LDAP', 'Checks user information in WebEx against local information. If users have made changes to first/last name information, sync it back to the official LDAP listing.', '0 1 * * *', 'Wst_Cronjob_RevertNamesToLdap');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_ProcessOverrides', 'User Feed: Process Overrides', 'Checks overrides table for expiring overrides, and changes any overridden users back to being feed-managed', '15 1 * * *', 'Wst_Cronjob_ProcessOverrides');


		// session archives
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_ArchiveSessions', 'Archival: WebEx Sessions', 'Updates the local database with information on past WebEx sessions', '00 2 * * *', 'Wst_Cronjob_ArchiveSessions');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_ArchiveSessionsTrueUp', 'Archival: WebEx Sessions (true-up)', 'Fetches all sessions within the past week and archives them if they were not already archived. Sometimes there is a wide variance in when a WebEx session is posted for reporting - this job is a failsafe against delayed reporting.', '15 2 * * 1', 'Wst_Cronjob_ArchiveSessionsTrueUp');

		// attendee archives
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_ArchiveAttendees', 'Archival: Session Attendees', 'Updates the local database with information on past WebEx session attendees', '30 2 * * *', 'Wst_Cronjob_ArchiveAttendees');

		// recording archives
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_ArchiveRecordingsDaily', 'Archival: Recordings (daily)', 'Updates the local database with information on past WebEx session recordings (from past 2 days)', '00 3 * * *', 'Wst_Cronjob_ArchiveRecordingsDaily');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_ArchiveRecordingsWeekly', 'Archival: Recordings (weekly true-up)', 'Retrieves recording information from the past 2 weeks to ensure no gaps exist between WebEx and the local database', '15 3 * * 1', 'Wst_Cronjob_ArchiveRecordingsWeekly');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_RecordingArchiveTrueUp', 'Archival: Recording Information True-Up', 'Looks for any changes in recording names / owners and transfers those changes to local database. Also estimates which recordings have been trashed in WebEx.', '30 3 * * *', 'Wst_Cronjob_RecordingArchiveTrueUp');

		// metrics
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_FetchDailyMetrics', 'Daily Metrics STEP 1: Fetch', 'Gets metrics from WebEx sessions, recordings, attendees. Information pull happens from previous day. Stores metrics locally.', '00 4 * * *', 'Wst_Cronjob_FetchDailyMetrics');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_VerifyDailyMetrics', 'Daily Metrics STEP 2: Verify', 'Checks sessions from X days in the past or before (configurable) to make sure session / attendee / recording numbers are accurate. Preps for push to metrics dashboard.', '10 4 * * *', 'Wst_Cronjob_VerifyDailyMetrics');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_SendDailyMetrics', 'Daily Metrics STEP 3: Send', 'Sends verified metrics to metrics dashboard', '20 4 * * *', 'Wst_Cronjob_SendDailyMetrics');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_MonthlyMetricsGapChecking', 'Daily Metrics: Gap Checking', 'Checks for gaps in previous month\'s metrics and backfills them', '30 4 5 * *', 'Wst_Cronjob_MonthlyMetricsGapChecking');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_RebuildLocalMetricsTable', 'Daily Metrics: Rebuild Local Metrics Table', 'Takes all local metrics and rebuilds the local metrics-on-the-fly table', '40 4 * * *', 'Wst_Cronjob_RebuildLocalMetricsTable');

		// recording downloading / uploading
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_MarkExpiredRecordingsForDownload', 'Recording Downloads: Mark Expired Recordings for Deletion', 'Marks all recordings past the expiration limit (in settings) for download / deletion.', '45 4 * * *', 'Wst_Cronjob_MarkExpiredRecordingsForDownload');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_DownloadWebexRecordings', 'Recording Downloads: Download Next Recording', 'Attempts to download the next recording in the queue', '0,30 0,1,2,3,4,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23 * * *', 'Wst_Cronjob_DownloadWebexRecordings');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_DeleteDownloadedRecordingsFromWebex', 'Recording Downloads: Delete Downloaded Recordings', 'Deletes recordings that have been successfully downloaded from the WebEx server. Puts them into Trash at present.', '15 5 * * *', 'Wst_Cronjob_DeleteDownloadedRecordingsFromWebex');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_UploadRecordingsToCloudStorage', 'Recording Downloads: Upload to Cloud Storage', 'Uploads recording files to cloud storage', '15,45 0,1,2,3,4,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23 * * *', 'Wst_Cronjob_UploadRecordingsToCloudStorage');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_SendWeeklyRecordingDigests', 'Recording Downloads: Send Weekly Digests', 'Sends users a summary of their active recordings with expiration and direct links', '25 5 * * 1', 'Wst_Cronjob_SendWeeklyRecordingDigests');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_DeleteTempRecordingFiles', 'Recording Downloads: Clear Local Cache', 'Deletes local files from cache if they are old enough and successfully uploaded to cloud storage', '28 5 * * *', 'Wst_Cronjob_DeleteTempRecordingFiles');

		// Threshold checks
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_BoxTokenCheck', 'System: Box API Token Check', 'Checks the status of the Box API Token and alerts admins if there is no token.', '0 1 * * *', 'Wst_Cronjob_BoxTokenCheck');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_TelephonyUsageCheck', 'System: Telephony Threshold Check', 'Checks telephony usage during the current billing period and notifies admins if over the pre-set threshold.', '0 1 * * *', 'Wst_Cronjob_TelephonyUsageCheck');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_StorageUsageCheck', 'System: Recording Storage Threshold Check', 'Checks recording storage usage and notifies admins if over the pre-set threshold.', '0 1 * * *', 'Wst_Cronjob_StorageUsageCheck');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_StagingJobCheck', 'System: Staging Cronjob Check', 'Checks to be sure the user staging cronjob has not remained disabled for too long.', '5 1 * * *', 'Wst_Cronjob_StagingJobCheck');

		// Override emails
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_ProcessOverrideEmails', 'System: Send Override Expiration Emails', 'Sends email to the responsible party for all entered user overrides when expiration is within certain thresholds (defined in app variables)', '15 1 * * *', 'Wst_Cronjob_ProcessOverrideEmails');


		// API Health and Lock Files
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_Checkapi', 'System: API Status', 'Performs a basic check to see if the WebEx API is active. Upon multiple failures may disable the other app cronjobs.', '*/5 0,1,2,3,4,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23 * * *', 'Wst_Cronjob_Checkapi');
		//$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_Apihealthreport', 'System: API Health Report', 'Reports # of API failures over the last 24 hours in a daily report e-mail.', '0 0 * * *', 'Wst_Cronjob_Apihealthreport');
		//$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_ActiveHostsCheck', 'System: Active Hosts Threshold Check', 'Checks to make sure # of active hosts is within an acceptable threshold of the site maximum.', '20 0 * * *', 'Wst_Cronjob_ActiveHostsCheck');

		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_Detectlockfiles', 'Cron Jobs: Stale Job Detection', 'Checks to see if there are any cronjob lock files that have existed for over 1 hour', '*/15 0,1,2,3,4,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23 * * *', 'Wst_Cronjob_Detectlockfiles');
		$cronjobs[] = new Ot_Cron_Job('Wst_Cronjob_Deletelockfiles', 'Cron Jobs: Stale Job Deletion', 'Checks to see if there are any cronjob lock files that have existed for over 1 hour, and then deletes them', '*/30 0,1,2,3,4,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23 * * *', 'Wst_Cronjob_Deletelockfiles');

		$register = new Ot_Cron_JobRegister();
		$register->registerJobs($cronjobs);
	}

	public function _initTriggers() {

		$register = new Ot_Trigger_EventRegister();

		$trigger = new Ot_Trigger_Event('Wst_System_Event', 'System Event', 'Interacts with an event during an automated or API process');
		$trigger->addOption('subject', 'Subject line for the event')
			->addOption('summary', 'Brief summary of the event')
			->addOption('details', 'Longer description and information of event')
			->addOption('timestamp', 'Timestamp of the event (Format: Y-m-d @ H:i:s A)');

		$register->registerTriggerEvent($trigger);

		//// weekly digest trigger
		//$trigger = new Ot_Trigger_Event('Wst_Weekly_Recordings_Digest', 'Weekly Recordings Digest', 'Weekly email summary for users with 1 or more recordings in WebEx, with download options and expiration windows');
		//$trigger->addOption('todayDate', 'Today\'s Date - date of the digest\'s sending')
		//	->addOption('emailAddress', 'Email address for the exception that is expiring')
		//	->addOption('webExId', 'User\'s WebEx ID - should match the email, generally')
		//	->addOption('recordingInformationList', 'List of active recordings, links to their downloads, and dates they will be automatically removed from WebEx')
		//	->addOption('removalTimeframe', 'The institutional timeframe for deleting recordings (in days). Numerical value only. Value is stored as an application variable.')
		//	->addOption('myRecordingsUrl', 'Link that users will see in the digest to get them to userservices/my-recordings page in WST.');

		//$register->registerTriggerEvent($trigger);

		// new box collaboration trigger
		//$trigger = new Ot_Trigger_Event('Wst_New_Cloud_Storage_Share', 'New Cloud Storage Share Notification', 'Email sent to users whose recordings have been uploaded to a cloud storage service for the first time, notifying them about the collaboration.');
		//$trigger->addOption('username', 'Username of the user (without email suffix)')
		//	->addOption('fullName', 'Full Name of the user')
		//	->addOption('serviceName', 'Name of the service (e.g. Box, Google Drive, etc.) - as it should appear to the user')
		//	->addOption('serviceUrl', 'URL to be given to the user to access the cloud storage service.')
		//	->addOption('folderUrl', 'URL to be given to the user to access the specific shared folder.')
		//	->addOption('sharedFolderName', 'Display name of the shared folder in the cloud storage platform');

		// new box collaboration trigger - HTML version
		$trigger = new Ot_Trigger_Event('Wst_New_Cloud_Storage_Share_Html', 'New Cloud Storage Share Notification (HTML)', 'Email sent to users whose recordings have been uploaded to a cloud storage service for the first time, notifying them about the collaboration.');
		$trigger->addOption('username', 'Username of the user (without email suffix)')
			->addOption('htmlEmailBody', 'HTML Email Body in its entirety. Includes all message text, including share information and link to recording management page');

		$register->registerTriggerEvent($trigger);

		// weekly digest trigger - HTML version
		$trigger = new Ot_Trigger_Event('Wst_Weekly_Recordings_Digest_Html', 'Weekly Recordings Digest (HTML)', 'Weekly email summary for users with 1 or more recordings in WebEx, with download options and expiration windows. HTML version.');
		$trigger->addOption('emailAddress', 'Email address for user with recordings')
			->addOption('htmlEmailBody', 'HTML Email Body in its entirety. Includes all message text, including list of active recordings, links to their downloads, and dates they will be automatically removed from WebEx');

		$register->registerTriggerEvent($trigger);

		// override expiration trigger - HTML version
		$trigger = new Ot_Trigger_Event('Wst_Override_Expiration_Warning_Html', 'User Override Expiration Warning (HTML)', 'Notification to the person designated as the responsible party for a user override that the user override is within an expiration threshold (defined in app settings)');
		$trigger->addOption('emailAddress', 'Email address for responsible party')
			->addOption('username', 'Username / WebEx ID for the modified user')
			->addOption('htmlEmailBody', 'HTML Email Body in its entirety. Includes all message text, details on how to address the expiration');

		$register->registerTriggerEvent($trigger);
	}

	public function _initApiMethods() {
		$register = new Ot_Api_Register();

		$getUserEligibility = new Ot_Api_Endpoint('get-user-eligibility', 'Get User Eligibility');
		$getUserEligibility->setMethodClassname('Wst_Apiendpoint_Getusereligibility');
		$register->registerApiEndpoint($getUserEligibility);

		$activateAccount = new Ot_Api_Endpoint('activate-account', 'Activate WebEx User Account');
		$activateAccount->setMethodClassname('Wst_Apiendpoint_Activateaccount');
		$register->registerApiEndpoint($activateAccount);

		$getRecordingUsage = new Ot_Api_Endpoint('get-recording-usage', 'Get Recording Storage Usage');
		$getRecordingUsage->setMethodClassname('Wst_Apiendpoint_Getrecordingusage');
		$register->registerApiEndpoint($getRecordingUsage);

	}

	protected function _initViewHelper() {
		/*$view = new Zend_View();

		$cssAdds = array(
			"https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css",
			"public/css/overrides.css"
		);
		foreach($cssAdds as $css) {
			$view->headLink(array('rel' => 'stylesheet', 'href' => $css), 'APPEND')->appendStylesheet($css);
		}*/
	}
}