<?php

/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/22/16
 * Time: 9:43 AM
 */
class Wst_Cronjob_Apihealthreport implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {
		$logModel = new Wst_Model_Dbtable_Log();

		$logModel->addLogMessage('Cronjob Start', 'Wst_Cronjob_Apihealthreport', 1, 'INFO', 'cronjob', 'Wst_Cronjob_Apihealthreport');

		$now = time();
		$secondsToCheckForFailures = 24 * 60 * 60; // 24hrs * 60mins * 60sec
		$apiFailureWindowOpenTime = $now - $secondsToCheckForFailures;

		$where = $logModel->getAdapter()->quoteInto("subject = ?", "Api Status");
		$where .= $logModel->getAdapter()->quoteInto(" AND message = ?", "down");
		$where .= $logModel->getAdapter()->quoteInto(" AND timestamp >= ?", $apiFailureWindowOpenTime);
		$where .= $logModel->getAdapter()->quoteInto(" AND timestamp <= ?", $now);

		$resultSet = $logModel->fetchAll($where);

		$numberOfFailuresInsideWindow = count($resultSet);

		$logModel->addLogMessage('Cronjob Processing', "Daily report: $numberOfFailuresInsideWindow API failure(s) over the past $secondsToCheckForFailures seconds", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Apihealthreport');

		// trigger to send daily report
		$hoursToCheckForFailures = $secondsToCheckForFailures / 60 / 60;

		$et = new Ot_Trigger_Dispatcher();
		$et->setVariables(array(
			'subject'   => 'Apihealthcheck Cronjob Report',
			'summary'   => 'API Health Report (Apihealthcheck)',
			'details'   => "API failures found in the past $secondsToCheckForFailures seconds ($hoursToCheckForFailures hours): $numberOfFailuresInsideWindow",
			'timestamp' => date('Y-m-d @ H:i:s A')
		));
		$et->dispatch('Wst_System_Event');

		$logModel->addLogMessage('Cronjob End', 'Wst_Cronjob_Apihealthreport', 1, 'INFO', 'cronjob', 'Wst_Cronjob_Apihealthreport');
	}
}