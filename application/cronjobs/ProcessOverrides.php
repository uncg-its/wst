<?php

/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/22/16
 * Time: 9:43 AM
 */
class Wst_Cronjob_ProcessOverrides implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {
		$logModel = new Wst_Model_Dbtable_Log();

		$logModel->addLogMessage('Cronjob Start', 'Wst_Cronjob_ProcessOverrides', 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessOverrides');
		
		$overridesModel = new Wst_Model_Dbtable_Useroverrides();
		$userListModel = new Wst_Model_Dbtable_Userlist();


		// SCRAP THIS FOR NOW.

		// look for new overrides.
		//$logModel->addLogMessage('Cronjob Processing', "Checking for new (pending) overrides", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessOverrides');

		// look for overrides that are pending, then make them happen.
		//$pendingOverrides = $overridesModel->getPendingOverrides();
		//$numberOfPendingOverrides = count($pendingOverrides);
		//
		//$logModel->addLogMessage('Cronjob Processing', "Fetched $numberOfPendingOverrides pending " . pluralize($numberOfPendingOverrides, "override", "s"), 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessOverrides');
		//
		//if ($pendingOverrides) {
		//	foreach($pendingOverrides as $o) {
		//		// perform the action - this will set user to "manual"
		//		if ($o["user_status"] == "active") {
		//			$result = $userListModel->reactivateUserInUserList($o["euid"]);
		//		} else if $o["user_status"] == "inactive") {
		//			$result = $userListModel->inactivateUserInUserList($o["euid"]);
		//		}
		//
		//		// mark the override as "in-effect"
		//		$updateOverrideResult = $overridesModel->changeOverrideStatus($o["o_id"], "in-effect");
		//
		//		$logModel->addLogMessage('Cronjob Processing', "Override $overrideId is now in the active window; processing override to make user euid={$o["euid"]} {$o["user_status"]}", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessOverrides');
		//	}
		//}


		// check for expired overrides.

		$logModel->addLogMessage('Cronjob Processing', "Checking for expiring overrides", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessOverrides');

		$expiringOverrides = $overridesModel->getExpiringOverrides();
		$numberOfExpiredOverrides = $expiringOverrides ? count($expiringOverrides) : 0; // 'false' will still be counted as 1

//		dump($expiringOverrides, true);

		$logModel->addLogMessage('Cronjob Processing', "Fetched $numberOfExpiredOverrides expiring " . pluralize($numberOfExpiredOverrides, "override", "s"), 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessOverrides');

		if ($expiringOverrides) {

			// lookup model for later
			$lookupHelper = new Wst_Model_Helpers_Lookup();

			// queue model
			$queueModel = new Wst_Model_Dbtable_Apiqueue();

			foreach($expiringOverrides as $o) {
				// revert user to "feed"
				$overrideId = $o['o_id'];
				$userToRevert = $o['euid'];
				$result = $userListModel->revertUserToFeedSource($userToRevert);
				if (count($result) == 1) {
					$logModel->addLogMessage('Cronjob Raw Result', "User with euid=$userToRevert was successfully reverted to `feed` source.", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessOverrides');
				} else {
					$logModel->addLogMessage('Cronjob Raw Result', "User with euid=$userToRevert WAS NOT reverted to `feed` source.", 2, 'WARNING', 'cronjob', 'Wst_Cronjob_ProcessOverrides');

					// trigger
					$et = new Ot_Trigger_Dispatcher();
					$et->setVariables(array(
						'subject'   => 'ProcessOverrides Cronjob ERROR',
						'summary'   => 'User not successfully reverted to `feed` status',
						'details'   => "User $userToRevert was not reverted to `feed` status as expected when processing an expired override.",
						'timestamp' => date('Y-m-d @ H:i:s A')
					));

					$et->dispatch('Wst_System_Event');
				}

				// record the timestamp at which we reverted the user
				$now = time();
				$setRevertTimestampResult = $overridesModel->setRevertedTimestampForOverride($overrideId, $now);

				$logModel->addLogMessage('Cronjob Processing', "Override $overrideId has expired; reverting euid $userToRevert to source=`feed` - timestamp $now recorded in overrides table", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessOverrides');

				// check user eligibility and take action accordingly. e.g. if the user was overridden active, and isn't actually eligible, deactivate the user

				$overrideStatus = $o["user_status"];

				$userEligibility = $lookupHelper->getUserEligibility(array($o["webExId"]));

				if ($overrideStatus == "active" && !$userEligibility["data"][$o["webExId"]]["eligible"]) {
					$logModel->addLogMessage('Cronjob Processing', "Euid $userToRevert is no longer eligible for WebEx - queuing delete action", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessOverrides');
					$result = $queueModel->queueDeleteAction($userToRevert, $o["webExId"], "Wst_Cronjob_ProcessOverrides");
				} else if ($overrideStatus == "inactive" && $userEligibility["data"][$o["webExId"]]["eligible"]) {
					$logModel->addLogMessage('Cronjob Processing', "Euid $userToRevert is eligible for WebEx - queuing edit action", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessOverrides');
					$result = $queueModel->queueEditAction($userToRevert, $o["webExId"], array(), "Wst_Cronjob_ProcessOverrides");
				}
				
			}
		}
		
		$logModel->addLogMessage('Cronjob End', 'Wst_Cronjob_ProcessOverrides', 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessOverrides');
	}
}