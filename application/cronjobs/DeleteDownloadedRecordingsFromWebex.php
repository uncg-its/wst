<?php

class Wst_Cronjob_DeleteDownloadedRecordingsFromWebex implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$recordingsReadyForDeletion = $recordingArchiveModel->getDownloadedRecordingsToBeDeletedFromWebex();

		$logModel->addLogMessage('Cronjob Processing', "Recordings ready for deletion: " . count($recordingsReadyForDeletion), 1, 'INFO', 'cronjob', __CLASS__);


		//dump("Stopping here until Download piece is complete.", true);

		if (count($recordingsReadyForDeletion) > 0) {

			$failedApiCallIds = array();
			foreach ($recordingsReadyForDeletion as $index => $recInfo) {
				$recId = $recInfo["recording_id"];

				$apiHelper = new Wst_Model_Helpers_Webexapihelper();
				$apiResult = $apiHelper->deleteRecording($recId);

				if ($apiResult["response"]["result"] == "SUCCESS") {
					$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
					$markDeletedResult = $recordingArchiveModel->trashRecording($recId);

					$logModel->addLogMessage('Cronjob Processing', "Recording id $recId deleted from WebEx successfully.", 1, 'INFO', 'cronjob', __CLASS__);
				} else {
					$failedApiCallIds[] = $recId;
					$exceptionId = $apiResult["response"]["exceptionId"];
					$exceptionMessage = $apiResult["response"]["message"];
					$logModel->addLogMessage('Cronjob Processing', "Warning: API call failure when deleting recording ID $recId. code: $exceptionId, message: $exceptionMessage", 2, 'WARNING', 'cronjob', __CLASS__);

					// helper model
					$helperModel = new Wst_Model_Helpers_General();
					// increment number of times this recording has failed
					$incrementFailureResult = $helperModel->incrementRecordingFailures($recId, "delete");

				}

			}

			$successfulApiCalls = count($recordingsReadyForDeletion) - count($failedApiCallIds);
			$logModel->addLogMessage('Cronjob Result', "Recordings deleted successfully: " . $successfulApiCalls . " out of " . count($recordingsReadyForDeletion), 1, 'INFO', 'cronjob', __CLASS__);
		} else {
			$logModel->addLogMessage('Cronjob Result', "No recordings to process.", 1, 'INFO', 'cronjob', __CLASS__);
		}


		// end cronjob

		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}