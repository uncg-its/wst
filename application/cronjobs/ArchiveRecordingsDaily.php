<?php

class Wst_Cronjob_ArchiveRecordingsDaily implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', 'Wst_Cronjob_ArchiveRecordingsDaily', 1, 'INFO', 'cronjob', 'Wst_Cronjob_ArchiveRecordingsDaily');

		// recording archive helper
		$archiveRecordingsHelper = new Wst_Model_Helpers_ArchiveRecordingsHelper();

		$result = $archiveRecordingsHelper->archivePastRecordings('Wst_Cronjob_ArchiveRecordingsDaily', 2);

		$logModel->addLogMessage('Cronjob End', 'Wst_Cronjob_ArchiveRecordingsDaily', 1, 'INFO', 'cronjob', 'Wst_Cronjob_ArchiveRecordingsDaily');
	}
}