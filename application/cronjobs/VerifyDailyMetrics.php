<?php

class Wst_Cronjob_VerifyDailyMetrics implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		// how many days ago are we set to fetch?
		$metricsDashboardConfig = Zend_Registry::get("metricsDashboard");
		$daysAgo = $metricsDashboardConfig["daysAgo"];

		// get timestamp (local) for x days ago
		$targetTimestamp = strtotime("-$daysAgo days 00:00:00");

		$logModel->addLogMessage('Cronjob Processing', "Fetching all records pending verification from local database.", 1, 'INFO', 'cronjob', __CLASS__);

		// look for metrics that need verification
		$metricsModel = new Wst_Model_Dbtable_Metrics();
		$metricsNeedingVerification = $metricsModel->getMetricsWithStatus("pending-verification");

		$verifiedMetrics = array();
		$skippedMetrics = array();

		$logModel->addLogMessage('Cronjob Processing', "Searching for unverified metrics from $daysAgo " . pluralize($daysAgo, "day", "s") . " ago or prior.", 1, 'INFO', 'cronjob', __CLASS__);

		foreach($metricsNeedingVerification as $time => $metric) {
			if ($time <= $targetTimestamp) {
				// make sure we get to 00:00:00 and 23:59:59 on this day, just in case something is wonky with the timestamp
				$start = strtotime("00:00:00", $time);
				$end = strtotime("23:59:59", $time);

				// grab metrics
				$metricsHelper = new Wst_Model_Helpers_MetricsHelper();
				$commonMetrics = $metricsHelper->getCommonMetricsBetweenTimestamps($start, $end);

				// we need to ignore some of them as they are specific to the time fetched rather than historical data
				$metricsToResetToZero = array("numberOfActiveUsers", "recordingStorageUsed");

				foreach($metricsToResetToZero as $k) {
					unset($commonMetrics[$k]);
				}

				// NEW WAY WITH JSON
				$updatedMetrics = json_decode($metric["rawData"], true);
				foreach($commonMetrics as $k => $v) {
					$updatedMetrics[$k] = $v;
				}
				$updatedMetrics = json_encode($updatedMetrics);

				// add timestamp and status
				$updateData = array(
					"rawData"   => $updatedMetrics,
					"timestamp" => time(),
					"status"    => 'verified'
				);

				$updateResult = $metricsModel->updateDailyMetric($updateData, $metric["metric_id"]);

				$verifiedMetrics[] = $metric["date"];
			} else {
				$skippedMetrics[] = $metric["date"];
			}
		}

		$logModel->addLogMessage('Cronjob Result', "Verification complete. Verified records: " . count($verifiedMetrics) . "; Skipped records: " . count($skippedMetrics), 1, 'INFO', 'cronjob', __CLASS__);

		// end cronjob
		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}