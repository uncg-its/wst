<?php

class Wst_Cronjob_MarkExpiredRecordingsForDownload implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);


		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$result = $recordingArchiveModel->markExpiredRecordingsForDownload();

		$logModel->addLogMessage('Cronjob Result', "Marked $result expired " . pluralize($result, "recording", "s") . " for download in local database.", 1, 'INFO', 'cronjob', __CLASS__);

		// end cronjob
		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}