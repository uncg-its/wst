<?php

class Wst_Cronjob_ArchiveSessions implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		// get date range.

		$appVarsModel = new Wst_Model_Dbtable_Applicationvars();

		$now = time();

		// is there a known last update time?
		$lastSessionArchiveTimestamp = $appVarsModel->getVar("lastSessionArchiveTimestamp");

		// determine earliest date WebEx will allow for reporting
		$lowerBound = strtotime("first day of this month -3 months 00:00:00 UTC");

		if (!$lastSessionArchiveTimestamp) {
			// no last known update.
			// set to earliest WebEx will allow. performance issues???
			$createTimeStart = $lowerBound;

			$logModel->addLogMessage('Cronjob Processing', "Found no pre-existing value for lastSessionArchiveTimestamp; defaulting to earliest WebEx will allow.", 1, 'INFO', 'cronjob', __CLASS__);

		} else {
			// let's try the date from the DB.

			if ($lastSessionArchiveTimestamp < $lowerBound) {
				// date from DB is out of scope. set to default.

				$createTimeStart = $lowerBound;

				$logModel->addLogMessage('Cronjob Processing', "lastSessionArchiveTimestamp is OUT OF SCOPE. There may be gaps in the records. Proceeding to fetch session list.", 2, 'WARNING', 'cronjob', __CLASS__);

				// trigger
				$et = new Ot_Trigger_Dispatcher();
				$et->setVariables(array(
					'subject'   => 'ArchiveSessions Cronjob Report',
					'summary'   => 'Last Session Archive Date out of scope',
					'details'   => "Last session archival timestamp of $lastSessionArchiveTimestamp is out of scope. WebEx allows reporting data to be accessed from three months prior to the current month. The last session archival timestamp falls outside of this window, so there may be gaps in the reporting data! Contact WebEx support to get a custom report to fill in any gaps manually.",
					'timestamp' => date('Y-m-d @ H:i:s A')
				));
				$et->dispatch('Wst_System_Event');

			} else {
				$createTimeStart = strtotime("-1 day", $lastSessionArchiveTimestamp); // build in some CYA

				$logModel->addLogMessage('Cronjob Processing', "lastSessionArchiveTimestamp value is valid and in scope. Proceeding to fetch session list.", 1, 'INFO', 'cronjob', __CLASS__);
			}
		}


		$archiveSessionsHelper = new Wst_Model_Helpers_ArchiveSessionsHelper();
		$result = $archiveSessionsHelper->archivePastSessions(__CLASS__, $createTimeStart, $now);

		if ($result) {
			// update last pull timestamp
			$updateTimestampResult = $appVarsModel->setVar("lastSessionArchiveTimestamp", $now);
			$logModel->addLogMessage('Cronjob Processing', "Changed lastSessionArchiveTimestamp value to $now in the database.", 1, 'INFO', 'cronjob', __CLASS__);
		}

		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}