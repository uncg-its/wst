<?php

/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/22/16
 * Time: 9:43 AM
 */
class Wst_Cronjob_TelephonyUsageCheck implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		$configModel = new Wst_Model_Dbtable_Config();
		$telephonyUsageThreshold = $configModel->getVar('telephonyUsageThreshold');

		if (!$telephonyUsageThreshold) {
			$logModel->addLogMessage('Cronjob Result', "Could not check telephony usage - usage threshold has not been set.", 2, "WARNING", 'cronjob', __CLASS__);
			$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
			return;
		}

		$logModel->addLogMessage('Cronjob Processing', "Telephony usage threshold set to $telephonyUsageThreshold%", 1, "INFO", 'cronjob', __CLASS__);

		// this month's "billing period" for telephony
		$telephonyResetDay = $configModel->getVar('telephonyResetDay');

		if (!$telephonyResetDay || $telephonyResetDay == 0) { $telephonyResetDay = 1; }
		if ($telephonyResetDay < 10) {
			$telephonyResetDay = "0$telephonyResetDay";
		}

		$now = time();

		$adjustedThisMonth = date("m/$telephonyResetDay/Y", $now);
		$adjustedLastMonth = date("m/$telephonyResetDay/Y", strtotime("-1 month", strtotime($adjustedThisMonth)));
		$adjustedNextMonth = date("m/$telephonyResetDay/Y", strtotime("+1 month", strtotime($adjustedThisMonth)));

		if (strtotime($adjustedThisMonth) <= time()) {
			$startOfBillingMonth = $adjustedThisMonth;
			$endOfBillingMonth = $adjustedNextMonth;
		} else {
			$startOfBillingMonth = $adjustedLastMonth;
			$endOfBillingMonth = $adjustedThisMonth;
		}

		$endOfBillingMonth = date("m/d/Y", strtotime("-1 days", strtotime($endOfBillingMonth)));

		$billingPeriodString = $startOfBillingMonth . " - " . $endOfBillingMonth;

		$logModel->addLogMessage('Cronjob Processing', "Telephony billing period: $billingPeriodString", 1, "INFO", 'cronjob', __CLASS__);

		// monthly quota used this month / total quota

		// quota
		$monthlyTelephonyBudget = $configModel->getVar("monthlyTelephonyBudget");
		if (!$monthlyTelephonyBudget) {
			$logModel->addLogMessage('Cronjob Result', "Could not check telephony usage - no budget has been set.", 2, "WARNING", 'cronjob', __CLASS__);
			$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
			return;
		}

		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();
		$sessionsWithTelephony = $sessionArchiveModel->getTelephonyUsageBetweenTimestamps(strtotime($startOfBillingMonth), strtotime($endOfBillingMonth));
		$logModel->addLogMessage('Cronjob Processing', "Retrieved sessions with telephony. Count: " . count($sessionsWithTelephony), 1, "INFO", 'cronjob', __CLASS__);

		$tollMinutesUsed = 0;
		foreach ($sessionsWithTelephony as $index => $session) {
			$tollMinutesUsed += $session["call_in_minutes"];
		}

		// budget used
		$tollMinuteCost = $configModel->getVar('tollMinuteCost');
		$budgetUsed = round($tollMinutesUsed * $tollMinuteCost, 2);

		$pctBudgetUsed = round($budgetUsed / $monthlyTelephonyBudget * 100, 2);

		$logModel->addLogMessage('Cronjob Processing', "Minutes used: $tollMinutesUsed; Budget: $budgetUsed / $monthlyTelephonyBudget ($pctBudgetUsed%)", 1, "INFO", 'cronjob', __CLASS__);

		if ($pctBudgetUsed >= $telephonyUsageThreshold) {
			// trigger
			$logModel->addLogMessage('Cronjob Processing', "Telephony usage exceeds warning threshold of $telephonyUsageThreshold%", 2, 'WARNING', 'cronjob', __CLASS__);

			// trigger if over a certain threshold
			$et = new Ot_Trigger_Dispatcher();
			$et->setVariables(array(
				'subject'   => 'TelephonyUsageCheck Cronjob Report',
				'summary'   => 'Telephony usage WARNING (TelephonyUsageCheck)',
				'details'   => "Telephony budget of \$$monthlyTelephonyBudget has exceeded $telephonyUsageThreshold% usage for this billing period ($billingPeriodString) - current usage is $pctBudgetUsed%.",
				'timestamp' => date('Y-m-d @ H:i:s A')
			));
			$et->dispatch('Wst_System_Event');
		} else {
			$logModel->addLogMessage('Cronjob Processing', "Telephony Usage is within acceptable limit.", 1, "INFO", 'cronjob', __CLASS__);
		}


		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}