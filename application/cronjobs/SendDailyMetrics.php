<?php

class Wst_Cronjob_SendDailyMetrics implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
		
		// fetch verified record from local DB
		$metricsModel = new Wst_Model_Dbtable_Metrics();
		$verifiedMetrics = $metricsModel->getMetricsWithStatus("verified");
		
		$logModel->addLogMessage('Cronjob Processing', "Retrieved " . count($verifiedMetrics) . " verified " . pluralize($verifiedMetrics, "entry", "entries", true) . " from local database.", 1, 'INFO', 'cronjob', __CLASS__);

		// send metrics to dashboard
		$logModel->addLogMessage('Cronjob Processing', "Sending metrics to metrics dashboard.", 1, 'INFO', 'cronjob', __CLASS__);

		$metricsHelper = new Wst_Model_Helpers_MetricsHelper();
		$sendErrors = array();
		foreach($verifiedMetrics as $metric) {
			$id = $metric["metric_id"];
            $date = $metric["date"];

			$unsetFields = array("date", "metric_id", "timestamp", "status", "error_data");
			foreach($unsetFields as $f) {
				unset($metric[$f]);
			}

			$updateResult = $metricsModel->updateDailyMetric(array(
				"status" => "sending"
			), $id);

			$sendResult = $metricsHelper->sendMetrics($metric["rawData"], $date);

			$logModel->addLogMessage('Cronjob Raw Result', json_encode($sendResult), 1, 'INFO', 'cronjob', __CLASS__);

			if ($sendResult["success"]) {
				// update status in DB
				$updateResult = $metricsModel->updateDailyMetric(array(
					"status" => "complete",
					"error_data" => null
				), $id);
			} else {
				// add to sendErrors array
				$errorData = array(
					"sentData" => $metric,
					"code" => $sendResult["code"],
					"message" => $sendResult["message"],
					"body" => $sendResult["body"]
				);
				$sendErrors[$metric["date"]] = json_encode($errorData);

				// update status in DB
				$updateResult = $metricsModel->updateDailyMetric(array(
					"status" => "error",
					"error_data" => json_encode($errorData)
				), $id);
			}
		}

		if (count($sendErrors) > 0) {
			$message = "API error while sending " . count($sendErrors) . pluralize($sendErrors, " metric", "s") . ".";
			$logModel->addLogMessage('Cronjob Result', $message, 2, 'WARNING', 'cronjob', __CLASS__);

			// trigger
			$et = new Ot_Trigger_Dispatcher();
			$et->setVariables(array(
				'subject'   => 'SendDailyMetrics Cronjob WARNING',
				'summary'   => 'FAILED Sending Metrics (SendDailyMetrics)',
				'details'   => $message,
				'timestamp' => date('Y-m-d @ H:i:s A')
			));

			$et->dispatch('Wst_System_Event');
		} else {
			$logModel->addLogMessage('Cronjob Result', count($verifiedMetrics) . " verified " . pluralize($verifiedMetrics, "metric", "s") . " successfully sent to dashboard.", 1, 'INFO', 'cronjob', __CLASS__);
		}

		// end cronjob
		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}