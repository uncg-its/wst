<?php

class Wst_Cronjob_StagingJobCheck implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {
		$logModel = new Wst_Model_Dbtable_Log();
		$cronjobModel = new Wst_Model_Dbtable_Cronjobs();

		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		$localConfig = Zend_Registry::get('localConfig');
		$maxDaysWithoutStaging = $localConfig["maxDaysWithoutStaging"];

		$lastStagingRuntime = $cronjobModel->getLastCronjobRuntime('Wst_Cronjob_Stageusers');
		$now = time();

		$daysAgo = ($now - $lastStagingRuntime) / 86400; // 86400 = 60s * 60m * 24h

		$logModel->addLogMessage('Cronjob Processing', "Last staging job run occurred at timestamp $lastStagingRuntime, $daysAgo days ago", 1, "INFO", 'cronjob', __CLASS__);

		if ($daysAgo > $maxDaysWithoutStaging) {
			// trigger
			$logModel->addLogMessage('Cronjob Processing', "Last staging cronjob has not run in over $maxDaysWithoutStaging day(s)", 2, 'WARNING', 'cronjob', __CLASS__);

			// trigger if over a certain threshold
			$et = new Ot_Trigger_Dispatcher();
			$et->setVariables(array(
				'subject'   => 'StagingJobCheck Cronjob Report',
				'summary'   => 'Staging Cronjob WARNING (StagingJobCheck)',
				'details'   => "WARNING: user staging cronjob has not run for $daysAgo days (max allowable: $maxDaysWithoutStaging). This is not typical and should likely be addressed in the application cronjob settings.",
				'timestamp' => date('Y-m-d @ H:i:s A')
			));
			$et->dispatch('Wst_System_Event');
		} else {
			$logModel->addLogMessage('Cronjob Processing', "Last staging cronjob runtime is within acceptable limit of $maxDaysWithoutStaging.", 1, "INFO", 'cronjob', __CLASS__);
		}


		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}