<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/24/16
 * Time: 4:41 PM
 */

class Wst_Cronjob_BuildApiQueue implements Ot_Cron_JobInterface
{
	public function execute($lastRunDt = null)
	{
		// local config
		$localConfig = Zend_Registry::get('localConfig');

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', 'Wst_Cronjob_BuildApiQueue', 1, 'INFO', 'cronjob', 'Wst_Cronjob_BuildApiQueue');

		// apiqueue model
		$queueModel = new Wst_Model_Dbtable_Apiqueue();

		// get staging records
		$stagingDataModel = new Wst_Model_Dbtable_Userstaging();
		$stagingRecords = $stagingDataModel->fetchAll()->toArray();

		$numStagingRecordsFound = count($stagingRecords);
		$numStagingRecordsFoundPlural = $numStagingRecordsFound == 1 ? "" : "s";
		$message = "Retrieved $numStagingRecordsFound record$numStagingRecordsFoundPlural from staging table.";

		$logModel->addLogMessage('Cronjob Processing', $message, 1, 'INFO', 'cronjob', 'Wst_Cronjob_BuildApiQueue');

		// user list model
		$userListModel = new Wst_Model_Dbtable_Userlist();

		$queueEntries = 0;

		$userListMap = $userListModel->getUserEuidMap(); // so that we don't have to query the database for EVERY staged user
		$userListMapSize = count($userListMap);
		if (!$userListMap) {
			$userListMapSize = 0;
		}
		$userListMapSizePlural = $userListMapSize == 1 ? "" : "s";
		$message = "Retrieved $userListMapSize existing user record$userListMapSizePlural from local user list.";

		$logModel->addLogMessage('Cronjob Processing', $message, 1, 'INFO', 'cronjob', 'Wst_Cronjob_BuildApiQueue');

		foreach($stagingRecords as $key => $staged) {
			// TODO - threshold check. put the approved changes into an array first, and wait on the db inserts. then count the changes to be sure we're not doing too much. three separate arrays would be good for logging - track adds, edits, deletes separately. can then use the queueXxxAction calls the same way, iterating over the array.

			// init array
			$queueData = array();

			// find user in current users map if exists
			$euid = $staged["euid"];
			$userInfo = false;
			$userManuallyManaged = false;

			if ($userListMap && isset($userListMap[$euid])) {
				$userInfo = $userListMap[$euid];
				$userManuallyManaged = $userInfo["source"] == "manual";
			}

			$userIsEligible = $staged["eligibility_for_webex"] == "eligible";

			// begin logic tree
			if ($userIsEligible || $userManuallyManaged) {
				if ($userInfo) {
					// user exists in the local users list - check on what's changed...

					// default values
					$userInfoChanged = false;
					$userCurrentlyInactive = false;

					$shouldQueueEditAction = false;

					// check for updates to user info
					if ($staged["checksum"] != $userInfo["checksum"]) {
						//$logModel->addLogMessage('Cronjob Temp Data', "Checksums: staged: " . $staged["checksum"] . "; userlist: " . $userInfo["checksum"], 1, 'INFO', 'cronjob', 'Wst_Cronjob_BuildApiQueue');
						// un, fn, ln, or email do not match
						// what fields don't match?
						$userInfoFields = array("firstName", "lastName", "email", "dept_number", "org_short_code");
						foreach($userInfoFields as $item) {
							if ($staged[$item] != $userInfo[$item]) {
								$queueData[$item] = $staged[$item];
							}
						}
						if ($staged["webExId"] != $userInfo["webExId"]) {
							$queueData["newWebExId"] = $staged["webExId"];
						}

						$userInfoChanged = true;
					}

					// check for eligibility change - if previously inactive, make active again
					if ($userInfo["status"] == "inactive") {
						$userCurrentlyInactive = true;
					}

					// logic to decide if the action should be queued.
					if ($userInfoChanged) {
						// there has been a name / username / email change...
						if (!$userManuallyManaged) {
							// user managed by the feed - ok to queue.
							$shouldQueueEditAction = true;
						} else {
							// user manually managed - need to be careful
							if (!$userCurrentlyInactive) {
								// if the user is not currently manually inactive, it's ok to queue.
								$shouldQueueEditAction = true;
							}
						}
					} else {
						// no name change. only other thing to look for is a move from ineligible --> eligible
						if ($userCurrentlyInactive) {
							if (!$userManuallyManaged) {
								// only if the user is not manually managed - ok to queue.
								$shouldQueueEditAction = true;
							}
						}
					}



					if ($shouldQueueEditAction) {
						$result = $queueModel->queueEditAction($euid, $userInfo["webExId"], $queueData, "Wst_Cronjob_BuildApiQueue");
						$queueEntries++;
					}
				} else {
					// user does not exist in local users list.
					// -- if it's a file instance, the user should be treated as an ADD.
					// -- if it's a signup instance, the user can be ignored.
					if ($localConfig["userSourceType"] == "file") {

						// does the user have a pending add already? we don't necessarily need this anymore, but it's here for safety.
						$pendingAdds = $queueModel->getCallsWithStatus("pending", "add");
						$userHasPendingAdd = false;

						if ($pendingAdds) {
							foreach($pendingAdds as $q_id => $data) {
								if ($data["euid"] == $euid) {
									$userHasPendingAdd = true;
								}
							}
						}

						// if the user has a pending add already, do NOT queue another.
						if (!$userHasPendingAdd) {
							// add user to webex and users list
							$queueData["email"] = $staged["email"];
							$queueData["firstName"] = $staged["firstName"];
							$queueData["lastName"] = $staged["lastName"];
							$queueData["dept_number"] = $staged["dept_number"];
							$queueData["org_short_code"] = $staged["org_short_code"];

							$result = $queueModel->queueAddAction($euid, $staged["webExId"], $queueData, "Wst_Cronjob_BuildApiQueue");
							$queueEntries++;
						} else {
							// log
							$logModel->addLogMessage('Cronjob Processing', "Add action for euid $euid not queued; an add action is already pending for this user in the queue.", 1, 'INFO', 'cronjob', 'Wst_Cronjob_BuildApiQueue');
						}
					}
				}
			} else {
				if ($userInfo && !$userManuallyManaged) {
					// remove from active users in webex
					$result = $queueModel->queueDeleteAction($euid, $staged["webExId"], "Wst_Cronjob_BuildApiQueue");
					$queueEntries++;
				}
			}
			// delete staging table entry
			$where = $stagingDataModel->getAdapter()->quoteInto("id = ?", $staged["id"]);
			$stagingDataModel->delete($where);
		}

		$ignoredEntries = count($stagingRecords) - $queueEntries;

		// log message
		$queuePlural = ($queueEntries == 1) ? "y" : "ies";
		$ignoredPlural = ($ignoredEntries == 1) ? "y" : "ies";
		$message = "Queued $queueEntries entr$queuePlural in the API Queue. Ignored $ignoredEntries irrelevant staging entr$ignoredPlural.";

		$logModel->addLogMessage('Cronjob Result', $message, 1, 'INFO', 'cronjob', 'Wst_Cronjob_BuildApiQueue');

		// check how many calls were queued...if it's over the threshold disable the api queue until it can be spot-checked.

		$expectedSourceMinCount = $localConfig["expectedSourceMinCount"];
		$apiQueueThreshold = $localConfig["apiQueueThreshold"] / 100;
		$maxAcceptableStagingEntries = ceil($expectedSourceMinCount * $apiQueueThreshold);

		$logModel->addLogMessage('Cronjob Processing', "Threshold check: max acceptable number of queue entries is set to $maxAcceptableStagingEntries", 1, 'INFO', 'cronjob', 'Wst_Cronjob_BuildApiQueue');

		if ($queueEntries > $maxAcceptableStagingEntries) {

			// log
			$logModel->addLogMessage('Cronjob Processing', "WARNING: API Queue count threshold exceeded. Number of entries queued: $queueEntries; max allowed: $maxAcceptableStagingEntries. Disabling queue processing cronjob.", 2, 'WARNING', 'cronjob', 'Wst_Cronjob_BuildApiQueue');

			// stop the crons.
			$cronModel = new Wst_Model_Dbtable_Cronjobs();
			$cronsToStop = array("Wst_Cronjob_ProcessApiQueue");
			foreach ($cronsToStop as $cron) {
				$cronModel->disableCronjob($cron);
				$logModel->addLogMessage('Cronjob Processing', "Disabled cronjob $cron", 1, 'INFO', 'cronjob', 'Wst_Cronjob_BuildApiQueue');
			}
			// trigger
			$et = new Ot_Trigger_Dispatcher();
			$et->setVariables(array(
				'subject'   => 'BuildApiQueue Cronjob ERROR',
				'summary'   => 'FAILED API Queue Threshold Check (BuildApiQueue)',
				'details'   => "Number of API queue entries ($queueEntries) is more than max allowed ($maxAcceptableStagingEntries). API Queue processing cronjob has been disabled.",
				'timestamp' => date('Y-m-d @ H:i:s A')
			));

			$et->dispatch('Wst_System_Event');
		}


		$logModel->addLogMessage('Cronjob End', 'Wst_Cronjob_BuildApiQueue', 1, 'INFO', 'cronjob', 'Wst_Cronjob_BuildApiQueue');
	}
}