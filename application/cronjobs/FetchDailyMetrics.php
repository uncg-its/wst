<?php

class Wst_Cronjob_FetchDailyMetrics implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		$daysAgo = 1;

		// get timestamps (local) for x days ago
		$start = strtotime("-$daysAgo days 00:00:00");
		$end = strtotime("-$daysAgo days 23:59:59");

		// double check that metrics for this date do not exist already for some reason?
		$metricsModel = new Wst_Model_Dbtable_Metrics();
		$metricsMap = $metricsModel->getMetricsMap();

		if (!isset($metricsMap[$start])) {

			// get initial metrics
			$logModel->addLogMessage('Cronjob Processing', "Fetching initial metrics from $daysAgo " . pluralize($daysAgo, "day", "s") . " ago from local database.", 1, 'INFO', 'cronjob', __CLASS__);

			$metricsHelper = new Wst_Model_Helpers_MetricsHelper();
			$commonMetrics = $metricsHelper->getCommonMetricsBetweenTimestamps($start, $end);

			// add timestamp and status
			$additionalMetricsData = array(
				"timestamp" => time(),
				"status"    => 'pending-verification'
			);

			$finalMetrics = array_merge($commonMetrics, $additionalMetricsData);

			// insert into local DB
			$result = $metricsModel->insertMetrics($finalMetrics);
			$logModel->addLogMessage('Cronjob Processing', "Inserted metrics into local database.", 1, 'INFO', 'cronjob', __CLASS__);
		} else {
			$logModel->addLogMessage('Cronjob Processing', "Metrics for " . date("Y-m-d", $start) . " already exist in local database. Skipping.", 1, 'INFO', 'cronjob', __CLASS__);
		}

		// end cronjob
		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}