b<?php

class Wst_Cronjob_UploadRecordingsToCloudStorage implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		// which cloud service(s)?
		$cloudStorageConfig = Zend_Registry::get('cloudStorage');
		$activeServices = explode(",", $cloudStorageConfig["activeServices"]);
		$logModel->addLogMessage('Cronjob Processing', "Active cloud services: " . implode(", ", $activeServices), 1, 'INFO', 'cronjob', __CLASS__);

		// grab the next 1 recording that needs to be uploaded.
		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$nextRecordingToUpload = $recordingArchiveModel->getNextRecordingToUpload();

        $uploadResults = array();

		if (count($nextRecordingToUpload) > 0) {
            $recId = $nextRecordingToUpload[0]["recording_id"];

            $logModel->addLogMessage('Cronjob Processing', "Recording ID $recId ready to be uploaded to cloud storage", 1, 'INFO', 'cronjob', __CLASS__);

            $recordingUploadHelper = new Wst_Model_Helpers_RecordingUploadHelper();

            foreach ($activeServices as $service) {
                $logModel->addLogMessage('Cronjob Processing', "Beginning upload of Recording ID $recId to service '$service'", 1, 'INFO', 'cronjob', __CLASS__);

                $uploadResults[$service] = $recordingUploadHelper->uploadRecording($recId, $service);
                $resultSuccess = ($uploadResults[$service]["success"]) ? "success" : "failure";
                $uploadResults[$service]["successString"] = $resultSuccess;

	            $logModel->addLogMessage('Cronjob Raw Result', json_encode($uploadResults[$service]), 1, 'INFO', 'cronjob', __CLASS__);

                $logModel->addLogMessage('Cronjob Processing', "Upload result: $resultSuccess. Message: " . $uploadResults[$service]["message"], 1, 'INFO', 'cronjob', __CLASS__);
            }

            $successString = "";
            $cloudStorageInfo = array();
            $failures = array();

            foreach($uploadResults as $s => $result) {
                $successString .= "$s: " . $result["successString"];
                if (!$result["success"]) {
                    $failures[] = $s;
                }

                $cloudStorageInfo[$s] = array(
                	"link" => $result["downloadLink"],
	                "fileId" => $result["fileId"],
	                "parentFolderId" => $result["parentFolderId"]
                );
            }

            $updateCloudInfoResult = $recordingArchiveModel->setCloudStorageInfo($recId, json_encode($cloudStorageInfo));


            if (count($failures) == count($activeServices)) {
                // all uploads failed
                $logModel->addLogMessage('Cronjob Result', "Upload results summary (with failures) - $successString", 2, 'WARNING', 'cronjob', __CLASS__);

                // log an upload failure
	            $helperModel = new Wst_Model_Helpers_General();
	            $incrementFailureResult = $helperModel->incrementRecordingFailures($recId, "upload");

            } else if (count($failures) > 0) {
                // only some uploads failed
                $logModel->addLogMessage('Cronjob Result', "Upload results summary (with failures) - $successString", 2, 'WARNING', 'cronjob', __CLASS__);

                // set status to 'uploaded-with-errors'
	            $changeStatusResult = $recordingArchiveModel->changeUploadStatus($recId, "uploaded-with-errors");

            } else {
            	// no uploads failed
                $logModel->addLogMessage('Cronjob Result', "Upload results summary - $successString", 1, 'INFO', 'cronjob', __CLASS__);

	            // set status to 'uploaded'
	            $changeStatusResult = $recordingArchiveModel->changeUploadStatus($recId, "uploaded");
            }

        } else {
            $logModel->addLogMessage('Cronjob Result', "No recordings to upload.", 1, 'INFO', 'cronjob', __CLASS__);
        }



		// end cronjob
		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}