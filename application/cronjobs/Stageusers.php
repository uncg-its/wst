<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/24/16
 * Time: 4:41 PM
 */

class Wst_Cronjob_Stageusers implements Ot_Cron_JobInterface
{
	public function execute($lastRunDt = null)
	{
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', 'Wst_Cronjob_Stageusers', 1, 'INFO', 'cronjob', 'Wst_Cronjob_Stageusers');

		// get JSON from file on server.
		$localConfig = Zend_Registry::get('localConfig');

		$userSourceType = $localConfig["userSourceType"];

		$logModel->addLogMessage('Cronjob Processing', "Staging started: user source type detected as '$userSourceType' ", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Stageusers');

		$filePath = $localConfig["userDiffFilePath"];
		$userDataArray = null;

		$continueProcessing = true;
		$trigger = false;

		$usersStaged = 0;

		if (file_exists($filePath)) {
			// get file contents
			$userDataJson = file_get_contents($filePath);
			$logModel->addLogMessage('Cronjob Processing', "Staging file found successfully at $filePath", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Stageusers');

			// check whether valid or missing
			$userDataArray = @json_decode($userDataJson, true);
			if ($userDataArray === null) {
				$continueProcessing = false;
				$logSummary = "Error parsing users from local source";
				if (json_last_error() !== JSON_ERROR_NONE) {
					// there was an error parsing the JSON.
					$logError = "Error parsing the staging source file JSON - invalid JSON detected";
					$logModel->addLogMessage('Cronjob Processing', $logError, 2, 'WARNING', 'cronjob', 'Wst_Cronjob_Stageusers');
					$trigger = true;
				} else {
					// no data in the JSON file
					$logError = "No data found in JSON file.";
					$logModel->addLogMessage('Cronjob Processing', $logError, 1, 'INFO', 'cronjob', 'Wst_Cronjob_Stageusers');
					// no need to trigger for this one.
				}
			}
		} else {
			$continueProcessing = false;
			$logSummary = "Error parsing users from local source";
			$logError = "Staging file NOT FOUND at $filePath.";
			$logModel->addLogMessage('Cronjob Processing', $logError, 2, 'WARNING', 'cronjob', 'Wst_Cronjob_Stageusers');
			$trigger = true;
		}

		// reindex user data array
		$userDataArrayByUsername = array();
		foreach($userDataArray as $index => $user) {
			$userDataArrayByUsername[strtolower($user["uid"])] = $user;
		}

		// are there still pending / in-progress API calls in the queue? don't proceed if so.

		$logModel->addLogMessage('Cronjob Processing', "Successfully read in JSON data", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Stageusers');
		$queueModel = new Wst_Model_Dbtable_Apiqueue();

		$callStatusesToWatchFor = array("pending", "in-progress");
		$incompleteCalls = $queueModel->getCallsWithStatus($callStatusesToWatchFor);

		if ($incompleteCalls) {
			$continueProcessing = false;
			$callStatusesString = implode(" / ", $callStatusesToWatchFor);
			$logSummary = "API queue not ready for additional entries";
			$logError = "Still " . count($incompleteCalls) . " $callStatusesString API " . pluralize($incompleteCalls, "call", "s") . " in the API queue at time of staging operation. Aborting staging operation.";
			$logModel->addLogMessage('Cronjob Processing', $logError, 2, 'WARNING', 'cronjob', 'Wst_Cronjob_Stageusers');
			$trigger = true;
		}


		if ($continueProcessing) {
			$logModel->addLogMessage('Cronjob Processing', "No Pending or In Progress API calls. Continuing to process data", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Stageusers');
			$stageUsersModel = new Wst_Model_Dbtable_Userstaging();
			// is staging table empty? if not, do not proceed
			$recordsInStagingTable = $stageUsersModel->fetchAll()->toArray();
			if (count($recordsInStagingTable) > 0) {
				// there are records - abort.
				$logModel->addLogMessage('Cronjob Processing', "Fatal Error: staging table not empty. Staging operation aborted.", 3, 'ERROR', 'cronjob', 'Wst_Cronjob_Stageusers');

				// trigger
				$et = new Ot_Trigger_Dispatcher();
				$et->setVariables(array(
					'subject'   => 'Stageusers Cronjob ERROR',
					'summary'   => 'Staging table not empty',
					'details'   => 'user_staging table was not empty at the time the Stageusers cronjob was executed.',
					'timestamp' => date('Y-m-d @ H:i:s A')
				));

				$et->dispatch('Wst_System_Event');
			} else {
				// threshold check
				$stagingEntryCount = count($userDataArray);
				$expectedSourceMinCount = $localConfig["expectedSourceMinCount"];
				if ($stagingEntryCount < $expectedSourceMinCount) {
					// log
					$logModel->addLogMessage('Cronjob Processing', "Fatal Error: users found in LDAP feed ($stagingEntryCount) is less than expected source min count ($expectedSourceMinCount). Aborting staging and disabling subsequent cronjobs.", 3, 'ERROR', 'cronjob', 'Wst_Cronjob_Stageusers');
					// stop the crons.
					$cronModel = new Wst_Model_Dbtable_Cronjobs();
					$cronsToStop = array("Wst_Cronjob_BuildApiQueue", "Wst_Cronjob_ProcessApiQueue");
					foreach ($cronsToStop as $cron) {
						$cronModel->disableCronjob($cron);
						$logModel->addLogMessage('Cronjob Processing', "Disabled cronjob $cron", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Stageusers');
					}
					// trigger
					$et = new Ot_Trigger_Dispatcher();
					$et->setVariables(array(
						'subject'   => 'Stageusers Cronjob ERROR',
						'summary'   => 'FAILED Threshold Check (Stageusers)',
						'details'   => "Number of users found in LDAP feed ($stagingEntryCount) is less than expected source min count ($expectedSourceMinCount). Other cronjobs have been disabled.",
						'timestamp' => date('Y-m-d @ H:i:s A')
					));

					$et->dispatch('Wst_System_Event');

					return;
				}

				$logModel->addLogMessage('Cronjob Processing', "Threshold checks passed.", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Stageusers');

				// there are no previously staged records - proceed
				$lastInsertedRecordId = false; // init

				$localConfig = Zend_Registry::get('localConfig');
				// $eligibilityFields = array_map('trim', explode(",", $localConfig["eligibilityList"]));
				$emailSuffix = $localConfig["emailSuffix"];
                $lookupModel = new Wst_Model_Helpers_Lookup();

                // getting last update timestamp.
				$appVarsModel = new Wst_Model_Dbtable_Applicationvars();
				$latestChangeTimestamp = $appVarsModel->getVar("lastFeedTimestamp"); // for storing in app vars
				if (empty($latestChangeTimestamp)) {
					$latestChangeTimestamp = 0;
				}
				$ignoreChangesBeforeTimestamp = $latestChangeTimestamp; // should be set here but won't change as we go.
				$operationTimestamp = time(); // for logging

				ini_set('max_execution_time', 600);

				foreach ($userDataArray as $item => $value) {
					// check eligibility here...
					$ldapUpdateTimestamp = $value["timestamp"];
					if ($ldapUpdateTimestamp > $ignoreChangesBeforeTimestamp) {

                        $uncgUsername = strtolower($value["uid"]);
						$eligibilityStatus = "ineligible"; // default
						if ($value["status"] != "not-in-ldap") {

							$checkTeacherFeed = $localConfig["checkTeacherFeed"];

						    $lookupResult = $lookupModel->getUserEligibility(array($uncgUsername), "file", $checkTeacherFeed, $userDataArrayByUsername);
						    if (isset($lookupResult["data"][$uncgUsername]) && $lookupResult["data"][$uncgUsername]["eligible"]) {
						        $eligibilityStatus = "eligible";
                            }

//						    foreach ($eligibilityFields as $field) {
//								if ($value[$field] == "TRUE") {
//									$eligibilityStatus = "eligible";
//									break;
//								}
//							}

						}



						$firstName = $value["uncgpreferredgivenname"];
						if ($firstName == null) {
							$firstName = "";
						}

						$lastName = $value["uncgpreferredsurname"];
						if ($lastName == null) {
							$lastName = "";
						}

						$emailAddress = $uncgUsername . $emailSuffix;

						$departmentNumber = isset($value["departmentnumber"]) ? $value["departmentnumber"] : 0; // default is 0
						$orgShortCode = isset($value["uncgorgnshortname"]) ? $value["uncgorgnshortname"] : "undefined"; // default is "undefined"

						// generate checksum
						$generalHelper = new Wst_Model_Helpers_General();
						$checksumData = array(
							"webExId"        => $uncgUsername,
							"firstName"      => $firstName,
							"lastName"       => $lastName,
							"email"          => $emailAddress,
							"dept_number"    => $departmentNumber,
							"org_short_code" => $orgShortCode
						);
						$checksum = $generalHelper->generateLocalChecksum(null, $checksumData);

						$logModel->addLogMessage('Cronjob Processing', "Attempting to stage user $uncgUsername", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Stageusers');
						$insertData = array(
							"euid"                    => $value["uidnumber"],
							"webExId"                 => $uncgUsername,
							"firstName"               => $firstName,
							"lastName"                => $lastName,
							"email"                   => $emailAddress,
							"ldap_status"             => $value["status"],
							"eligibility_for_webex"   => $eligibilityStatus,
							"record_change_timestamp" => $ldapUpdateTimestamp,
							"checksum"                => $checksum,
							"timestamp"               => $operationTimestamp,
							"dept_number"             => $departmentNumber,
							"org_short_code"          => $orgShortCode
						);

						$lastInsertedRecordId = $stageUsersModel->insert($insertData); // TODO - need a method for this?
						$logModel->addLogMessage('Cronjob Processing', "Staged user $uncgUsername successfully.", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Stageusers');
						$usersStaged++;
						if ($ldapUpdateTimestamp > $latestChangeTimestamp) {
							$latestChangeTimestamp = $ldapUpdateTimestamp;
						}
					}
				}
				// update latest import timestamp
				$appVarsModel->setVar("lastFeedTimestamp", $latestChangeTimestamp);
				$logModel->addLogMessage('Cronjob Processing', "Changed latest LDAP record timestamp to $latestChangeTimestamp", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Stageusers');

				$usersStagedPlural = ($usersStaged == 1) ? "" : "s";
				$message = "$usersStaged user$usersStagedPlural staged successfully.";

				$logModel->addLogMessage('Cronjob Processing', $message, 1, 'INFO', 'cronjob', 'Wst_Cronjob_Stageusers');
				
			}

		} else {
			// we didn't process for some reason.
			if ($trigger) {
				if (!isset($logSummary)) {
					$logSummary = "Unknown error. Something went wrong.";
				}

				if (!isset($logError)) {
					$logError = "Unknown error. Something went wrong.";
				}

				$et = new Ot_Trigger_Dispatcher();
				$et->setVariables(array(
					'subject'   => 'Stageusers Cronjob WARNING',
					'summary'   => $logSummary,
					'details'   => $logError,
					'timestamp' => date('Y-m-d @ H:i:s A')
				));

				$et->dispatch('Wst_System_Event');
			}
		}

		$logModel->addLogMessage('Cronjob End', 'Wst_Cronjob_Stageusers', 1, 'INFO', 'cronjob', 'Wst_Cronjob_Stageusers');
	}
}