<?php

class Wst_Cronjob_ArchiveSessionsTrueUp implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		$archiveSessionsHelper = new Wst_Model_Helpers_ArchiveSessionsHelper();
		$result = $archiveSessionsHelper->archivePastSessions(__CLASS__, strtotime("-1 week 00:00:00 UTC"));

		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}