<?php

class Wst_Cronjob_BoxTokenCheck implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		$configModel = new Wst_Model_Dbtable_Config();
		$accessToken = $configModel->getVar('access_token');
		$refreshToken = $configModel->getVar('refresh_token');

		$errors = array();

		if (is_null($accessToken)) {
			$message = "Box API Access token is not set.";
			$errors[] = $message;
			$logModel->addLogMessage('Cronjob Processing', $message, 2, "WARNING", 'cronjob', __CLASS__);
		} else {
			$logModel->addLogMessage('Cronjob Processing', "Box API Access token is set properly.", 1, "INFO", 'cronjob', __CLASS__);
		}

		if (is_null($refreshToken)) {
			$message = "Box API Refresh token is not set.";
			$errors[] = $message;
			$logModel->addLogMessage('Cronjob Processing', $message, 2, "WARNING", 'cronjob', __CLASS__);
		} else {
			$logModel->addLogMessage('Cronjob Processing', "Box API Refresh token is set properly.", 1, "INFO", 'cronjob', __CLASS__);
		}


		if (count($errors) > 0) {
			$logModel->addLogMessage('Cronjob Result', "Token errors found - uploads to Box storage will not work properly until this is fixed!", 2, "WARNING", 'cronjob', __CLASS__);

			// trigger if over a certain threshold
			$et = new Ot_Trigger_Dispatcher();

			$message = "The following problems with the Box API token were detected:" . PHP_EOL;
			foreach ($errors as $error) {
				$message .= "-- $error" . PHP_EOL;
			}

			$message .= "Recording uploads to Box will not work until this problem is corrected!";

			$et->setVariables(array(
				'subject'   => 'BoxTokenCheck Cronjob Report',
				'summary'   => 'Box API Token WARNING (BoxTokenCheck)',
				'details'   =>  $message,
				'timestamp' => date('Y-m-d @ H:i:s A')
			));
			$et->dispatch('Wst_System_Event');

		} else {
			$logModel->addLogMessage('Cronjob Result', "Box API Token status OK", 1, "INFO", 'cronjob', __CLASS__);
		}


		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}