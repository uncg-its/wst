<?php

/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/22/16
 * Time: 9:43 AM
 */
class Wst_Cronjob_RebuildLocalMetricsTable implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {
		$logModel = new Wst_Model_Dbtable_Log();

		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		$metricsModel = new Wst_Model_Dbtable_Metrics();
		$allMetrics = $metricsModel->getMetricsMap();

		$logModel->addLogMessage('Cronjob Processing', "Fetched all metrics.", 1, 'INFO', 'cronjob', __CLASS__);

		$metricKeys = array();

		$keysToGetRidOf = array('timestamp', 'status', 'error_data');
		$keysToIgnore = array_merge(array('metric_id', 'date'), $keysToGetRidOf);
		foreach ($allMetrics as $timestamp => $metric) {
			foreach ($metric as $key => $value) {
				if (in_array($key, $keysToGetRidOf)) {
					unset($allMetrics[$timestamp][$key]);
				}

				if (!in_array($key, $keysToIgnore)) {
					if (!in_array($key, $metricKeys)) {
						$metricKeys[] = $key;
					}
				}
			}
		}

		$metricsFlyModel = new Wst_Model_Dbtable_MetricsOnthefly();
		$clearTable = $metricsFlyModel->clearTable();

		$logModel->addLogMessage('Cronjob Processing', "Cleared local metrics table.", 1, 'INFO', 'cronjob', __CLASS__);

		$addCols = $metricsFlyModel->addDbColumns($metricKeys, array("rawData" => "TEXT", "recordingStorageUsed" => "FLOAT"));

		$logModel->addLogMessage('Cronjob Processing', "Metrics columns added.", 1, 'INFO', 'cronjob', __CLASS__);

		$populateResult = $metricsFlyModel->populateDbColumns($allMetrics);

		$logModel->addLogMessage('Cronjob Processing', "Metrics populated.", 1, 'INFO', 'cronjob', __CLASS__);

		$logModel->addLogMessage('Cronjob Result', "Metrics table rebuilt successfully.", 1, 'INFO', 'cronjob', __CLASS__);

		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}