<?php

class Wst_Cronjob_ArchiveAttendees implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {
		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', 'Wst_Cronjob_ArchiveAttendees', 1, 'INFO', 'cronjob', 'Wst_Cronjob_ArchiveAttendees');

		// instantiate attendee archive model
		$attendeeArchiveModel = new Wst_Model_Dbtable_Attendeearchive();

		// FIRST get all session IDs with unparsed attendees
		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();
		$sessionsToParse = $sessionArchiveModel->getSessionsWithUnparsedAttendees();

		//dump($sessionsToParse, true);

		$logModel->addLogMessage('Cronjob Processing', "Found " . count($sessionsToParse) . " " . pluralize($sessionsToParse, "session", "s") . " needing attendee information processed.", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ArchiveAttendees');
		
		$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();

		$centerCrosswalk = array(
			"Meeting Center" => "mc",
			"Training Center" => "tc",
			"Event Center" => "ec",
			"Support Center" => "sc"
		);

		$insertedIds = array();

		//$count = 0; // testing

		foreach($sessionsToParse as $confId => $confData) {

			$centerName = $confData["center"];
			$centerAbbrev = $centerCrosswalk[$centerName];

			$details = $webexApiHelper->getSessionAttendeeHistory($confId, $centerAbbrev);

			// parse for only the details
			if ($details["response"]["result"] == "SUCCESS") {
			//	switch ($centerAbbrev) {
			//		case "mc":
			//			$arrayKey = "history:meetingAttendeeHistory";
			//			break;
			//		case "tc":
			//			$arrayKey = "history:trainingAttendeeHistory";
			//			break;
			//		case "ec":
			//			$arrayKey = "history:eventAttendeeHistory";
			//			break;
			//		case "sc":
			//			$arrayKey = "history:supportAttendeeHistory";
			//			break;
			//		default:
			//			$arrayKey = false;
			//			break;
			//	}

				$attendeeDetails = false;

				$dataArray = $details["data"];

				if (isset($dataArray["history:confID"])) {
					// single record
					$attendeeDetails[] = $dataArray;
				} else {
					$attendeeDetails = $dataArray;
				}

				//dump($attendeeDetails, true);

				foreach($attendeeDetails as $key => $details) {
					// deal with webex api discrepancies by center...
					$name = (isset($details["history:name"])) ? $details["history:name"] : $details["history:attendeeName"];
					$email = (isset($details["history:email"])) ? $details["history:email"] : $details["history:attendeeEmail"];
					$joinTime = (isset($details["history:joinTime"])) ? $details["history:joinTime"] : $details["history:startTime"];
					$leaveTime = (isset($details["history:leaveTime"])) ? $details["history:leaveTime"] : $details["history:endTime"];
					$registered = (isset($details["history:registered"])) ? $details["history:registered"] : null;
					$invited = (isset($details["history:invited"])) ? $details["history:invited"] : null;
					$voipDuration = (isset($details["history:voipDuration"])) ? $details["history:voipDuration"] : null;


					$insertData = array(
						'conf_id'          => $details["history:confID"],
						'name'             => $name,
						'email'            => $email,
						'join_time_utc'    => strtotime($joinTime . " UTC"),
						'leave_time_utc'   => strtotime($leaveTime . " UTC"),
						'duration'         => $details["history:duration"],
						'participant_type' => $details["history:participantType"],
						'ip_address'       => $details["history:ipAddress"],
						'client_agent'     => $details["history:clientAgent"],
						'registered'       => $registered,
						'invited'          => $invited,
						'voip_duration'    => $voipDuration
					);

					$insertedId = $attendeeArchiveModel->insertArchive($insertData);
					$insertedIds[] = $insertedId;

					// mark it as processed in the session archive
					$markAsProcessedResult = $sessionArchiveModel->setSessionAsParsed($details["history:confID"]);

				}
			}

			//$count++; // for testing
		}

		$logModel->addLogMessage('Cronjob Processing', "Successfully inserted " . count($insertedIds) . " " . pluralize($insertedIds, "attendee", "s") . " into archive table.", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ArchiveAttendees');

		

		//$et = new Ot_Trigger_Dispatcher();
		//$et->setVariables(array(
		//	'subject'   => 'Apihealthcheck Cronjob Report',
		//	'summary'   => 'API Health Report (Apihealthcheck)',
		//	'details'   => "API failures found in the past $secondsToCheckForFailures seconds ($hoursToCheckForFailures hours): $numberOfFailuresInsideWindow",
		//	'timestamp' => date('Y-m-d @ H:i:s A')
		//));
		//$et->dispatch('Wst_System_Event');



		$logModel->addLogMessage('Cronjob End', 'Wst_Cronjob_ArchiveAttendees', 1, 'INFO', 'cronjob', 'Wst_Cronjob_ArchiveAttendees');
	}
}