<?php

/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/22/16
 * Time: 9:43 AM
 */
class Wst_Cronjob_RevertNamesToLdap implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {
		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', 'Wst_Cronjob_RevertNamesToLdap', 1, 'INFO', 'cronjob', 'Wst_Cronjob_RevertNamesToLdap');

		// webex api helper
		$apiHelper = new Wst_Model_Helpers_Webexapihelper();
		$result = $apiHelper->getActiveWebexUsers(); // any reason to do all users? probably not.

		// api queue model
		$queueModel = new Wst_Model_Dbtable_Apiqueue();

		if ($result["response"]["result"] == "SUCCESS") {
			$userListArray = $result["data"];
			$numberOfUsers = count($userListArray);

			$logModel->addLogMessage('Cronjob Processing', "Retrieved user list from WebEx successfully ($numberOfUsers users retrieved)", 1, 'INFO', 'cronjob', 'Wst_Cronjob_RevertNamesToLdap');

			// TODO - is there a better way to do this?
			$webexUserListArrayByUsername = array();
			foreach($userListArray as $u) {
				$webexUserListArrayByUsername[$u["use:webExId"]] = $u;
			}

			$logModel->addLogMessage('Cronjob Processing', "Reindexed WebEx user list array by [webExId].", 1, 'INFO', 'cronjob', 'Wst_Cronjob_RevertNamesToLdap');

			$userListModel = new Wst_Model_Dbtable_Userlist();
			$localUserListMap = $userListModel->getUserEuidMap();

			$usersToTrueUp = array();

			if ($localUserListMap !== false) {
				$localUserCount = count($localUserListMap);

				$logModel->addLogMessage('Cronjob Processing', "Retrieved user list from local database successfully ($localUserCount users retrieved)", 1, 'INFO', 'cronjob', 'Wst_Cronjob_RevertNamesToLdap');

				foreach ($localUserListMap as $user) {
					$localEuid = $user["euid"];
					$localWebexId = $user["webExId"];
					$localChecksum = $user["checksum"];
					if (array_key_exists($localWebexId, $webexUserListArrayByUsername)) {
						$webexUserDetails = $webexUserListArrayByUsername[$localWebexId];
						$wId = $webexUserDetails["use:webExId"];
						$wFn = $webexUserDetails["use:firstName"];
						$wLn = $webexUserDetails["use:lastName"];
						$wEmail = $webexUserDetails["use:email"];

						$generalHelper = new Wst_Model_Helpers_General();
						//$webexChecksum = generateChecksum($wId, $wFn, $wLn, $wEmail);
						$checksumData = array(
							"webExId"        => $wId,
							"firstName"      => $wFn,
							"lastName"       => $wLn,
							"email"          => $wEmail,
							"dept_number"    => $user["dept_number"],
							"org_short_code" => $user["org_short_code"]
						);

						$webexChecksum = $generalHelper->generateLocalChecksum(null, $checksumData);

						if ($webexChecksum != $localChecksum) {
							$logModel->addLogMessage('Cronjob Processing', "User info mismatch found for user $wId -- local checksum: $localChecksum; webex checksum: $webexChecksum", 1, 'INFO', 'cronjob', 'Wst_Cronjob_RevertNamesToLdap');

							// reset information to what it is in LDAP (local)

							// put user in array. we'll do a threshold check before API calls.
							$usersToTrueUp[$localEuid] = array(
								"webExId"   => $wId,
								"firstName" => $user["firstName"],
								"lastName"  => $user["lastName"]
							);
						}
					}
				}

				$changedUsersTotal = count($usersToTrueUp);

				$logModel->addLogMessage('Cronjob Processing', "True-up calculations found finished. Total users to change: $changedUsersTotal", 1, 'INFO', 'cronjob', 'Wst_Cronjob_RevertNamesToLdap');

				// TODO: threshold checks

				foreach($usersToTrueUp as $euid => $queueData) {

					$nameDataArray = array(
						"firstName" => $queueData["firstName"],
						"lastName" => $queueData["lastName"]
					);

					$queueResult = $queueModel->queueEditAction($euid, $queueData["webExId"], $nameDataArray, "Wst_Cronjob_RevertNamesToLdap", "n"); // add the last "n" in order to not update user list.

					if ($queueResult["status"] == "success") {
						$logModel->addLogMessage('Cronjob Processing', "Edit action queued successfully for user {$queueData["webExId"]}", 1, 'INFO', 'cronjob', 'Wst_Cronjob_RevertNamesToLdap');
					} else {
						$logModel->addLogMessage('Cronjob Processing', "Problem while queueing edit action for user {$queueData["webExId"]}", 2, 'WARNING', 'cronjob', 'Wst_Cronjob_RevertNamesToLdap');
					}
				}

			}

		} else {
			// log the problem.
			$logModel->addLogMessage('Cronjob Processing', "API call to fetch WebEx user list FAILED.", 1, 'WARNING', 'cronjob', 'Wst_Cronjob_RevertNamesToLdap');
		}

		//$et = new Ot_Trigger_Dispatcher();
		//$et->setVariables(array(
		//	'subject'   => 'Apihealthcheck Cronjob Report',
		//	'summary'   => 'API Health Report (Apihealthcheck)',
		//	'details'   => "API failures found in the past $secondsToCheckForFailures seconds ($hoursToCheckForFailures hours): $numberOfFailuresInsideWindow",
		//	'timestamp' => date('Y-m-d @ H:i:s A')
		//));
		//$et->dispatch('Wst_System_Event');



		$logModel->addLogMessage('Cronjob End', 'Wst_Cronjob_RevertNamesToLdap', 1, 'INFO', 'cronjob', 'Wst_Cronjob_RevertNamesToLdap');
	}
}