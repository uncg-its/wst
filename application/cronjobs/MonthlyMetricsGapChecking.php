<?php

class Wst_Cronjob_MonthlyMetricsGapChecking implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		// this month
		$pastInterval = strtotime("-1 month");

		$lastMonth = date("Y-m", $pastInterval);
		$daysInLastMonth = date("t", $pastInterval);

		$logModel->addLogMessage('Cronjob Processing', "Fetching daily metrics from $lastMonth.", 1, 'INFO', 'cronjob', __CLASS__);

		// get all recorded metrics from last month
		$metricsModel = new Wst_Model_Dbtable_Metrics();
		$metricsMap = $metricsModel->getMetricsForMonth($lastMonth);

		$logModel->addLogMessage('Cronjob Processing', "Found " . count($metricsMap) . " daily " . pluralize($metricsMap, "metric", "s") . " from $lastMonth out of an expected $daysInLastMonth.", 1, 'INFO', 'cronjob', __CLASS__);

		$metricsHelper = new Wst_Model_Helpers_MetricsHelper();

		// loop through the days of last month to figure out which ones we are missing.
		$backfilledMetrics = array();
		for($day = 1; $day <= $daysInLastMonth; $day++) {
			$dayString = sprintf("-%02d", $day);
			$completeDate = $lastMonth . $dayString;

			$timestamp = strtotime($completeDate . " 00:00:00");

			if (!isset($metricsMap[$timestamp])) {
				// we're missing this day!
				$logModel->addLogMessage('Cronjob Processing', "Missing metrics from $completeDate - backfilling.", 1, 'INFO', 'cronjob', __CLASS__);

				$start = strtotime($completeDate . " 00:00:00");
				$end = strtotime($completeDate . " 23:59:59");

				$commonMetrics = $metricsHelper->getCommonMetricsBetweenTimestamps($start, $end);

				$metricsRaw = json_decode($commonMetrics["rawData"], true);
				// reset metrics that are realtime-only
				$metricsToResetToZero = array("numberOfActiveUsers", "recordingStorageUsed");

				foreach($metricsToResetToZero as $k) {
					$metricsRaw[$k] = 0;
				}

				// add timestamp and status
				$metricDataToInsert = array(
					"date" => $commonMetrics["date"],
					"rawData" => json_encode($metricsRaw),
					"timestamp" => time(),
					"status"    => 'pending-verification'
				);

				$finalMetrics = array_merge($commonMetrics, $metricDataToInsert);

				// insert into local DB
				$result = $metricsModel->insertMetrics($finalMetrics);
				$logModel->addLogMessage('Cronjob Processing', "Inserted metrics from $completeDate into local database.", 1, 'INFO', 'cronjob', __CLASS__);

				$backfilledMetrics[] = $completeDate;
			}
		}

		if (count($backfilledMetrics) > 0) {
			$message = "Backfilled metrics from: " . implode(", ", $backfilledMetrics);
		} else {
			$message = "No backfilling of metrics was required.";
		}

		$logModel->addLogMessage('Cronjob Result', $message, 1, 'INFO', 'cronjob', __CLASS__);
		
		// end cronjob
		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}