<?php

class Wst_Cronjob_ProcessOverrideEmails implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		// general needed helpers
		$htmlEmailHelper = new Wst_Model_Helpers_HtmlEmail();
		$userListModel = new Wst_Model_Dbtable_Userlist();

		// overrides model - check first. if none in effect, then exit.
		$overridesModel = new Wst_Model_Dbtable_Useroverrides();
		$activeOverrides = $overridesModel->getInEffectOverrides();

		$logModel->addLogMessage('Cronjob Processing', 'Active Overrides pulled: ' . count($activeOverrides), 1, 'INFO', 'cronjob', __CLASS__);

		if (count($activeOverrides) == 0) {
			$logModel->addLogMessage('Cronjob Result', 'No active overrides; no further action necessary.', 1, 'INFO', 'cronjob', __CLASS__);
			$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
			return;
		}

		// config model
		$configModel = new Wst_Model_Dbtable_Config();
		$intervals = $configModel->getVar('overrideEmailIntervals');

		if (empty($intervals)) {
			$logModel->addLogMessage('Cronjob Result', 'No email intervals set in the app; no further action necessary.', 1, 'INFO', 'cronjob', __CLASS__);
			$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
			return;
		}

		$logModel->addLogMessage('Cronjob Processing', 'Email intervals set to: ' . $intervals, 1, 'INFO', 'cronjob', __CLASS__);

		// there is some value set for the intervals. parse them
		$intervalsArray = array_map(function($i) {
			$trimmed = trim($i);
			if (!is_numeric($trimmed)) {
				throw new Exception("Error: misconfiguration of override email intervals. Invalid numeric value '$trimmed' found.");
			}
			return $trimmed;
		}, explode(",",$intervals));

		rsort($intervalsArray); // intervals will be from largest to smallest (longest to shortest)
		$dayInSeconds = 24 * 60 * 60; // h * m * s

		$notificaitonsQueued = 0;
		foreach($activeOverrides as $override) {
            $send = false; // which one to send

			$logModel->addLogMessage('Cronjob Processing', 'Checking override ' . $override["o_id"] . ", expires " . $override["end_timestamp"], 1, 'INFO', 'cronjob', __CLASS__);

			$emailsSent = is_null($override["notifications"]) ? array() : explode(",", $override["notifications"]);
			if ($emailsSent == $intervals) {
				// all notifications have been sent, can skip this one. this will just be a quick check - could still fail if notification thresholds have been changed.
				$logModel->addLogMessage('Cronjob Result', 'All emails have been sent for this override. No further action necessary.', 1, 'INFO', 'cronjob', __CLASS__);

				continue;
			} else {
				$now = time();
				foreach($intervalsArray as $k => $i) {
					$threshold = $override["end_timestamp"] - ($i * $dayInSeconds); // subtract the right number of days from the override's expiration date

					if ($now >= $threshold) {
						if (!in_array($i, $emailsSent)) {
							$laterEmails = array_slice($intervalsArray, $k); // here's the notifications that would come afterward.
							$laterEmailWasSent = false; // watch just in case a later email was already sent

							foreach($laterEmails as $l) {
								if (in_array($l, $emailsSent)) {
									$laterEmailWasSent = true;
								}
							}

							if (!$laterEmailWasSent) {
								// it needs to be sent, has not already been sent, and a later email was not already sent

								// one last check...is the start date out of bounds? email is sent at the start date.
								if ($override["start_timestamp"] >= $threshold) {
									// start time was after the notification would have been sent. log and skip.
									$logModel->addLogMessage('Cronjob Processing', $i . '-day notification skipped because start time for override ' . $override["o_id"] . ' was less than ' . $i . ' day(s) ago', 1, 'INFO', 'cronjob', __CLASS__);
								} else {
									// all good, send it.
									$send = $i;
								}
							}
						}
					}
				}
			}

			if ($send !== false) {
				$logModel->addLogMessage('Cronjob Processing', $send . '-day notification email needs to be sent for override ' . $override["o_id"], 1, 'INFO', 'cronjob', __CLASS__);

				// build email
				$user = $userListModel->getUserByEuid($override["euid"]);
				$username = $user["webExId"];

				$htmlEmailBody = $htmlEmailHelper->head("UNCG WebEx - User Override for $username");

				$status = $override["user_status"];

				$htmlBody = $htmlEmailHelper->overrideEmail($username, $status, date("m/d/Y h:i:s a", $override["start_timestamp"]), date("m/d/Y h:i:s a", $override["end_timestamp"]));

				$htmlEmailBody .= $htmlEmailHelper->body("UNCG WebEx - User Override for $username", $htmlBody);

				// trigger email
				$et = new Ot_Trigger_Dispatcher();
				$et->setVariables(array(
					'emailAddress'  => $override["contact_email"],
					'username'      => $username,
					'htmlEmailBody' => $htmlEmailBody
				));
				$et->dispatch('Wst_Override_Expiration_Warning_Html');

				$logModel->addLogMessage('Cronjob Result', $send . '-day notification email queued', 1, 'INFO', 'cronjob', __CLASS__);

				// add the entry to the user's info in the database
				$result = $overridesModel->updateNotifications($override["o_id"], $send);
				if (!is_null($result)) {
					$logModel->addLogMessage('Cronjob Result', 'Override ' . $override["o_id"] . ' notification data updated', 1, 'INFO', 'cronjob', __CLASS__);
				} else {
					$logModel->addLogMessage('Cronjob Result', 'Override ' . $override["o_id"] . ' notification data WAS NOT updated successfully.', 2, 'WARNING', 'cronjob', __CLASS__);
				}


			} else {
				$logModel->addLogMessage('Cronjob Result', 'No notifications necessary for override ' . $override["o_id"], 1, 'INFO', 'cronjob', __CLASS__);
			}
		}



		// end
		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}