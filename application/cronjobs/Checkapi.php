<?php

/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/22/16
 * Time: 9:43 AM
 */
class Wst_Cronjob_Checkapi implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null)
	{
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', 'Wst_Cronjob_Checkapi', 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');

		// what crons do we care about, if we need to do something?
		$cronModel = new Wst_Model_Dbtable_Cronjobs();
		$cronsToUpdate = array(
			"Wst_Cronjob_BuildApiQueue",
			"Wst_Cronjob_ProcessApiQueue",
			"Wst_Cronjob_Stageusers",
			"Wst_Cronjob_RevertNamesToLdap",
		);

		// take 2
		$localConfig = Zend_Registry::get("localConfig");
		$permittedApiCheckFailures = $localConfig["permittedApiCheckFailures"];
		$apiFailureCheckWindowSeconds = $localConfig["apiFailureCheckWindow"];
		$logModel->addLogMessage('Cronjob Processing', "Retrieved values for API failure threshold", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');

		// check recorded API status with our app
		$appVarsModel = new Wst_Model_Dbtable_Applicationvars();
		$webexApiStatus = $appVarsModel->getVar("webexApiStatus");
		$appStatus = $appVarsModel->getVar("appStatus");

		// check API status with WebEx
		//$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();

		// NEW
		$helper = new Wst_Model_Helpers_General();

		$logModel->addLogMessage('Cronjob Processing', "Making WebEx API call", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');
		$response = $helper->getResponseCode();

		//$result = $webexApiHelper->getWebexSiteInfo();
		//$logModel->addLogMessage('Cronjob Processing', "Completed WebEx API call", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');

		//$responseCode = $result["response"]["httpCode"];
		//$logModel->addLogMessage('Cronjob Processing', "API call response code: `$responseCode`", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');

		$success = $response['success'];
		$message = $response['message'];

		if ($success) {
			$version = $response['version'];
			$code = $response['code'];

			$logMessage = "Successful API test. Response: `$version $code $message`";
		} else {
			$logMessage = "API test failed. Error: `$message`";
		}

		$logModel->addLogMessage('Cronjob Processing', $logMessage, 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');

		//if ($responseCode == "HTTP/1.1 200 OK") {
		if ($success && $code == 200) {
			// success
			$logModel->addLogMessage('Api Status', "up", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');
			// bring things back up if we were down.
			if ($webexApiStatus == "down") {
				$webexApiStatus = "up"; // update the var to current status for further processing
				// update db
				$appVarsModel->setVar("webexApiStatus", $webexApiStatus);

				// log result
				$logModel->addLogMessage('Cronjob Processing', "Updated API status in local database to `$webexApiStatus`", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');
			}
		} else {
			// failure
			$logModel->addLogMessage('Api Status', "down", 2, 'WARNING', 'cronjob', 'Wst_Cronjob_Checkapi');
			if ($webexApiStatus == "up") {
				$webexApiStatus = "down"; // update the var to current status for further processing
				// update db
				$appVarsModel->setVar("webexApiStatus", $webexApiStatus);

				// log result
				$logModel->addLogMessage('Cronjob Processing', "Updated API status in local database to `$webexApiStatus`", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');

			}
		}

		// check for exceeded # of failures within designated window
		$logModel->addLogMessage('Cronjob Processing', "Looking for >= $permittedApiCheckFailures API failure(s) over the past $apiFailureCheckWindowSeconds seconds", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');

		$now = time();
		$apiFailureWindowOpenTime = $now - $apiFailureCheckWindowSeconds;

		$where = $logModel->getAdapter()->quoteInto("subject = ?", "Api Status");
		$where .= $logModel->getAdapter()->quoteInto(" AND message = ?", "down");
		$where .= $logModel->getAdapter()->quoteInto(" AND timestamp >= ?", $apiFailureWindowOpenTime);
		$where .= $logModel->getAdapter()->quoteInto(" AND timestamp <= ?", $now);

		$resultSet = $logModel->fetchAll($where);

		$numberOfFailuresInsideWindow = count($resultSet);
		$logModel->addLogMessage('Cronjob Processing', "Found $numberOfFailuresInsideWindow API failure(s) over the past $apiFailureCheckWindowSeconds seconds", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');

		if ($numberOfFailuresInsideWindow >= $permittedApiCheckFailures) {
			// SOUND THE ALARM. ABANDON SHIP. EVERY MAN FOR HIMSELF.
			if ($appStatus == "down") { // if app is already down...
				$logModel->addLogMessage('Cronjob Processing', "Too many API failures within the designated window. App already disabled; no further action necessary.", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');
			} else { // app needs to be taken down...
				$logModel->addLogMessage('Cronjob Processing', "Too many API failures within the designated window. Initiating safeguards.", 2, 'WARNING', 'cronjob', 'Wst_Cronjob_Checkapi');

				// disable the crons
				foreach ($cronsToUpdate as $cron) {
					$affectedCrons = $cronModel->disableCronjob($cron);
					$logModel->addLogMessage('Cronjob Processing', "Disabled cronjob $cron", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');
				}

				// update db
				$appVarsModel->setVar("appStatus", "down");
				$logModel->addLogMessage('Cronjob Processing', "Updated WST instance status in local database to `down`", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');

				$et = new Ot_Trigger_Dispatcher();
				$et->setVariables(array(
					'subject'   => 'Checkapi Cronjob WARNING',
					'summary'   => 'FAILED Threshold Check (Checkapi)',
					'details'   => "There have been more than $permittedApiCheckFailures API check failures within the last $apiFailureCheckWindowSeconds seconds. The WST instance has been disabled.",
					'timestamp' => date('Y-m-d @ H:i:s A')
				));

				$et->dispatch('Wst_System_Event');

			}
		} else {
			$logModel->addLogMessage('Cronjob Processing', "API health check OK; within normal parameters.", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');

			// bring things back up if we were down, but only if the API is currently up.
			if ($appStatus == "down") {
				if ($webexApiStatus == "up") {
					// reenable crons
					foreach ($cronsToUpdate as $cron) {
						$affectedCrons = $cronModel->enableCronjob($cron);
						$logModel->addLogMessage('Cronjob Processing', "Re-enabled cronjob $cron", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');
					}

					// update db
					$appVarsModel->setVar("appStatus", "up");
					$logModel->addLogMessage('Cronjob Processing', "Updated WST instance status in local database to `up`", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');

					$logModel->addLogMessage('Cronjob Processing', "WST instance has been re-enabled.", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');
				}
			}
		}

		$logModel->addLogMessage('Cronjob End', 'Wst_Cronjob_Checkapi', 1, 'INFO', 'cronjob', 'Wst_Cronjob_Checkapi');

	}
}