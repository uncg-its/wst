<?php

class Wst_Cronjob_ArchiveRecordingsWeekly implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', 'Wst_Cronjob_ArchiveRecordingsWeekly', 1, 'INFO', 'cronjob', 'Wst_Cronjob_ArchiveRecordingsWeekly');

		// recording archive helper
		$archiveRecordingsHelper = new Wst_Model_Helpers_ArchiveRecordingsHelper();

		$result = $archiveRecordingsHelper->archivePastRecordings('Wst_Cronjob_ArchiveRecordingsWeekly', 14);

		$logModel->addLogMessage('Cronjob End', 'Wst_Cronjob_ArchiveRecordingsWeekly', 1, 'INFO', 'cronjob', 'Wst_Cronjob_ArchiveRecordingsWeekly');
	}
}