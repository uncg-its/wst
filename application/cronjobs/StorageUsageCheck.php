<?php

/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/22/16
 * Time: 9:43 AM
 */
class Wst_Cronjob_StorageUsageCheck implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		$configModel = new Wst_Model_Dbtable_Config();
		$storageUsageThreshold = $configModel->getVar('storageUsageThreshold');

		if (!$storageUsageThreshold) {
			$logModel->addLogMessage('Cronjob Result', "Could not check storage usage - usage threshold has not been set.", 2, "WARNING", 'cronjob', __CLASS__);
			$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
			return;
		}

		$logModel->addLogMessage('Cronjob Processing', "Storage usage threshold set to $storageUsageThreshold%", 1, "INFO", 'cronjob', __CLASS__);

		// recording storage usage

		$localConfig = Zend_Registry::get('localConfig');
		$siteRecordingStorageLimit = $localConfig['siteRecordingStorageLimit'];

		if (empty($siteRecordingStorageLimit)) {
			$logModel->addLogMessage('Cronjob Result', "Could not check storage usage - site recording storage limit has not been set.", 2, "WARNING", 'cronjob', __CLASS__);
			$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
			return;
		}

		$logModel->addLogMessage('Cronjob Processing', "Site storage limit: $siteRecordingStorageLimit MB", 1, "INFO", 'cronjob', __CLASS__);

		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$recResult = $recordingArchiveModel->getCurrentRecordingStorageUsage();

		$recordingStorageUsage = round($recResult[0]["recording_usage"], 2);

		$percentStorageLimitUsed = round(($recordingStorageUsage / $siteRecordingStorageLimit) * 100, 2);

		$logModel->addLogMessage('Cronjob Processing', "Current usage: $recordingStorageUsage MB ($percentStorageLimitUsed% of limit)", 1, "INFO", 'cronjob', __CLASS__);

		if ($percentStorageLimitUsed >= $storageUsageThreshold) {
			// trigger
			$logModel->addLogMessage('Cronjob Processing', "Storage usage exceeds warning threshold of $storageUsageThreshold%", 2, 'WARNING', 'cronjob', __CLASS__);

			// trigger if over a certain threshold
			$et = new Ot_Trigger_Dispatcher();
			$et->setVariables(array(
				'subject'   => 'StorageUsageCheck Cronjob Report',
				'summary'   => 'Storage usage WARNING (StorageUsageCheck)',
				'details'   => "Storage limit of $siteRecordingStorageLimit MB has exceeded $storageUsageThreshold% usage - current usage is $percentStorageLimitUsed%.",
				'timestamp' => date('Y-m-d @ H:i:s A')
			));
			$et->dispatch('Wst_System_Event');
		} else {
			$logModel->addLogMessage('Cronjob Processing', "Storage Usage is within acceptable limit.", 1, "INFO", 'cronjob', __CLASS__);
		}


		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}