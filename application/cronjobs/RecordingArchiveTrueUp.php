<?php

class Wst_Cronjob_RecordingArchiveTrueUp implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		// get recordings from local DB
		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$activeRecordingsListLocal = $recordingArchiveModel->getActiveRecordingArchives();
		
		$logModel->addLogMessage('Cronjob Processing', "Retrieved local recordings list. Found " . count($activeRecordingsListLocal) . " active " . pluralize($activeRecordingsListLocal, "recording", "s") . " total.", 1, 'INFO', 'cronjob', __CLASS__);

		$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();

		$updatedRecords = array();
		$trashedRecords = array();

		foreach($activeRecordingsListLocal as $index => $recording) {
			$id = $recording["recording_id"];

			$recordingInfoFromWebexResult = $webexApiHelper->getRecordingDetails($id);

			if ($recordingInfoFromWebexResult["response"]["result"] == "SUCCESS") {
				// was found - has the user info or name changed?
				$webExRecording = $recordingInfoFromWebexResult["data"]["ep:recording"];

				$updateData = array();

				// check name
				$localName = $recording['name'];
				$webExName = $webExRecording["ep:name"];

				if ($localName != $webExName) {
					$updateData["name"] = $webExName;
					$logModel->addLogMessage('Cronjob Processing', "Recording $id has new name. Changing $localName to $webExName in local database.", 1, 'INFO', 'cronjob', __CLASS__);
				}

				// check user
				$localUser = strtolower($recording['host_webex_id']);
				$webExUser = strtolower($webExRecording['ep:hostWebExID']);
				
				if ($localUser != $webExUser) {
					$updateData["host_webex_id"] = $webExUser;
					$logModel->addLogMessage('Cronjob Processing', "Recording $id has new host id. Changing $localUser to $webExUser in local database.", 1, 'INFO', 'cronjob', __CLASS__);
				}

				if (count($updateData) > 0) {
					// update if stuff has changed
					$updateResult = $recordingArchiveModel->updateRecording($updateData, $id);
					$updatedRecords[] = $id;
				}

			} else {
				$exceptionId = $recordingInfoFromWebexResult["response"]["exceptionId"];
				if ($exceptionId == "000015") {
					// was not found - it was put in the trash
					$trashResult = $recordingArchiveModel->trashRecording($id);
					//$deleteResult = $recordingArchiveModel->changeRecordingStatus($id, "in-trash");
					$trashedRecords[] = $id;
					$logModel->addLogMessage('Cronjob Processing', "Recording $id no longer exists in WebEx. Changing status to 'in-trash' in local database.", 1, 'INFO', 'cronjob', __CLASS__);
				} else {
					$exceptionMessage = $recordingInfoFromWebexResult["response"]["message"];
					// some other error. don't touch our records
					$logModel->addLogMessage('Cronjob Processing', "Error retrieving record $id - code: $exceptionId, message: $exceptionMessage .", 2, 'WARNING', 'cronjob', __CLASS__);

					$et = new Ot_Trigger_Dispatcher();
					$et->setVariables(array(
						'subject'   => 'RecordingArchiveTrueUp Cronjob WARNING',
						'summary'   => 'WARNING - Unexpected Error fetching recording (RecordingArchiveTrueUp)',
						'details'   => "Trying to retrieve recording id $id from WebEx, encountered an unexpected error - error code $exceptionId ($exceptionMessage)",
						'timestamp' => date('Y-m-d @ H:i:s A')
					));
					$et->dispatch('Wst_System_Event');
				}
			}
		}

		$logModel->addLogMessage('Cronjob Result', "Updated records: " . count($updatedRecords) . "; Records marked in-trash: " . count($trashedRecords), 1, 'INFO', 'cronjob', __CLASS__);

		// now check trash for days past the trash threshold
		$trashedRecordingsListLocal = $recordingArchiveModel->getTrashedRecordingArchives();

		$logModel->addLogMessage('Cronjob Processing', "Retrieved local trashed recordings list. Found " . count($trashedRecordingsListLocal) . " trashed " . pluralize($trashedRecordingsListLocal, "recording", "s") . " total.", 1, 'INFO', 'cronjob', __CLASS__);

		$recordsMarkedDeleted = array();

		// get threshold
		$localConfig = Zend_Registry::get('localConfig');
		$trashDaysBeforeDelete = $localConfig["siteTrashDaysBeforeDelete"];

		$earliestKeptTimestamp = strtotime("- $trashDaysBeforeDelete days");

		$logModel->addLogMessage("Cronjob Processing", "Site setting for trash window: $trashDaysBeforeDelete day(s). Looking for recordings trashed before $earliestKeptTimestamp.", 1, 'INFO', 'cronjob', __CLASS__);

		foreach($trashedRecordingsListLocal as $r) {
			$trashedTimestamp = $r["trashed_timestamp"];
			$id = $r["recording_id"];

			$recordingInfoFromWebexResult = $webexApiHelper->getRecordingDetails($id);

			// was it in the trash and now active again?
			if ($recordingInfoFromWebexResult["response"]["result"] == "SUCCESS") {
				$updateData["status"] = 'active';
				$updateData["trashed_timestamp"] = NULL;

				$updateResult = $recordingArchiveModel->updateRecording($updateData, $id);

				$logModel->addLogMessage('Cronjob Processing', "Recording $id was removed from Trash in WebEx. Changing status to 'active' in local database.", 1, 'INFO', 'cronjob', __CLASS__);
			} else if ($trashedTimestamp < $earliestKeptTimestamp) {
				// mark as deleted
				$deleteResult = $recordingArchiveModel->markRecordingAsDeleted($id);
				$logModel->addLogMessage('Cronjob Processing', "Trashed recording $id is past the deletion threshold of $trashDaysBeforeDelete day(s). Changing status to 'deleted' in local database.", 1, 'INFO', 'cronjob', __CLASS__);
				$recordsMarkedDeleted[] = $id;
			}

		}

		$logModel->addLogMessage('Cronjob Result', "Records marked deleted: " . count($recordsMarkedDeleted), 1, 'INFO', 'cronjob', __CLASS__);

		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}