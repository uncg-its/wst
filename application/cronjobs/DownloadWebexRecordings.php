<?php

class Wst_Cronjob_DownloadWebexRecordings implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		// helper model
		$helperModel = new Wst_Model_Helpers_General();

		// init success
		$downloadSuccess = true;

		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$nextRecordingToDownload = $recordingArchiveModel->getNextRecordingToDownload();

		if (isset($nextRecordingToDownload[0]["recording_id"])) {
			$idToDownload = $nextRecordingToDownload[0]["recording_id"];

			$logModel->addLogMessage('Cronjob Processing', "Next recording ID to be downloaded: $idToDownload", 1, 'INFO', 'cronjob', __CLASS__);

			$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();

			// does it still exist in WebEx or has user done something to it?
			$recordingInfo = $webexApiHelper->getRecordingDetails($idToDownload);

			// config vars
			$configModel = new Wst_Model_Dbtable_Config();
			$allowedFailures = $configModel->getVar('arfDownloadRetries');

			if ($recordingInfo["response"]["result"] != "SUCCESS") {
				if ($recordingInfo["response"]["exceptionId"] == "000015") {
					// not found. don't proceed with download; mark as deleted-by-user; download status to "not-downloaded"
					$logModel->addLogMessage('Cronjob Processing', "Recording $idToDownload was not found in WebEx - marking as 'deleted-by-user'", 1, 'INFO', 'cronjob', __CLASS__);

					$markDeletedResult = $recordingArchiveModel->trashRecording($idToDownload);
					$statusChangeResult = $recordingArchiveModel->changeDownloadStatus($idToDownload, "not-downloaded");

				} else {
					// error is something else. leave it as-is, and increment failed downloads.
					$logModel->addLogMessage('Cronjob Processing', "Unexpected API error when looking up recording $idToDownload", 1, 'INFO', 'cronjob', __CLASS__);

					// increment failure count, threshold check
					$incrementFailureResult = $helperModel->incrementRecordingFailures($idToDownload, "download");
					$downloadSuccess = false;

				}
			} else {
				// no error. ok to proceed.

				// download it
				$downloadResult = $webexApiHelper->downloadArfFile($idToDownload);
				if ($downloadResult["success"]) {
					// download succeeded
					$logModel->addLogMessage('Cronjob Processing', "ARF file downloaded successfully to " . realpath($downloadResult["filename"]), 1, 'INFO', 'cronjob', __CLASS__);

                    // record size in DB
                    $downloadedFileSizeInMegabytes = $downloadResult["fileInfo"]["size"] / 1024 / 1024;
                    $setSizeResult = $recordingArchiveModel->setDownloadedFileSize($idToDownload, $downloadedFileSizeInMegabytes);


					// check expected size vs. actual size
					$expectedSizeMb = $nextRecordingToDownload[0]["size_mb"];
					$actualSizeMb = $downloadedFileSizeInMegabytes; // reported in B

					// get allowable threshold
					$thresholdMb = $configModel->getVar("arfSizeDifferenceThresholdMegabytes");
					if (!$thresholdMb) { $thresholdMb = 0; } // default

					$sizeDifferenceMb = abs($actualSizeMb - $expectedSizeMb);
					$logModel->addLogMessage('Cronjob Processing', "ARF File Size: expected (WebEx): $expectedSizeMb MB; actual download (API): $actualSizeMb MB", 1, 'INFO', 'cronjob', __CLASS__);

					if ($sizeDifferenceMb > $thresholdMb) {
						// threshold for discrepancy in file size has been exceeded.
						$logModel->addLogMessage('Cronjob Processing', "WARNING: ARF File Size difference of $sizeDifferenceMb MB exceeds allowed threshold of $thresholdMb MB.", 2, 'WARNING', 'cronjob', __CLASS__);

						// delete local file
						$localPath = $downloadResult["filename"];
						unlink($localPath);

						// increment failure count, threshold check
						$incrementFailureResult = $helperModel->incrementRecordingFailures($idToDownload, "download");
						$downloadSuccess = false;

					} else {
						// all is acceptable. mark recording 'downloaded' in database; record actual size; mark ready for upload to cloud storage.
						$logModel->addLogMessage('Cronjob Processing', "ARF File Size difference of $sizeDifferenceMb MB is within allowed threshold of $thresholdMb MB", 1, 'INFO', 'cronjob', __CLASS__);


						$markDownloadedResult = $recordingArchiveModel->changeDownloadStatus($idToDownload, "downloaded");
						$markPendingUploadResult = $recordingArchiveModel->changeUploadStatus($idToDownload, "pending");
						// we will take care of deleting the file from WebEx in a subsequent job.
					}

				} else {
					// download API call failed.
					$logModel->addLogMessage('Cronjob Processing', "WARNING: ARF download failed. Message: " . $downloadResult["message"], 2, 'WARNING', 'cronjob', __CLASS__);

					// increment failure count, threshold check
					$incrementFailureResult = $helperModel->incrementRecordingFailures($idToDownload, "download");
					$downloadSuccess = false;
				}

			}

			if ($downloadSuccess) {
				$message = "Next recording in queue ($idToDownload) downloaded successfully.";
			} else {
				$message = "Failure downloading recording $idToDownload.";
			}
			$logModel->addLogMessage('Cronjob Result', $message, 1, 'INFO', 'cronjob', __CLASS__);

		} else {
			$logModel->addLogMessage('Cronjob Result', "No recordings need to be downloaded.", 1, 'INFO', 'cronjob', __CLASS__);
		}

		// end cronjob
		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}