<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/24/16
 * Time: 4:41 PM
 */

class Wst_Cronjob_ProcessApiQueue implements Ot_Cron_JobInterface
{
	public function execute($lastRunDt = null)
	{
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', 'Wst_Cronjob_ProcessApiQueue', 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessApiQueue');

		// models
		$queueModel = new Wst_Model_Dbtable_Apiqueue();
		$userListModel = new Wst_Model_Dbtable_Userlist();
		$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();

		// get API call limit from registry
		$localConfig = Zend_Registry::get("localConfig");
		$queueProcessingLimit = $localConfig["queueProcessingLimit"];

		// get pending entries from API queue
		$where = $queueModel->getAdapter()->quoteInto("status = ?", "pending");
		$pendingApiCalls = $queueModel->fetchAll($where, "q_id ASC", $queueProcessingLimit)->toArray();

		$numApiCallsFound = count($pendingApiCalls);
		$numApiCallsFoundPlural = $numApiCallsFound == 1 ? "" : "s";
		$message = "Retrieved $numApiCallsFound record$numApiCallsFoundPlural from staging table.";

		$logModel->addLogMessage('Cronjob Processing', $message, 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessApiQueue');

		// TODO - try-catch here to better report errors. for starters incorporate what the model is giving back upon database call.
		$successfulCalls = 0;
		//$failedCalls = 0; // tODO - make this an array that stores at least something about the call that failed. include details that you can use at a glance.
		$failedCalls = array();

		// how many attempts are allowed before failsafes are triggered?
		$attemptLimit = $localConfig["queueRetryAttempts"];

		foreach($pendingApiCalls as $call) {
			// set status of API Queue entry to "in-progress"
			$where = $queueModel->getAdapter()->quoteInto("q_id = ?", $call['q_id']);
			$updateStatusResult = $queueModel->update(array("status" => "in-progress"), $where);

            $attemptValue = (empty($call["attempt"])) ? 0 : $call["attempt"];

			if ($call["action"] == "add") {

				// API call
				$addUserResult = $webexApiHelper->addWebexUser($call["firstName"], $call["lastName"], $call["webExId"], $call["email"], array(
					"dept_number" => $call["dept_number"],
					"org_short_code" => $call["org_short_code"]
				));

				if ($addUserResult["response"]["result"] == "SUCCESS") {
					// user ID returned from WebEx
					$addedUserId = $addUserResult["data"]["use:userId"];

					if ($call['shouldUpdateUserList']) {
						// add user to user list
						$addResult = $userListModel->addNewUserToUserList($call["euid"], $call["webExId"], $call["firstName"], $call["lastName"], $call["email"], $call["dept_number"], $call["org_short_code"], $addedUserId);
					}

					$callStatus = "complete";
					$callMessage = $addUserResult["response"]["message"];

					$successfulCalls++;

				} else {
					$callStatus = "error";
					$callMessage = $addUserResult["response"]["message"];
					$failedCalls[$call["q_id"]] = array(
						"webExId" => $call["webExId"],
						"action"    => $call["action"]
					);
				}

			} else if ($call["action"] == "edit") {

				// API call prep
				$fieldsToLookAt = array("firstName", "lastName", "email", "newWebExId", "dept_number", "org_short_code");
				$editFields = array();
				foreach($fieldsToLookAt as $f) {
					if (!is_null($call[$f])) {
						$editFields[$f] = $call[$f];
					}
				}
				// API call
				$editUserResult = $webexApiHelper->editWebexUser($call["webExId"], $editFields);

				if ($editUserResult["response"]["result"] == "SUCCESS") {

					// if present, newWebExId replaces webExId
					$updateData = $editFields;
					if (array_key_exists("newWebExId", $updateData)) {
						$updateData["webExId"] = $editFields["newWebExId"];
						unset($updateData["newWebExId"]);
					}

					if ($call['shouldUpdateUserList']) {
						// update user list table
						// overwrite regardless. if we performed an edit, the user should be active.
						$updateData["status"] = "active";
						$updateResult = $userListModel->editUserInUserList($call["euid"], $updateData);
					}

					$callStatus = "complete";
					$callMessage = $editUserResult["response"]["message"];

					$successfulCalls++;

				} else {
					$callStatus = "error";
					$callMessage = $editUserResult["response"]["message"];

					$failedCalls[$call["q_id"]] = array(
						"webExId" => $call["webExId"],
						"action"    => $call["action"]
					);
				}

			} else if ($call["action"] == "delete") {

				// API call
				$deleteUserResult = $webexApiHelper->deleteWebexUser($call["webExId"]);
				if ($deleteUserResult["response"]["result"] == "SUCCESS") {

					if ($call['shouldUpdateUserList']) {
						// delete from user list
						$inactivateResult = $userListModel->inactivateUserInUserList($call["euid"]);
					}

					$callStatus = "complete";
					$callMessage = $deleteUserResult["response"]["message"];

					$successfulCalls++;

				} else {
					$callStatus = "error";
					$callMessage = $deleteUserResult["response"]["message"];

					$failedCalls[$call["q_id"]] = array(
						"webExId" => $call["webExId"],
						"action"    => $call["action"]
					);
				}


			} else {
				$callStatus = "error";
				$callMessage = "API queue processing error - unknown action '{$call['action']}'";

				$failedCalls[$call["q_id"]] = array(
					"webExId" => $call["webExId"],
					"action"    => $call["action"]
				);
			}

			if ($callStatus == "error") {
				$logModel->addLogMessage('Cronjob Processing', $callMessage, 1, 'WARNING', 'cronjob', 'Wst_Cronjob_ProcessApiQueue');

				if($attemptValue < $attemptLimit) {
				    $attemptValue = $attemptValue + 1;
                    $callStatus = 'pending';

                    unset($failedCalls[$call["q_id"]]);
                }

			}

			// change status of API call
			$updateStatusData = array(
				"status"             => $callStatus,
				"message"            => $callMessage,
                "attempt"            => $attemptValue,
				"timestampAttempted" => time()
			);

			$updateStatusResult = $queueModel->update($updateStatusData, $where);
		}

		$numFailedCalls = count($failedCalls);

		$totalCalls = $successfulCalls + $numFailedCalls;
		$message = "Total Pending API Calls Processed: $totalCalls; Successful: $successfulCalls; Failed: $numFailedCalls";

		$logModel->addLogMessage('Cronjob Processing', $message, 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessApiQueue');

		if (!empty($failedCalls)) {
			$summaryString = "";
			foreach ($failedCalls as $f) {
				$summaryString .= "-- {$f["action"]} action failed for user: {$f["webExId"]}" . PHP_EOL;
			}

			$et = new Ot_Trigger_Dispatcher();
			$et->setVariables(array(
				'subject'   => 'ProcessApiQueue Cronjob WARNING',
				'summary'   => 'WARNING API failed calls summary (ProcessApiQueue)',
				'details'   => "1 or more API queue items failed to process after $attemptLimit attempt(s). Staging cronjob has been disabled." . PHP_EOL . $summaryString,
				'timestamp' => date('Y-m-d @ H:i:s A')
			));
			$et->dispatch('Wst_System_Event');

			// disable staging cronjob
			$cronModel = new Wst_Model_Dbtable_Cronjobs();
			$cronsToUpdate = array(
				"Wst_Cronjob_Stageusers"
			);

			foreach($cronsToUpdate as $cron) {
				$disableResult = $cronModel->disableCronjob($cron);
				$logModel->addLogMessage('Cronjob Processing', "Disabled cronjob $cron", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessApiQueue');
			}

		}


		$logModel->addLogMessage('Cronjob End', 'Wst_Cronjob_ProcessApiQueue', 1, 'INFO', 'cronjob', 'Wst_Cronjob_ProcessApiQueue');
	}
}