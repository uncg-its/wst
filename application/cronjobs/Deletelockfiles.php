<?php
class Wst_Cronjob_Deletelockfiles implements Ot_Cron_JobInterface
{
    
    public function execute($lastRunDt = null)
    {
        
        $returnArray = array();
        
        $cachePath = APPLICATION_PATH . '/../cache/';

        // get file listing, sorted by oldest first ([0] in the array will be the oldest file from the directory
        $fileListing = array_diff(scandir($cachePath, 1), array('..', '.'));
        
        if(count($fileListing) > 0) {
            
            $lockFileArray = array();
            $oneHourAgo = time() - 3600;
            $helper = new Wst_Model_Helpers_General();
            
            foreach($fileListing as $thisFile) {
                
                // check to see if it's a lock file
                if($helper->endsWith($thisFile, '.lock')) {
                    
                    // check to see if the file was created more than an hour ago
                    $fileTime = filemtime($cachePath . '/' . $thisFile);
                    
                    if($fileTime < $oneHourAgo) {
                        $lockFileArray[$thisFile] = date('Y-m-d @ H:i:s A', $fileTime);    
                    }
                    
                }

            }
        }
        
        // if lock files exist that are older than an hour, then send an email and delete the file
        if(!empty($lockFileArray)) {
            
            $et = new Ot_Trigger_Dispatcher();
            foreach($lockFileArray as $lockFileName => $lockFileTime) {
                
                unlink($cachePath . $lockFileName);
                
                $et->setVariables(array(
                'subject'           => 'Lock File Deleted: ' . $lockFileName . ' (SUCCESS)',
                'summary'           => 'Lock File "' . $lockFileName . '" existed for over one hour and has now been deleted',
                'details'           => 'Lock File "' . $lockFileName . '" was created on: ' . $lockFileTime,
                'timestamp'         => date('Y-m-d @ H:i:s A')
                ));

                $et->dispatch('Wst_System_Event');
            }
            
        }
        
    }
}