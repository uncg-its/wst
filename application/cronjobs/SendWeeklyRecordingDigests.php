<?php

class Wst_Cronjob_SendWeeklyRecordingDigests implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);


		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$recordingsByUser = $recordingArchiveModel->getActiveRecordingsByUser();

		$localConfig = Zend_Registry::get('localConfig');

		$configModel = new Wst_Model_Dbtable_Config();
		$expireWindow = $configModel->getVar('recordingDeletionWindow');

		$myRecordingsUrlReplacement = $configModel->getVar('myRecordingsLinkReplacement');
		$myRecordingsUrl = (!$myRecordingsUrlReplacement) ? "https://" : $myRecordingsUrlReplacement;
		// TODO - this must be fixed. How to get base URL of app??

		$generalHelper = new Wst_Model_Helpers_General();

		$emailsQueued = 0;

		if (!$expireWindow) {
			$logModel->addLogMessage('Cronjob Processing', "ERROR: no value for app var 'recordingDeletionWindow' has been set.", 3, 'ERROR', 'cronjob', __CLASS__);

			$et = new Ot_Trigger_Dispatcher();
			$et->setVariables(array(
				'subject'   => 'SendWeeklyRecordingsDigests',
				'summary'   => 'FAILURE: Send Weekly Recordings Digests',
				'details'   => "Error: WST 'recordingDeletionWindow' is not set. Cannot process weekly recording digests.",
				'timestamp' => date('Y-m-d @ H:i:s A')
			));
			$et->dispatch('Wst_System_Event');

		} else if (count($recordingsByUser) > 0) {
			$logModel->addLogMessage('Cronjob Processing', "app var 'recordingDeletionWindow' set to $expireWindow day(s).", 1, 'INFO', 'cronjob', __CLASS__);

			foreach($recordingsByUser as $user => $recordings) {
				//$recordingList = "";
				$recordingListHtml = "";
				foreach ($recordings as $index => $recInfo) {
					//$downloadLink = $generalHelper->getModifiedDownloadLink($recInfo["file_url"]); // retiring this in favor of the my-recordings page

					$expireDate = strtotime("+$expireWindow days", $recInfo["create_time_utc"]);
					$expireDateString = date("m/d/Y h:i:s a", $expireDate);
					$earliestDeleteTimestamp = $configModel->getVar('earliestDeleteTimestamp');
					if ($earliestDeleteTimestamp != "" && $expireDate < $earliestDeleteTimestamp) {
						$expireDateString = gmdate("m/d/Y h:i:s a", $earliestDeleteTimestamp);
					}

					//$recordingList .= "\n\t- {$recInfo["name"]}\n\t\t-- created " . date("m/d/Y h:i:s a", $recInfo["create_time_utc"]) . "\n\t\t-- expires " . $expireDateString . "\n\t\t-- link: $downloadLink\n";
					$recordingListHtml .= "<li>{$recInfo["name"]}<ul><li>created " . date("m/d/Y h:i:s a", $recInfo["create_time_utc"]) . "</li><li>expires " . $expireDateString . "</li></ul></li><br>";
				}

				$todayDate = date("m/d/Y h:i:s a");
				$emailAddress = $user . $localConfig["emailSuffix"];

				$appUrl = $localConfig["appUrl"];

				$emailHelper = new Wst_Model_Helpers_HtmlEmail();
				$htmlEmailBody = $emailHelper->head("UNCG WebEx Recordings Digest");

				$htmlBody = <<<HTML
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">You are receiving this message as a courtesy to inform you of the status of your WebEx recordings, for user account '$user'. According to internal records, this account holds at least one active recording in the UNCG WebEx environment.</p>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Below is a list of <strong>current recordings for '$user', as of $todayDate</strong>. Click the button below to proceed to the recording management page, where you can download copies of each recording, and also access them via cloud storage platforms.</p>
<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;" width="100%">
  <tbody>
    <tr>
      <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;" valign="top">
        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto; margin: 0 auto;">
          <tbody>
            <tr>
              <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center;" valign="top" bgcolor="#3498db" align="center"> <a href="{$appUrl}userservices/my-recordings" target="_blank" style="display: inline-block; color: #ffffff; background-color: #003366; border: solid 1px #003366; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #003366;">Manage My UNCG WebEx Recordings</a> </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>

<ul>
$recordingListHtml
</ul>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">WebEx has a quota limit for the amount of recordings that can be stored within their platform. Thus, all recordings are automatically removed from WebEx after $expireWindow day(s). For convenience, prior to its removal from WebEx, each recording is automatically transferred to one of UNCG's cloud storage providers (currently Box - http://box.uncg.edu) and shared with the host, thereby making all recordings available beyond the {$expireWindow}-day expiration window. To share an ARF file from its location in cloud storage, we recommend that you visit https://its.uncg.edu/WebEx/Recordings for best practices. </p>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Please visit <a href="http://go.uncg.edu/webex:digestemail">http://go.uncg.edu/webex</a>, or contact 6-TECH (256-8324), if you have additional questions about this information.</p>

<hr>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">As always, ITS reminds you to confirm the safety of all links in electronic communications by hovering over them with your cursor and confirming legitimacy of the destination.</p>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Never click on unverified website links. Report suspicious messages and links to 6-TECH at (336) 256-TECH (8324) or 6-TECH@uncg.edu.</p> 

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">This email is an official communication from the University of North Carolina at Greensboro. You may verify official university emails by checking the Verified Campus Communications Repository. If you have questions about the VCCR, or the authenticity of an email message you have received, please contact the sender of the message or search the UNCG website for "VCCR".</p>

HTML;

				$htmlEmailBody .= $emailHelper->body("Your UNCG WebEx Recordings Digest", $htmlBody);

				// non-HTML version
				//$et = new Ot_Trigger_Dispatcher();
				//$et->setVariables(array(
				//	'todayDate'                => $todayDate,
				//	'emailAddress'             => $emailAddress,
				//	'webExId'                  => $user,
				//	'recordingInformationList' => $recordingList,
				//	'removalTimeframe'         => $expireWindow,
				//	'myRecordingsUrl'          => $myRecordingsUrl
				//));
				//$et->dispatch('Wst_Weekly_Recordings_Digest');

				// HTML version
				$etHtml = new Ot_Trigger_Dispatcher();
				$etHtml->setVariables(array(
					'emailAddress'  => $emailAddress,
					'htmlEmailBody' => $htmlEmailBody
				));
				$etHtml->dispatch('Wst_Weekly_Recordings_Digest_Html');

				$emailsQueued++;
			}
		}

		$logModel->addLogMessage('Cronjob Result', $emailsQueued . " weekly recording digest " . pluralize($emailsQueued, "email", "s") . " queued for sending.", 1, 'INFO', 'cronjob', __CLASS__);

		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

	}
}