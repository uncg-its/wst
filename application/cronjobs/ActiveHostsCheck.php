<?php

/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/22/16
 * Time: 9:43 AM
 */
class Wst_Cronjob_ActiveHostsCheck implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {
		$logModel = new Wst_Model_Dbtable_Log();

		$logModel->addLogMessage('Cronjob Start', 'Wst_Cronjob_ActiveHostsCheck', 1, 'INFO', 'cronjob', 'Wst_Cronjob_ActiveHostsCheck');

		$apiHelper = new Wst_Model_Helpers_Webexapihelper();
		$siteInfoRequest = $apiHelper->getWebexSiteInfo();

		if ($siteInfoRequest["response"]["result"] == "SUCCESS") {

			$logModel->addLogMessage('Cronjob Processing', "Site Info API call successful", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ActiveHostsCheck');

			$siteMetadata = $siteInfoRequest["data"]["serv:message"]["serv:body"]["serv:bodyContent"]["ns1:siteInstance"]["ns1:metaData"];

			$hostLimit = $siteMetadata["ns1:accountNumLimit"];
			$activeUserCount = $siteMetadata["ns1:activeUserCount"];

			$logModel->addLogMessage('Cronjob Processing', "Host Limit: $hostLimit; Active User Count: $activeUserCount", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ActiveHostsCheck');

			$thresholdPercent = 90; // TODO - configurable variable

			$logModel->addLogMessage('Cronjob Processing', "Checking utilization using $thresholdPercent% threshold...", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ActiveHostsCheck');

			if ($activeUserCount >= $hostLimit * ($thresholdPercent / 100)) {
				$logModel->addLogMessage('Cronjob Processing', "Host utilization exceeds threshold of $thresholdPercent%", 2, 'WARNING', 'cronjob', 'Wst_Cronjob_ActiveHostsCheck');

				// trigger if over a certain threshold
				$et = new Ot_Trigger_Dispatcher();
				$et->setVariables(array(
					'subject'   => 'ActiveHostsCheck Cronjob Report',
					'summary'   => 'Host usage WARNING (ActiveHostsCheck)',
					'details'   => "$activeUserCount host accounts used out of a limit of $hostLimit; this value is above the threshold of $thresholdPercent% usage. Please take action on the site to ensure that sufficient host licenses are available.",
					'timestamp' => date('Y-m-d @ H:i:s A')
				));
				$et->dispatch('Wst_System_Event');
			} else {
				$logModel->addLogMessage('Cronjob Processing', "Host utilization within allowed threshold of $thresholdPercent%", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ActiveHostsCheck');
			}


		} else {
			// api call failed.
			$logModel->addLogMessage('Cronjob Processing', "Site Info API call FAILED.", 1, 'INFO', 'cronjob', 'Wst_Cronjob_ActiveHostsCheck');
		}

		$logModel->addLogMessage('Cronjob End', 'Wst_Cronjob_ActiveHostsCheck', 1, 'INFO', 'cronjob', 'Wst_Cronjob_ActiveHostsCheck');
	}
}