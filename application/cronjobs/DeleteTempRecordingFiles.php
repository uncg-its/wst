<?php

class Wst_Cronjob_DeleteTempRecordingFiles implements Ot_Cron_JobInterface {
	public function execute($lastRunDt = null) {

		// log model
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('Cronjob Start', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);

		// get recordings map by id
		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$recordingsById = $recordingArchiveModel->getRecordingArchiveMap();

		$logModel->addLogMessage('Cronjob Processing', "Retrieved recording archives from database", 1, 'INFO', 'cronjob', __CLASS__);

		// get files list from arf-downloads/ folder
		$path = "../arf-downloads/";
		$downloadFolderContents = scandir($path);

		// how long do we keep files?
		$configModel = new Wst_Model_Dbtable_Config();
		$daysToKeepRecordingFilesLocally = $configModel->getVar("localRecordingCacheCleaningWindow");

		if (!$daysToKeepRecordingFilesLocally) {
			$logModel->addLogMessage('Cronjob Warning', "Error: local recording cache cleaning window app variable is not set. Cannot proceed with cleaning local cache.", 2, 'WARNING', 'cronjob', __CLASS__);
			$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
			return;
		}

		$secondsToKeepRecordingFilesLocally = $daysToKeepRecordingFilesLocally * 24 * 60 * 60; // days * hours * minutes * seconds

		$logModel->addLogMessage('Cronjob Processing', "Searching for files older than $daysToKeepRecordingFilesLocally " . pluralize($daysToKeepRecordingFilesLocally, "day", "s") . " ($secondsToKeepRecordingFilesLocally seconds)", 1, 'INFO', 'cronjob', __CLASS__);

		// for each file, look it up by id (remove .arf in search string)
		// if the file is older than X days (set in app settings, look up file info via PHP) AND uploaded to cloud storage successfully, then store it in "to-delete" array
		$arfFilesFound = 0;

		$filenamesToDelete = array();
		$filenamesToKeep = array();

		$serverSpaceFreed = 0; // MB

		if (count($downloadFolderContents) > 0) {
			foreach ($downloadFolderContents as $filename) {
				// init
				$delete = false;

				if (substr($filename, -4) === ".arf") {
					$arfFilesFound++;
					$recId = str_replace(".arf", "", $filename);
					// uploaded?
					if ($recordingsById[$recId]["upload_status"] == "uploaded") {
						// outside the "keep" threshold?
						$fileCreationTimeInSecondsAgo = time() - filemtime($path . $filename);
						if ($fileCreationTimeInSecondsAgo >= $secondsToKeepRecordingFilesLocally) {
							$delete = true;
						}
					}
				}

				if ($delete) {
					$filenamesToDelete[] = $filename;
				} else {
					$filenamesToKeep[] = $filename;
				}

			}

		}

		$logModel->addLogMessage('Cronjob Processing', "Found $arfFilesFound ARF " . pluralize($arfFilesFound, "file", "s") . " in path '$path'", 1, 'INFO', 'cronjob', __CLASS__);

		if (count($filenamesToDelete) > 0) {
			$logModel->addLogMessage('Cronjob Data', "Deleting following files: " . implode(",",$filenamesToDelete), 1, 'INFO', 'cronjob', __CLASS__);
		}

		$logModel->addLogMessage('Cronjob Processing', "Found " . count($filenamesToDelete) . " ARF " . pluralize($filenamesToDelete, "file", "s") . " for deletion", 1, 'INFO', 'cronjob', __CLASS__);

		// loop through the "to-delete" array and delete files from memory.

		$deletedFiles = array();
		foreach ($filenamesToDelete as $file) {
			if (unlink($path . $file)) {
				$recId = str_replace(".arf", "", $file);
				$recSizeOnDisk = $recordingsById[$recId]["downloaded_file_size_mb"];
				$serverSpaceFreed += $recSizeOnDisk;
				$deletedFiles[] = $file;
			}
		}

		$deletionDiff = count($filenamesToDelete) - count($deletedFiles);
		if ($filenamesToDelete == 0) {
			$logModel->addLogMessage('Cronjob Result', "No local recordings to delete.", 1, 'INFO', 'cronjob', __CLASS__);
		} else if ($deletionDiff == 0) {
			$logModel->addLogMessage('Cronjob Result', "Successfully deleted " . count($deletedFiles) . " ARF " . pluralize($deletedFiles, "file", "s") . " from server path '$path'", 1, 'INFO', 'cronjob', __CLASS__);
		} else {
			$logModel->addLogMessage('Cronjob Warning', "Warning: failed to delete $deletionDiff " . pluralize($deletionDiff, "file", "s"), 2, 'WARNING', 'cronjob', __CLASS__);

			$et = new Ot_Trigger_Dispatcher();
			$et->setVariables(array(
				'subject'   => 'DeleteTempRecordingFiles Cronjob Report',
				'summary'   => 'FAILED to delete some local ARF files from server',
				'details'   => "Trying to delete " . count($filenamesToDelete) . " ARF " . pluralize($filenamesToDelete, "file", "s") . " from server - only able to delete " . count($deletedFiles) . ". Files that failed to delete: " . implode(", ", array_diff($filenamesToDelete, $deletedFiles)),
				'timestamp' => date('Y-m-d @ H:i:s A')
			));
			$et->dispatch('Wst_System_Event');
		}

		// end cronjob

		$logModel->addLogMessage('Cronjob Result', "Server space freed: " . round($serverSpaceFreed, 2) . " MB", 1, 'INFO', 'cronjob', __CLASS__);

		$logModel->addLogMessage('Cronjob End', __CLASS__, 1, 'INFO', 'cronjob', __CLASS__);
	}
}