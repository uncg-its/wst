<?php

class Wst_Apiendpoint_Activateaccount extends Ot_Api_EndpointTemplate
{

	/**
	 * Receives username and attempts to activate user's WebEx account.
	 *
	 * Params
	 * ===========================
	 * Required Parameters:
	 *   - jsonData: one key-value pair: 'username':'webExId'
	 *
	 * EXAMPLE
	 * ===========================
	 *
	 * jsonData example:
	 * {"username":"jdoe1"}
	 *
	 */
	public function post($params)
	{
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('API Endpoint Call Start', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);

		if (!isset($params['jsonData'])) {
			$logModel->addLogMessage('API Endpoint Call Result', "Invalid input - missing jsonData parameter", 1, 'INFO', 'apiendpoint', __CLASS__);
			$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);
			throw new Ot_Exception_Input('You must provide the jsonData parameter');
		} else {

			if (!Zend_Auth::getInstance()->hasIdentity()) {
				$logModel->addLogMessage('API Endpoint Call Result', "Access Denied", 1, 'INFO', 'apiendpoint', __CLASS__);
				$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);
				throw new Ot_Exception_Access('msg-error-apiAccessDenied');
			}

			$helperModel = new Wst_Model_Helpers_General();

			$isJson = $helperModel->isJSON($params['jsonData']);
			if(!$isJson) {
				$logModel->addLogMessage('API Endpoint Call Result', "Invalid input - invalid JSON format", 1, 'INFO', 'apiendpoint', __CLASS__);
				$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);
				throw new Ot_Exception_Input('You must provide a valid json string in the jsonData parameter');
			}

			$data = json_decode($params['jsonData']);
			if (!isset($data->username)) {
				$logModel->addLogMessage('API Endpoint Call Result', "Invalid input - JSON data is missing the 'username' element", 1, 'INFO', 'apiendpoint', __CLASS__);
				$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);
				throw new Ot_Exception_ApiMissingParams('You must provide an element with key "username" in the jsonData parameter.');
			}

			$username = $data->username;



			$responseArray = array(
				"username" => $username
			);

			$logModel->addLogMessage('API Endpoint Call Processing', "Input data validated successfully. Proceeding.", 1, 'INFO', 'apiendpoint', __CLASS__);

			// start by looking the user up in the source to double-check eligibility - use LDAP.

			$lookupHelper = new Wst_Model_Helpers_Lookup();
			$lookupResult = $lookupHelper->getUserEligibility($username, "ldap");

			$lookedUpUserData = $lookupResult["data"][$username];

			$logModel->addLogMessage('API Endpoint Call Processing', "Performed eligibility lookup for user $username", 1, 'INFO', 'apiendpoint', __CLASS__);
			$logModel->addLogMessage('API Endpoint Call Data', json_encode($lookedUpUserData), 1, 'INFO', 'apiendpoint', __CLASS__);

			if (!isset($lookedUpUserData["eligible"]) || !$lookedUpUserData["eligible"]) {
				// user is not eligible.

				$logModel->addLogMessage('API Endpoint Call Result', "Failure: user $username is not eligible for an account.", 1, 'INFO', 'apiendpoint', __CLASS__);

				$responseArray["created"] = false;
				$responseArray["message"] = "User is not eligible for WebEx account.";

				$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);
				return $responseArray;
			}

			// found and eligible!

			// now look the user up locally to make sure they don't exist in WebEx currently. if they do but are "inactive" we'll do an edit action instead of an add, so we'll check on that too.

			$userListModel = new Wst_Model_Dbtable_Userlist();
			$localUserInfo = $userListModel->getUserByWebExId($username);

			$transactionMode = "add";

			if ($localUserInfo) {
				// user found locally.
				if ($localUserInfo["status"] == "active") {
					// exists in WebEx already, do nothing.
					$responseArray["created"] = false;
					$responseArray["message"] = "User account already exists in WebEx.";

					$logModel->addLogMessage('API Endpoint Call Result', "Failure: user $username is already an active user in WebEx.", 1, 'INFO', 'apiendpoint', __CLASS__);

					$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);
					return $responseArray;
				} else if ($localUserInfo["status"] == "inactive") {
					// user exists already - change mode to "edit"
					$transactionMode = "edit";
				}
			}

			// ok, ready to go. based on mode, let's perform the transaction.

			// get the user info
			$thisLdapUser = $lookedUpUserData["rawData"];

			$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();

			if ($transactionMode == "add") {
				// add the user
				$logModel->addLogMessage('API Endpoint Call Processing', "User does not yet exist in WebEx - adding.", 1, 'INFO', 'apiendpoint', __CLASS__);

				$apiCallStatus = $webexApiHelper->addWebexUser($thisLdapUser['data']['uncgpreferredgivenname'], $thisLdapUser['data']['uncgpreferredsurname'], $username, $thisLdapUser['data']['uncgemail'], array(
					"dept_number" => $thisLdapUser["data"]["dept_number"],
					"org_short_code" => $thisLdapUser["data"]["org_short_code"]
				));

				if ($apiCallStatus["response"]["result"] == "SUCCESS") {
					$logModel->addLogMessage('API Endpoint Call Processing', "add-user API call successful.", 1, 'INFO', 'apiendpoint', __CLASS__);

					$addedUserId = $apiCallStatus["data"]["use:userId"];

					$dbAddResult = $userListModel->addNewUserToUserList($thisLdapUser['data']['uidnumber'], $username, $thisLdapUser['data']['uncgpreferredgivenname'], $thisLdapUser['data']['uncgpreferredsurname'], $thisLdapUser['data']['uncgemail'], $thisLdapUser['data']['departmentnumber'], $thisLdapUser['data']['uncgorgnshortname'], $addedUserId);

					$logModel->addLogMessage('API Endpoint Call Processing', "local database updated successfully.", 1, 'INFO', 'apiendpoint', __CLASS__);

					$responseArray["created"] = true;
					$responseArray["message"] = "New user account created successfully in WebEx.";

					$logModel->addLogMessage('API Endpoint Call Result', "Activation success for user $username.", 1, 'INFO', 'apiendpoint', __CLASS__);
				} else {
					$failureString = "code:" . $apiCallStatus["response"]["exceptionId"] . "; message:" . $apiCallStatus["response"]["message"];
					$logModel->addLogMessage('API Endpoint Call Result', "API call FAILED - $failureString", 1, 'INFO', 'apiendpoint', __CLASS__);
					$responseArray["created"] = false;
					$responseArray["message"] = "API error while creating WebEx account - $failureString";
				}
			} else if ($transactionMode == "edit") {
				// reactivate the user
				$logModel->addLogMessage('API Endpoint Call Processing', "User exists in WebEx but is currently inactive - reactivating.", 1, 'INFO', 'apiendpoint', __CLASS__);

				$apiCallStatus = $webexApiHelper->editWebexUser($username, array());

				if ($apiCallStatus["response"]["result"] == "SUCCESS") {
					$logModel->addLogMessage('API Endpoint Call Processing', "edit-user API call successful.", 2, 'WARNING', 'apiendpoint', __CLASS__);

					$dbEditResult = $userListModel->reactivateUserInUserList($thisLdapUser['data']['uidnumber']);

					$logModel->addLogMessage('API Endpoint Call Processing', "local database updated successfully.", 1, 'INFO', 'apiendpoint', __CLASS__);

					$responseArray["created"] = true;
					$responseArray["message"] = "Existing user account reactivated successfully in WebEx.";
					$logModel->addLogMessage('API Endpoint Call Result', "Activation success for user $username.", 1, 'INFO', 'apiendpoint', __CLASS__);
				} else {
					$failureString = "code:" . $apiCallStatus["response"]["exceptionId"] . "; message:" . $apiCallStatus["response"]["message"];
					$logModel->addLogMessage('API Endpoint Call Result', "API call FAILED - $failureString", 2, 'WARNING', 'apiendpoint', __CLASS__);
					$responseArray["created"] = false;
					$responseArray["message"] = "API error while reactivating WebEx account - $failureString";
				}

			}

			// provide sign-in link
			if ($responseArray["created"]) {
				$localConfig = Zend_Registry::get('localConfig');
				$responseArray["signInLink"] = $localConfig["webexLoginUrl"];
			}

			$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);

			return $responseArray;
		}
	}

}