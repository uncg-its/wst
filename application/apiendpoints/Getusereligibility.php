<?php

class Wst_Apiendpoint_Getusereligibility extends Ot_Api_EndpointTemplate
{

	/**
	 * Receives username and checks for eligibility in WebEx.
	 *
	 *
	 * Params
	 * ===========================
	 * Required Parameters:
	 *   - jsonData: one key-value pair: 'username':'webExId'
	 *
	 * ===========================
	 * Optional Parameters:
	 *   - source: one key-value pair: 'source':'local'
	 *    -- defaults to "file" to do file-based (feed) lookup; can also be 'ldap' for a live lookup
	 *
	 *
	 * EXAMPLE
	 * ===========================
	 *
	 * jsonData example:
	 *
	 * {"username":"jdoe1"}
	 * {"username":"jdoe1","source":"ldap"}
	 *
	 */
	public function post($params)
	{
		$logModel = new Wst_Model_Dbtable_Log();
		$logModel->addLogMessage('API Endpoint Call Start', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);


		if (!isset($params['jsonData'])) {
			$logModel->addLogMessage('API Endpoint Call Result', "Invalid input - missing jsonData parameter", 1, 'INFO', 'apiendpoint', __CLASS__);
			$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);
			throw new Ot_Exception_Input('You must provide the jsonData parameter');
		} else {

			if (!Zend_Auth::getInstance()->hasIdentity()) {
				$logModel->addLogMessage('API Endpoint Call Result', "Access Denied", 1, 'INFO', 'apiendpoint', __CLASS__);
				$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);
				throw new Ot_Exception_Access('msg-error-apiAccessDenied');
			}

			$helperModel = new Wst_Model_Helpers_General();

			$isJson = $helperModel->isJSON($params['jsonData']);
			if(!$isJson) {
				$logModel->addLogMessage('API Endpoint Call Result', "Invalid input - invalid JSON format", 1, 'INFO', 'apiendpoint', __CLASS__);
				$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);
				throw new Ot_Exception_Input('You must provide a valid json string in the jsonData parameter');
			}

			$data = json_decode($params['jsonData']);
			if (!isset($data->username)) {
				$logModel->addLogMessage('API Endpoint Call Result', "Invalid input - JSON data is missing the 'username' element", 1, 'INFO', 'apiendpoint', __CLASS__);
				$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);
				throw new Ot_Exception_ApiMissingParams('You must provide an element with key "username" in the jsonData parameter.');
			}

			$username = $data->username;

			if (isset($data->source)) {
				if ($data->source == "ldap") {
					$source = "ldap";
				} else if ($data->source == "file") {
					$source = "file";
				} else {
					$logModel->addLogMessage('API Endpoint Call Result', "Invalid input - invalid data provided for the 'source' element", 1, 'INFO', 'apiendpoint', __CLASS__);
					$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);
					throw new Ot_Exception_ApiMissingParams('Value for key "source" must be "file" or "ldap", if provided');
				}
			} else {
				$source = "file"; // default
			}

			$responseArray = array(
				"lookupData" => array(
					"username" => $username,
					"source" => $source
				)
			);

			$logModel->addLogMessage('API Endpoint Call Processing', "Input data validated successfully. Proceeding.", 1, 'INFO', 'apiendpoint', __CLASS__);

			// start by looking the user up in the source.

			$lookupHelper = new Wst_Model_Helpers_Lookup();
			$lookupResult = $lookupHelper->getUserEligibility($username, $source);

			$lookedUpUserData = $lookupResult["data"][$username];
			$logModel->addLogMessage('API Endpoint Call Processing', "Performed eligibility lookup for user $username", 1, 'INFO', 'apiendpoint', __CLASS__);
			$logModel->addLogMessage('API Endpoint Call Data', json_encode($lookedUpUserData), 1, 'INFO', 'apiendpoint', __CLASS__);

			$wasFoundInSource = false;
			if ($source == "file") {
				if (!is_null($lookedUpUserData["rawData"])) {
					$wasFoundInSource = true;
				}
			} else if ($source == "ldap") {
				if ($lookedUpUserData["ldapSuccess"]) {
					$wasFoundInSource = true;
				}
			}

			// now look the user up locally to see if they exist in WebEx currently
			$userListModel = new Wst_Model_Dbtable_Userlist();
			$localUserInfo = $userListModel->getUserByWebExId($username);

			$userExistsInWebEx = false;

			$logModel->addLogMessage('API Endpoint Call Processing', "Performed local database lookup for user $username", 1, 'INFO', 'apiendpoint', __CLASS__);
			$logModel->addLogMessage('API Endpoint Call Data', json_encode($localUserInfo), 1, 'INFO', 'apiendpoint', __CLASS__);

			if ($localUserInfo && $localUserInfo["status"] == "active") {
				$userExistsInWebEx = true;
			}


			// user was not found in source, so they are not eligible...though they could theoretically still exist in WebEx...
			if (!$wasFoundInSource) {

				$logModel->addLogMessage('API Endpoint Call Result', "Ineligible - user $username was not found in the source ($source).", 1, 'INFO', 'apiendpoint', __CLASS__);

				$responseArray["wasFoundInSource"] = false;
				$responseArray["isEligible"] = false;
				$responseArray["isActive"] = $userExistsInWebEx;


				$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);
				return $responseArray;
			}

			// so the user was found in source...now...

			$responseArray["wasFoundInSource"] = true;

			// errors...
			if (count($lookupResult["errors"]) > 0) {
				$logModel->addLogMessage('API Endpoint Call Result', "Errors encountered during user eligibility lookup.", 1, 'INFO', 'apiendpoint', __CLASS__);
				$logModel->addLogMessage('API Endpoint Call Data', json_encode($lookupResult), 1, 'INFO', 'apiendpoint', __CLASS__);
				$responseArray["errors"] = $lookupResult["errors"];

				$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);
				return $responseArray;
			}

			// let's add eligibility fields...
			$responseArray["eligibilityFields"] = $lookupResult["eligibilityFields"];

			// PRIMARY ACCOUNT INFO IS ONLY AVAILABLE FROM LIVE LDAP LOOKUP
			if ($source == "ldap") {
				// is it primary?
				$responseArray["isPrimaryAccount"] = $lookedUpUserData["isPrimary"];

				if (!$responseArray["isPrimaryAccount"]) {

					$logModel->addLogMessage('API Endpoint Call Result', "Ineligible - user $username is not a primary account.", 1, 'INFO', 'apiendpoint', __CLASS__);

					// not a primary account, so not eligible. let's get outta here.
					$responseArray["isEligible"] = false;
					$responseArray["isActive"] = $userExistsInWebEx;

					$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);
					return $responseArray;
				}
			}


			// proceed with the other eligibility items

			$responseArray["isEligible"] = $lookedUpUserData["eligible"];
			if ($responseArray["isEligible"]) {
				$qualifiesVia = strip_tags($lookedUpUserData["qualifiesVia"]);
				$eligibleString = "Eligible - user qualifies via $qualifiesVia";
				$responseArray["qualifiesVia"] = $qualifiesVia;
			} else {
				$eligibleString = "Ineligible - user does not qualify via LDAP flags or Canvas Teacher Feed.";
			}

			$responseArray["isActive"] = $userExistsInWebEx;

			$localConfig = Zend_Registry::get('localConfig');

			if ($userExistsInWebEx) {
				$responseArray["signInLink"] = $localConfig["webexLoginUrl"];
			} else {
				$responseArray["activation"] = array(
					"url" => $localConfig["appUrl"] . "api/activate-account",
					"type" => "POST",
					"params" => "key,username"
				);
			}


			$logModel->addLogMessage('API Endpoint Call Result', $eligibleString, 1, 'INFO', 'apiendpoint', __CLASS__);

			$logModel->addLogMessage('API Endpoint Call End', __CLASS__, 1, 'INFO', 'apiendpoint', __CLASS__);

			return $responseArray;

		}
	}

}