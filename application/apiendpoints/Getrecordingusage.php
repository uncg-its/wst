<?php

class Wst_Apiendpoint_Getrecordingusage extends Ot_Api_EndpointTemplate {

	/**
	 * Provides current recording usage data.
	 *
	 * Params
	 * ===========================
	 * Required Parameters:
	 *   - none
	 *
	 *
	 */
	public function post($params) {
		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$recResult = $recordingArchiveModel->getCurrentRecordingStorageUsage();
		$recordingStorageUsage = round($recResult[0]["recording_usage"], 2);

		return array(
			'recording_usage' => $recordingStorageUsage,
		);

	}

}