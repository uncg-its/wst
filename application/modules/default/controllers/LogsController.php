<?php

class LogsController extends Zend_Controller_Action {

	public function indexAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Browse Logs');

	}

	public function allEntriesAction() {
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		// js file for sortable table
		$this->view->headScript()->appendFile($this->view->baseUrl() . '/public/scripts/ui-sortable.js');

		// models / helpers / etc.
		$logModel = new Wst_Model_Dbtable_Log();
		$localConfig = Zend_Registry::get("localConfig");

		// form fields?
		if ($this->_getParam('resetButton') == 'Reset') {
			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'logs', 'action' => 'all-entries'), 'default', true);
		}

		$filter = array(
			'username'    => trim($this->_getParam('username')),
			'message'     => trim($this->getParam('message')),
			'priority'    => trim($this->getParam('priority')),
			'attributeId' => trim($this->getParam('attributeId')),
			'sort'        => $this->getParam('sort', 'logId'),
			'direction'   => $this->getParam('direction', 'desc')
		);

		$select = $logModel->getAdapter()->select();
		$select->from($logModel->info('name'));

		$select->order($filter['sort'] . ' ' . $filter['direction']);

		if ($filter['username'] != '') {
			$where = $logModel->getAdapter()->quoteInto("username = ?", $filter['username']);
			$select->where($where);
		}

		if ($filter['message'] != '') {
			$where = $logModel->getAdapter()->quoteInto("message LIKE ?", "%" . $filter['message'] . "%");
			$select->where($where);
		}

		if ($filter['priority'] != '') {
			$where = $logModel->getAdapter()->quoteInto("priority = ?", $filter['priority']);
			$select->where($where);
		}

		if ($filter['attributeId'] != '') {
			$where = $logModel->getAdapter()->quoteInto("attributeId LIKE ?", "%" . $filter['attributeId'] . "%");
			$select->where($where);
		}

		$adapter = new Zend_Paginator_Adapter_DbSelect($select);

		$paginator = new Zend_Paginator($adapter);
		$paginator->setDefaultItemCountPerPage($localConfig["itemsPerPage"]);
		$paginator->setCurrentPageNumber($this->getParam("page", 1));

		// form stuff
		$form = new Wst_Form_Searchlogs();
		$form->populate($filter);

		$this->view->assign(array(
			'form' => $form,
			'paginator' => $paginator,
			'sort' => $filter['sort'],
			'direction' => $filter['direction']
		));

		$this->_helper->pageTitle("Browse App Log");
	}

	public function apiStatusHistoryAction() {
		// models / helpers / etc.
		$logModel = new Wst_Model_Dbtable_Log();
		$localConfig = Zend_Registry::get("localConfig");

		$select = $logModel->getAdapter()->select();
		$select->from($logModel->info('name'), array('message','timestamp'));

		$where = $logModel->getAdapter()->quoteInto("subject = ?", "Api Status");
		$select->where($where);

		$select->order("timestamp DESC");

		$adapter = new Zend_Paginator_Adapter_DbSelect($select);

		$paginator = new Zend_Paginator($adapter);
		$paginator->setDefaultItemCountPerPage($localConfig["itemsPerPage"]);
		$paginator->setCurrentPageNumber($this->getParam("page", 1));

		$this->view->paginator = $paginator;

		$this->_helper->pageTitle("Browse App Log");
	}

	public function apiOutagesAction() {

		// models / helpers / etc.
		$logModel = new Wst_Model_Dbtable_Log();
		$localConfig = Zend_Registry::get("localConfig");

		$select = $logModel->getAdapter()->select();
		$select->from($logModel->info('name'), array('message','timestamp'));

		$where = $logModel->getAdapter()->quoteInto("subject = ?", "Api Status");
		$where .= $logModel->getAdapter()->quoteInto(" AND message = ?", "down");
		$select->where($where);

		$select->order("timestamp DESC");

		$adapter = new Zend_Paginator_Adapter_DbSelect($select);

		$paginator = new Zend_Paginator($adapter);
		$paginator->setDefaultItemCountPerPage($localConfig["itemsPerPage"]);
		$paginator->setCurrentPageNumber($this->getParam("page", 1));

		$this->view->paginator = $paginator;

		$this->_helper->pageTitle("API Outage History");
		
	}

	public function apiQueueAllEntriesAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		// js file for sortable table
		$this->view->headScript()->appendFile($this->view->baseUrl() . '/public/scripts/ui-sortable.js');

		// models / helpers / etc.
		$queueModel = new Wst_Model_Dbtable_Apiqueue();
		$localConfig = Zend_Registry::get("localConfig");

		// form fields?
		if ($this->_getParam('resetButton') == 'Reset') {
			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'logs', 'action' => 'api-queue-all-entries'), 'default', true);
		}

		$filter = array(
			'euid'      => trim($this->_getParam('euid')),
			'webExId'   => trim($this->getParam('webExId')),
			'status'    => trim($this->getParam('status')),
			'queue_action'    => trim($this->getParam('queue_action')),
			'message'   => trim($this->getParam('message')),
			'sort'      => $this->getParam('sort', 'q_id'),
			'direction' => $this->getParam('direction', 'desc')
		);

		$select = $queueModel->getAdapter()->select();
		$select->from($queueModel->info('name'));

		$select->order($filter['sort'] . ' ' . $filter['direction']);

		if ($filter['euid'] != '') {
			$where = $queueModel->getAdapter()->quoteInto("euid = ?", $filter['euid']);
			$select->where($where);
		}

		if ($filter['webExId'] != '') {
			$where = $queueModel->getAdapter()->quoteInto("webExId LIKE ?", "%" . $filter['webExId'] . "%");
			$select->where($where);
		}

		if ($filter['status'] != '') {
			$where = $queueModel->getAdapter()->quoteInto("status = ?", $filter['status']);
			$select->where($where);
		}

		if ($filter['queue_action'] != '') {
			$where = $queueModel->getAdapter()->quoteInto("action = ?", $filter['queue_action']);
			$select->where($where);
		}

		if ($filter['message'] != '') {
			$where = $queueModel->getAdapter()->quoteInto("message LIKE ?", "%" . $filter['message'] . "%");
			$select->where($where);
		}

		$adapter = new Zend_Paginator_Adapter_DbSelect($select);

		$paginator = new Zend_Paginator($adapter);
		$paginator->setDefaultItemCountPerPage($localConfig["itemsPerPage"]);
		$paginator->setCurrentPageNumber($this->getParam("page", 1));


		// form stuff
		$form = new Wst_Form_Searchapiqueue();
		$form->populate($filter);

		$this->view->assign(array(
			'form' => $form,
			'paginator' => $paginator,
			'sort' => $filter['sort'],
			'direction' => $filter['direction']
		));

		$this->_helper->pageTitle("Browse API Queue");
	}

	public function apiQueueFailedEntriesAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		// models / helpers / etc.
		$queueModel = new Wst_Model_Dbtable_Apiqueue();
		$localConfig = Zend_Registry::get("localConfig");

		$select = $queueModel->getAdapter()->select();
		$select->from($queueModel->info('name'));

		$select->where($queueModel->getAdapter()->quoteInto("status = ?", "error"));

		$select->order("q_id DESC");

		$adapter = new Zend_Paginator_Adapter_DbSelect($select);

		$paginator = new Zend_Paginator($adapter);
		$paginator->setDefaultItemCountPerPage($localConfig["itemsPerPage"]);
		$paginator->setCurrentPageNumber($this->getParam("page", 1));

		$this->view->paginator = $paginator;

		$this->_helper->pageTitle("Browse Failed API Queue Entries");
	}
	
	public function apiQueueEntryDetailsAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		// referer
		$request = $this->getRequest();
		$referer = $request->getHeader('referer');

		$get = Zend_Registry::get('getFilter');

		$queueModel = new Wst_Model_Dbtable_Apiqueue();

		$entryDetails = $queueModel->getCall($get->id);

		if (!empty($entryDetails)) {
			switch($entryDetails["status"]) {
				case "complete":
					$statusIcon = "<i class=\"fa fa-check text-success\"></i>";
					break;
				case "in-progress":
					$statusIcon = "<i class=\"fa fa-ellipsis-h\"></i>";
					break;
				case "pending":
					$statusIcon = "<i class=\"fa fa-clock-o\"></i>";
					break;
				case "error":
					$statusIcon = "<i class=\"fa fa-times text-error\"></i>";
					break;
				case "ignored":
					$statusIcon = "<i class=\"fa fa-trash\"></i>";
					break;
				default:
					$statusIcon = "";
					break;
			}

			$entryDetails["status_icon"] = $statusIcon;
		}

		$this->view->assign(array(
			'id' => $get->id,
			'entryDetails' => empty($entryDetails) ? false : $entryDetails,
			'referer' => $referer
		));
	}

	public function apiQueueSetEntryPendingAction($redirect = true, $id = null) {

		// models
		if (is_null($id)) {
			$get = Zend_Registry::get('getFilter');
			$id = $get->id;
		}

		$queueModel = new Wst_Model_Dbtable_Apiqueue();

		if (is_numeric($id)) {
			$result = $queueModel->setCallStatus($id, 'pending');
			if ($result == 1) {
				$this->_helper->messenger->addSuccess("Success: queue entry $id set to 'pending'.");
			} else {
				$this->_helper->messenger->addError("Error: queue entry $id not found.");
			}

            $result = $queueModel->setCallAttemptNumber($id, '0');
            if ($result == 1) {
                $this->_helper->messenger->addSuccess("Success: queue entry $id attempt count set to '0'.");
            } else {
                $this->_helper->messenger->addError("Error: queue entry $id not found.");
            }

		} else {
			$result = null;
			$this->_helper->messenger->addError("Error: invalid value for queue entry ID.");
		}

		if ($redirect) {
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNeverRender();

			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'logs', 'action' => 'api-queue-entry-details', 'id' => $id), 'default', true);
		}

		return $result;

	}

	public function apiQueueSetEntryIgnoredAction($redirect = true, $id = null) {

		// models
		if (is_null($id)) {
			$get = Zend_Registry::get('getFilter');
			$id = $get->id;
		}

		$queueModel = new Wst_Model_Dbtable_Apiqueue();

		if (is_numeric($id)) {
			$result = $queueModel->setCallStatus($id, 'ignored');
			if ($result == 1) {
				$this->_helper->messenger->addSuccess("Success: queue entry $id set to 'ignored'.");
			} else {
				$this->_helper->messenger->addError("Error: queue entry $id not found.");
			}
		} else {
			$result = null;
			$this->_helper->messenger->addError("Error: invalid value for queue entry ID.");
		}

		if ($redirect) {
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNeverRender();

			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'logs', 'action' => 'api-queue-entry-details', 'id' => $id), 'default', true);
		}

		return $result;
	}

	public function modifyMultipleQueueEntriesAction() {
		$get = Zend_Registry::get('getFilter');
		$ids = $get->id;
		//dump($get->entryAction, true);

		$successful = array();
		$failed = array();

		if ($get->entryAction == "Requeue") {
			$method = "apiQueueSetEntryPendingAction";
		} else if ($get->entryAction == "Ignore") {
			$method = "apiQueueSetEntryIgnoredAction";
		} else {
			throw new Exception("Error: invalid entryAction specified");
		}

		foreach($ids as $id) {
			$result = $this->$method(false, $id); // don't redirect, but return instead
			if ($result == 1) {
				$successful[] = $id;
			} else {
				$failed[] = $id;
			}
		}

		if (count($successful) > 0) {
			$this->_helper->messenger->addSuccess("Success: " . count($successful) . " " . pluralize($successful, "entry", "entries", true) . " " . strtolower($get->entryAction) . "d.");
		}

		if (count($failed) > 0) {
			$this->_helper->messenger->addError("Error: " . count($failed) . " " . pluralize($failed, "entry", "entries", true) . " could not be " . strtolower($get->entryAction) . "d.");
		}

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNeverRender();

		$this->_helper->redirector->gotoRoute(array('controller' => 'logs', 'action' => 'api-queue-failed-entries'), 'default', true);

	}
}