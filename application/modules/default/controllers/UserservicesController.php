<?php

class UserservicesController extends Zend_Controller_Action {

	public function indexAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('User Services');

		$loggedInUser = Zend_Auth::getInstance()->getIdentity();
		if(!empty($loggedInUser)) {
			//echo "AWFNAWIENAWIEGNAWE";
		}

	}

	public function myRecordingsAction() {

		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('My WebEx Recordings');

		$loggedInUser = Zend_Auth::getInstance()->getIdentity();
		if(!empty($loggedInUser)) {
			$this->view->loggedInUser = $loggedInUser;

			$username = $loggedInUser->username;
			//$username = "mslibera"; // for testing

			$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
			$recordingsForUser = $recordingArchiveModel->getAllRecordingsForWebExId($username);

			// prep work
			$configModel = new Wst_Model_Dbtable_Config();
			$downloadLinkReplacement = $configModel->getVar('downloadLinkReplacement');

			$expireWindow = $configModel->getVar('recordingDeletionWindow');
			$this->view->recordingDeletionWindow = $expireWindow;

			// massage the content for the view.
			foreach ($recordingsForUser as $index => $r) {

				// download link replacement
				if (!empty($downloadLinkReplacement)) {
					$originalDownloadLink = $r["file_url"];
					$linkBits = explode("?RCID=", $originalDownloadLink);
					$rcidParam = $linkBits[1];

					$recordingsForUser[$index]["file_url"] = $downloadLinkReplacement . "?RCID=" . $rcidParam;
				}

				// parse out the cloud links
				if (!is_null($r["cloud_storage_info"])) {
					$recordingsForUser[$index]["cloud_storage_info"] = json_decode($r["cloud_storage_info"], true);
				}

				// add expire date
				$recordingsForUser[$index]["expire_time_utc"] = strtotime("+$expireWindow days", $r["create_time_utc"]);

				// user-readable status
				switch($r["status"]) {
					case "deleted-by-user":
					case "deleted-by-admin":
					case "deleted-by-wst":
						$userReadableStatus = "<nobr><i class='fa fa-times'></i> Deleted</nobr>";
						break;
					case "in-trash":
						$userReadableStatus = "<nobr><i class='fa fa-trash'></i> In Trash</nobr>";
						break;
					case "active":
						$userReadableStatus = "<nobr><i class='fa fa-check'></i> Active</nobr>";
						break;
					default:
						$userReadableStatus = ucwords($r["status"]);
						break;
				}
				$recordingsForUser[$index]["status-user-readable"] = $userReadableStatus;
			}

			$this->view->recordings = $recordingsForUser;

			$adapter = new Zend_Paginator_Adapter_Array($recordingsForUser);

			$paginator = new Zend_Paginator($adapter);
			$paginator->setDefaultItemCountPerPage(10); // probably shouldn't do this, but want to keep the shared folders section visible...
			$paginator->setCurrentPageNumber($this->getParam("page", 1));

			$this->view->paginator = $paginator;

			// now for the shared cloud folder(s)...

			$shareModel = new Wst_Model_Dbtable_RecordingFolderCloudShare();
			$shareFoldersForWebexId = $shareModel->getSharesForWebexId($username);

			//dump($shareFoldersForWebexId, true);

			if (count($shareFoldersForWebexId) > 0) {
				// append a link. hard code possible services here for now.
				// use FOLDER_ID for the placeholder
				$links = array(
					"box" => "https://uncg.app.box.com/files/0/f/FOLDER_ID"
				);

				foreach ($shareFoldersForWebexId as $index => $s) {
					if (isset($links[$s["service_name"]])) {
						$shareFoldersForWebexId[$index]["link"] = str_replace("FOLDER_ID", $s["folder_id"], $links[$s["service_name"]]);
					} else {
						$shareFoldersForWebexId[$index]["link"] = "";
					}
				}
			}


			$this->view->shareFolders = $shareFoldersForWebexId;

		}
	}
}