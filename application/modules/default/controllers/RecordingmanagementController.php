<?php

class RecordingmanagementController extends Zend_Controller_Action {

	public function indexAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Recording Management');
	}

	public function lookupByIdAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$form = new Wst_Form_Lookuprecordingsbyid();

		if ($this->getRequest()->isPost()) {
			if ($form->isValid($this->getRequest()->getPost())) {
				$apiHelper = new Wst_Model_Helpers_Webexapihelper();

				$recIdsRaw = $form->getValue('recordingIds');

				$helper = new Wst_Model_Helpers_General();
				$recIdsArray = $helper->textareaToArray($recIdsRaw);

				$recInfo = array();
				$notFound = array();
				foreach($recIdsArray as $id) {
					$result = $apiHelper->getRecordingDetails($id);

					if($result["response"]["result"] == "SUCCESS") {

						$recInfo[] = array(
							"id"       => $id,
							"recDate"  => $result["data"]["ep:recording"]["ep:createTime"],
							"username" => $result["data"]["ep:recording"]["ep:hostWebExID"],
							"name"     => $result["data"]["ep:recording"]["ep:name"],
							"size"     => $result["data"]["ep:recording"]["ep:size"],
							"duration" => $result["data"]["ep:recording"]["ep:duration"]
						);
					} else {
						$notFound[] = $id;
					}
				}

				$this->view->recordingInformation = $recInfo;
				$this->view->notFound = $notFound;
			}
		}

		$this->view->form = $form;

		$this->_helper->pageTitle('Look Up Recordings By ID');
	}

	public function userRecordingsAction() {

		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('List User Recordings');

		$form = new Wst_Form_Lookupuserrecordings();
		$this->view->form = $form;
		$username = '';

		if ($this->getRequest()->isPost()) {
			if ($form->isValid($this->getRequest()->getPost())) {
				$username = $form->getValue('username');
			}
		}
		//$username = "mslibera"; // for testing

		$userListModel = new Wst_Model_Dbtable_Userlist();
		$user = $userListModel->getUserByWebExId($username);

		if(!empty($username) && $user) {

			$this->view->user = $user;

			$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
			$recordingsForUser = $recordingArchiveModel->getAllRecordingsForWebExId($username);

			// prep work
			$configModel = new Wst_Model_Dbtable_Config();
			$downloadLinkReplacement = $configModel->getVar('downloadLinkReplacement');

			$expireWindow = $configModel->getVar('recordingDeletionWindow');
			$this->view->recordingDeletionWindow = $expireWindow;

			// massage the content for the view.
			foreach ($recordingsForUser as $index => $r) {

				// download link replacement
				if (!empty($downloadLinkReplacement)) {
					$originalDownloadLink = $r["file_url"];
					$linkBits = explode("?RCID=", $originalDownloadLink);
					$rcidParam = $linkBits[1];

					$recordingsForUser[$index]["file_url"] = $downloadLinkReplacement . "?RCID=" . $rcidParam;
				}

				// parse out the cloud links
				if (!is_null($r["cloud_storage_info"])) {
					$recordingsForUser[$index]["cloud_storage_info"] = json_decode($r["cloud_storage_info"], true);
				}

				// add expire date
				$recordingsForUser[$index]["expire_time_utc"] = strtotime("+$expireWindow days", $r["create_time_utc"]);

				// user-readable status
				switch($r["status"]) {
					case "deleted-by-user":
					case "deleted-by-admin":
					case "deleted-by-wst":
						$userReadableStatus = "<nobr><i class='fa fa-times'></i> Deleted</nobr>";
						break;
					case "in-trash":
						$userReadableStatus = "<nobr><i class='fa fa-trash'></i> In Trash</nobr>";
						break;
					case "active":
						$userReadableStatus = "<nobr><i class='fa fa-check'></i> Active</nobr>";
						break;
					default:
						$userReadableStatus = ucwords($r["status"]);
						break;
				}
				$recordingsForUser[$index]["status-user-readable"] = $userReadableStatus;
			}

			$this->view->recordings = $recordingsForUser;

			$adapter = new Zend_Paginator_Adapter_Array($recordingsForUser);

			$paginator = new Zend_Paginator($adapter);
			$paginator->setDefaultItemCountPerPage(10); // probably shouldn't do this, but want to keep the shared folders section visible...
			$paginator->setCurrentPageNumber($this->getParam("page", 1));

			$this->view->paginator = $paginator;

			// now for the shared cloud folder(s)...

			$shareModel = new Wst_Model_Dbtable_RecordingFolderCloudShare();
			$shareFoldersForWebexId = $shareModel->getSharesForWebexId($username);

			//dump($shareFoldersForWebexId, true);

			if (count($shareFoldersForWebexId) > 0) {
				// append a link. hard code possible services here for now.
				// use FOLDER_ID for the placeholder
				$links = array(
					"box" => "https://uncg.app.box.com/files/0/f/FOLDER_ID"
				);

				foreach ($shareFoldersForWebexId as $index => $s) {
					if (isset($links[$s["service_name"]])) {
						$shareFoldersForWebexId[$index]["link"] = str_replace("FOLDER_ID", $s["folder_id"], $links[$s["service_name"]]);
					} else {
						$shareFoldersForWebexId[$index]["link"] = "";
					}
				}
			}


			$this->view->shareFolders = $shareFoldersForWebexId;

		}
	}

	public function manageFailedTransfersAction() {

		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$recordingsWithFailedTransfers = $recordingArchiveModel->getRecordingsWithFailedTransfers();

		$recordingsWithIgnoredTransfers = $recordingArchiveModel->getRecordingsWithIgnoredTransfers();

		$this->view->recordings = $recordingsWithFailedTransfers;
		$this->view->recordingsIgnored = $recordingsWithIgnoredTransfers;

		$this->_helper->pageTitle('Manage Failed Transfers');
	}

    public function viewPendingRecordingDeletionsAction() {

        $recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
        $this->view->recordings = $recordingArchiveModel->getDownloadedRecordingsToBeDeletedFromWebex();

        $this->_helper->pageTitle('Recordings to be Deleted Next');
    }

    public function auditRecordingStorageAction() {
	    // add page styles
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

	    $localConfig = Zend_Registry::get('localConfig');
	    $storageQuotaMegabytes = $localConfig["siteRecordingStorageLimit"];

	    // model
	    $recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();

	    // all recordings
	    $allRecordings = $recordingArchiveModel->getRecordingArchives();

	    // result array
	    $storageUsed = array();

	    foreach ($allRecordings as $index => $recording) {
		    if (!isset($storageUsed[$recording["status"]])) {
			    $storageUsed[$recording["status"]] = $recording["size_mb"];
		    } else {
			    $storageUsed[$recording["status"]] += $recording["size_mb"];
		    }
	    }

	    //dump($storageUsed, true);


	    // API time...
	    $webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
	    $apiRecordings = $webexApiHelper->getAllRecordings();
	    //dump($apiRecordings["data"]["ep:recording"], true);

		// API only gives back active recordings at this time...
	    $storageUsedApi = array(
	    	"active" => 0
	    );

	    foreach ($apiRecordings["data"]["ep:recording"] as $index => $recording) {
		    $storageUsedApi["active"] += $recording["ep:size"];
	    }

	    $this->view->assign(array(
	        "storageQuotaMegabytes" => $storageQuotaMegabytes,
		    "storageUsedArray" => $storageUsed,
		    "storageUsedApiArray" => $storageUsedApi
	    ));

	    $this->_helper->pageTitle('Recording Storage Audit');
    }

    public function diffRecordingsAction() {
	    // add page styles
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

	    $get = Zend_Registry::get('getFilter');
	    $filter = $get->filter;

	    // get last runtime for the recording cronjob
		$cronjobModel = new Wst_Model_Dbtable_Cronjobs();
	    $lastRunDt = $cronjobModel->getLastCronjobRuntime("Wst_Cronjob_ArchiveRecordingsDaily");

	    // local
	    $recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$localRecordings = $recordingArchiveModel->getRecordingArchives();
	    // reindex...
	    $localRecordingsById = array();
	    foreach ($localRecordings as $index => $recording) {
		    $localRecordingsById[$recording["recording_id"]] = $recording;
	    }


	    // API
	    $webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
	    $apiRecordingsResult = $webexApiHelper->getAllRecordings();
	    $apiRecordings = $apiRecordingsResult["data"]["ep:recording"]; // API only gives back active recordings at this time...
		// reindex...
	    $apiRecordingsById = array();
	    foreach ($apiRecordings as $index => $recording) {
		    $apiRecordingsById[$recording["ep:recordingID"]] = $recording;
	    }


	    // DIFF IT!
	    $diff = array();
	    foreach ($localRecordingsById as $id => $recording) {
		    $diff[$id]["localStatus"] = $recording["status"];
		    if (isset($apiRecordingsById[$id])) {
			    $diff[$id]["apiStatus"] = "exists";
		    } else {
			    $diff[$id]["apiStatus"] = "does not exist";
		    }
	    }

	    foreach ($apiRecordingsById as $id => $recording) {
		    if (!isset($diff[$id])) {
			    $diff[$id]["localStatus"] = "does not exist";
			    $diff[$id]["apiStatus"] = "exists";
			    if (strtotime($recording["ep:createTime"]) >= $lastRunDt) {
				    $diff[$id]["createdAfterLastCronRun"] = true;
				    $diff[$id]["createTime"] = $recording["ep:createTime"];
			    }
		    }
	    }

	    // what result do we have?
		$acceptableResults = array(
			"active" => "exists",
			"in-trash" => "does not exist",
			"deleted-by-user" => "does not exist"
		);

	    foreach ($diff as $id => $diffData) {
		    $diff[$id]["syncStatus"] = false;

		    if (isset($acceptableResults[$diffData["localStatus"]])) {
			    if ($diffData["apiStatus"] == $acceptableResults[$diffData["localStatus"]]) {
				    $diff[$id]["syncStatus"] = true;
			    }
		    }
	    }

	    $showing = "ERROR";

	    // filtering.
	    if ($filter == "") { // default
	    	foreach ($diff as $id => $diffData) {
			    if ($diffData["syncStatus"] || (!$diffData["syncStatus"] && isset($diffData["createdAfterLastCronRun"]))) {
				    // filter out good ones and ones with possible timing issues
				    unset($diff[$id]);
			    }
		    }
		    $showing = "Out-of-sync Results (not including timing issues)";
	    } else if ($filter == "all-issues") {
		    foreach ($diff as $id => $diffData) {
			    if ($diffData["syncStatus"]) {
				    // filter out only good results
				    unset($diff[$id]);
			    }
		    }
		    $showing = "All Out-of-sync Results";
	    } else if ($filter == "all") {
	    	// do nothing - all results.
		    $showing = "All Results";
	    }


	    $this->view->assign(array(
	    	"diff" => $diff,
		    "lastRunDt" => $lastRunDt,
		    "showing" => $showing,
		    "filter" => $filter
	    ));


	    $this->_helper->pageTitle('Diff Recordings');

	    //dump($localRecordingsById, true);
    }

	public function viewPendingServerPurgeAction() {

		// get recordings map by id
		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$recordingsById = $recordingArchiveModel->getRecordingArchiveMap();

		// get files list from arf-downloads/ folder
		$path = "../arf-downloads/";
		$downloadFolderContents = scandir($path);

		// how long do we keep files?
		$configModel = new Wst_Model_Dbtable_Config();
		$daysToKeepRecordingFilesLocally = $configModel->getVar("localRecordingCacheCleaningWindow");

		$this->view->cleaningWindow = $daysToKeepRecordingFilesLocally;

		if (!$daysToKeepRecordingFilesLocally) {
			throw new Ot_Exception("Error: local recording cache window not set in WST Config.");
		}

		$secondsToKeepRecordingFilesLocally = $daysToKeepRecordingFilesLocally * 24 * 60 * 60; // days * hours * minutes * seconds

		// for each file, look it up by id (remove .arf in search string)
		// if the file is older than X days (set in app settings, look up file info via PHP) AND uploaded to cloud storage successfully, then store it in "to-delete" array
		$arfFilesFound = 0;

		$filenamesToDelete = array();
		$filenamesToKeep = array();

		if (count($downloadFolderContents) > 0) {
			foreach ($downloadFolderContents as $filename) {
				// init
				$delete = false;

				$isArf = substr($filename, -4) === ".arf";

				if ($isArf) {
					$arfFilesFound++;
					$recId = str_replace(".arf", "", $filename);
					// uploaded?
					if (isset($recordingsById[$recId]) && $recordingsById[$recId]["upload_status"] == "uploaded") {
						// outside the "keep" threshold?
						$fileCreationTimeInSecondsAgo = time() - filemtime($path . $filename);
						if ($fileCreationTimeInSecondsAgo >= $secondsToKeepRecordingFilesLocally) {
							$filenamesToDelete[] = $recordingsById[$recId];
						} else {
							$filenamesToKeep[] = $recordingsById[$recId];
						}
					}
				}

			}

		}

		$this->view->filesToDelete = $filenamesToDelete;
		$this->view->filesToKeep = $filenamesToKeep;

		$this->_helper->pageTitle('Next Server Purge - pending deletions');
	}

	public function updateFailedTransferAction() {
		$get = Zend_Registry::get('getFilter');
		$id = $get->id;
		$type = $get->type;
		$action = $get->updateAction;

		$logModel = new Wst_Model_Dbtable_Log();

		if (is_null($id)) {
			throw new Ot_Exception("Error: ID not provided.");
		}

		if (is_null($type)) {
			throw new Ot_Exception("Error: Transaction type not provided.");
		}

		if ($type != "upload" && $type != "download") {
			throw new Ot_Exception("Error: Transaction type invalid.");
		}

		if (is_null($action)) {
			throw new Ot_Exception("Error: Transaction type not provided.");
		}

		// action => new status
		$allowedActions = array(
			"requeue" => "pending",
			"unrequeue" => "failed",
			"ignore" => "ignored",
			"unignore" => "failed"
		);

		if (!isset($allowedActions[$action])) {
			throw new Ot_Exception("Error: Action method invalid.");
		}

		$logModel->addLogMessage("Action Processing", "Admin user modifying failed transfer -- id: $id, type: $type, action: $action", 1, 'INFO', 'method', __CLASS__);

		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$recording = $recordingArchiveModel->getRecordingById($id);

		if (is_null($recording)) {
			$this->_helper->messenger->addError("Error: recording $id not found in database. No changes made.");
			$logModel->addLogMessage("Action Result", "Modification unsuccessful - recording $id not found in database.", 1, 'INFO', 'method', __CLASS__);
		} else {
			$statusField = $type . "_status";

			$updateData = array(
				$statusField => $allowedActions[$action]
			);

			$where = $recordingArchiveModel->getAdapter()->quoteInto("recording_id = ?", $id);

			$updateResult = $recordingArchiveModel->update($updateData, $where);
			if ($updateResult > 0) {
				// success
				$message = "Success - $type action {$action}d for recording $id.";
				if ($action == "requeue") {
					$message .= " [<a href='" . $this->view->url(array("controller" => "recordingmanagement", "action" => "update-failed-transfer", "id" => $id, "type" => $type, "updateAction" => "unrequeue"), "default", true) . "'>Undo</a>]";
				}
				$this->_helper->messenger->addSuccess($message);
				$logModel->addLogMessage("Action Result", "Modification successful.", 1, 'INFO', 'method', __CLASS__);
			} else {
				// failed
				$this->_helper->messenger->addError("Error: database error during $action $type action for recording $id.");
				$logModel->addLogMessage("Action Result", "Modification unsuccessful - database error.", 1, 'INFO', 'method', __CLASS__);
			}
		}

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNeverRender();

		$this->_helper->redirector->gotoRoute(array("controller" => "recordingmanagement", "action" => "manage-failed-transfers"), "default", true);
	}


	// BOX API STUFF
	public function getTokenAction() {

        // add page styles
        $this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
        $this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$boxCreds = Zend_Registry::get('boxApi');
		$configModel = new Wst_Model_Dbtable_Config();
        $helperModel = new Wst_Model_Helpers_General();

		$token_info = array(
            'access_token' => $helperModel->obfuscate($configModel->getVar('access_token'), 4),
            'refresh_token' => $helperModel->obfuscate($configModel->getVar('refresh_token'), 4)
        );

		$this->view->boxCreds = $boxCreds;
		$this->view->token_info = $token_info;

		$this->_helper->pageTitle('Get Box Access Token');

	}

	public function exchangeBoxCodeForTokenAction() {
		$get = Zend_Registry::get('getFilter');
		$boxCreds = Zend_Registry::get('boxApi');

		if ($get->state != $boxCreds['csrfPreventionString']) {
			die('not the correct security string');
		}

		$boxHelper = new Wst_Model_Helpers_BoxApiHelper();
		$result = $boxHelper->getBoxTokenFromCode($get->code);

		$configModel = new Wst_Model_Dbtable_Config();

		$configModel->setVar("access_token", $result->access_token);
		$configModel->setVar("refresh_token", $result->refresh_token);

		$this->_helper->messenger->addSuccess('Token information updated successfully.');
		$this->_helper->redirector->gotoRoute(array('controller' => 'recordingmanagement', 'action' => 'index'), 'default', true);

	}
}