<?php
/**
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 *
 * This license is also available via the world-wide-web at
 * http://itdapps.ncsu.edu/bsd.txt
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to itappdev@ncsu.edu so we can send you a copy immediately.
 *
 * @package    Index_Controller
 * @category   Controller
 * @copyright  Copyright (c) 2007 NC State University Office of
 *             Information Technology
 * @license    http://itdapps.ncsu.edu/bsd.txt  BSD License
 * @version    SVN: $Id: $
 */

/**
 * Main controller for the application
 *
 * @package    Index_Controller
 * @category   Controller
 * @copyright  Copyright (c) 2007 NC State University Office of
 *             Information Technology
 */
class TestController extends Zend_Controller_Action {

	/**
	 * shows the homepage
	 *
	 */
	public function indexAction() {
		$stuff = "<p>Nothing to test!</p>";
		$scriptStartTime = time();

		/* ******************** */
		/* BEGIN TESTING SCRIPT */
		/* ******************** */
        

		/* ******************** */
		/*  END TESTING SCRIPT  */
		/* ******************** */

		$scriptEndTime = time();
		$this->view->assign(array(
			"scriptStartTime" => $scriptStartTime,
			"scriptEndTime"   => $scriptEndTime,
			"scriptExecTime"  => $scriptEndTime - $scriptStartTime,
			"stuff"           => $stuff
		));

		$this->view->stuff = $stuff;
	}


	public function backfillUsersAction() {

		//die("disabled for safety"); // COMMENT THIS LINE OUT WHEN YOU NEED THIS

		$localConfig = Zend_Registry::get('localConfig');
		$filePath = $localConfig["userDiffFilePath"];

		$userDataArray = null;

		if (file_exists($filePath)) {
			// get file contents
			$userDataJson = file_get_contents($filePath);

			// check whether valid or missing
			$userDataArray = @json_decode($userDataJson, true);

			// reindex
			$userDataArrayByUsername = array();
			foreach ($userDataArray as $element) {
				$userDataArrayByUsername[$element['uid']] = $element;
			}

			//$stuff = count($userDataArrayByUsername);
		}

		$apiHelper = new Wst_Model_Helpers_Webexapihelper();
		$result = $apiHelper->getActiveWebexUsers();
		$webexUsers = $result['data'];

		$stuff = $webexUsers;

		$usersToInsert = array();
		foreach ($webexUsers as $webexUser) {
			$id = strtoupper($webexUser["use:webExId"]);
			//echo "checking for $id<br>";

			// gotta call the WebEx API to get the user id number...sucks.
			$result = $apiHelper->getWebexUserDetails($id);
			$webExUserId = $result["data"]["use:userId"];

			if (isset($userDataArrayByUsername[$id])) {
				$euid = $userDataArrayByUsername[$id]["uidnumber"];
				//echo "found euid $euid<br>";

				//$usersToInsert[$euid] = $webexUser;
				extract($userDataArrayByUsername[$id]);

				$usersToInsert[$euid] = array(
					"webExId"        => strtolower($uid),
					"firstName"      => $uncgpreferredgivenname,
					"lastName"       => $uncgpreferredsurname,
					"email"          => strtolower($uid) . "@uncg.edu",
					"webExUserId"    => $webExUserId,
					"dept_number"    => $departmentnumber,
					"org_short_code" => $uncgorgnshortname
				);
			}
		}

		$userListHelper = new Wst_Model_Dbtable_Userlist();
		$usersInUserList = $userListHelper->getUserEuidMap();

		foreach ($usersToInsert as $euid => $info) {
			if (isset($usersInUserList[$euid])) {
				unset($usersToInsert[$euid]);
			}
		}

		//dump($usersToInsert, true);

		foreach ($usersToInsert as $euid => $info) {
			if (!isset($usersInUserList[$euid])) { // should always be true
				$dbResult = $userListHelper->addNewUserToUserList($euid, $info["webExId"], $info["firstName"], $info["lastName"], $info["email"], $info["dept_number"], $info["org_short_code"], $info["webExUserId"]);
			}
		}


		$stuff = $usersToInsert;
		//$stuff = $userDataArrayByUsername;

		$this->view->stuff = $stuff;
	}

	public function backfillRecordingsAction() {
		die('disabled for safety'); // comment when you need this

		$archiveRecordingsHelper = new Wst_Model_Helpers_ArchiveRecordingsHelper();

		$result = $archiveRecordingsHelper->archivePastRecordings("test_backfillRecordingsAction");

		$this->view->stuff = $result;

	}

	public function convertToTimestampsAction() {
		die('disabled for safety'); // comment when you need this

		// CONVERT SESSION TIMES TO TIMESTAMP
		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();
		$archives = $sessionArchiveModel->getAllArchives();

		$sessionsConverted = array();

		foreach ($archives as $a) {
			$startTimeString = $a["start_time_utc"];
			$endTimeString = $a["end_time_utc"];

			$startTime = new DateTime($startTimeString, new DateTimeZone("GMT"));
			$startTimestamp = $startTime->getTimestamp();

			$endTime = new DateTime($endTimeString, new DateTimeZone("GMT"));
			$endTimestamp = $endTime->getTimestamp();

			//$stuff = strtotime("04/21/2016 15:40:28");

			$where = $sessionArchiveModel->getAdapter()->quoteInto("conf_id = ?", $a["conf_id"]);
			$updateResult = $sessionArchiveModel->update(array(
				"start_time_utc" => $startTimestamp,
				"end_time_utc"   => $endTimestamp
			), $where);

			$sessionsConverted[] = $updateResult;
		}

		// CONVERT ATTENDEE TIMES TO TIMESTAMP
		$atttendeeArchiveModel = new Wst_Model_Dbtable_Attendeearchive();
		$archives = $atttendeeArchiveModel->getAllArchives();

		$attendeesConverted = array();

		foreach ($archives as $a) {
			$joinTimeString = $a["join_time_utc"];
			$leaveTimeString = $a["leave_time_utc"];

			$joinTime = new DateTime($joinTimeString, new DateTimeZone("GMT"));
			$joinTimestamp = $joinTime->getTimestamp();

			$leaveTime = new DateTime($leaveTimeString, new DateTimeZone("GMT"));
			$leaveTimestamp = $leaveTime->getTimestamp();

			//$stuff = strtotime("04/21/2016 15:40:28");

			$where = $atttendeeArchiveModel->getAdapter()->quoteInto("attendee_id = ?", $a["attendee_id"]);
			$updateResult = $atttendeeArchiveModel->update(array(
				"join_time_utc"  => $joinTimestamp,
				"leave_time_utc" => $leaveTimestamp
			), $where);

			$attendeesConverted[] = $updateResult;
		}

		// CONVERT RECORDING TIMES TO TIMESTAMP
		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$archives = $recordingArchiveModel->getRecordingArchives();

		$recordingsConverted = array();

		foreach ($archives as $a) {

			$timestamp = strtotime($a["create_time_local"]);

			$where = $recordingArchiveModel->getAdapter()->quoteInto("recording_id = ?", $a["recording_id"]);
			$updateResult = $recordingArchiveModel->update(array(
				"create_time_local" => $timestamp
			), $where);

			$recordingsConverted[] = $updateResult;
		}

		//$stuff = count($archives);

		$this->view->assign(array(
			"sessionsConverted"   => $sessionsConverted,
			"attendeesConverted"  => $attendeesConverted,
			"recordingsConverted" => $recordingsConverted
		));
	}

	public function backfillMetricsAction() {
		die("disabled for safety."); // comment this line when you need this.

		$daysToFetch = 90; // change this as you need it.

		$metricsHelper = new Wst_Model_Helpers_MetricsHelper();
		$metrics = array();

		for ($x = $daysToFetch; $x >= 2; $x--) {
			$start = strtotime("-$x days 00:00:00");
			$end = strtotime("-$x days 23:59:59");

			$metrics[] = $metricsHelper->getCommonMetricsBetweenTimestamps($start, $end);
		}

		// send metrics to dashboard
		$metricsModel = new Wst_Model_Dbtable_Metrics();

		$insertedIds = array();
		$sendErrors = array();

		$metricsToResetToZero = array("numberOfActiveUsers", "recordingStorageUsed");

		foreach ($metrics as $key => $m) {

			foreach ($metricsToResetToZero as $k) {
				$m[$k] = 0;
			}

			// add timestamp
			$m["timestamp"] = time();

			$result = $metricsModel->insertMetrics($m);
			$insertedIds[] = $result;

			//$sendResult = $metricsHelper->sendMetrics($m);
			//if (!$sendResult["success"]) {
			//    $sendErrors[] = $m["date"];
			//}
		}

		$this->view->assign(array(
			"numberOfDays" => $daysToFetch,
			"insertedIds"  => $insertedIds,
			"sendErrors"   => $sendErrors
		));
	}

	public function lookupRecordingsAction() {
		//die("disabled for safety."); // comment this line when you need this.

		$idsToLookUp = array(
			"74603237",
			"71608522",
			"72545837"
		);

		$apiHelper = new Wst_Model_Helpers_Webexapihelper();

		$recInfo = array();
		foreach ($idsToLookUp as $id) {
			$result = $apiHelper->getRecordingDetails($id);

			$recInfo[] = array(
				"id"       => $id,
				"recDate"  => $result["data"]["ep:recording"]["ep:createTime"],
				"username" => $result["data"]["ep:recording"]["ep:hostWebExID"],
				"name"     => $result["data"]["ep:recording"]["ep:name"],
				"size"     => $result["data"]["ep:recording"]["ep:size"],
				"duration" => $result["data"]["ep:recording"]["ep:duration"]
			);
		}

		$stuff = $recInfo;

		$this->view->stuff = $stuff;
	}

	public function lookupUserEligibilityAction() {
		$get = Zend_Registry::get('getFilter');
		$localConfig = Zend_Registry::get('localConfig');

		// init vars
		$eligibilityStatus = false;
		$eligibilitySource = false;
		$pseudoLog = array(); // will contain text strings

		$username = $get->username;

		$eligibilityFields = explode(",", $localConfig["eligibilityList"]);
		$pseudoLog[] = "eligibility fields: " . $localConfig["eligibilityList"];


		//lookup user in LDAP
		$lookupModel = new Wst_Model_Helpers_Lookup();
		$thisLdapUser = $lookupModel->getUsernameFromLdap($username, $eligibilityFields);
		$pseudoLog[] = "thisLdapUser set based on username $username";

		if (!$thisLdapUser['status'] || !$thisLdapUser['bindStatus']) {
			$pseudoLog[] = "error binding to LDAP and getting data for $username";
		} else {
			$accountType = $thisLdapUser['data']['uncgaccounttype'];

			if ($accountType == 'PRIMARY') {
				$pseudoLog[] = "account type is PRIMARY, continuing";
				foreach ($eligibilityFields as $field) {
					$thisField = trim($field);
					if ($thisLdapUser['data'][$thisField] == "TRUE") {
						$eligibilityStatus = true;
						$eligibilitySource = "LDAP flag $thisField";
						$pseudoLog[] = "user $username qualifies based on flag $thisField";
						break;
					}
				}

				if (!$eligibilityStatus) {
					$pseudoLog[] = "user did not qualify via LDAP. checking teacher feed.";

					// 2nd-tier check - CST feed
					$canvasTeacherFeedHelper = new Wst_Model_Helpers_CanvasTeacherFeedHelper();
					$feedCallResult = $canvasTeacherFeedHelper->isUserCurrentTeacherInCanvas($username);

					if (!$feedCallResult["success"]) {
						$pseudoLog[] = "Teacher Feed API call failed.";
					} else {
						if ($feedCallResult["isTeacher"]) {
							$eligibilityStatus = true;
							$eligibilitySource = "Canvas Teacher Feed";
							$pseudoLog[] = "user $username qualifies based on teacher feed.";
						} else {
							$pseudoLog[] = "user $username did not qualify via teacher feed.";
						}
					}
				}
			} else {
				$pseudoLog[] = "account type is not PRIMARY, aborting.";
			}
		}


		$this->view->assign(array(
			"username"          => $username,
			"eligibilityStatus" => $eligibilityStatus,
			"eligibilitySource" => $eligibilitySource,
			"pseudoLog"         => $pseudoLog
		));


	}

	public function backfillTrackingCodesAction() {
		die("disabled for safety"); // COMMENT THIS LINE OUT WHEN YOU NEED THIS

		$localConfig = Zend_Registry::get('localConfig');
		$filePath = $localConfig["userDiffFilePath"];

		$userDataArray = null;

		if (file_exists($filePath)) {
			// get file contents
			$userDataJson = file_get_contents($filePath);

			// check whether valid or missing
			$userDataArray = @json_decode($userDataJson, true);

			// reindex
			$userDataArrayByEuid = array();
			foreach ($userDataArray as $element) {
				$userDataArrayByEuid[$element['uidnumber']] = $element;
			}

			//$stuff = count($userDataArrayByUsername);
		}

		$userListModel = new Wst_Model_Dbtable_Userlist();
		$usersInUserList = $userListModel->getUserEuidMap();

		$usersToModify = array();
		foreach ($usersInUserList as $euid => $user) {
			if (isset($userDataArrayByEuid[$euid])) {
				$usersToModify[$euid] = array(
					"dept_number"    => $userDataArrayByEuid[$euid]["departmentnumber"],
					"org_short_code" => $userDataArrayByEuid[$euid]["uncgorgnshortname"]
				);
			}

		}

		//dump($usersToModify, true);


		$failures = 0;
		foreach ($usersToModify as $euid => $info) {
			$where = $userListModel->getAdapter()->quoteInto("euid = ?", $euid);
			$addCodesResult = $userListModel->update($info, $where);

			if (!is_numeric($addCodesResult)) {
				$failures++;
			} else {
				// queue an API call to update the new tracking codes!
				$webExId = $usersInUserList[$euid]["webExId"];

				$queueModel = new Wst_Model_Dbtable_Apiqueue();
				$queueEditActionResult = $queueModel->queueEditAction($euid, $webExId, $info, "TestController::backfillTrackingCodesAction");
			}

			//die("stopping here.");
		}


		$stuff = array(
			"failures"      => $failures,
			"usersToModify" => $usersToModify
		);
		//$stuff = $userDataArrayByUsername;

		$this->view->stuff = $stuff;


	}

	public function backfillSessionTrackingCodesAction() {
		die("disabled for safety"); // COMMENT THIS LINE OUT WHEN YOU NEED THIS

		// get users by webexid
		$userListModel = new Wst_Model_Dbtable_Userlist();
		$usersByWebexId = $userListModel->getUserWebexIdMap();

		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();
		$where = $sessionArchiveModel->getAdapter()->quoteInto("tracking_codes IS NULL", "");

		$sessionsWithoutTrackingCodes = $sessionArchiveModel->fetchAll($where)->toArray();

		//dump($sessionsWithoutTrackingCodes, true);

		$failures = array();
		foreach ($sessionsWithoutTrackingCodes as $key => $session) {
			$hostWebExId = $session["host_username"];

			if (isset($usersByWebexId[$hostWebExId])) {
				$departmentNumber = $usersByWebexId[$hostWebExId]["dept_number"];
				$orgShortCode = $usersByWebexId[$hostWebExId]["org_short_code"];


				// hard code for now...
				$trackingCodes = array(
					"1" => $departmentNumber,
					"2" => $orgShortCode
				);

				// custom codes?
				$customTrackingCodes = json_decode($usersByWebexId[$hostWebExId]["custom_tracking"], true);
				if (!is_null($customTrackingCodes)) {
					$trackingCodes = $trackingCodes + $customTrackingCodes; // + operator to preserve keys
				}
				//dump($trackingCodes, true);

				$sessionId = $session["conf_id"];
				$updateData = array(
					"tracking_codes" => json_encode($trackingCodes)
				);

				$where = $sessionArchiveModel->getAdapter()->quoteInto("conf_id = ?", $sessionId);
				$updateCodesResult = $sessionArchiveModel->update($updateData, $where);
				if (!is_numeric($updateCodesResult) || $updateCodesResult == 0) {
					$failures[] = $sessionId;
				}
			}
		}

		$stuff = array(
			"failures"                     => $failures,
			"sessionsWithoutTrackingCodes" => $sessionsWithoutTrackingCodes
		);

		$this->_helper->viewRenderer('index');
		$this->view->stuff = $stuff;
	}


	public function purgeEditActionsForInactiveUsersAction() {
		die("disabled for safety"); // COMMENT THIS LINE OUT WHEN YOU NEED THIS

		$queueModel = new Wst_Model_Dbtable_Apiqueue();
		$pendingEditActions = $queueModel->getCallsWithStatus("pending", "edit");

		// get user map
		$userListModel = new Wst_Model_Dbtable_Userlist();
		$usersByWebExId = $userListModel->getUserWebexIdMap();

		$queueEntriesToDelete = array();
		foreach ($pendingEditActions as $index => $action) {
			$user = $action["webExId"];
			if (isset($usersByWebExId[$user])) { // this should always happen
				if ($usersByWebExId[$user]["status"] == "inactive") {
					$queueEntriesToDelete[] = $action["q_id"];
				}
			}
		}

		// got them!
		// now...do it.

		$failedDeletions = array();
		$successfulDeletions = array();

		foreach ($queueEntriesToDelete as $index => $id) {
			$where = $queueModel->getAdapter()->quoteInto("q_id = ?", $id);
			$deleteResult = $queueModel->delete($where);

			if ($deleteResult != 0) {
				$successfulDeletions[] = $id;
			} else {
				$failedDeletions[] = $id;
			}
		}

		$this->_helper->viewRenderer('index');
		$this->view->stuff = array(
			"deletionsNeeded"     => count($queueEntriesToDelete),
			"failedDeletions"     => $failedDeletions,
			"successfulDeletions" => $successfulDeletions
		);

		//return $pendingEditActions;
	}

	public function backfillSharedFoldersAction() {
		//die("disabled for safety"); // COMMENT THIS OUT WHEN YOU NEED THIS

		$boxApiHelper = new Wst_Model_Helpers_BoxApiHelper();

		// init
		$newShares = array();
		$successfulShares = array();

		// which folder name do we want to traverse?
		$folderName = "WebEx Recording Uploads (uncg) [auto]";

		$foldersResponse = $boxApiHelper->getFolderContents(0);

		if ($foldersResponse->type == "error") {
			$errorInfo = $foldersResponse->context_info->errors;
			$messages = array();

			foreach ($errorInfo as $e) {
				$messages[] = $e->message;
			}

			$messageString = implode(",", $messages);
			throw new Exception("Error retrieving folder for Box account. Error message(s): $messageString");
		} else {
			$folders = $foldersResponse->item_collection->entries;

			$folderId = false;

			foreach ($folders as $f) {
				if ($f->name == $folderName) {
					$folderId = $f->id;
				}
			}

			if (!$folderId) {
				throw new Exception("Folder with name '$folderName' not found in account.");
			} else {
				$destinationFolderResponse = $boxApiHelper->getFolderContents($folderId);

				if ($destinationFolderResponse->type == "error") {
					$errorInfo = $destinationFolderResponse->context_info->errors;
					$messages = array();

					foreach ($errorInfo as $e) {
						$messages[] = $e->message;
					}

					$messageString = implode(",", $messages);
					throw new Exception("Error retrieving folder ID $folderId. Error message(s): $messageString");
				} else {
					// these are the year folders. we will need to parse each of these out.
					$yearFolders = $destinationFolderResponse->item_collection->entries;

					if (count($yearFolders) > 0) {

						foreach ($yearFolders as $yf) {
							// get its id
							$yearFolderId = $yf->id;
							// parse its child folders
							$yearFolderContentsResponse = $boxApiHelper->getFolderContents($yearFolderId);
							foreach ($yearFolderContentsResponse->item_collection->entries as $folder) {
								// get folder name
								$targetFolderName = $folder->name;
								// get folder id
								$targetFolderId = $folder->id;
								// parse username from folder name
								$targetFolderNamePieces = explode(" - ", $targetFolderName);
								$username = $targetFolderNamePieces[1];

								// does the folder have a share already?
								$addShare = true;
								$existingSharesResponse = $boxApiHelper->getFolderCollaborations($targetFolderId);
								if ($existingSharesResponse->total_count > 0) {
									// a share exists. with this user? we have to parse email for this, sadly.
									$existingShares = $existingSharesResponse->entries;
									foreach ($existingShares as $s) {
										$email = $s->accessible_by->login;
										$emailBits = explode("@", $email);
										$sharedUsername = strtolower($emailBits[0]);

										if ($sharedUsername == $username) {
											// a share exists for this user; don't add another.
											$addShare = false;
										}
									}
								}
								// add share info to array
								if ($addShare) {
									// look up user to get name
									$userListModel = new Wst_Model_Dbtable_Userlist();
									$user = $userListModel->getUserByWebExId($username);
									$fullName = $user["firstName"] . " " . $user["lastName"];


									$newShares[] = array(
										"fullName" => $fullName,
										"folderId" => $targetFolderId,
										"username" => $username
									);
								}

							}
						}
					} else {
						throw new Exception("Folder ID $folderId contains no subfolders.");
					}

				}
			}
		}

		if (count($newShares) > 0) {
			foreach ($newShares as $share) {
				$result = $boxApiHelper->addFolderCollaboration($share["folderId"], $share["username"]);
				if ($result->type != "error") {
					// trigger
					// get shared folder name from the API call results
					$folderName = $result->item->name;
					$folderUrl = "https://uncg.app.box.com/files/0/f/" . $share["folderId"];

					// app info
					$localConfig = Zend_Registry::get('localConfig');
					$appUrl = $localConfig["appUrl"];

					$configModel = new Wst_Model_Dbtable_Config();
					$myRecordingsLink = $configModel->getVar('myRecordingsLinkReplacement');
					if (!$myRecordingsLink) {
						$myRecordingsLink = "{$appUrl}userservices/my-recordings";
					}

					// HTML EMAIL

					$emailHelper = new Wst_Model_Helpers_HtmlEmail();
					$htmlEmailBody = $emailHelper->head("UNCG WebEx Recordings - New Cloud Storage Share available");

					$htmlBody = <<<HTML
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hello {$share["fullName"]},</p>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Your UNCG WebEx recordings (ARF format) are now accessible via the Box @ UNCG online file storage service. Any recordings you make in WebEx will be automatically downloaded from WebEx and uploaded to this folder for your convenience.</p>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">You may access this folder by signing into Box at <a href="http://box.uncg.edu">http://box.uncg.edu</a>, using your UNCG credentials. Your shared folder is titled <i>$folderName</i>.</p>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">You may also access the folder directly at the following URL, if you already have a UNCG Box account: <a href="$folderUrl">$folderUrl</a></p>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">For a list of your current WebEx Recordings, including links to the files and all shared folders in Box, please click the button below.</a></p>

<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;" width="100%">
  <tbody>
    <tr>
      <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;" valign="top">
        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto; margin: 0 auto;">
          <tbody>
            <tr>
              <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center;" valign="top" bgcolor="#3498db" align="center"> <a href="$myRecordingsLink" target="_blank" style="display: inline-block; color: #ffffff; background-color: #003366; border: solid 1px #003366; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #003366;">Manage My UNCG WebEx Recordings</a> </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Please visit <a href="http://go.uncg.edu/webex:cloudshareemail">http://go.uncg.edu/webex</a>, or contact 6-TECH (256-8324), if you have additional questions about this information.</p>

<hr>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">As always, ITS reminds you to confirm the safety of all links in electronic communications by hovering over them with your cursor and confirming legitimacy of the destination.</p>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Never click on unverified website links. Report suspicious messages and links to 6-TECH at (336) 256-TECH (8324) or 6-TECH@uncg.edu.</p> 

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">This email is an official communication from the University of North Carolina at Greensboro. You may verify official university emails by checking the Verified Campus Communications Repository. If you have questions about the VCCR, or the authenticity of an email message you have received, please contact the sender of the message or search the UNCG website for "VCCR".</p>

HTML;

					$htmlEmailBody .= $emailHelper->body("UNCG WebEx Recordings - New Cloud Storage Share available", $htmlBody);

					// HTML version
					$etHtml = new Ot_Trigger_Dispatcher();
					$etHtml->setVariables(array(
						'username'      => $share["username"],
						'htmlEmailBody' => $htmlEmailBody
					));
					$etHtml->dispatch('Wst_New_Cloud_Storage_Share_Html');

					$successfulShares[] = $share;

				}
			}
		}

		$stuff = array(
			"message"          => "Successful Shares: " . count($successfulShares) . " out of " . count($newShares),
			"successfulShares" => $successfulShares,
			"totalShares"      => $newShares
		);

		$this->_helper->viewRenderer('index');
		$this->view->stuff = $stuff;
	}

	public function backfillDefaultPhoneNumbersAction() {
		die('disabled for safety'); // comment out when you need this

		$scriptStartTime = time();

		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		// get
		$get = Zend_Registry::get('getFilter');

		$batchSize = $get->batchSize ?: 15;
		$startAt = $get->startAt ?: 0;

		// api helper
		$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();

		// SAVE / LOAD USER LIST FROM CACHE - so that we know that we're using the same one
		$frontend = array(
			'lifetime'                => 7200,
			'automatic_serialization' => true
		);

		$backend = array(
			'cache_dir' => APPLICATION_PATH . '/../cache'
		);

		$cache = Zend_Cache::factory('core',
			'File',
			$frontend,
			$backend
		);
		Zend_Registry::set('cache', $cache);

		if ($startAt == 0) {
			$cache->remove('users');
		}

		if (!$result1 = $cache->load('users')) {
			$userSource = 'API';
			$result = $webexApiHelper->getAllWebexUsers();
			$allUsers = $result['data'];
			$cache->save($allUsers, 'users');
		} else {
			$userSource = 'cache';
			$allUsers = $result1;
		}

		$usersUpdated = array();

		$oldValue = '1,,,,,,';
		$newValue = '1,,5555555555,,,,';
		//$oldValue = '1,,5555555555,,,,'; // testing
		//$newValue = '1,,5555555550,,,,'; // testing

		// let's not go past the end of the array now, shall we?
		$terminate = (isset($allUsers[$startAt + $batchSize])) ? $startAt + $batchSize : count($allUsers) - 1;

		// check to see if user has phone number set
		for ($x = $startAt; $x <= $terminate; $x++) {
			$userShortData = $allUsers[$x];
			//foreach($allUsers as $userShortData) {

			$username = $userShortData['use:webExId'];
			//$username = 'zztest0013'; // testing

			// get full user record
			$user = $webexApiHelper->getWebexUserDetails($username);

			$updateNeeded = false;
			$xml = '<phones>';
			// office phone
			if (!isset($user['data']['use:phones']['com:phone']) || $user['data']['use:phones']['com:phone'] == $oldValue) {
				$xml .= "<phone>$newValue</phone>";
				$updateNeeded = true;
			}
			// cell phone
			if (!isset($user['data']['use:phones']['com:mobilePhone']) || $user['data']['use:phones']['com:mobilePhone'] == $oldValue) {
				$xml .= "<mobilePhone>$newValue</mobilePhone>";
				$updateNeeded = true;
			}

			if ($updateNeeded) {
				$xml .= '</phones>';
				$result = $webexApiHelper->editWebexUser($username, [], $xml);

				// re-deactivate user if they were already deactivated at time of this script running
				if ($userShortData['use:active'] === 'DEACTIVATED') {
					$webexApiHelper->deleteWebexUser($username);
				}

				$usersUpdated[] = [$username => $result['response']['result']];;
			} else {
				$usersUpdated[] = [$username => "SKIPPED"];
			}
			//break; // testing
		}

		// show "Next Batch" button
		$showButton = isset($allUsers[$x + 1]);

		$scriptEndTime = time();
		$this->view->assign(array(
			"scriptStartTime" => $scriptStartTime,
			"scriptEndTime"   => $scriptEndTime,
			"scriptExecTime"  => $scriptEndTime - $scriptStartTime,
			"usersUpdated"    => $usersUpdated,
			"batchSize"       => $batchSize,
			"nextBatchStart"  => $startAt + $batchSize,
			"userSource"      => $userSource,
			"showButton"      => $showButton,
			"percentComplete" => ($terminate + 1) / count($allUsers) * 100
		));

	}

	public function bulkDeleteUsersAction() {
		die('disabled for safety');

		$usernamesToDelete = array(); // leave empty to default to all
		$usernamesToKeep = array(); // replace with usernames to keep around

		$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();

		$allUsers = $webexApiHelper->getActiveWebexUsers();

		//$stuff = $allUsers;

		$potentialDeletes = array();
		foreach ($allUsers["data"] as $remoteUser) {
			$username = $remoteUser["use:webExId"];
			if ($remoteUser["use:userRoleType"] == "HOST") {
				// don't delete SITEADMIN users regardless.
				$potentialDeletes[$username] = $remoteUser;
			}
		}


		foreach ($usernamesToKeep as $keep) {
			if (isset($potentialDeletes[$keep])) {
				unset($potentialDeletes[$keep]);
			}
		}

		// $potentialDeletes now contains our final list.
		$apiQueueModel = new Wst_Model_Dbtable_Apiqueue();
		$userListModel = new Wst_Model_Dbtable_Userlist();
		$localUsers = $userListModel->getUserWebexIdMap();

		// catch these here for manual deletion.
		$usersRemoteButNotLocal = array();
		$usersQueuedForDeletion = array();

		foreach ($potentialDeletes as $webexId => $user) {
			if (isset($localUsers[$webexId])) {
				$localUser = $localUsers[$webexId];
				$usersQueuedForDeletion[$webexId] = $user;
				//dump($localUser, true);
				$apiQueueModel->queueDeleteAction($localUser["euid"], $webexId, 'manual', 'y');
			} else {
				$usersRemoteButNotLocal[$webexId] = $user;
			}
		}

		$stuff = array(
			"queued"    => count($usersQueuedForDeletion),
			"not local" => $usersRemoteButNotLocal
		);

		$this->_helper->viewRenderer('index');
		$this->view->stuff = $stuff;


	}
}