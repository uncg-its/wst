<?php
/**
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 *
 * This license is also available via the world-wide-web at
 * http://itdapps.ncsu.edu/bsd.txt
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to itappdev@ncsu.edu so we can send you a copy immediately.
 *
 * @package    Index_Controller
 * @category   Controller
 * @copyright  Copyright (c) 2007 NC State University Office of      
 *             Information Technology
 * @license    http://itdapps.ncsu.edu/bsd.txt  BSD License
 * @version    SVN: $Id: $
 */

/**
 * Main controller for the application
 *
 * @package    Index_Controller
 * @category   Controller
 * @copyright  Copyright (c) 2007 NC State University Office of      
 *             Information Technology
 */
class ItcController extends Zend_Controller_Action
{
    protected $startTimestamp;
    protected $endTimestamp;

    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array()) {

        $dateRangeModel = new Wst_Model_Dbtable_ReportDateRanges();
        $rangesForUser = $dateRangeModel->getRangesForLoggedInUser();

        // default
        $this->startTimestamp = strtotime("-30 days");
        $this->endTimestamp = time();

        // no dates for user at all; use last 30 days
        if ($rangesForUser) {

            // any active?
            foreach($rangesForUser as $index => $range) {
                if ($range["isActive"] == "y") {
                    // set
                    $this->startTimestamp = $rangesForUser[$index]['startTimestamp'];
                    $this->endTimestamp = $rangesForUser[$index]['endTimestamp'];

                    break;
                }
            }
        }

        parent::__construct($request, $response, $invokeArgs);
    }

    /**
     * shows the homepage
     *
     */
    public function indexAction()
    {
	    // add page styles
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

	    $this->_helper->pageTitle('ITC Panel');

	    // figure out whether to display reporting options
	    $reportGroupModel = new Wst_Model_Dbtable_ReportGroups();
	    $reportGroups = $reportGroupModel->getReportGroupsForLoggedInUser();

	    $this->view->showReports = $reportGroups;


    }

    public function reportGroupsAction() {
	    // add page styles
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

	    $this->_helper->pageTitle('My Report Groups');

	    $reportGroupModel = new Wst_Model_Dbtable_ReportGroups();
	    $this->view->reportGroups = $reportGroupModel->getReportGroupsForLoggedInUser();

    }

    public function createReportGroupAction() {
	    // add page styles
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

	    $this->_helper->pageTitle('My Report Groups');

	    $form = new Wst_Form_Reportgroup();

	    $varNames = array(
	    	'reportGroupName',
	        'orgShortCodes'
	    );

	    if ($this->getRequest()->isPost()) {
		    if ($form->isValid($this->getRequest()->getPost())) {
			    $codesString = implode(',', $form->getValue('orgShortCodes'));

			    $identity = Zend_Auth::getInstance()->getIdentity();

			    $insertArray = array(
			    	'accountId' => $identity->accountId,
				    'displayName' => $form->getValue('reportGroupName') ?: "My Report Group",
				    'orgShortCodeList' => $codesString
			    );

			    $reportGroupModel = new Wst_Model_Dbtable_ReportGroups();
			    $result = $reportGroupModel->insert($insertArray);

			    if ($result) {
			    	$this->_helper->messenger->addSuccess('Successfully created report group.');
			    } else {
				    $this->_helper->messenger->addError('Error creating report group!');
			    }

			    $this->_helper->redirector->gotoRoute(array('controller' => 'itc', 'action' => 'report-groups'), 'default', true);

		    }
	    }


	    $arrayElementTypes = array(
		    'Zend_Form_Element_Multiselect',
		    'Zend_Form_Element_MultiCheckbox',
		    'Twitter_Bootstrap_Form_Element_MultiCheckbox' // need to add this for it to work properly.
	    );

	    foreach($varNames as $item) {

		    $thisType = $form->getElement($item)->getType();
		    //echo $thisType;
		    if(!empty($$item)) {

			    //dump($thisType);

			    if(in_array($thisType, $arrayElementTypes)) {

				    $form->getElement($item)->setValue(explode(',', $$item->var_value));
			    } else {
				    $form->getElement($item)->setValue($$item->var_value);
			    }

			    $this->view->$item = $$item->var_value;
		    }

	    }

	    $this->view->form = $form;

    }


    public function editReportGroupAction() {
	    // add page styles
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

	    $this->_helper->pageTitle('Edit Report Group');

	    $reportGroupModel = new Wst_Model_Dbtable_ReportGroups();
	    $get = Zend_Registry::get('getFilter');

	    $id = $get->reportGroupId;

	    $reportGroup = $reportGroupModel->find($id);

	    if (is_null($reportGroup)) {
	    	$this->_helper->messenger->addError('Error: specified Report Group not found.');
		    $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'report-groups'), 'default', true);
	    }

	    if (!$reportGroupModel->userOwnsReportGroup($id)) {
		    $this->_helper->messenger->addError('Error: the specified Report Group does not belong to you.');
		    $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'report-groups'), 'default', true);
	    }

	    $form = new Wst_Form_Reportgroup();

	    if ($this->getRequest()->isPost()) {
		    if ($form->isValid($this->getRequest()->getPost())) {
			    $codesString = implode(',', $form->getValue('orgShortCodes'));

			    $identity = Zend_Auth::getInstance()->getIdentity();

			    $updateArray = array(
				    'displayName' => $form->getValue('reportGroupName') ?: "My Report Group",
				    'orgShortCodeList' => $codesString
			    );

				try {
					$reportGroupModel = new Wst_Model_Dbtable_ReportGroups();
					$where = $reportGroupModel->getAdapter()->quoteInto("reportGroupId = ?", $id);
					$result = $reportGroupModel->update($updateArray, $where);

					$this->_helper->messenger->addSuccess('Successfully edited report group.');
				} catch (Exception $e) {
					$this->_helper->messenger->addError('Error editing report group: ' . $e->getMessage());
				}

			    $this->_helper->redirector->gotoRoute(array('controller' => 'itc', 'action' => 'report-groups'), 'default', true);

		    }
	    }

	    $form->getElement('orgShortCodes')->setValue(explode(',', $reportGroup->orgShortCodeList));
	    $form->getElement('reportGroupName')->setValue($reportGroup->displayName);


	    $this->view->form = $form;
    }

    public function deleteReportGroupAction() {

	    $this->_helper->viewRenderer->setNeverRender();
	    $this->_helper->layout->disableLayout();

	    $reportGroupModel = new Wst_Model_Dbtable_ReportGroups();
	    $get = Zend_Registry::get('getFilter');

	    $id = $get->reportGroupId;

	    $reportGroup = $reportGroupModel->find($id);

	    if (is_null($reportGroup)) {
		    $this->_helper->messenger->addError('Error: specified Report Group not found.');
		    $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'report-groups'), 'default', true);
	    }

	    if (!$reportGroupModel->userOwnsReportGroup($id)) {
		    $this->_helper->messenger->addError('Error: the specified Report Group does not belong to you.');
		    $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'report-groups'), 'default', true);
	    }

	    try {
		    $reportGroupModel = new Wst_Model_Dbtable_ReportGroups();
		    $where = $reportGroupModel->getAdapter()->quoteInto("reportGroupId = ?", $id);
		    $result = $reportGroupModel->delete($where);
		    $this->_helper->messenger->addSuccess('Report Group deleted successfully.');
	    } catch (Exception $e) {
		    $this->_helper->messenger->addError('Error: database error while attempting to delete Report Group');
	    }

	    $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'report-groups'), 'default', true);
	    
    }

    public function setDefaultReportGroupAction() {
        $this->_helper->viewRenderer->setNeverRender();
        $this->_helper->layout->disableLayout();

        $reportGroupModel = new Wst_Model_Dbtable_ReportGroups();
        $get = Zend_Registry::get('getFilter');

        $id = $get->reportGroupId;

        $reportGroup = $reportGroupModel->find($id);

        if (is_null($reportGroup)) {
            $this->_helper->messenger->addError('Error: specified Report Group not found.');
            $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'report-groups'), 'default', true);
        }

        if (!$reportGroupModel->userOwnsReportGroup($id)) {
            $this->_helper->messenger->addError('Error: the specified Report Group does not belong to you.');
            $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'report-groups'), 'default', true);
        }

        try {
            $identity = Zend_Auth::getInstance()->getIdentity();
            $accountId = $identity->accountId;

            $reportGroupModel = new Wst_Model_Dbtable_ReportGroups();

            // remove default
            $where = $reportGroupModel->getAdapter()->quoteInto("accountId = ?", $accountId);
            $result = $reportGroupModel->update(array('isDefault' => 'n'), $where);

            // set new default
            $where = $reportGroupModel->getAdapter()->quoteInto("reportGroupId = ?", $id);
            $result = $reportGroupModel->update(array('isDefault' => 'y'), $where);
            $this->_helper->messenger->addSuccess('Report Group set to default successfully.');
        } catch (Exception $e) {
            $this->_helper->messenger->addError('Error: database error while attempting to set Report Group to default');
        }

        $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'report-groups'), 'default', true);
    }

    public function usageSummaryAction() {
	    // add page styles
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

	    $reportGroupModel = new Wst_Model_Dbtable_ReportGroups();

	    // get user's report groups
	    $userReportGroups = $reportGroupModel->getReportGroupsForLoggedInUser();

	    if (count($userReportGroups) == 0) {
		    $this->_helper->messenger->addError('Error: you must set up at least one report group.');
		    $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'index'), 'default', true);
	    }

	    // get specified report group, or first one of the batch if none specified
	    $get = Zend_Registry::get('getFilter');
	    $reportGroupId = $get->reportGroupId;
	    if (empty($reportGroupId)) {
		    $reportGroupId = $userReportGroups[0]["reportGroupId"];
		    foreach ($userReportGroups as $group) {
		        if ($group["isDefault"] == "y") {
                    $reportGroupId = $group["reportGroupId"];
                }
            }
	    }

	    $reportGroup = $reportGroupModel->find($reportGroupId);

	    if (is_null($reportGroup)) {
		    $this->_helper->messenger->addError('Error: specified Report Group not found.');
		    $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'index'), 'default', true);
	    }

	    if (!$reportGroupModel->userOwnsReportGroup($reportGroupId)) {
		    $this->_helper->messenger->addError('Error: the specified Report Group does not belong to you.');
		    $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'index'), 'default', true);
	    }

	    $this->_helper->pageTitle('Usage Summary');

	    $this->view->reportGroups = $userReportGroups;
	    $this->view->reportGroupName = $reportGroup->displayName;

	    $reportGroupCodes = explode(",", $reportGroup->orgShortCodeList);

	    $this->view->reportGroupCodes = $reportGroupCodes;

	    $this->view->reportGroupId = $reportGroupId;

	    // get report array
	    $itcReportHelper = new Wst_Model_Helpers_ItcReportHelper();
	    $reportArray = $itcReportHelper->getReportArray($this->startTimestamp, $this->endTimestamp, $reportGroupCodes);

	    $this->view->reportArray = $reportArray;
        $this->view->dateRange = date("m/d/Y", $this->startTimestamp) . " - " . date("m/d/Y", $this->endTimestamp);

        // get all possible user dateRanges
        $dateRangeModel = new Wst_Model_Dbtable_ReportDateRanges();
        $this->view->dateRanges = $dateRangeModel->getRangesForLoggedInUser();

    }

    public function usageByDepartmentAction() {
	    // add page styles
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

	    $reportGroupModel = new Wst_Model_Dbtable_ReportGroups();

	    // get user's report groups
	    $userReportGroups = $reportGroupModel->getReportGroupsForLoggedInUser();

	    if (count($userReportGroups) == 0) {
		    $this->_helper->messenger->addError('Error: you must set up at least one report group.');
		    $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'usage-summary'), 'default', true);
	    }

	    // get specified report group, or first one of the batch if none specified
	    $get = Zend_Registry::get('getFilter');
	    $reportGroupId = $get->reportGroupId;
	    if (empty($reportGroupId)) {
		    $reportGroupId = $userReportGroups[0]["reportGroupId"];
	    }

	    $validUsageTypes = array('sessions', 'recordings', 'attendees');

	    $usageType = $get->usageType;
	    if (!in_array($usageType, $validUsageTypes)) {
		    $this->_helper->messenger->addError('Error: invalid usage type.');
		    $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'usage-summary'), 'default', true);
	    }

	    $this->view->usageType = $usageType;

	    $reportGroup = $reportGroupModel->find($reportGroupId);

	    if (is_null($reportGroup)) {
		    $this->_helper->messenger->addError('Error: specified Report Group not found.');
		    $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'usage-summary'), 'default', true);
	    }

	    if (!$reportGroupModel->userOwnsReportGroup($reportGroupId)) {
		    $this->_helper->messenger->addError('Error: the specified Report Group does not belong to you.');
		    $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'usage-summary'), 'default', true);
	    }

	    $this->_helper->pageTitle('Usage By Department');

	    // report group info
	    $this->view->reportGroups = $userReportGroups;
	    $this->view->reportGroupName = $reportGroup->displayName;

	    $reportGroupCodes = explode(",", $reportGroup->orgShortCodeList);
	    $this->view->reportGroupCodes = $reportGroupCodes;

	    $this->view->reportGroupId = $reportGroupId;

	    // just hand back what's needed.
	    $itcReportHelper = new Wst_Model_Helpers_ItcReportHelper();
	    $fullReport = $itcReportHelper->getReportArray($this->startTimestamp, $this->endTimestamp, $reportGroupCodes);

	    $this->view->reportData = $fullReport[$usageType];
	    $this->view->dateRange = date("m/d/Y", $this->startTimestamp) . " - " . date("m/d/Y", $this->endTimestamp);

	    // get all possible user dateRanges
        $dateRangeModel = new Wst_Model_Dbtable_ReportDateRanges();
	    $this->view->dateRanges = $dateRangeModel->getRangesForLoggedInUser();

    }

    public function reportDateRangesAction() {
        // add page styles
        $this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
        $this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

        $this->_helper->pageTitle('My Date Ranges');

        $dateRangeHelper = new Wst_Model_Dbtable_ReportDateRanges();
        $this->view->dateRanges = $dateRangeHelper->getRangesForLoggedInUser();

    }

    public function setActiveDateRangeAction() {

        $this->_helper->viewRenderer->setNeverRender();
        $this->_helper->layout->disableLayout();

        $dateRangeModel = new Wst_Model_Dbtable_ReportDateRanges();
        $get = Zend_Registry::get('getFilter');

        $id = $get->rangeId;

        $redirectAction = isset($get->redirectAction) ? $get->redirectAction : 'report-date-ranges';

        // build route redirect array
        $redirectRoute = array('controller' => 'itc', 'action' => $redirectAction);

        $params = array('reportGroupId', 'usageType', 'interval');
        foreach($params as $p) {
            if (isset($get->$p)) {
                $redirectRoute[$p] = $get->$p;
            }
        }

        if ($id == "none") {
            try {
                $identity = Zend_Auth::getInstance()->getIdentity();
                $accountId = $identity->accountId;

                // remove active designation from all
                $where = $dateRangeModel->getAdapter()->quoteInto("accountId = ?", $accountId);
                $result = $dateRangeModel->update(array('isActive' => 'n'), $where);

                $this->_helper->messenger->addSuccess('Date Range cleared successfully.');
            } catch (Exception $e) {
                $this->_helper->messenger->addError('Error: database error while attempting to clear active Date Range');
            }

            $this->_helper->redirector->gotoRoute($redirectRoute, 'default', true);
        }


        $reportGroup = $dateRangeModel->find($id);

        if (is_null($reportGroup)) {
            $this->_helper->messenger->addError('Error: specified Date Range not found.');
            $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'report-date-ranges'), 'default', true);
        }

        if (!$dateRangeModel->userOwnsRange($id)) {
            $this->_helper->messenger->addError('Error: the specified Date Range does not belong to you.');
            $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'report-date-ranges'), 'default', true);
        }

        try {
            $identity = Zend_Auth::getInstance()->getIdentity();
            $accountId = $identity->accountId;

            // remove default
            $where = $dateRangeModel->getAdapter()->quoteInto("accountId = ?", $accountId);
            $result = $dateRangeModel->update(array('isActive' => 'n'), $where);

            // set new default
            $where = $dateRangeModel->getAdapter()->quoteInto("rangeId = ?", $id);
            $result = $dateRangeModel->update(array('isActive' => 'y'), $where);
            $this->_helper->messenger->addSuccess('Date Range set to active successfully.');
        } catch (Exception $e) {
            $this->_helper->messenger->addError('Error: database error while attempting to set Date Range to active');
        }

        $this->_helper->redirector->gotoRoute($redirectRoute, 'default', true);

    }

    public function createReportDateRangeAction() {
        // add page styles
        $this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
        $this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

        // datepicker
        $this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/scripts/datepicker/css/datepicker.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/scripts/datepicker/css/datepicker.css');
        $this->view->headScript()->appendFile(
            $this->view->baseUrl() . '/public/scripts/datepicker/js/bootstrap-datepicker.js',
            'text/javascript'
        );

        $this->_helper->pageTitle('My Report Date Ranges');

        $form = new Wst_Form_Reportdaterange();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->getRequest()->getPost())) {

                $identity = Zend_Auth::getInstance()->getIdentity();

                $insertArray = array(
                    'accountID' => $identity->accountId,
                    'startTimestamp' => strtotime($form->getValue('start_date')),
                    'endTimestamp' => strtotime($form->getValue('end_date')) + (60 * 60 * 24) - 1, // (23:59:59)
                );

                $dateRangeModel = new Wst_Model_Dbtable_ReportDateRanges();
                $result = $dateRangeModel->insert($insertArray);

                if ($result) {
                    $this->_helper->messenger->addSuccess('Successfully created report date range.');
                } else {
                    $this->_helper->messenger->addError('Error creating date range!');
                }

                $this->_helper->redirector->gotoRoute(array('controller' => 'itc', 'action' => 'report-date-ranges'), 'default', true);

            }
        }

        $this->view->form = $form;
    }

    public function generateCsvAction() {

    	/*
    	 * We need GET params...
    	 *
    	 * - type of download (sessions, recordings, attendees)
    	 * - active report group (need from GET since default != shown)
    	 *
    	 * We already have...
    	 * - active date range (from __construct())
    	 *
    	 * */

	    $get = Zend_Registry::get('getFilter');

	    try {
		    // do we have all params?
		    if (!isset($get->reportType) || !isset($get->reportGroupId)) {
			    throw new Exception("Invalid parameter set.");
		    }

		    // report type good?
		    $validReportTypes = array('sessions', 'recordings', 'attendees');
		    if (!in_array($get->reportType, $validReportTypes)) {
			    throw new Exception("Invalid report type specified.");
		    }

		    // valid Report ID / ownership
		    $reportGroupId = $get->reportGroupId;
		    $reportGroupModel = new Wst_Model_Dbtable_ReportGroups();

		    $reportGroup = $reportGroupModel->find($reportGroupId);

		    if (is_null($reportGroup)) {
			    throw new Exception('specified Report Group not found.');
		    }

		    if (!$reportGroupModel->userOwnsReportGroup($reportGroupId)) {
			    throw new Exception('the specified Report Group does not belong to you.');
		    }



		    // get report
		    $reportGroupCodes = explode(",", $reportGroup->orgShortCodeList);
		    $itcReportHelper = new Wst_Model_Helpers_ItcReportHelper();
		    $fullReport = $itcReportHelper->getReportArray($this->startTimestamp, $this->endTimestamp, $reportGroupCodes);

		    // ok, now what...

		    if ($get->reportType == "sessions") {
				$headerArray = array(
					"Department Code",
					"Start Time",
					"Duration (minutes)",
					"# of Attendees"
				);

				$dataArray = array();
				foreach($fullReport["sessions"]["data"] as $orgShortCode => $data) {
					foreach($data as $session) {
						$dataArray[] = array(
							$orgShortCode,
							date("m/d/Y h:i:s a", $session["session_start_time"]),
							$session["duration"],
							$session["total_attendees"]
						);
					}
				}
		    } else if ($get->reportType == "recordings") {
			    $headerArray = array(
			        "Department Code",
				    "Recording Time",
				    "Duration",
				    "File Size (MB)"
			    );

			    $dataArray = array();
			    $helper = new Wst_Model_Helpers_General();
			    foreach($fullReport["recordings"]["data"] as $orgShortCode => $data) {
				    foreach($data as $recording) {
					    $dataArray[] = array(
						    $orgShortCode,
						    date("m/d/Y h:i:s a", $recording["create_time_utc"]),
						    $helper->secondsToTime($recording["duration_sec"]),
						    round($recording["size_mb"], 2)
					    );
				    }
			    }

		    } else if ($get->reportType == "attendees") {
			    $headerArray = array(
					"Department Code",
				    "Join Time",
				    "Leave Time",
				    "Duration (minutes)",
				    "Participant Type",
				    "OS",
				    "Browser",
				    "OS Type"
			    );

			    $dataArray = array();
			    $helper = new Wst_Model_Helpers_General();
			    foreach($fullReport["attendees"]["data"] as $orgShortCode => $data) {
				    foreach($data as $attendee) {
					    $dataArray[] = array(
						    $orgShortCode,
						    date("m/d/Y h:i:s a", $attendee["join_time_utc"]),
						    date("m/d/Y h:i:s a", $attendee["leave_time_utc"]),
						    $attendee["duration"],
						    ucwords(strtolower($attendee["participant_type"])) ?: 0,
						    $attendee["operating_system"],
						    $attendee["browser"],
						    $attendee["os_type"]
					    );
				    }
			    }
		    } else {
			    throw new Exception('problem parsing report type.');
		    }

	    } catch (Exception $e) {
		    $this->_helper->messenger->addError("Error: " . $e->getMessage());
		    $this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'usage-summary'), 'default', true);
	    }




	    // get the CSV going
	    $this->_helper->layout->disableLayout();
	    $this->_helper->viewRenderer->setNeverRender();

	    // write to temp file
	    $tmpName = tempnam(sys_get_temp_dir(), 'data');
	    $file = fopen($tmpName, "w");

	    fputcsv($file, $headerArray);
	    foreach ($dataArray as $data) {
		    fputcsv($file, $data);
	    }

	    fclose($file);

	    $timestamp = date("Ymd-His");

	    // http headers for download
	    header("Content-Description: File Transfer");
	    header("Content-type: text/csv");
	    header("Content-Disposition: attachment; filename=\"{$get->reportType}_report_$timestamp.csv\"");
	    header("Content-Transfer-Encoding: binary");
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Cache-Control: public");
	    header("Pragma: public");
	    header("Content-Length: " . filesize($tmpName));
	    //header("Content-Length: 10000");
	    //ob_end_flush();
	    ob_clean();
	    flush();
	    @readfile($tmpName);

	    unlink($tmpName);
    }

	public function usageTrendsAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$reportGroupModel = new Wst_Model_Dbtable_ReportGroups();

		// get user's report groups
		$userReportGroups = $reportGroupModel->getReportGroupsForLoggedInUser();

		if (count($userReportGroups) == 0) {
			$this->_helper->messenger->addError('Error: you must set up at least one report group.');
			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'index'), 'default', true);
		}

		// get specified report group, or first one of the batch if none specified
		$get = Zend_Registry::get('getFilter');
		$reportGroupId = $get->reportGroupId;
		if (empty($reportGroupId)) {
			$reportGroupId = $userReportGroups[0]["reportGroupId"];
			foreach ($userReportGroups as $group) {
				if ($group["isDefault"] == "y") {
					$reportGroupId = $group["reportGroupId"];
				}
			}
		}

		$reportGroup = $reportGroupModel->find($reportGroupId);

		if (is_null($reportGroup)) {
			$this->_helper->messenger->addError('Error: specified Report Group not found.');
			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'index'), 'default', true);
		}

		if (!$reportGroupModel->userOwnsReportGroup($reportGroupId)) {
			$this->_helper->messenger->addError('Error: the specified Report Group does not belong to you.');
			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'index'), 'default', true);
		}

		$interval = $get->interval ?: "daily";
		$supportedIntervals = array(
			'daily' => array(
				"limit_text" => "31 days",
				"limit_days" => 31,
			),
			'weekly' => array(
				"limit_text" => "15 weeks",
				"limit_days" => 105, // 15 weeks
			),
			'monthly' => array(
				"limit_text" => "12 months",
				"limit_days" => 365, // 12 months
			)
		);

		if (!isset($supportedIntervals[$interval])) {
			$this->_helper->messenger->addError('Error: the specified interval is invalid.');
			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'itc', 'action' => 'index'), 'default', true);
		}

		$this->_helper->pageTitle('Usage Trends');

		// determine the structure of the sorting array
		$latestTimestamp = $this->endTimestamp;
		$earliestTimestamp = strtotime("-" . $supportedIntervals[$interval]["limit_days"] . " days", $this->endTimestamp);

		if ($this->startTimestamp < $earliestTimestamp) {
			$this->_helper->messenger->addWarning('<i class="fa fa-exclamation-circle"></i> <strong>Notice</strong>: this data does not cover the entire active date range. ' . ucwords($interval) . ' usage trend reports are limited to the past <strong>' . $supportedIntervals[$interval]["limit_text"] . '</strong>.');
		}

		if ($earliestTimestamp < $this->startTimestamp) {
			$earliestTimestamp = $this->startTimestamp;
		}

		// init
		$sortedData = array();
		$currentTimestamp = $latestTimestamp;

		if ($interval == "daily") {
			// we will need each day separately
			do {
				$thisDate = date('m/d/Y', $currentTimestamp);
				$sortedData[$thisDate] = array(
					"valid_dates" => array($thisDate),
					"data" => array(
						"sessions" => 0,
						"recordings" => 0,
						"attendees" => 0,
						"mobile_users" => 0,
						"desktop_users" => 0
					)
				);
				$currentTimestamp -= 86400;
			} while ($currentTimestamp >= $earliestTimestamp);
		} else if ($interval == "weekly") {
			// group seven days
			do {
				$weekLowerBound = $currentTimestamp - (86400 * 7);
				if ($weekLowerBound < $earliestTimestamp) $weekLowerBound = $earliestTimestamp;

				$displayName = date("m/d/Y", $weekLowerBound) . " - " . date("m/d/Y", $currentTimestamp);
				$sortedData[$displayName] = array(
					"valid_dates" => array(),
					"data" => array(
						"sessions" => 0,
						"recordings" => 0,
						"attendees" => 0,
						"mobile_users" => 0,
						"desktop_users" => 0
					)
				);

				do {
					$sortedData[$displayName]["valid_dates"][] = date("m/d/Y", $currentTimestamp);
					$currentTimestamp -= 86400;
				} while ($currentTimestamp >= $weekLowerBound);

			} while ($currentTimestamp >= $earliestTimestamp);
		} else if ($interval == "monthly") {
			// groups will vary!

			do {
				$monthDisplayName = date('m/Y', $currentTimestamp);
				if (!isset($sortedData[$monthDisplayName])) {
					$sortedData[$monthDisplayName] = array(
						"valid_dates" => array(),
						"data" => array(
							"sessions" => 0,
							"recordings" => 0,
							"attendees" => 0,
							"mobile_users" => 0,
							"desktop_users" => 0
						)
					);
				}

				$sortedData[$monthDisplayName]["valid_dates"][] = date("m/d/Y", $currentTimestamp);
				$currentTimestamp -= 86400;

			} while($currentTimestamp >= $earliestTimestamp);

		}


		// ok, now get report array
		$itcReportHelper = new Wst_Model_Helpers_ItcReportHelper();
		$reportGroupCodes = explode(",", $reportGroup->orgShortCodeList);
		$reportArray = $itcReportHelper->getReportArray($this->startTimestamp, $this->endTimestamp, $reportGroupCodes);

		// iterate over data and insert it into the proper place in the sortedData array...

		foreach ($reportArray["sessions"]["data"] as $department) {
			foreach($department as $session) {
				$sessionDate = date("m/d/Y", $session["start_time_utc"]);
				foreach ($sortedData as $displayName => &$data) {
					if (in_array($sessionDate, $data["valid_dates"])) {
						$data["data"]["sessions"]++;
						break;
					}
				}
			}
		}

		foreach ($reportArray["recordings"]["data"] as $department) {
			foreach($department as $recording) {
				$recordingDate = date("m/d/Y", $recording["create_time_utc"]);
				foreach ($sortedData as $displayName => &$data) {
					if (in_array($recordingDate, $data["valid_dates"])) {
						$data["data"]["recordings"]++;
						break;
					}
				}
			}
		}

		foreach ($reportArray["attendees"]["data"] as $department) {
			foreach($department as $attendee) {
				$attendeeDate = date("m/d/Y", $attendee["join_time_utc"]);
				foreach ($sortedData as $displayName => &$data) {
					if (in_array($attendeeDate, $data["valid_dates"])) {
						$data["data"]["attendees"]++;
						if ($attendee["os_type"] == "mobile") {
							$data["data"]["mobile_users"]++;
						} else {
							$data["data"]["desktop_users"]++;
						}
						break;
					}
				}
			}
		}

		$sortedData = array_reverse($sortedData);

		// format data for highcharts
		$highchartsData = array(
			"seriesPoints" => array(),
			"seriesData" => array()
		);
		foreach($sortedData as $point => $data) {
			$highchartsData["seriesPoints"][] = $point;
			foreach($data["data"] as $name => $datum) {
				if (!isset($highchartsData["seriesData"][$name])) {
					$highchartsData["seriesData"][$name] = array();
				}
				$highchartsData["seriesData"][$name][] = $datum;
			}
		}

		// view vars
		$this->view->interval = $interval;
		$this->view->sortedData = $sortedData;

		$localConfig = Zend_Registry::get('localConfig');
		$this->view->environmentName = $localConfig["environmentName"];

		$this->view->highchartsData = $highchartsData;

		$this->view->reportGroups = $userReportGroups;
		$this->view->reportGroupName = $reportGroup->displayName;
		$this->view->reportGroupCodes = $reportGroupCodes;
		$this->view->reportGroupId = $reportGroupId;
		$this->view->reportArray = $reportArray;
		$this->view->dateRange = date("m/d/Y", $this->startTimestamp) . " - " . date("m/d/Y", $this->endTimestamp);

		// get all possible user dateRanges
		$dateRangeModel = new Wst_Model_Dbtable_ReportDateRanges();
		$this->view->dateRanges = $dateRangeModel->getRangesForLoggedInUser();

	}

	public function departmentCodeLookupAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Department Code Lookup');

		// get users by short code
		$userListModel = new Wst_Model_Dbtable_Userlist();
		$users = $userListModel->getActiveUsersList()->toArray();

		$usersByShortCode = array();
		foreach($users as $user) {
			if (isset($usersByShortCode[$user['org_short_code']])) {
				$usersByShortCode[$user['org_short_code']][] = $user;
			} else {
				$usersByShortCode[$user['org_short_code']] = array($user);
			}
		}

		ksort($usersByShortCode);

		$deptsToExclude = array('undefined');
		foreach($deptsToExclude as $dept) {
			if (isset($usersByShortCode[$dept])) {
				unset($usersByShortCode[$dept]);
			}
		}

		$this->view->usersByShortCode = $usersByShortCode;
	}

}