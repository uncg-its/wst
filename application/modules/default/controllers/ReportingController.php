<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 4/22/16
 * Time: 3:23 PM
 */
class ReportingController extends Zend_Controller_Action {

	public function indexAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Reporting');

	}

	public function listSessionsFromApiAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('List Sessions (from API)');

		$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
		$helper = new Wst_Model_Helpers_General();
		$get = Zend_Registry::get('getFilter');

		// TODO - allow for date input
		// for now, using last 30 days

		if (!isset($get->centers) || $get->centers == "all") {
			$centersToFetch = array("mc","tc","ec","sc");
		} else {
			$centersToFetch = explode(",", $get->centers);
		}

		$meetingsList = $webexApiHelper->listSessions(array(
			"centers" => $centersToFetch
		));

		//dump($meetingsList, true);

		$centerCrosswalk = array(
			"mc" => "Meeting Center",
			"tc" => "Training Center",
			"ec" => "Event Center",
			"sc" => "Support Center"
		);

		if ($meetingsList["response"] == "SUCCESS") {
			//dump($meetingsList, true);

			// parse out by center now...
			$masterList = array();
			$startTimeArray = array();

			$centerSessionNameCrosswalk = array(
				"mc" => "meetingUsageHistory",
				"tc" => "trainingSessionHistory",
				"ec" => "eventSessionHistory",
				"sc" => "supportSessionHistory"
			);

			foreach($meetingsList["data"] as $centerAbbrev => $list) {
				$centerSessionName = "history:" . $centerSessionNameCrosswalk[$centerAbbrev];

				// catch singles.
				if (isset($list[$centerSessionName]["history:confID"])) {
					$list[$centerSessionName] = array($list[$centerSessionName]);
				}

				foreach($list[$centerSessionName] as $k => $details) {

					//dump($details, true);

					$details["center"] = $centerCrosswalk[$centerAbbrev];
					$details["centerAbbrev"] = $centerAbbrev;

					// start / end time stuff...
					$startTimeKey = (isset($details["history:meetingStartTime"])) ? "history:meetingStartTime" : "history:sessionStartTime";
					$endTimeKey = (isset($details["history:meetingEndTime"])) ? "history:meetingEndTime" : "history:sessionEndTime";

					// pull start times out for sorting purposes
					$startTimeArray[] = $details[$startTimeKey];

					// adjust start and end times from GMT
					$details["startTimeLocal"] = $helper->convertGmtToLocal($details[$startTimeKey]);
					$details["endTimeLocal"] = $helper->convertGmtToLocal($details[$endTimeKey]);

					// add to master list
					$masterList[] = $details;

				}
			}

			array_multisort($startTimeArray, SORT_DESC, $masterList);
			
			//dump($masterList, true);

			$localConfig = Zend_Registry::get("localConfig");
			$perPage = $localConfig["itemsPerPage"];

			$adapter = new Zend_Paginator_Adapter_Array($masterList);

			$paginator = new Zend_Paginator($adapter);
			$paginator->setDefaultItemCountPerPage($perPage);
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));

			$this->view->assign(array(
				'paginator' => $paginator,
				'numberOfResults' => count($masterList),
			));

			//dump($paginator, true);
		} else {
			$adapter = new Zend_Paginator_Adapter_Array(array());
			$paginator = new Zend_Paginator($adapter);

			$this->view->paginator = $paginator;
			$this->view->numberOfResults = 0;
		}

		$centersPulledArray = array();

		if (isset($get->centers) && $get->centers != "all") {
			foreach(explode(",", $get->centers) as $c) {
				$centersPulledArray[] = $centerCrosswalk[$c];
			}
		}

		if (count($centersPulledArray) > 0) {
			$centersPulledString = implode(", ", $centersPulledArray);
		} else {
			$centersPulledString = implode(", ", $centerCrosswalk);
		}

		$this->view->centersPulledString = $centersPulledString;
		$this->view->form = new Wst_Form_Centerfilter();
		
		foreach($meetingsList["errorMessages"] as $m) {
			$this->_helper->messenger->addError("Error: API call failure - <em>$m</em>");
		}

	}

	public function listSessionsAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		// js file for sortable table
		$this->view->headScript()->appendFile($this->view->baseUrl() . '/public/scripts/ui-sortable.js');

		$this->_helper->pageTitle('List Sessions');

		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();
		$helper = new Wst_Model_Helpers_General();
		$get = Zend_Registry::get('getFilter');

		// search form
		if ($this->_getParam('resetButton') == 'Reset') {
			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'reporting', 'action' => 'list-sessions'), 'default', true);
		}

		$filter = array(
			'center'         => trim($this->_getParam('center')),
			'session_name'   => trim($this->getParam('session_name')),
			'host_username'  => trim($this->getParam('host_username')),
			'start_time_start' => trim($this->getParam('start_time_start')),
			'start_time_end'   => trim($this->getParam('start_time_end')),
			'duration_min'       => trim($this->getParam('duration_min')),
			'duration_max'       => trim($this->getParam('duration_max')),
			'sort'           => $this->getParam('sort', 'start_time_utc'),
			'direction'      => $this->getParam('direction', 'desc')
		);

		$select = $sessionArchiveModel->getAdapter()->select();
		$select->from($sessionArchiveModel->info('name'));

		$select->order($filter['sort'] . ' ' . $filter['direction']);

		if ($filter['center'] != '') {
			$where = $sessionArchiveModel->getAdapter()->quoteInto("center = ?", $filter['center']);
			$select->where($where);
		}

		if ($filter['session_name'] != '') {
			$where = $sessionArchiveModel->getAdapter()->quoteInto("session_name LIKE ?", "%" . $filter['session_name'] . "%");
			$select->where($where);
		}

		if ($filter['host_username'] != '') {
			$where = $sessionArchiveModel->getAdapter()->quoteInto("host_username = ?", $filter['host_username']);
			$select->where($where);
		}

		if ($filter['start_time_start'] != '') {
			$start = strtotime($filter['start_time_start']);
			$where = $sessionArchiveModel->getAdapter()->quoteInto("start_time_utc >= ?", $start);
			$select->where($where);
		}

		if ($filter['start_time_end'] != '') {
			$end = strtotime($filter['start_time_end']);
			$where = $sessionArchiveModel->getAdapter()->quoteInto("start_time_utc <= ?", $end);
			$select->where($where);
		}

		if ($filter['duration_min'] != '') {
			$where = $sessionArchiveModel->getAdapter()->quoteInto("duration >= ?", $filter['duration_min']);
			$select->where($where);
		}

		if ($filter['duration_max'] != '') {
			$where = $sessionArchiveModel->getAdapter()->quoteInto("duration <= ?", $filter['duration_max']);
			$select->where($where);
		}

		//$meetingsList = $sessionArchiveModel->getSessionsList("all");

		$localConfig = Zend_Registry::get("localConfig");

		$adapter = new Zend_Paginator_Adapter_DbSelect($select);

		$paginator = new Zend_Paginator($adapter);
		$paginator->setDefaultItemCountPerPage($localConfig["itemsPerPage"]);
		$paginator->setCurrentPageNumber($this->getParam("page", 1));

		//dump($meetingsList, true);

		//$centerCrosswalk = array(
		//	"mc" => "Meeting Center",
		//	"tc" => "Training Center",
		//	"ec" => "Event Center",
		//	"sc" => "Support Center"
		//);
		
		//if ($meetingsList) {
		//
		//	//dump($meetingsList, true);
		//
		//	$localConfig = Zend_Registry::get("localConfig");
		//	$perPage = $localConfig["itemsPerPage"];
		//
		//	$adapter = new Zend_Paginator_Adapter_Array($meetingsList);
		//
		//	$paginator = new Zend_Paginator($adapter);
		//	$paginator->setDefaultItemCountPerPage($perPage);
		//	$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		//
		//	$numberOfResults = count($meetingsList);
		//
		//	//dump($paginator, true);
		//} else {
		//	$adapter = new Zend_Paginator_Adapter_Array(array());
		//	$paginator = new Zend_Paginator($adapter);
		//
		//	$numberOfResults = 0;
		//}
		//
		//$centersPulledArray = array();
		//
		//if (isset($get->centers) && $get->centers != "all") {
		//	foreach(explode(",", $get->centers) as $c) {
		//		$centersPulledArray[] = $centerCrosswalk[$c];
		//	}
		//}
		//
		//if (count($centersPulledArray) > 0) {
		//	$centersPulledString = implode(", ", $centersPulledArray);
		//} else {
		//	$centersPulledString = implode(", ", $centerCrosswalk);
		//}

		$form = new Wst_Form_Searchsessions();
		$form->populate($filter);

		$this->view->assign(array(
			'form' => $form,
			'paginator' => $paginator,
			'sort' => $filter['sort'],
			'direction' => $filter['direction'],
			//'centersPulledString' => $centersPulledString,
			'numberOfResults' => $paginator->getTotalItemCount()
		));
	}

	public function sessionDetailsFromApiAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Session Details');

		$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
		$helper = new Wst_Model_Helpers_General();

		$get = Zend_Registry::get("getFilter");

		$details = $webexApiHelper->getSessionDetails($get->confID, $get->center);

		// parse for only the details
		if ($details["response"]["result"] == "SUCCESS") {
			switch($get->center) {
				case "mc":
					$arrayKey = "history:meetingUsageHistory";
					$centerName = "Meeting Center";
					break;
				case "tc":
					$arrayKey = "history:trainingSessionHistory";
					$centerName = "Training Center";
					break;
				case "ec":
					$arrayKey = "history:eventSessionHistory";
					$centerName = "Event Center";
					break;
				case "sc":
					$arrayKey = "history:supportSessionHistory";
					$centerName = "Support Center";
					break;
				default:
					$arrayKey = false;
					$centerName = false;
					break;
			}

			$sessionDetails = false;

			if ($arrayKey) {
				$sessionDetails = $details["data"][$arrayKey];

				// start / end time stuff...
				$startTimeKey = (isset($sessionDetails["history:meetingStartTime"])) ? "history:meetingStartTime" : "history:sessionStartTime";
				$endTimeKey = (isset($sessionDetails["history:meetingEndTime"])) ? "history:meetingEndTime" : "history:sessionEndTime";

				// adjust start and end times from GMT
				$sessionDetails["startTimeLocal"] = $helper->convertGmtToLocal($sessionDetails[$startTimeKey]);
				$sessionDetails["endTimeLocal"] = $helper->convertGmtToLocal($sessionDetails[$endTimeKey]);

			}

			$this->view->centerName = $centerName;
			$this->view->centerAbbrev = $get->center;
			$this->view->sessionDetails = $sessionDetails;
		}


	}

	public function sessionDetailsAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Session Details');

		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();
		$helper = new Wst_Model_Helpers_General();

		$get = Zend_Registry::get("getFilter");

		$details = $sessionArchiveModel->getSessionByConfId($get->confID);

		// parse for only the details
		if ($details) {

			// adjust start and end times from GMT
			//$details["start_time_local"] = $helper->convertGmtToLocal($details["start_time_utc"]);
			//$details["end_time_local"] = $helper->convertGmtToLocal($details["end_time_utc"]);

			$this->view->sessionDetails = $details;
		}
		
		// get recordings for session
		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$recordingsFromSession = $recordingArchiveModel->getActiveRecordingsForConfId($get->confID);

		$this->view->recordingsFromSession = $recordingsFromSession;

	}

	public function sessionAttendeeDetailsFromApiAction() {

		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Session Attendee Details');

		$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
		$helper = new Wst_Model_Helpers_General();

		$get = Zend_Registry::get("getFilter");

		$details = $webexApiHelper->getSessionAttendeeHistory($get->confID, $get->center);

		// parse for only the details
		if ($details["response"]["result"] == "SUCCESS") {
			switch($get->center) {
				case "mc":
					$arrayKey = "history:meetingAttendeeHistory";
					$centerName = "Meeting Center";
					break;
				case "tc":
					$arrayKey = "history:trainingAttendeeHistory";
					$centerName = "Training Center";
					break;
				case "ec":
					$arrayKey = "history:eventAttendeeHistory";
					$centerName = "Event Center";
					break;
				case "sc":
					$arrayKey = "history:supportAttendeeHistory";
					$centerName = "Support Center";
					break;
				default:
					$arrayKey = false;
					$centerName = false;
					break;
			}

			$attendeeDetails = false;

			if ($arrayKey) {
				$dataArray = $details["data"][$arrayKey];

				if (isset($dataArray["history:confID"])) {
					// single record
					$attendeeDetails[] = $dataArray;
				} else {
					$attendeeDetails = $dataArray;
				}
			}

			//$formattedAttendeeDetails = array();
			foreach($attendeeDetails as $key => &$details) {
				// set some defaults
				if ($get->center == "mc") {
					$details["history:sessionKey"] = $details["history:meetingKey"];
					$details["history:attendeeEmail"] = $details["history:email"];
					$details["history:attendeeName"] = $details["history:name"];
					$details["history:startTime"] = $details["history:joinTime"];
					$details["history:endTime"] = $details["history:leaveTime"];
				}

				// adjust start and end times from GMT
				$details["startTimeLocal"] = $helper->convertGmtToLocal($details["history:startTime"]);
				$details["endTimeLocal"] = $helper->convertGmtToLocal($details["history:endTime"]);

				// parse out OS and browser
				$osBrowserPieces = explode(",", $details["history:clientAgent"]);

				$details["operatingSystem"] = $osBrowserPieces[0];
				$details["browser"] = $osBrowserPieces[1];

				$helper = new Wst_Model_Helpers_General();
				$details["osIcon"] = $helper->displayOsIcon($details["operatingSystem"]);
				$details["browserIcon"] = $helper->displayBrowserIcon($details["browser"]);
			}


			$sessionName = "";

			if ($get->center == "sc") {
				// no specific name
				$sessionName = "WebEx Support Center Session #{$get->confID}";
			} else if (count($attendeeDetails) > 0) {
				$sessionName = $attendeeDetails[0]["history:confName"];
			}

			$this->view->confID = $get->confID;
			$this->view->sessionName = $sessionName;
			$this->view->centerName = $centerName;
			$this->view->centerAbbrev = $get->center;
			$this->view->attendeeDetails = $attendeeDetails;
		}

	}

	public function sessionAttendeeDetailsAction() {

		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Session Attendee Details');

		$attendeeArchiveModel = new Wst_Model_Dbtable_Attendeearchive();
		$helper = new Wst_Model_Helpers_General();

		$get = Zend_Registry::get("getFilter");

		$details = $attendeeArchiveModel->getAttendeesForSessionByConfId($get->confID);
		// parse for only the details

		//dump($details,true);

		if (count($details) > 0) {
			$helper = new Wst_Model_Helpers_General();

			foreach($details as $key => $d) {

				// adjust start and end times from GMT
				//$details[$key]["join_time_local"] = $helper->convertGmtToLocal($d["join_time_utc"]);
				//$details[$key]["leave_time_local"] = $helper->convertGmtToLocal($d["leave_time_utc"]);

				// parse out OS and browser
				$osBrowserPieces = explode(",", $d["client_agent"]);

				$operating_system = $osBrowserPieces[0];
				$details[$key]["operating_system"] = $operating_system;
				$details[$key]["os_icon"] = $helper->displayOsIcon($operating_system);

				$browser = $osBrowserPieces[1];
				$details[$key]["browser"] = $browser;
				$details[$key]["browser_icon"] = $helper->displayBrowserIcon($browser);

			}

			if ($details[0]["session_name"] == "WEBEX_SUPPORT_SESSION") {
				// no specific name
				$sessionName = "WebEx Support Center Session #{$get->confID}";
			} else {
				$sessionName = $details[0]["session_name"];
			}

			$this->view->confID = $get->confID;
			$this->view->sessionName = $sessionName;
			$this->view->attendeeDetails = $details;
		}

	}

	public function sessionsInProgressAction() {
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Sessions In Progress');

		$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
		$inProgressList = $webexApiHelper->getSessionsInProgress();

		$helper = new Wst_Model_Helpers_General();

		if ($inProgressList["response"]["result"] == "SUCCESS") {
			$sessionsCombinedList = array();

			// did we get more than one service active? data format is different depending...
			$dataList = $inProgressList["data"]["ep:services"];

			// TODO - refactor this somehow.
			if (isset($dataList["ep:serviceType"])) {
				// just one center.
				$currentCenter = $dataList["ep:serviceType"];

				// now...just one meeting...or more?
				if (isset($dataList["ep:sessions"]["ep:sessionKey"])) {
					// yep, just one.
					$sessionKey = $dataList["ep:sessions"]["ep:sessionKey"];
					$sessionData = $dataList["ep:sessions"];
					$sessionData["ep:serviceType"] = $currentCenter; // appending this for our use.

					$sessionsCombinedList[$sessionKey] = $sessionData; // append to master list
				} else {
					// more than one...

					foreach($dataList["ep:sessions"] as $key => $info) {
						$sessionData = $info;
						$sessionData["ep:serviceType"] = $currentCenter;

						$sessionsCombinedList[$info["ep:sessionKey"]] = $sessionData; // append to master list
					}
				}
			} else {
				// more than one center
				foreach($dataList as $c_key => $center) {
					$currentCenter = $center["ep:serviceType"];

					// one meeting or many?
					if (isset($center["ep:sessions"]["ep:sessionKey"])) {
						// just one
						$sessionKey = $center["ep:sessions"]["ep:sessionKey"];
						$sessionData = $center["ep:sessions"];
						$sessionData["ep:serviceType"] = $currentCenter; // appending this for our use.

						$sessionsCombinedList[$sessionKey] = $sessionData; // append to master list
					} else {
						// many
						foreach($center["ep:sessions"] as $key => $info) {
							$sessionData = $info;
							$sessionData["ep:serviceType"] = $currentCenter;

							$sessionsCombinedList[$info["ep:sessionKey"]] = $sessionData; // append to master list
						}
					}
				}
			}

			// this is not needed as this endpoint returns LOCAL time. *SMH*
			//foreach($sessionsCombinedList as $k => $s) {
			//	// adjust start times from GMT
			//	$sessionsCombinedList[$k]["startTimeLocal"] = $helper->convertGmtToLocal($s["ep:startTime"]);
			//}

			$localConfig = Zend_Registry::get("localConfig");
			$perPage = $localConfig["itemsPerPage"];

			$adapter = new Zend_Paginator_Adapter_Array($sessionsCombinedList);

			$paginator = new Zend_Paginator($adapter);
			$paginator->setDefaultItemCountPerPage($perPage);
			$paginator->setCurrentPageNumber($this->_getParam('page', 1));

			$this->view->assign(array(
				'paginator' => $paginator,
				'numberOfResults' => count($sessionsCombinedList)
			));

			//dump($paginator, true);
		} else {
			$this->view->paginator = false;
			$this->view->numberOfResults = 0;
			//$this->_helper->messenger->addError("Error: API call failure - <em>{$inProgressList["response"]["message"]}</em>");
		}
	}

	public function listRecordingsAction() {

		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		// js file for sortable table
		$this->view->headScript()->appendFile($this->view->baseUrl() . '/public/scripts/ui-sortable.js');

		$this->_helper->pageTitle("Site Recordings");

		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();
		//$recordingsList = $recordingArchiveModel->getRecordingArchives();

		// search form
		if ($this->_getParam('resetButton') == 'Reset') {
			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'reporting', 'action' => 'list-recordings'), 'default', true);
		}

		$filter = array(
			'host_webex_id'     => trim($this->getParam('host_webex_id')),
			'name'              => trim($this->getParam('name')),
			'create_time_start' => trim($this->getParam('create_time_start')),
			'create_time_end'   => trim($this->getParam('create_time_end')),
			'duration_min'      => trim($this->getParam('duration_min')),
			'duration_max'      => trim($this->getParam('duration_max')),
			'size_min'          => trim($this->getParam('size_min')),
			'size_max'          => trim($this->getParam('size_max')),
			'status'            => trim($this->getParam('status')),
			'sort'              => $this->getParam('sort', 'create_time_utc'),
			'direction'         => $this->getParam('direction', 'desc')
		);

		$select = $recordingArchiveModel->getAdapter()->select();
		$select->from(array('a' => $recordingArchiveModel->info('name')));
		$select->joinLeft(array('b' => $sessionArchiveModel->info('name')), 'a.conf_id = b.conf_id');

		$select->order($filter['sort'] . ' ' . $filter['direction']);

		if ($filter['host_webex_id'] != '') {
			$where = $recordingArchiveModel->getAdapter()->quoteInto("host_webex_id = ?", $filter['host_webex_id']);
			$select->where($where);
		}

		if ($filter['name'] != '') {
			$where = $recordingArchiveModel->getAdapter()->quoteInto("name LIKE ?", "%" . $filter['name'] . "%");
			$select->where($where);
		}

		if ($filter['create_time_start'] != '') {
			$start = strtotime($filter['create_time_start']);
			$where = $recordingArchiveModel->getAdapter()->quoteInto("create_time_utc >= ?", $start);
			$select->where($where);
		}

		if ($filter['create_time_end'] != '') {
			$end = strtotime($filter['create_time_end']);
			$where = $recordingArchiveModel->getAdapter()->quoteInto("create_time_utc <= ?", $end);
			$select->where($where);
		}

		if ($filter['duration_min'] != '') {
			$where = $recordingArchiveModel->getAdapter()->quoteInto("duration_sec >= ?", $filter['duration_min'] * 60);
			$select->where($where);
		}

		if ($filter['duration_max'] != '') {
			$where = $recordingArchiveModel->getAdapter()->quoteInto("duration_sec <= ?", $filter['duration_max'] * 60);
			$select->where($where);
		}

		if ($filter['size_min'] != '') {
			$where = $recordingArchiveModel->getAdapter()->quoteInto("size_mb >= ?", $filter['size_min']);
			$select->where($where);
		}

		if ($filter['size_max'] != '') {
			$where = $recordingArchiveModel->getAdapter()->quoteInto("size_mb <= ?", $filter['size_max']);
			$select->where($where);
		}

		if ($filter['status'] != '') {
			$where = $recordingArchiveModel->getAdapter()->quoteInto("status = ?", $filter['status']);
			$select->where($where);
		}

		$localConfig = Zend_Registry::get("localConfig");

		$adapter = new Zend_Paginator_Adapter_DbSelect($select);

		$paginator = new Zend_Paginator($adapter);
		$paginator->setDefaultItemCountPerPage($localConfig["itemsPerPage"]);
		$paginator->setCurrentPageNumber($this->getParam("page", 1));

		//foreach($recordingsList as $key => $data) {
		//
		//	$original = $data["duration_sec"];
		//	$hours = floor($original / (60 * 60));
		//	$minutes = floor(($original / 60) % 60);
		//	$seconds = floor($original % 60);
		//
		//	$string = sprintf("%02d:%02d:%02d", $hours, $minutes, $seconds);
		//
		//	$recordingsList[$key]["duration_string"] = $string;
		//}
		//
		//$adapter = new Zend_Paginator_Adapter_Array($recordingsList);
		//
		//$paginator = new Zend_Paginator($adapter);
		//$paginator->setDefaultItemCountPerPage($perPage);
		//$paginator->setCurrentPageNumber($this->_getParam('page', 1));

		$form = new Wst_Form_Searchrecordings();
		$form->populate($filter);

		$this->view->assign(array(
			'form' => $form,
			'paginator' => $paginator,
			'sort' => $filter['sort'],
			'direction' => $filter['direction'],
			'numberOfResults' => $paginator->getTotalItemCount()
		));
	}
	
	public function recordingDetailsAction() {
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$get = Zend_Registry::get('getFilter');
		$recId = $get->recId;

		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$recordingDetails = $recordingArchiveModel->getRecordingById($recId);

		// parse time
		$helper = new Wst_Model_Helpers_General();
		
		$string = $helper->secondsToTime($recordingDetails["duration_sec"]);
		$recordingDetails["duration_string"] = $string;

		$this->view->recordingDetails = $recordingDetails;

		$this->_helper->pageTitle("Recording Details for recording ID $recId");
	}

	public function recordingDetailsFromApiAction() {
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$get = Zend_Registry::get('getFilter');
		$recId = $get->recId;

		$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
		$fetchResult = $webexApiHelper->getRecordingDetails($recId);

		$recordingDetails = $fetchResult["data"]["ep:recording"];

		// parse time
		$helper = new Wst_Model_Helpers_General();

		$string = $helper->secondsToTime($recordingDetails["ep:duration"]);
		$recordingDetails["duration_string"] = $string;

		$this->view->recordingDetails = $recordingDetails;

		$this->_helper->pageTitle("Recording Details (API) for recording ID $recId");
	}

	public function listMetricsAction() {
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		// js file for sortable table
		$this->view->headScript()->appendFile($this->view->baseUrl() . '/public/scripts/ui-sortable.js');

		$localConfig = Zend_Registry::get("localConfig");

		$this->_helper->pageTitle("Metrics");

		$metricsModel = new Wst_Model_Dbtable_Metrics();

		// form fields?
		if ($this->_getParam('resetButton') == 'Reset') {
			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'reporting', 'action' => 'list-metrics'), 'default', true);
		}

		$filter = array(
			'date'      => trim($this->_getParam('date')),
			'status'    => trim($this->getParam('status')),
			'sort'      => $this->getParam('sort', 'date'),
			'direction' => $this->getParam('direction', 'desc')
		);

		$select = $metricsModel->getAdapter()->select();
		$select->from($metricsModel->info('name'));

		$select->order($filter['sort'] . ' ' . $filter['direction']);

		if ($filter['date'] != '') {
			$where = $metricsModel->getAdapter()->quoteInto("date = ?", $filter['date']);
			$select->where($where);
		}

		if ($filter['status'] != '') {
			$where = $metricsModel->getAdapter()->quoteInto("status = ?", $filter['status']);
			$select->where($where);
		}

		$adapter = new Zend_Paginator_Adapter_DbSelect($select);

		$paginator = new Zend_Paginator($adapter);
		$paginator->setDefaultItemCountPerPage($localConfig["itemsPerPage"]);
		$paginator->setCurrentPageNumber($this->getParam("page", 1));


		// form stuff
		$form = new Wst_Form_Searchmetrics();
		$form->populate($filter);

		$this->view->assign(array(
			'form' => $form,
			'paginator' => $paginator,
			'sort' => $filter['sort'],
			'direction' => $filter['direction']
		));

	}

	public function metricDetailsAction() {
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$get = Zend_Registry::get("getFilter");

		$this->_helper->pageTitle("Metric details for id {$get->id}");

		$metricsModel = new Wst_Model_Dbtable_Metrics();
		$metricDetails = $metricsModel->getMetricById($get->id);

		if ($metricDetails) {
			switch($metricDetails["status"]) {
				case "pending-verification":
					$iconHtml = "<i class='fa fa-warning text-warning'></i>";
					break;
				case "verified":
					$iconHtml = "<i class='fa fa-check'></i>";
					break;
				case "sending":
					$iconHtml = "<i class='fa fa-exchange'></i>";
					break;
				case "complete":
					$iconHtml = "<i class='fa fa-check-circle text-success'></i>";
					break;
				case "error":
					$iconHtml = "<i class='fa fa-times text-error'></i>";
					break;
				default:
					$iconHtml = "";
					break;
			}

			$metricDetails["status_icon"] = $iconHtml;

			if ($metricDetails["status"] == "pending-verification") {
				$this->_helper->messenger->addWarning("<i class='fa fa-warning'></i> <b>Note</b>: these metrics have not been finalized yet.");
			}

			$metricDataDecoded = json_decode($metricDetails["rawData"], true);
			unset($metricDataDecoded["rawData"]); // don't need this.

			//$metricDetails = array_merge($metricDetails, $metricDataDecoded);
		}

		$this->view->metricDetails = $metricDetails;
		$this->view->metricDataDecoded = $metricDataDecoded;
	}

	public function retryMetricSendAction() {
		$get = Zend_Registry::get('getFilter');
		$id = $get->id;

		$metricsModel = new Wst_Model_Dbtable_Metrics();

		$updateData = array(
			"status" => 'verified',
			"error_data" => null
		);

		$result = $metricsModel->updateDailyMetric($updateData, $id);

		if ($result == 1) {
			$this->_helper->messenger->addSuccess("Success: metric entry $id reset to 'verified'.");
		} else {
			$this->_helper->messenger->addError("Error: metric entry $id not found.");
		}

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNeverRender();

		$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'reporting', 'action' => 'metric-details', 'id' => $id), 'default', true);

	}

	public function getCommunicationsListAction() {
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$get = Zend_Registry::get('getFilter');
		if (isset($get->filter)) {
			$filter = $get->filter;
		} else {
			$filter = 'active';
		}

		$userListModel = new Wst_Model_Dbtable_Userlist();

		$users = array();

		if ($filter == 'active') {
			$users = $userListModel->getActiveUsersList();

			if (count($users) > 0) {
				$users = $users->toArray();
			}

		} else if ($filter == 'with-recordings') {
			$users = $userListModel->getUsersWithActiveRecordings();
		} else if ($filter == 'with-recordings-all-time') {
			$users = $userListModel->getUsersWithRecordings();
		}

		$csv = false;
		if (isset($get->csv) && $get->csv == "y") {

			// disable rendering
			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNeverRender();

			// write to temp file
			$tmpName = tempnam(sys_get_temp_dir(), 'data');
			$file = fopen($tmpName, "w");

			$headers = array("Username", "Email", "First Name", "Last Name");
			fputcsv($file, $headers);
			foreach ($users as $u) {
				fputcsv($file, array(
					$u["webExId"],
					$u["email"],
					$u["firstName"],
					$u["lastName"]
				));
			}

			fclose($file);

			// http headers for download
			header("Content-Description: File Transfer");
			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename=\"communications-list.csv\"");
			header("Content-Transfer-Encoding: binary");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: public");
			header("Pragma: public");
			header("Content-Length: " . filesize($tmpName));
			//header("Content-Length: 10000");
			//ob_end_flush();
			ob_clean();
			flush();
			@readfile($tmpName);

			unlink($tmpName);
		}



		$this->view->users = $users;
		$this->view->filter = $filter;
	}

}