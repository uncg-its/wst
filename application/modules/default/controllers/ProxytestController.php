<?php
/**
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 *
 * This license is also available via the world-wide-web at
 * http://itdapps.ncsu.edu/bsd.txt
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to itappdev@ncsu.edu so we can send you a copy immediately.
 *
 * @package    Index_Controller
 * @category   Controller
 * @copyright  Copyright (c) 2007 NC State University Office of      
 *             Information Technology
 * @license    http://itdapps.ncsu.edu/bsd.txt  BSD License
 * @version    SVN: $Id: $
 */

/**
 * Main controller for the application
 *
 * @package    Index_Controller
 * @category   Controller
 * @copyright  Copyright (c) 2007 NC State University Office of      
 *             Information Technology
 */
class ProxytestController extends Zend_Controller_Action
{
          
    /**
     * shows the homepage
     *
     */
    public function indexAction()
    {
        $get = Zend_Registry::get('getFilter');

        $proxy = Zend_Registry::get('proxy');

        $proxyWithPort = $proxy['url'];
        if (!empty($proxy['port'])) {
            $proxyWithPort .= ":" . $proxy['port'];
        }

        $this->view->proxyUsed = $proxyWithPort;

        $urlToTest = "https://{$get->hostname}.webex.com";
        $this->view->urlTested = $urlToTest;

        $webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
        $result = $webexApiHelper->testProxy($urlToTest);

        dump($result, true);
        $this->view->result = ($result !== false);
        $this->view->rawResult = $result;
    }
}