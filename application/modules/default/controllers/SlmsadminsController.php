<?php

class SlmsadminsController extends Zend_Controller_Action {

	public function indexAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('SLMS Administrator Panel');

		// set initial display vars for bootstrap
		$this->view->panelSpan = 4; // assume 4 panels to start

		// api status check
		$appVarsModel = new Wst_Model_Dbtable_Applicationvars();
		$status = $appVarsModel->getVar("webexApiStatus");
		$this->view->apiStatus = $status;

		// api status icon
		$apiStatusIcon = ($status == "down") ? "fa-times text-error" : "fa-check text-success";
		$this->view->apiStatusIcon = $apiStatusIcon;

		// api status panel class
		$apiPanelClass = ($status == "down") ? " panel-danger" : "";
		$this->view->apiPanelClass = $apiPanelClass;

		// last feed timestamp
		$lastFeedTimestamp = $appVarsModel->getVar("lastFeedTimestamp");
		$this->view->lastFeedTime = date("m/d/Y H:i:s T", $lastFeedTimestamp);

		// recording storage usage

		$localConfig = Zend_Registry::get('localConfig');
		$siteRecordingStorageLimit = $localConfig['siteRecordingStorageLimit'];
		$this->view->siteRecordingStorageLimit = $siteRecordingStorageLimit;

		if ($siteRecordingStorageLimit > 0) {
			// only proceed if this is actually set.
			$this->view->panelSpan = 3; // change to 4 panels

			$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
			$recResult = $recordingArchiveModel->getCurrentRecordingStorageUsage();
			$recordingStorageUsage = round($recResult[0]["recording_usage"], 2);
			$this->view->recordingStorageUsage = $recordingStorageUsage;

			$percentStorageLimitUsed = round(($recordingStorageUsage / $siteRecordingStorageLimit) * 100, 2);
			$this->view->percentStorageLimitUsed = $percentStorageLimitUsed;

			$recordingPanelClass = "";
			$recordingPanelIcon = "<i class=\"fa fa-check-circle text-success\"></i>";
			if ($percentStorageLimitUsed > 80) {
				$recordingPanelClass = " panel-danger";
				$recordingPanelIcon = "<i class=\"fa fa-warning text-error\"></i>";
			} else if ($percentStorageLimitUsed > 50) {
				$recordingPanelClass = " panel-warning";
				$recordingPanelIcon = "<i class=\"fa fa-exclamation-circle text-warning\"></i>";
			}

			$this->view->recordingPanelClass = $recordingPanelClass;
			$this->view->recordingPanelIcon = $recordingPanelIcon;
		}

		// number of active hosts
		$userListModel = new Wst_Model_Dbtable_Userlist();
		$activeHosts = count($userListModel->getActiveUsersList());
		$this->view->activeHosts = $activeHosts;

		$siteActiveHostsLimit = $localConfig['siteActiveHostsLimit'];
		$this->view->siteActiveHostsLimit = $siteActiveHostsLimit;

		$percentHostsLimitUsed = round(($activeHosts / $siteActiveHostsLimit) * 100, 2);
		$this->view->percentHostsLimitUsed = $percentHostsLimitUsed;

		$hostsPanelClass = "";
		$hostsPanelIcon = "<i class=\"fa fa-check-circle text-success\"></i>";
		if ($percentHostsLimitUsed > 90) { // TODO - configurable vars for these values, here and above
			$hostsPanelClass = " panel-danger";
			$hostsPanelIcon = "<i class=\"fa fa-warning text-error\"></i>";
		} else if ($percentHostsLimitUsed > 75) {
			$hostsPanelClass = " panel-warning";
			$hostsPanelIcon = "<i class=\"fa fa-exclamation-circle text-warning\"></i>";
		}

		$this->view->hostsPanelClass = $hostsPanelClass;
		$this->view->hostsPanelIcon = $hostsPanelIcon;

	}
}