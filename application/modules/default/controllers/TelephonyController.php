<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 4/22/16
 * Time: 3:23 PM
 */
class TelephonyController extends Zend_Controller_Action {

	public function indexAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Manage Account Telephony');

		$configModel = new Wst_Model_Dbtable_Config();

		// set initial display vars for bootstrap
		$this->view->panelSpan = 3; // assume 4 panels to start

		// number of users with telephony privileges
		$userListModel = new Wst_Model_Dbtable_Userlist();
		$usersWithTelephony = $userListModel->getUsersWithTelephonyPrivileges();

		$this->view->usersWithTelephony = $usersWithTelephony;

		// this month's "billing period" for telephony
		$telephonyResetDay = $configModel->getVar('telephonyResetDay');

		if (!$telephonyResetDay || $telephonyResetDay == 0) { $telephonyResetDay = 1; }
		if ($telephonyResetDay < 10) {
			$telephonyResetDay = "0$telephonyResetDay";
		}

		$now = time();
		//$now = strtotime("2016-02-01");

		$adjustedThisMonth = date("m/$telephonyResetDay/Y", $now);
		$adjustedLastMonth = date("m/$telephonyResetDay/Y", strtotime("-1 month", strtotime($adjustedThisMonth)));
		$adjustedNextMonth = date("m/$telephonyResetDay/Y", strtotime("+1 month", strtotime($adjustedThisMonth)));

		if (strtotime($adjustedThisMonth) <= time()) {
			$startOfBillingMonth = $adjustedThisMonth;
			$endOfBillingMonth = $adjustedNextMonth;
		} else {
			$startOfBillingMonth = $adjustedLastMonth;
			$endOfBillingMonth = $adjustedThisMonth;
		}

		$endOfBillingMonth = date("m/d/Y", strtotime("-1 days", strtotime($endOfBillingMonth)));

		$billingPeriodString = $startOfBillingMonth . " - " . $endOfBillingMonth;
		$this->view->billingPeriodString = $billingPeriodString;

		// monthly quota used this month / total quota

		// quota
		$monthlyTelephonyBudget = $configModel->getVar("monthlyTelephonyBudget");
		if (!$monthlyTelephonyBudget) {
			$monthlyTelephonyBudget = 0;
		}

		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();
		$sessionsWithTelephony = $sessionArchiveModel->getTelephonyUsageBetweenTimestamps(strtotime($startOfBillingMonth), strtotime($endOfBillingMonth));

		//dump($sessionsWithTelephony);

		$tollMinutesUsed = 0;
		// TODO - consider adding toll free?
		foreach ($sessionsWithTelephony as $index => $session) {
			$tollMinutesUsed += $session["call_in_minutes"];
		}

		// budget used
		$tollMinuteCost = $configModel->getVar('tollMinuteCost');
		// TODO - maybe some notifications if the vars aren't set?
		$budgetUsed = round($tollMinutesUsed * $tollMinuteCost, 2);

		$pctBudgetUsed = $budgetUsed / $monthlyTelephonyBudget * 100;

		$this->view->budgetUsed = $budgetUsed;
		$this->view->tollMinutesUsed = $tollMinutesUsed;
		$this->view->monthlyTelephonyBudget = $monthlyTelephonyBudget;
		$this->view->pctBudgetUsed = $pctBudgetUsed;

		// budget icon
		$budgetPanelClass = "";
		$budgetPanelIcon = "<i class=\"fa fa-check-circle text-success\"></i>";
		if ($pctBudgetUsed > 80) {
			$budgetPanelClass = " panel-danger";
			$budgetPanelIcon = "<i class=\"fa fa-warning text-error\"></i>";
		} else if ($pctBudgetUsed > 50) {
			$budgetPanelClass = " panel-warning";
			$budgetPanelIcon = "<i class=\"fa fa-exclamation-circle text-warning\"></i>";
		}

		$this->view->budgetPanelClass = $budgetPanelClass;
		$this->view->budgetPanelIcon = $budgetPanelIcon;

		// number of sessions this month that used telephony
		$this->view->sessionsWithTelephonyThisBillingPeriod = count($sessionsWithTelephony);


	}

	public function addTelephonyAction() {
		// logs...
		$logModel = new Wst_Model_Dbtable_Log();

		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Add Telephony for User');

		$form = new Wst_Form_Addtelephony();
		$this->view->form = $form;

		if ($this->getRequest()->isPost()) {
			if ($form->isValid($this->getRequest()->getPost())) {

				// add the telephony in WebEx
				$username = $form->getValue("webExId");
				$privs = $form->getValue('telephonyPrivileges');
				$customTrackingCode = $form->getValue('customTrackingCode');

				$logModel->addLogMessage('Action Processing', "Adding telephony privileges for user '$username'. Privs: " . implode(",",$privs), 1, 'INFO', 'controller', __FUNCTION__);

				$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
				$addTelephonyResult = $webexApiHelper->editTelephonyForUser($username, $privs); // not overriding.
				// tracking code
				$addCustomTracking = $webexApiHelper->addTrackingCodesToUser($username, array("3" => $customTrackingCode)); // TODO - make sure this worked right.

				if ($addTelephonyResult["response"]["result"] == "SUCCESS") {
					$logModel->addLogMessage('Action Result', "Success adding privileges in WebEx.", 1, 'INFO', 'controller', __FUNCTION__);
					// update local user list
					$userListModel = new Wst_Model_Dbtable_Userlist();
					$updateLocalUserResult = $userListModel->updateTelephonyForUser($username, $privs);
					$updateLocalUserTrackingResult = $userListModel->updateCustomTrackingCode($username, $customTrackingCode); // TODO - make sure this worked right.
					if (is_numeric($updateLocalUserResult)) {
						$logModel->addLogMessage('Action Result', "Success updating local user table.", 1, 'INFO', 'controller', __FUNCTION__);
						$this->_helper->messenger->addSuccess("Telephony successfully added for user '$username'");
					} else {
						$logModel->addLogMessage('Action Result', "ERROR updating local user list.", 2, 'WARNING', 'controller', __FUNCTION__);
						$this->_helper->messenger->addError("ERROR: Telephony added in WebEx, but local user list not updated properly!");
					}
				} else {
					$logModel->addLogMessage('Action Result', "ERROR adding privileges in WebEx.", 1, 'INFO', 'controller', __FUNCTION__);
					$logModel->addLogMessage('Action Raw Result', json_encode($addTelephonyResult), 1, 'INFO', 'controller', __FUNCTION__);
					$this->_helper->messenger->addError("ERROR: Could not update telephony in WebEx. User is unchanged.");
				}

				$this->_helper->redirector->gotoRoute(array("controller" => "telephony", "action" => "index"));

			}
		}

	}

	public function manageUsersAction() {

		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Manage Users with Telephony Privileges');

		// show users with telephony.
		$userListModel = new Wst_Model_Dbtable_Userlist();

		// helper
		$helper = new Wst_Model_Helpers_General();

		//$this->view->usersWithTelephony = $usersWithTelephony;
		//$this->view->numberOfResults = count($usersWithTelephony);

		// search form
		if ($this->_getParam('resetButton') == 'Reset') {
			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'telephony', 'action' => 'manage-users'), 'default', true);
		}

		$filter = array(
			'webExId'              => trim($this->_getParam('webExId')),
			'lastName'             => trim($this->getParam('lastName')),
			'firstName'            => trim($this->getParam('firstName')),
			'telephony_privileges' => trim($this->getParam('telephony_privileges')),
			'sort'                 => $this->getParam('sort', 'webExId'),
			'direction'            => $this->getParam('direction', 'asc')
		);

		$select = $userListModel->getAdapter()->select();
		$select->from($userListModel->info('name'));
		$where = $userListModel->getAdapter()->quoteInto("telephony_privileges IS NOT NULL", "");
		$select->where($where);

		$select->order($filter['sort'] . ' ' . $filter['direction']);

		if ($filter['webExId'] != '') {
			$where = $userListModel->getAdapter()->quoteInto("webExId LIKE ?", "%" . $filter['webExId'] . "%");
			$select->where($where);
		}

		if ($filter['lastName'] != '') {
			$where = $userListModel->getAdapter()->quoteInto("lastName LIKE ?", "%" . $filter['lastName'] . "%");
			$select->where($where);
		}

		if ($filter['firstName'] != '') {
			$where = $userListModel->getAdapter()->quoteInto("firstName LIKE ?", "%" . $filter['firstName'] . "%");
			$select->where($where);
		}

		if ($filter['telephony_privileges'] != '') {
			$where = $userListModel->getAdapter()->quoteInto("telephony_privileges LIKE ?", "%" . $filter['telephony_privileges'] . "%");
			$select->where($where);
		}

		//$meetingsList = $userListModel->getSessionsList("all");

		$localConfig = Zend_Registry::get("localConfig");

		$adapter = new Zend_Paginator_Adapter_DbSelect($select);

		$paginator = new Zend_Paginator($adapter);
		$paginator->setDefaultItemCountPerPage($localConfig["itemsPerPage"]);
		$paginator->setCurrentPageNumber($this->getParam("page", 1));

		$form = new Wst_Form_Searchtelephonyusers();
		$form->populate($filter);

		$this->view->assign(array(
			'form' => $form,
			'paginator' => $paginator,
			'sort' => $filter['sort'],
			'direction' => $filter['direction'],
			'telephonyKeys' => $helper->telephonyKeys,
			'numberOfResults' => $paginator->getTotalItemCount()
		));
	}


	public function editTelephonyAction() {
		// logs...
		$logModel = new Wst_Model_Dbtable_Log();

		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Edit Telephony for User');

		$get = Zend_Registry::get('getFilter');

		// existing data
		$userListModel = new Wst_Model_Dbtable_Userlist();
		$user = $userListModel->getUserByWebExId($get->webExId);
		$userTelephony = $user["telephony_privileges"];

		$trackingCodes = json_decode($user["custom_tracking"], true);
		$customTrackingCode = (isset($trackingCodes["3"])) ? $trackingCodes["3"] : "";

		$form = new Wst_Form_Edittelephony();
		$form->getElement("telephonyPrivileges")->setValue(explode(",", $userTelephony));
		$form->getElement("customTrackingCode")->setValue($customTrackingCode);

		if ($this->getRequest()->isPost()) {
			if ($form->isValid($this->getRequest()->getPost())) {

				// edit the telephony in WebEx
				$username = $get->webExId;
				$privs = $form->getValue('telephonyPrivileges');
				$customTrackingCode = $form->getValue('customTrackingCode');

				$logModel->addLogMessage('Action Processing', "Editing telephony privileges for user '$username'. Privs: " . implode(",",$privs), 1, 'INFO', 'controller', __FUNCTION__);

				$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
				$editTelephonyResult = $webexApiHelper->editTelephonyForUser($username, $privs, true); // overriding.
				// tracking code
				$addCustomTracking = $webexApiHelper->addTrackingCodesToUser($username, array("3" => $customTrackingCode)); // TODO - make sure this worked right.

				if ($editTelephonyResult["response"]["result"] == "SUCCESS") {
					$logModel->addLogMessage('Action Result', "Success editing privileges in WebEx.", 1, 'INFO', 'controller', __FUNCTION__);
					// update local user list
					$updateLocalUserResult = $userListModel->updateTelephonyForUser($username, $privs);
					$updateLocalUserTrackingResult = $userListModel->updateCustomTrackingCode($username, $customTrackingCode); // TODO - make sure this worked right.
					if (is_numeric($updateLocalUserResult)) {
						$logModel->addLogMessage('Action Result', "Success updating local user table.", 1, 'INFO', 'controller', __FUNCTION__);
						$this->_helper->messenger->addSuccess("Telephony successfully added for user '$username'");
					} else {
						$logModel->addLogMessage('Action Result', "ERROR updating local user list.", 2, 'WARNING', 'controller', __FUNCTION__);
						$this->_helper->messenger->addError("ERROR: Telephony edited in WebEx, but local user list not updated properly!");
					}
				} else {
					$logModel->addLogMessage('Action Result', "ERROR adding privileges in WebEx.", 1, 'INFO', 'controller', __FUNCTION__);
					$logModel->addLogMessage('Action Raw Result', json_encode($editTelephonyResult), 1, 'INFO', 'controller', __FUNCTION__);
					$this->_helper->messenger->addError("ERROR: Could not update telephony in WebEx. User is unchanged.");
				}

				$this->_helper->redirector->gotoRoute(array("controller" => "telephony", "action" => "manage-users"));

			}
		}

		$this->view->form = $form;
		$this->view->webExId = $get->webExId;
	}

	public function reportingSummaryAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/scripts/datepicker/css/datepicker.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/scripts/datepicker/css/datepicker.css');
		$this->view->headScript()->appendFile(
			$this->view->baseUrl() . '/public/scripts/datepicker/js/bootstrap-datepicker.js',
			'text/javascript'
		);

		$this->_helper->pageTitle('Telephony Reporting Summary by Tracking Code');

		// search form
		if ($this->_getParam('resetButton') == 'Reset') {
			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'telephony', 'action' => 'reporting-summary'), 'default', true);
		}

		$startTimeUtcRaw = $this->getParam('start_time_start');
		$endTimeUtcRaw = $this->getParam('start_time_end');

		$trackingCodeCategory = trim($this->_getParam('trackingCodeCategory'));
		$startTimeUtc = strtotime(trim($startTimeUtcRaw));
		$endTimeUtc = strtotime(trim($endTimeUtcRaw));

		$filter = array(
			'trackingCodeCategory' => $trackingCodeCategory,
			"start_time_start" => empty($startTimeUtcRaw) ? "" : date("m/d/Y", $startTimeUtc),
			"start_time_end" => empty($endTimeUtcRaw) ? "" : date("m/d/Y", $endTimeUtc)
		);

		// gotta do this manually. ugh.


		$emptyStart = false;
		$emptyEnd = false;
		// which sessions to fetch?
		if (empty($startTimeUtc)) {
			$startTimeUtc = 0;
			$emptyStart = true;
		}

		if (empty($endTimeUtc)) {
			$endTimeUtc = time();
			$emptyEnd = true;
		}
		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();
		$sessionsWithTrackingCodes = $sessionArchiveModel->getArchivesBetweenTimestamps($startTimeUtc, $endTimeUtc);

		//dump($sessionsWithTrackingCodes, true);
		$trackingCodesModel = new Wst_Model_Dbtable_Trackingcodes();
		$trackingCodes = $trackingCodesModel->getTrackingCodesById();

		// get the sessions arranged by tracking code. array("code category" => array("code value" => array("sessions" => value, "sessionsWithTelephony" => value)))
		$sessionsArranged = array();
		foreach($trackingCodes as $id => $codeInfo) {
			$sessionsArranged[$id] = array(
				"categoryName" => $codeInfo["display_name"]
			);
		}

		foreach ($sessionsWithTrackingCodes as $k => $session) {
			$sessionTrackingCodes = json_decode($session["tracking_codes"], true);
			$hasTelephony = ($session["call_in_participants"] > 0 || $session["call_out_participants"] > 0);
			$sessionTypeKey = $hasTelephony ? "sessionsWithTelephony" : "sessions";
			if (count($sessionTrackingCodes) > 0) {
				foreach ($sessionTrackingCodes as $code => $value) {
					if (!isset($sessionsArranged[$code]["data"][$value][$sessionTypeKey])) {
						$sessionsArranged[$code]["data"][$value][$sessionTypeKey] = 0; // init
						if ($sessionTypeKey == "sessionsWithTelephony") {
							// init
							$sessionsArranged[$code]["data"][$value]["telephonyMinutes"] = 0;
						}
					}

					$sessionsArranged[$code]["data"][$value][$sessionTypeKey] += 1;
					if ($sessionTypeKey == "sessionsWithTelephony") {
						$sessionsArranged[$code]["data"][$value]["telephonyMinutes"] += ($session["call_in_minutes"] + $session["call_in_tollfree_minutes"] + $session["call_out_domestic_minutes"] + $session["call_out_international_minutes"]);
					}
				}
			}
		}

		// filter if necessary

		if (!empty($trackingCodeCategory)) {
			foreach ($sessionsArranged as $id => $compiledInfo) {
				if ($id != $trackingCodeCategory) {
					unset($sessionsArranged[$id]);
				}
			}
		}

		// date range
		if ($emptyStart) {
			if ($emptyEnd) {
				$dateRange = "all sessions";
			} else {
				$dateRange = "up to $endTimeUtcRaw";
			}
		} else if ($emptyEnd) {
			$dateRange = "$startTimeUtcRaw onward";
		} else {
			$dateRange = "$startTimeUtcRaw through $endTimeUtcRaw";
		}


		$form = new Wst_Form_Reportingbytrackingcode();
		$form->populate($filter);

		$this->view->assign(array(
			"form"           => $form,
			"sessions"       => $sessionsArranged,
			"dateRange"      => $dateRange,
			"startTimestamp" => $emptyStart ? 0 : $startTimeUtc,
			"endTimestamp"   => $emptyEnd ? time() : $endTimeUtc
		));

	}

	public function trackingCodeDetailsAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/scripts/datepicker/css/datepicker.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/scripts/datepicker/css/datepicker.css');
		$this->view->headScript()->appendFile(
			$this->view->baseUrl() . '/public/scripts/datepicker/js/bootstrap-datepicker.js',
			'text/javascript'
		);

		$get = Zend_Registry::get('getFilter');
		$trackingId = $get->codeId;
		$trackingValue = $get->code;
		$startTime = $get->startTimestamp;
		$endTime = $get->endTimestamp;
		$telephony = $get->telephony;

		$trackingCodeModel = new Wst_Model_Dbtable_Trackingcodes();
		$codeDetails = $trackingCodeModel->getCodeInformationById($trackingId);

		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();
		$sessionsInDateRange = $sessionArchiveModel->getArchivesBetweenTimestamps($startTime, $endTime);

		$sessionsFiltered = array();
		foreach ($sessionsInDateRange as $index => $session) {
			// tracking code
			$codes = json_decode($session["tracking_codes"], true);
			if (isset($codes[$trackingId]) && $codes[$trackingId] == $trackingValue) {
				// telephony
				$sessionHasTelephony = ($session["call_in_participants"] > 0 || $session["call_out_participants"] > 0) ? "y" : "n";
				if ($sessionHasTelephony == $telephony) {
					$sessionsFiltered[] = $session;
				}
			}
		}


		$this->_helper->pageTitle('Telephony Reporting Details');





		$this->view->assign(array(
			"startTime" => $startTime,
			"endTime" => $endTime,
			"telephony" => $telephony,
			"codeDetails" => $codeDetails,
			"codeValue" => $trackingValue,
			"sessions" => $sessionsFiltered
		));

	}

	public function usersByTrackingCodeAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$get = Zend_Registry::get('getFilter');
		if (isset($get->codeId)) {
			if (isset($get->codeValue)) {
				// we have a code ID and value- display user list
				$userListModel = new Wst_Model_Dbtable_Userlist();
				$usersByCode = $userListModel->getUsersByTrackingCode($get->codeId);

				$usersWithCode = isset($usersByCode[$get->codeValue]) ? $usersByCode[$get->codeValue] : null;

				// tracking code info
				$trackingCodeModel = new Wst_Model_Dbtable_Trackingcodes();
				$codeInfo = $trackingCodeModel->getCodeInformationById($get->codeId);

				$this->view->code = $codeInfo;
				$this->view->codeValue = $get->codeValue;
				$this->view->usersWithCode = $usersWithCode;
				$this->view->displayMode = "users";
			} else {
				// we have just a code ID - show summary and allow user to filter deeper
				$userListModel = new Wst_Model_Dbtable_Userlist();
				$usersWithCode = $userListModel->getUsersByTrackingCode($get->codeId);

				// tracking code info
				$trackingCodeModel = new Wst_Model_Dbtable_Trackingcodes();
				$codeInfo = $trackingCodeModel->getCodeInformationById($get->codeId);

				$this->view->code = $codeInfo;
				$this->view->usersByTrackingCode = $usersWithCode;
				$this->view->displayMode = "summary";
			}
		} else {
			// get tracking codes first, display them to select.
			$trackingCodeModel = new Wst_Model_Dbtable_Trackingcodes();
			$codes = $trackingCodeModel->getTrackingCodesById();

			$this->view->codes = $codes;
			$this->view->displayMode = "codes";
		}

		$this->_helper->pageTitle('Users By Tracking Code');
	}

}