<?php

class MaintenanceController extends Zend_Controller_Action {

	public function indexAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('WST Maintenance');

	}

	public function disableAppAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Disable App');

		// get core cron jobs
		$localConfig = Zend_Registry::get('localConfig');
		$coreCronJobs = array_map("trim", explode(",", $localConfig["coreCronJobs"]));

		// get currently disabled cronjobs so that we skip them
		$cronjobsModel = new Wst_Model_Dbtable_Cronjobs();
		$alreadyDisabledJobs = $cronjobsModel->getCoreCronjobsWithStatus("disabled");

		// compute which need disabling
		$jobsToDisable = array_diff($coreCronJobs, $alreadyDisabledJobs);

		sort($jobsToDisable);
		sort($alreadyDisabledJobs);

		$this->view->jobsToDisable = $jobsToDisable;
		$this->view->alreadyDisabledJobs = $alreadyDisabledJobs;

		//dump($jobsToDisable, true);


		if ($this->getRequest()->isPost()) {
			// log model
			$logModel = new Wst_Model_Dbtable_Log();
			$logModel->addLogMessage('Function Start', 'disableAppAction', 1, 'INFO', 'controller', 'disableAppAction');

			$postData = $_POST;

			if (isset($postData["confirm"]) && $postData["confirm"] == "confirm") {
				$logModel->addLogMessage('Function Processing', "Found " . count($jobsToDisable) . " cronjob(s) to disable, out of " . count($coreCronJobs) . " core cronjob(s) total. " . count($alreadyDisabledJobs) . " core cronjob(s) already disabled.", 1, 'INFO', 'controller', 'disableAppAction');

				$disabledJobs = array();
				foreach($jobsToDisable as $c) {
					$result = $cronjobsModel->disableCronjob($c);

					if ($result == 1) {
						$disabledJobs[] = $c;
					}
				}

				//dump($disabledJobs, true);

				if (count($disabledJobs) == count($jobsToDisable)) {
					$this->_helper->messenger->addSuccess("<strong>Success</strong>: all core cronjobs disabled successfully.");
					$logModel->addLogMessage('Function Result', count($disabledJobs) . ' cronjob(s) disabled successfully. No unexpected errors.', 1, 'INFO', 'controller', 'disableAppAction');
				} else {
					$notDisabledJobs = array_diff($jobsToDisable, $disabledJobs);
					$notDisabledJobsString = implode("</li><li>", $notDisabledJobs);
					$logModel->addLogMessage('Function Result', count($disabledJobs) . ' cronjob(s) disabled successfully.' . count($notDisabledJobs) . ' cronjob(s) failed to deactivate.', 2, 'WARNING', 'controller', 'disableAppAction');
					$this->_helper->messenger->addWarning("<strong>Warning</strong>: some cronjobs were not disabled due to a query error, possibly because they are already inactive: <ul><li>$notDisabledJobsString</li></ul>");
				}
			}

			$logModel->addLogMessage('Function End', 'disableAppAction', 1, 'INFO', 'controller', 'disableAppAction');

			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNeverRender();

			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'maintenance', 'action' => 'index'), 'default', true);

		}
	}

	public function reenableAppAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('Re-Enable App');

		// get core cron jobs
		$localConfig = Zend_Registry::get('localConfig');
		$coreCronJobs = array_map("trim", explode(",", $localConfig["coreCronJobs"]));

		// get currently enabled cronjobs so that we skip them
		$cronjobsModel = new Wst_Model_Dbtable_Cronjobs();
		$alreadyEnabledJobs = $cronjobsModel->getCoreCronjobsWithStatus("enabled");

		// compute which need enabling
		$jobsToEnable = array_diff($coreCronJobs, $alreadyEnabledJobs);

		sort($jobsToEnable);
		sort($alreadyEnabledJobs);

		$this->view->jobsToEnable = $jobsToEnable;
		$this->view->alreadyEnabledJobs = $alreadyEnabledJobs;

		//dump($jobsToEnable, true);


		if ($this->getRequest()->isPost()) {
			// log model
			$logModel = new Wst_Model_Dbtable_Log();
			$logModel->addLogMessage('Function Start', 'reenableAppAction', 1, 'INFO', 'controller', 'reenableAppAction');

			$postData = $_POST;

			if (isset($postData["confirm"]) && $postData["confirm"] == "confirm") {
				$logModel->addLogMessage('Function Processing', "Found " . count($jobsToEnable) . " cronjob(s) to re-enable, out of " . count($coreCronJobs) . " core cronjob(s) total. " . count($alreadyEnabledJobs) . " core cronjob(s) already re-enabled.", 1, 'INFO', 'controller', 'reenableAppAction');

				$enabledJobs = array();
				foreach($jobsToEnable as $c) {
					$result = $cronjobsModel->enableCronjob($c);

					if ($result == 1) {
						$enabledJobs[] = $c;
					}
				}

				//dump($enabledJobs, true);

				if (count($enabledJobs) == count($jobsToEnable)) {
					$this->_helper->messenger->addSuccess("<strong>Success</strong>: all core cronjobs enabled successfully.");
					$logModel->addLogMessage('Function Result', count($enabledJobs) . ' cronjob(s) re-enabled successfully. No unexpected errors.', 1, 'INFO', 'controller', 'reenableAppAction');
				} else {
					$notEnabledJobs = array_diff($jobsToEnable, $enabledJobs);
					$notEnabledJobsString = implode("</li><li>", $notEnabledJobs);
					$logModel->addLogMessage('Function Result', count($enabledJobs) . ' cronjob(s) re-enabled successfully.' . count($notEnabledJobs) . ' cronjob(s) failed to re-enable.', 2, 'WARNING', 'controller', 'reenableAppAction');
					$this->_helper->messenger->addWarning("<strong>Warning</strong>: some cronjobs were not enabled due to a query error, possibly because they are already active: <ul><li>$notEnabledJobsString</li></ul>");
				}

				$logModel->addLogMessage('Function End', 'reenableAppAction', 1, 'INFO', 'controller', 'reenableAppAction');
			}

			$this->_helper->layout->disableLayout();
			$this->_helper->viewRenderer->setNeverRender();

			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'maintenance', 'action' => 'index'), 'default', true);

		}
	}
}