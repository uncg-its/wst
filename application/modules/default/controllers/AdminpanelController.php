<?php

class AdminpanelController extends Zend_Controller_Action {

	public function indexAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('WST Admin Panel');

	}

	public function configureAction() {

		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$configModel = new Wst_Model_Dbtable_Config();

		// var names must match form field keys
		$configVarNames = array(
			'recordingDeletionWindow',
			'localRecordingCacheCleaningWindow',
			'arfSizeDifferenceThresholdMegabytes',
			'arfDownloadRetries',
			'arfUploadRetries',
			'arfDeleteRetries',
			'recordingUploadFolderName',
			'downloadLinkReplacement',
			'myRecordingsLinkReplacement',
			'earliestDeleteTimestamp',
			'tollMinuteCost',
			'monthlyTelephonyBudget',
			'telephonyResetDay',
			'defaultTelephonyPrivileges',
			'telephonyPrivilegeOptions',
			'telephonyUsageThreshold',
			'storageUsageThreshold',
			'overrideEmailIntervals'
		);

		foreach($configVarNames as $item) {
			$$item = $configModel->find($item);
		}

		$form = new Wst_Form_Wstconfig();

		if ($this->getRequest()->isPost()) {

			if ($form->isValid($this->getRequest()->getPost())) {

				foreach($configVarNames as $item) {

					$newValue = $form->getValue($item);

					if(is_array($newValue)) {
						$newValue = implode(',',$form->getValue($item));
					}

					$configModel->setVar($item, $newValue);

				}

				$this->_helper->messenger->addSuccess('Configuration Changed');
				$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'adminpanel', 'action' => 'configure'), 'default', true);

			}

		}

		$arrayElementTypes = array(
			'Zend_Form_Element_Multiselect',
			'Zend_Form_Element_MultiCheckbox',
			'Twitter_Bootstrap_Form_Element_MultiCheckbox' // need to add this for it to work properly.
		);

		foreach($configVarNames as $item) {

			$thisType = $form->getElement($item)->getType();
			//echo $thisType;
			if(!empty($$item)) {

				//dump($thisType);

				if(in_array($thisType, $arrayElementTypes)) {

					$form->getElement($item)->setValue(explode(',', $$item->var_value));
				} else {
					$form->getElement($item)->setValue($$item->var_value);
				}

				$this->view->$item = $$item->var_value;
			}

		}

		$this->view->form = $form;

		$this->_helper->pageTitle('Configure WST Variables');
	}
}