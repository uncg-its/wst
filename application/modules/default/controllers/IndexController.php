<?php
/**
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 *
 * This license is also available via the world-wide-web at
 * http://itdapps.ncsu.edu/bsd.txt
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to itappdev@ncsu.edu so we can send you a copy immediately.
 *
 * @package    Index_Controller
 * @category   Controller
 * @copyright  Copyright (c) 2007 NC State University Office of      
 *             Information Technology
 * @license    http://itdapps.ncsu.edu/bsd.txt  BSD License
 * @version    SVN: $Id: $
 */

/**
 * Main controller for the application
 *
 * @package    Index_Controller
 * @category   Controller
 * @copyright  Copyright (c) 2007 NC State University Office of      
 *             Information Technology
 */
class IndexController extends Zend_Controller_Action
{
          
    /**
     * shows the homepage
     *
     */
    public function indexAction()
    {
	    // TODO - switch this to be a separate page instead?
	    $get = Zend_Registry::get('getFilter');
	    $comingFromWebEx = $get->signOnError;

	    $this->view->showModal = false;
	    if (!is_null($comingFromWebEx) && $comingFromWebEx == "y") {
		    $ec = $get->EC;

		    $helper = new Wst_Model_Helpers_General();

		    $this->view->errorCode = $ec;
		    $this->view->errorMessage = $helper->getWebexErrorMessageFromCode($ec);
		    $this->view->showModal = true;
	    }

	    // add page styles
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => './public/css/overrides.css'), 'APPEND')->appendStylesheet('./public/css/overrides.css');
	    $this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

        $localConfig = Zend_Registry::get('localConfig');
        $this->view->localConfig = $localConfig;

        // check if logged in first
        $loggedInUser = Zend_Auth::getInstance()->getIdentity();
        if(!empty($loggedInUser)) {
            
            $logModel = new Wst_Model_Dbtable_Log();
            //$logModel->addLogMessage('Action Start', 'indexAction', 1, 'INFO', 'controller', 'index');
            
            $username = Zend_Auth::getInstance()->getIdentity()->username;


	        // check type of instance of WST
	        if ($localConfig['userSourceType'] == 'signup') {

		        // TESTING
		        /*$this->view->message = "We are testing";
				$this->view->successStatus = true;
				$this->view->eligibilityStatus = true;
				$this->view->ldapUser = array(
					'status' => true,
					'data' => true, // hack for testing
					'bindStatus' => true
				);*/


		        $logModel->addLogMessage('Action Processing', 'Source Type: Signup. Line: ' . __LINE__, 1, 'INFO', 'controller', 'index');

		        $userListModel = new Wst_Model_Dbtable_Userlist(); // prep for user list operations

		        // this is a signup instance, so take the username directly from
		        // the authenticated user, and check eligibility etc


		        //lookup user in LDAP
		        $lookupModel = new Wst_Model_Helpers_Lookup();


		        $eligibilityStatusResult = $lookupModel->getUserEligibility(array($username));

		        $eligibilityStatus = $eligibilityStatusResult["data"][$username]["eligible"];

                if ($eligibilityStatusResult["data"][$username]["ldapSuccess"]) {
                    $this->view->ldapUser = $eligibilityStatusResult["data"][$username]["rawData"];
                }

		        $this->view->eligibilityStatus = $eligibilityStatus;

		        if ($eligibilityStatus) {
		        	$thisLdapUser = $eligibilityStatusResult["data"][$username]["rawData"];
					//$this->view->ldapUser = $thisLdapUser;
			        //dump($thisLdapUser, true);

			        $logModel->addLogMessage('Action Processing', 'Eligibility Status true. Line: ' . __LINE__, 1, 'INFO', 'controller', 'index');
			        // now check webex and create/reactivate if necessary
			        $webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
			        $thisUserWebexStatus = $webexApiHelper->getWebexUserStatus($username);

			        $logModel->addLogMessage('Action Processing', 'Webex user status. Line: ' . __LINE__ . '. Data: ' . json_encode($thisUserWebexStatus), 1, 'INFO', 'controller', 'index');

			        $successStatus = true;
			        if ($thisUserWebexStatus['userExistsInWebEx'] && $thisUserWebexStatus['userActiveInWebEx']) {
				        // user exists in webex, and the user is active, so there's nothing for us to do.
				        $logModel->addLogMessage('Action Processing', 'User exists in webex and is active. Line: ' . __LINE__, 1, 'INFO', 'controller', 'index');
				        $message = 'You are already eligible and active in webex.';
			        } elseif ($thisUserWebexStatus['userExistsInWebEx'] && !$thisUserWebexStatus['userActiveInWebEx']) {
				        // user exists in webex, but is inactive
				        // activate user now
				        $logModel->addLogMessage('Action Processing', 'User exists in webex but is not active. Line: ' . __LINE__, 1, 'INFO', 'controller', 'index');
				        $attributes = array(
					        //'firstName' => $thisLdapUser['data']['uncgpreferredgivenname'],
					        //'lastName' => $thisLdapUser['data']['uncgpreferredsurname'],
					        //'email' => $thisLdapUser['data']['uncgemail'],
				        );
				        $apiCallStatus = $webexApiHelper->editWebexUser($username, $attributes);
				        if ($apiCallStatus["response"]["result"] == "SUCCESS") {
					        $logModel->addLogMessage('Action Processing', 'Edit user api call success. Line: ' . __LINE__, 1, 'INFO', 'controller', 'index');
					        $message = 'Your WebEx account has been re-activated and is ready for you to use.';

					        $dbEditResult = $userListModel->reactivateUserInUserList($thisLdapUser['data']['uidnumber']);

				        } else {
					        $logModel->addLogMessage('Action Processing', 'Edit user api call failure. Line: ' . __LINE__, 1, 'INFO', 'controller', 'index');
					        $successStatus = false;
					        $message = 'An error occurred during the re-activation of your WebEx account.';
				        }
			        } elseif (!$thisUserWebexStatus['userExistsInWebEx']) {
				        // user does not exist in webex
				        // create user now
				        $logModel->addLogMessage('Action Processing', 'User does not exist in webex. Line: ' . __LINE__, 1, 'INFO', 'controller', 'index');
				        $apiCallStatus = $webexApiHelper->addWebexUser($thisLdapUser['data']['uncgpreferredgivenname'], $thisLdapUser['data']['uncgpreferredsurname'], $username, $thisLdapUser['data']['uncgemail']);
				        if ($apiCallStatus["response"]["result"] == "SUCCESS") {
					        $logModel->addLogMessage('Action Processing', 'Add user api call success. Line: ' . __LINE__, 1, 'INFO', 'controller', 'index');
					        $message = 'Your WebEx account is ready for you to use now.';

					        $addedUserId = $apiCallStatus["data"]["use:userId"];

					        $dbAddResult = $userListModel->addNewUserToUserList($thisLdapUser['data']['uidnumber'], $username, $thisLdapUser['data']['uncgpreferredgivenname'], $thisLdapUser['data']['uncgpreferredsurname'], $thisLdapUser['data']['uncgemail'], $thisLdapUser['data']['departmentnumber'], $thisLdapUser['data']['uncgorgnshortname'], $addedUserId);

					        $this->_redirect($localConfig['webexLoginUrl']);
				        } else {
					        $logModel->addLogMessage('Action Processing', 'Add user api call failure. Message: ' . $apiCallStatus["response"]["message"] . ' Line: ' . __LINE__, 1, 'INFO', 'controller', 'index');
					        $successStatus = false;
					        $message = 'An error occurred during the creation of your WebEx account.';
				        }
			        }

			        $this->view->message = $message;
			        $this->view->successStatus = $successStatus;

		        }


	        } else {
		        // this is a file instance.
		        //$this->_helper->redirector->gotoRoute(array('module' => 'ot', 'controller' => 'login', 'action' => 'index'), 'default', true);
		        // DOESN'T WORK.
	        }

        } 
       
        $this->view->loggedInUser = $loggedInUser;
        
    }
}