<?php

/**
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file _LICENSE.txt.
 *
 * This license is also available via the world-wide-web at
 * http://itdapps.ncsu.edu/bsd.txt
 *
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to itappdev@ncsu.edu so we can send you a copy immediately.
 *
 * @package    ErrorController
 * @category   Controller
 * @copyright  Copyright (c) 2007 NC State University Office of
 *             Information Technology
 * @license    BSD License
 * @version    SVN: $Id: $
 */
class UsermanagementController extends Zend_Controller_Action {

	private $logModel;

	public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array()) {
		parent::__construct($request, $response, $invokeArgs);
		$this->logModel = new Wst_Model_Dbtable_Log();
	}


	public function indexAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('User Management');
	}

	public function manageUsersAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		// js file for sortable table
		$this->view->headScript()->appendFile($this->view->baseUrl() . '/public/scripts/ui-sortable.js');

		$localUserListModel = new Wst_Model_Dbtable_Userlist();

		$select = $localUserListModel->getAdapter()->select();
		$select->from(array($localUserListModel->info('name')));

		// form fields?
		if ($this->_getParam('resetButton') == 'Reset') {
			$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'usermanagement', 'action' => 'manage-users'), 'default', true);
		}

		$filter = array(
			'webExId'   => trim($this->_getParam('webExId')),
			'firstName' => trim($this->getParam('firstName')),
			'lastName'  => trim($this->getParam('lastName')),
			'status'    => trim($this->getParam('status')),
			'source'    => trim($this->getParam('source')),
			'sort'      => $this->getParam('sort', 'webExId'),
			'direction' => $this->getParam('direction', 'asc')
		);

		$select->order($filter['sort'] . ' ' . $filter['direction']);

		if ($filter['webExId'] != '') {
			$where = $localUserListModel->getAdapter()->quoteInto("webExId LIKE ?", "%" . $filter['webExId'] . "%");
			$select->where($where);
		}

		if ($filter['firstName'] != '') {
			$where = $localUserListModel->getAdapter()->quoteInto("firstName LIKE ?", "%" . $filter['firstName'] . "%");
			$select->where($where);
		}

		if ($filter['lastName'] != '') {
			$where = $localUserListModel->getAdapter()->quoteInto("lastName LIKE ?", "%" . $filter['lastName'] . "%");
			$select->where($where);
		}
		
		if ($filter['status'] != '') {
			$where = $localUserListModel->getAdapter()->quoteInto("status = ?", $filter['status']);
			$select->where($where);
		}

		if ($filter['source'] != '') {
			$where = $localUserListModel->getAdapter()->quoteInto("source = ?", $filter['source']);
			$select->where($where);
		}
		
		$localConfig = Zend_Registry::get("localConfig");
		$perPage = $localConfig["itemsPerPage"];

		$adapter = new Zend_Paginator_Adapter_DbSelect($select);

		$paginator = new Zend_Paginator($adapter);
		$paginator->setDefaultItemCountPerPage($perPage);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));

		$form = new Wst_Form_Searchusers();
		$form->populate($filter);

		$this->view->assign(array(
			'form' => $form,
			'paginator' => $paginator,
			'sort' => $filter['sort'],
			'direction' => $filter['direction']
		));

		$this->_helper->pageTitle('List / Manage WebEx Users');
	}

	public function manuallyAddUserAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/scripts/datepicker/css/datepicker.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/scripts/datepicker/css/datepicker.css');
		$this->view->headScript()->appendFile(
			$this->view->baseUrl() . '/public/scripts/datepicker/js/bootstrap-datepicker.js',
			'text/javascript'
		);

		// begin form
		$form = new Wst_Form_Manuallyadduser();

		if ($this->getRequest()->isPost()) {
			if ($form->isValid($this->getRequest()->getPost())) {
				// helpers
				$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
				$userListModel = new Wst_Model_Dbtable_Userlist();
				$overridesModel = new Wst_Model_Dbtable_Useroverrides();

				// TODO - validate fields

				$euid = $form->getValue("euid");
				$webExId = $form->getValue("webExId");

				// does user exist in WebEx already?
				$existingUser = $webexApiHelper->getWebexUserStatus($euid);
				if (!$existingUser["userExistsInWebEx"]) {
					// user does not exist...proceed...

					//NEW WAY - put the entry in the overrides table, and let the cronjob pick stuff up and queue it.
					//calculate start and end timestamps
					if ($form->getValue("start_date")) {
						$startTimestamp = strtotime($form->getValue("start_date"));
					} else {
						$startTimestamp = time();
					}
					$endTimestamp = strtotime($form->getValue("end_date"));

					// add override information to overrides table

					$overrideResult = $overridesModel->addOverride($euid, $form->getValue('webExId'), $form->getValue("reason"), $form->getValue("contact_email"), $startTimestamp, $endTimestamp, "active");


					$this->_helper->messenger->addSuccess("Success: override for euid $euid (webExId $webExId) added successfully.");

					// queue the add action right now
					$userData = array(
						"email"          => $form->getValue("email"),
						"firstName"      => $form->getValue("firstName"),
						"lastName"       => $form->getValue("lastName"),
						"dept_number"    => "0",
						"org_short_code" => "undefined"
					);

					$apiQueueModel = new Wst_Model_Dbtable_Apiqueue();
					$apiQueueModel->queueAddAction($euid, $webExId, $userData, "manual");

					$this->logModel->addLogMessage('Controller Processing', "User added manually (euid $euid; webExId $webExId)", 1, 'INFO', 'controller', __CLASS__);

					$this->_helper->messenger->addSuccess("Success: override to add user with webExId = {$form->getValue("webExId")} has been created. The user will be created in WebEx when the API Queue runs next.");
					$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'usermanagement', 'action' => 'manage-users'), 'default', true);


					// OLD WAY
					// API call
					//$addUserResult = $webexApiHelper->addWebexUser($form->getValue("firstName"), $form->getValue("lastName"), $form->getValue("webExId"), $form->getValue("email"));
					//
					//// add to database as a source="manual" user
					//if ($addUserResult["response"]["result"] == "SUCCESS") {
					//	// user ID returned from WebEx
					//	$addedUserId = $addUserResult["data"]["use:userId"];
					//
					//	// add user to user list
					//	// TODO - some way to get the dept number and org short code??
					//	$addResult = $userListModel->addNewUserToUserList($form->getValue("euid"), $form->getValue("webExId"), $form->getValue("firstName"), $form->getValue("lastName"), $form->getValue("email"), "0", "undefined", $addedUserId, "manual");
					//
					//	$this->_helper->messenger->addSuccess("Success: user with webExId = {$form->getValue("webExId")} added manually to WebEx.");
					//	$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'usermanagement', 'action' => 'manage-users'), 'default', true);
					//} else {
					//	$this->_helper->messenger->addError("Error: unable to add user with webExId = {$form->getValue("webExId")} to WebEx.");
					//	$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'usermanagement', 'action' => 'manage-users'), 'default', true);
					//}

				} else {
					// error - user already exists in WebEx
					$this->_helper->messenger->addError("Error: user with webExId = {$form->getValue("webExId")} already exists in WebEx!");
					$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'usermanagement', 'action' => 'manually-add-user'), 'default', true);
				}
			}
		}

		$this->view->form = $form;

		$this->_helper->pageTitle('Manually Add WebEx User');
	}

	public function deactivateUserAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/scripts/datepicker/css/datepicker.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/scripts/datepicker/css/datepicker.css');
		$this->view->headScript()->appendFile(
			$this->view->baseUrl() . '/public/scripts/datepicker/js/bootstrap-datepicker.js',
			'text/javascript'
		);

		$form = new Wst_Form_Manuallydeactivateuser();

		$get = Zend_Registry::get('getFilter');
		$euid = $get->euid;

		// does user have an in-effect override in place already?
		$overridesModel = new Wst_Model_Dbtable_Useroverrides();
		$inEffectOverridesForUser = $overridesModel->getInEffectOverridesForEuid($euid);
		
		if ($this->getRequest()->isPost()) {
			if ($form->isValid($this->getRequest()->getPost())) {
				// get EUID from the form
				$euidFromForm = $form->getValue("euid");

				// get user webExId for API call.
				$userListModel = new Wst_Model_Dbtable_Userlist();
				$user = $userListModel->getUserByEuid($euidFromForm);

				if (!$user) {
					$this->_helper->messenger->addError("Fatal error: could not locate user with euid = $euidFromForm in users list. Cannot proceed.");
					$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'usermanagement', 'action' => 'user-details', 'euid' => $euidFromForm), 'default', true);
				}

				$webExId = $user['webExId'];

				// NEW WAY - put the entry in the overrides table, and let the cronjob pick stuff up and queue it.
				// cancel existing overrides if needed
				//if ($inEffectOverridesForUser) {
				//	$cancelResult = $overridesModel->cancelExistingUserOverrides($euidFromForm);
				//}
				//
				//// add new override
				//// calculate start and end timestamps
				//if ($form->getValue("start_date")) {
				//	$startTimestamp = strtotime($form->getValue("start_date"));
				//} else {
				//	$startTimestamp = time();
				//}
				//$endTimestamp = strtotime($form->getValue("end_date"));
				//
				//$addOverrideResult = $overridesModel->addOverride($euidFromForm, $form->getValue("reason"), $form->getValue("contact_email"), $startTimestamp, $endTimestamp, "inactive");
				//
				//$this->_helper->messenger->addSuccess("Success: override for euid $euid (webExId $webExId) added successfully.");

				// OLD WAY
				// first try API call...
				// API call
				$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
				$result = $webexApiHelper->deleteWebexUser($webExId);
				if ($result["response"]["result"] == "SUCCESS") {
					// update user list - also flips user over to manual
					$dbResult = $userListModel->inactivateUserInUserList($euidFromForm, true); // true for manual management

					// cancel existing overrides if needed
					if ($inEffectOverridesForUser) {
						$cancelResult = $overridesModel->cancelExistingUserOverrides($euidFromForm);
					}

					// add new override
					// calculate start and end timestamps
					//if ($form->getValue("start_date")) {
					//	$startTimestamp = strtotime($form->getValue("start_date"));
					//} else {
					//	$startTimestamp = time();
					//}
					$startTimestamp = time();
					$endTimestamp = strtotime($form->getValue("end_date"));

					$addOverrideResult = $overridesModel->addOverride($euidFromForm, null, $form->getValue("reason"), $form->getValue("contact_email"), $startTimestamp, $endTimestamp, "inactive");

					$this->logModel->addLogMessage('Controller Processing', "User deactivated manually (euid $euid; webExId $webExId)", 1, 'INFO', 'controller', __CLASS__);

					$this->_helper->messenger->addSuccess("Success: override for euid $euid (webExId $webExId) added successfully.");

				} else {
					$this->_helper->messenger->addError('Error during API call to deactivate user with webExId ' . $webExId. '.');
				}

				$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'usermanagement', 'action' => 'user-details', 'euid' => $euidFromForm), 'default', true);
			}
		} else {
			if ($inEffectOverridesForUser) {
				$this->_helper->messenger->addWarning("<b>Warning!</b> - this user already has an in-effect override in place! Continuing with this override will cancel the current override.");
			}
		}

		$this->view->form = $form;
		$this->view->euid = $get->euid;

		$this->_helper->pageTitle('Manually Deactivate WebEx User');
	}

	public function activateUserAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/scripts/datepicker/css/datepicker.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/scripts/datepicker/css/datepicker.css');
		$this->view->headScript()->appendFile(
			$this->view->baseUrl() . '/public/scripts/datepicker/js/bootstrap-datepicker.js',
			'text/javascript'
		);

		$form = new Wst_Form_Manuallydeactivateuser();

		$get = Zend_Registry::get('getFilter');
		$euid = $get->euid;

		// does user have an in-effect override in place already?
		$overridesModel = new Wst_Model_Dbtable_Useroverrides();
		$inEffectOverridesForUser = $overridesModel->getInEffectOverridesForEuid($euid);

		if ($this->getRequest()->isPost()) {
			if ($form->isValid($this->getRequest()->getPost())) {
				// get EUID from the form
				$euidFromForm = $form->getValue("euid");

				// get user webExId for API call.
				$userListModel = new Wst_Model_Dbtable_Userlist();
				$user = $userListModel->getUserByEuid($euidFromForm);

				if (!$user) {
					$this->_helper->messenger->addError("Fatal error: could not locate user with euid = $euidFromForm in users list. Cannot proceed.");
					$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'usermanagement', 'action' => 'user-details', 'euid' => $euidFromForm), 'default', true);
				}

				$webExId = $user['webExId'];

				// NEW WAY - put the entry in the overrides table, and let the cronjob pick stuff up and queue it.
				// cancel existing overrides if needed
				//if ($inEffectOverridesForUser) {
				//	$cancelResult = $overridesModel->cancelExistingUserOverrides($euidFromForm);
				//}
				//
				//// add new override
				//// calculate start and end timestamps
				//if ($form->getValue("start_date")) {
				//	$startTimestamp = strtotime($form->getValue("start_date"));
				//} else {
				//	$startTimestamp = time();
				//}
				//$endTimestamp = strtotime($form->getValue("end_date"));
				//
				//$addOverrideResult = $overridesModel->addOverride($euidFromForm, $form->getValue("reason"), $form->getValue("contact_email"), $startTimestamp, $endTimestamp, "active");
				//
				//$this->_helper->messenger->addSuccess("Success: override for euid $euid (webExId $webExId) added successfully.");



				// OLD WAY
				// first try API call...
				// API call
				$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
				$result = $webexApiHelper->editWebexUser($webExId);
				if ($result["response"]["result"] == "SUCCESS") {
					// update user list
					$dbResult = $userListModel->reactivateUserInUserList($euidFromForm, true); // true for manual management

					// cancel existing overrides if needed
					if ($inEffectOverridesForUser) {
						$cancelResult = $overridesModel->cancelExistingUserOverrides($euidFromForm);
					}

					// add new override
					// calculate start and end timestamps
					//if ($form->getValue("start_date")) {
					//	$startTimestamp = strtotime($form->getValue("start_date"));
					//} else {
					//	$startTimestamp = time();
					//}
					$startTimestamp = time();
					$endTimestamp = strtotime($form->getValue("end_date"));

					$addOverrideResult = $overridesModel->addOverride($euidFromForm, null, $form->getValue("reason"), $form->getValue("contact_email"), $startTimestamp, $endTimestamp, "active");

					$this->logModel->addLogMessage('Controller Processing', "User activated manually (euid $euid; webExId $webExId)", 1, 'INFO', 'controller', __CLASS__);

					$this->_helper->messenger->addSuccess("Success: override for euid $euid (webExId $webExId) added successfully.");

				} else {
					$this->_helper->messenger->addError('Error during API call to reactivate user with webExId ' . $webExId. '.');
				}

				$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'usermanagement', 'action' => 'user-details', 'euid' => $euidFromForm), 'default', true);
			}
		} else {
			if ($inEffectOverridesForUser) {
				$this->_helper->messenger->addWarning("<b>Warning!</b> - this user already has an in-effect override in place! Continuing with this override will cancel the current override.");
			}
		}

		$this->view->form = $form;
		$this->view->euid = $get->euid;

		$this->_helper->pageTitle('Manually Activate WebEx User');
	}

	/* OLD, DEFUNCT, UNUSED. CAN REMOVE?
	 *
	 * public function addActivationOverrideAction() {
		$get = Zend_Registry::get('getFilter');

		$userListModel = new Wst_Model_Dbtable_Userlist();

		// can we do this without re-fetching the user?
		$user = $userListModel->getUserByEuid($get->euid);
		
		if ($user) {
			$webExId = $user["webExId"];

			// API call
			$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
			$result = $webexApiHelper->editWebexUser($webExId);
			if ($result["response"]["result"] == "SUCCESS") {
				// DB
				$dbResult = $userListModel->reactivateUserInUserList($get->euid);
				$this->_helper->messenger->addSuccess("User with euid {$get->euid} successfully reactivated.");
			} else {
				$this->_helper->messenger->addError('Error during API call to deactivate user with euid ' . $get->id . '.');
			}

		} else {
			$this->_helper->messenger->addError("Error - user with euid {$get->euid} not found in database.");
		}

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNeverRender();

		$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'usermanagement', 'action' => 'user-details', 'euid' => $get->euid), 'default', true);
	}
	
	public function addDeactivationOverrideAction() {
		$get = Zend_Registry::get('getFilter');

		$userListModel = new Wst_Model_Dbtable_Userlist();
		// can we do this without re-fetching the user?
		$user = $userListModel->getUserByEuid($get->euid);

		if ($user) {
			$webExId = $user["webExId"];

			// API call
			$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
			$result = $webexApiHelper->deleteWebexUser($webExId);
			if ($result["response"]["result"] == "SUCCESS") {
				// DB
				$dbResult = $userListModel->inactivateUserInUserList($get->euid);
				$this->_helper->messenger->addSuccess("User with euid {$get->euid} successfully deactivated.");
			} else {
				$this->_helper->messenger->addError('Error during API call to deactivate user with euid ' . $get->id . '.');
			}

		} else {
			$this->_helper->messenger->addError("Error - user with euid {$get->euid} not found in database.");
		}

		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNeverRender();

		$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'usermanagement', 'action' => 'user-details', 'euid' => $get->euid), 'default', true);
	}
	
	public function forceRetireUserAction() {
		$get = Zend_Registry::get('getFilter');
		$this->view->userToForceRetire = $get->id;
	}*/

	public function userDetailsAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$get = Zend_Registry::get('getFilter');

		$userListModel = new Wst_Model_Dbtable_Userlist();
		$userDetails = $userListModel->getUserByEuid($get->euid);

		// get user overrides
		$overridesModel = new Wst_Model_Dbtable_Useroverrides();
		$overrides = $overridesModel->getAllOverridesForEuid($get->euid);

		$this->view->user = $userDetails;
		$this->view->userOverrides = $overrides;

		$this->_helper->pageTitle("User Details for {$userDetails["firstName"]} {$userDetails["lastName"]} ({$userDetails["webExId"]})");
	}

	public function userRecordingsAction() {

		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$get = Zend_Registry::get('getFilter');
		$this->view->webExId = $get->webExId;
		$this->view->euid = $get->euid;

		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$recordings = $recordingArchiveModel->getActiveRecordingsForWebExId($get->webExId);

		$helper = new Wst_Model_Helpers_General();

		foreach($recordings as $key => $r) {
			$recordings[$key]["duration_string"] = $helper->secondsToTime($r["duration_sec"]);
		}

		$this->view->recordings = $recordings;

		$this->_helper->pageTitle("View Recordings for user {$get->webExId}");
	}

	public function bulkCenterPrivilegesAction() {
		$this->_helper->pageTitle("Bulk Manage Center Privileges");
	}

	public function manageUserOverridesAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
		
		$get = Zend_Registry::get('getFilter');

		$localConfig = Zend_Registry::get("localConfig");
		$perPage = $localConfig["itemsPerPage"];

		$userOverridesModel = new Wst_Model_Dbtable_Useroverrides();

		if (isset($get->filter) && $get->filter == 'in-effect') {
			$activeOverrides = $userOverridesModel->getInEffectOverrides(true); // returns paginator object
			$activeOverrides->setDefaultItemCountPerPage($perPage);
			$activeOverrides->setCurrentPageNumber($this->_getParam('page', 1));

			$this->view->overrides= $activeOverrides;
		} else {
			$overrides = $userOverridesModel->getAllOverrides(true); // returns paginator object
			$overrides->setDefaultItemCountPerPage($perPage);
			$overrides->setCurrentPageNumber($this->_getParam('page', 1));

			$this->view->overrides = $overrides;
		}

		$this->view->filter = $get->filter;
		$this->_helper->pageTitle("Manage User Overrides");
	}

	public function overrideDetailsAction() {
		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$get = Zend_Registry::get('getFilter');

		$overridesModel = new Wst_Model_Dbtable_Useroverrides();
		$overrideDetails = $overridesModel->getOverride($get->id);

		$this->view->override = $overrideDetails;

		$this->_helper->pageTitle("Override Details - override id: {$get->id}");
	}

	public function cancelOverrideAction() {

		// which one?
		$get = Zend_Registry::get('getFilter');
		$id = $get->id;
		
		if ($id) {
			// cancel the override (the model will handle reverting the user.
			$overrideModel = new Wst_Model_Dbtable_Useroverrides();
			$result = $overrideModel->cancelOverride($id);

			// generate alert
			if ($result) {
				$this->_helper->messenger->addSuccess("Success: override with id $id removed. User reverted to source `feed`.");

				$this->logModel->addLogMessage('Controller Processing', "Override $id cancelled successfully. User reverted to source `feed`.", 1, 'INFO', 'controller', __CLASS__);

			} else {
				$this->_helper->messenger->addError("Error: could not locate override with id $id. No action taken.");
			}
		} else {
			$this->_helper->messenger->addError("Error: no override id provided. No action taken.");
		}

		// redirect
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNeverRender();

		$this->_helper->redirector->gotoRoute(array('module' => 'default', 'controller' => 'usermanagement', 'action' => 'override-details', 'id' => $id), 'default', true);
	}

	public function addLocalUserAction() {

		$form = new Wst_Form_Addusertolocal();

		if ($this->getRequest()->isPost()) {
			if ($form->isValid($this->getRequest()->getPost())) {
				// get WebEx ID from the form
				$webExId = strtolower($form->getValue("webExId"));
				
				// get info from webex
				$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
				$lookupResult = $webexApiHelper->getWebexUserDetails($webExId);

				if ($lookupResult["response"]["result"] == "SUCCESS") {					
					
					// grab info that we need
					$webExUserId = $lookupResult["data"]["use:userId"];
					$status = $lookupResult["data"]["use:active"] == "ACTIVATED" ? 'active' : 'inactive';
					
					// get euid from LDAP file
					$localConfig = Zend_Registry::get('localConfig');
					$filePath = $localConfig["userDiffFilePath"];

					$userDataArray = null;

					if (file_exists($filePath)) {
						// get file contents
						$userDataJson = file_get_contents($filePath);

						// check whether valid or missing
						$userDataArray = @json_decode($userDataJson, true);

						// reindex
						$userDataArrayByUsername = array();
						foreach($userDataArray as $element) {
							$userDataArrayByUsername[strtolower($element['uid'])] = $element;
						}

						//$stuff = count($userDataArrayByUsername);

						if (isset($userDataArrayByUsername[$webExId])) {
							
							$euid = $userDataArrayByUsername[$webExId]["uidnumber"];
							
							//does user already exist in local database?
							$userListModel = new Wst_Model_Dbtable_Userlist();
							$existingUser = $userListModel->getUserByEuid($euid);
							
							if (is_null($existingUser)) {
								// ALL SET
								// put together info for database

								// name info from LDAP
								$firstName = $userDataArrayByUsername[$webExId]["uncgpreferredgivenname"];
								$lastName = $userDataArrayByUsername[$webExId]["uncgpreferredsurname"];

								// construct email
								$emailSuffix = $localConfig["emailSuffix"];
								$email = $webExId . $emailSuffix;

								// tracking code info
								$deptNumber = $userDataArrayByUsername[$webExId]["departmentnumber"];
								$orgShortCode = $userDataArrayByUsername[$webExId]["uncgorgnshortname"];

								// checksum
								$generalHelper = new Wst_Model_Helpers_General();
								$checksumData = array(
									"webExId"        => $webExId,
									"firstName"      => $firstName,
									"lastName"       => $lastName,
									"email"          => $email,
									"dept_number"    => $deptNumber,
									"org_short_code" => $orgShortCode
								);
								$checksum = $generalHelper->generateLocalChecksum(null, $checksumData);

								$insertData = array(
									'euid'           => $euid,
									'webExId'        => $webExId,
									'firstName'      => $firstName,
									'lastName'       => $lastName,
									'email'          => $email,
									'webExUserId'    => $webExUserId,
									'checksum'       => $checksum,
									'status'         => $status,
									'source'         => 'feed',
									'dept_number'    => $deptNumber,
									'org_short_code' => $orgShortCode
								);

								// can't use this because of 'status' field...
								//$userListModel->addNewUserToUserList($euid, $webExId, $firstName, $lastName, $email, $webExUserId);

								$insertResult = $userListModel->insert($insertData);
								$this->_helper->messenger->addSuccess("Success! User $webExId added to local database.");

								$this->logModel->addLogMessage('Controller Processing', "Local user added manually (euid $euid; webExId $webExId)", 1, 'INFO', 'controller', __CLASS__);

							} else {
								$this->_helper->messenger->addError("Error: user $webExId (with euid $euid) already exists in local database.");
							}

							
						} else {
							$this->_helper->messenger->addError("Error: user $webExId does not exist in the LDAP feed file.");
						}
					} else {
						$this->_helper->messenger->addError("Error: could not locate LDAP feed file. Path: $filePath");
					}
				} else {
					$this->_helper->messenger->addError("API error while looking for user $webExId: " . $lookupResult["response"]["message"]);
				}

				$this->_helper->redirector->goToRoute(array('module' => 'default', 'controller' => 'usermanagement', 'action' => 'index'), 'default', true);
				
			}
		}

		$this->view->form = $form;

		$this->_helper->pageTitle('Add WebEx User to Local DB');
	}
	
	public function checkUserEligibilityAction() {

		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$form = new Wst_Form_Checkusereligibility();

		$localConfig = Zend_Registry::get('localConfig');
		$checkTeacherFeed = $localConfig["checkTeacherFeed"] ? "true" : "false";
		$userSourceType = $localConfig["userSourceType"] == "file" ? "file" : "ldap";

		$form->getElement('checkTeacherFeed')->setValue($checkTeacherFeed);
		$form->getElement('userSourceType')->setValue($userSourceType);

		if ($this->getRequest()->isPost()) {
			if ($form->isValid($this->getRequest()->getPost())) {
				$usernamesRaw = $form->getValue('usernames');
				$userSourceTypeFromForm = $form->getValue('userSourceType');
				$checkTeacherFeedFromForm = $form->getValue('checkTeacherFeed') == "true";

				$helper = new Wst_Model_Helpers_General();
				$usernamesArray = $helper->textareaToArray($usernamesRaw);

				$lookupHelper = new Wst_Model_Helpers_Lookup();

				$resultsArray = $lookupHelper->getUserEligibility($usernamesArray, $userSourceTypeFromForm, $checkTeacherFeedFromForm);

				$this->view->userSourceType = $userSourceTypeFromForm;
				$this->view->resultsArray = $resultsArray;
			}
		}

		$this->view->form = $form;
		$this->_helper->pageTitle('Check User Eligibility');
	}

	public function userListTrueUpAction() {

		// add page styles
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => $this->view->baseUrl() . '/public/css/overrides.css'), 'APPEND')->appendStylesheet($this->view->baseUrl() . '/public/css/overrides.css');
		$this->view->headLink(array('rel' => 'stylesheet', 'href' => 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css'), 'APPEND')->appendStylesheet('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');

		$this->_helper->pageTitle('User List True Up');

		$get = Zend_Registry::get('getFilter');

		if (isset($get->execute) && $get->execute == "y") {

			// need to true up the user list

			// get the current list of all users in WebEx
			$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
			$webexUsersResult = $webexApiHelper->getAllWebexUsers();

			if ($webexUsersResult["response"]["result"] != "SUCCESS") {
				throw new Exception("WebEx API call failed");
			}

			$webexUsers = $webexUsersResult["data"];
			// reindex
			$webexUsersById = array();
			foreach ($webexUsers as $user) {
				$webexUsersById[$user["use:webExId"]] = $user;
			}

			//dump($webexUsersById, true);

			// get local userlist table
			$userListModel = new Wst_Model_Dbtable_Userlist();
			$localUsersResult = $userListModel->getUserWebexIdMap();

			//dump($localUsersResult, true);

			// get overrides table

			$overridesModel = new Wst_Model_Dbtable_Useroverrides();
			$activeOverrides = $overridesModel->getInEffectOverrides();
			$activeOverridesByUser = array();
			foreach ($activeOverrides as $o) {
				$activeOverridesByUser[$o["webExId"]] = $o;
			}

			//dump($activeOverridesByUser, true);

			// exclude any user with active overrides
			foreach ($activeOverridesByUser as $user => $override) {
				if (isset($localUsersResult[$user])) {
					unset($localUsersResult[$user]);
				}
			}

			// just get usernames
			$usernamesToLookup = array_keys($localUsersResult);
			//dump($usernamesToLookup, true);

			// look up eligiblity for each user
			$lookupHelper = new Wst_Model_Helpers_Lookup();
			$userEligibility = $lookupHelper->getUserEligibility($usernamesToLookup, "file");

			//dump($userEligibility, true);

			// COMPARE!!!
			$changesNeeded = array();
			$flipToFeed = array();

			foreach ($userEligibility["data"] as $username => $e) {
				// is user eligible?
				if ($e["eligible"]) {
					// active in WebEx?
					if (!isset($webexUsersById[$username])) {
						$changesNeeded[$username] = "add";
					} else if ($webexUsersById[$username]["use:active"] != "ACTIVATED") {
						$changesNeeded[$username] = "edit";
					}
				} else {
					// active in WebEx?
					if (isset($webexUsersById[$username]) && $webexUsersById[$username]["use:active"] != "DEACTIVATED") {
						$changesNeeded[$username] = "delete";
					}
				}

				// do we need to flip them back to feed?
				if ($localUsersResult[$username]["source"] == "manual") {
					$flipToFeed[] = $username;
				}

			}

			// we know what needs to be done - do it.
			$queueModel = new Wst_Model_Dbtable_Apiqueue();

			foreach ($changesNeeded as $username => $action) {
				if ($action == "add") {
					$userData = array(
						"email"          => $localUsersResult[$username]["email"],
						"firstName"      => $localUsersResult[$username]["firstName"],
						"lastName"       => $localUsersResult[$username]["lastName"],
						"dept_number"    => $localUsersResult[$username]["dept_number"],
						"org_short_code" => $localUsersResult[$username]["org_short_code"]
					);

					$result = $queueModel->queueAddAction($localUsersResult[$username]["euid"], $username, $userData, $localUsersResult[$username]["source"]);
				} else if ($action == "edit") {
					$result = $queueModel->queueEditAction($localUsersResult[$username]["euid"], $username, array(), $localUsersResult[$username]["source"]);
				} else if ($action == "delete") {
					$result = $queueModel->queueDeleteAction($localUsersResult[$username]["euid"], $username, $localUsersResult[$username]["source"]);
				} else {
					throw new Exception("Invalid change method detected for user $username");
				}
			}


			foreach ($flipToFeed as $username) {
				$userListModel->revertUserToFeedSource($localUsersResult[$username]["euid"]);
			}


			$this->view->assign(array(
				"changesNeeded" => $changesNeeded,
				"flipToFeed"    => $flipToFeed,
				"executed"      => true
			));
		} else {
			$this->view->assign(array(
				"executed" => false
			));
		}

	}
}