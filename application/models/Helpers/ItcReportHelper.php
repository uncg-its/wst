<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 5/9/16
 * Time: 10:36 AM
 */

class Wst_Model_Helpers_ItcReportHelper {
	public function getReportArray($startTimestamp, $endTimestamp, $reportGroupCodes) {
		$reportArray = array(
			"sessions"   => array(
				"totals"=> array(
					"results" => 0,
					"unique_session_hosts" => 0
				),
				"data" => array(),
				"statistics" => array()
			),
			"recordings" => array(
				"totals"=> array(
					"results" => 0
				),
				"data" => array()
			),
			"attendees"  => array(
				"totals"=> array(
					"results" => 0,
					"mobile_users" => 0,
					"desktop_users" => 0,
					"unique_attendees" => 0
				),
				"data" => array(),
				"statistics" => array()
			)
		);

		// sessions

		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();
		$sessions = $sessionArchiveModel->getArchivesBetweenTimestamps($startTimestamp, $endTimestamp);
		// reindex by conf_id
		$helper = new Wst_Model_Helpers_General();
		$sessions = $helper->reindexArray($sessions, 'conf_id');

		if (count($sessions) > 0) {
			// unique session hosts
			$sessionsByHost = array();
			foreach ($sessions as $confId => $session) {
				$codes = json_decode($session["tracking_codes"], true);
				if (isset($codes["2"])) {
					$orgShortCode = $codes["2"];
					if (in_array($orgShortCode, $reportGroupCodes)) {
						// it belongs in our results
						$reportArray["sessions"]["totals"]["results"]++;

						$session["orgShortCode"] = $orgShortCode;
						$reportArray["sessions"]["data"][$orgShortCode][$confId] = $session;

						$hostUsername = $session["host_username"];
						if (!isset($sessionsByHost[$hostUsername])) {
							$sessionsByHost[$hostUsername] = 0;
						} else {
							$sessionsByHost[$hostUsername] += 1;
						}
					}
				}
			}
			$reportArray["sessions"]["totals"]["unique_session_hosts"] = count($sessionsByHost);
		}

		// recordings

		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$recordings = $recordingArchiveModel->getRecordingsBetweenTimestamps($startTimestamp, $endTimestamp);

		if (count($recordings) > 0) {
            foreach ($recordings as $recording) {
                if (!isset($sessions[$recording['conf_id']])) {
                    continue;
                }

                $parentSession = $sessions[$recording['conf_id']];

                $codes = json_decode($parentSession["tracking_codes"], true);
                if (isset($codes["2"])) {
                    $orgShortCode = $codes["2"];
                    if (in_array($orgShortCode, $reportGroupCodes)) {
                        // it belongs in our results
                        $reportArray["recordings"]["totals"]["results"]++;

                        $recording["orgShortCode"] = $orgShortCode;
                        $reportArray["recordings"]["data"][$orgShortCode][$recording["recording_id"]] = $recording;
                    }
                }
            }
        }

		// attendees

		$attendeeArchiveModel = new Wst_Model_Dbtable_Attendeearchive();
		$attendees = $attendeeArchiveModel->getArchivesBetweenTimestamps($startTimestamp, $endTimestamp);


		if (count($attendees) > 0) {
			$attendeesUnique = array();
			foreach ($attendees as $attendee) {
				if (!isset($sessions[$attendee['conf_id']])) {
					continue;
				}

				$parentSession = $sessions[$attendee['conf_id']];

				$codes = json_decode($parentSession["tracking_codes"], true);
				if (isset($codes["2"])) {
					$orgShortCode = $codes["2"];
					if (in_array($orgShortCode, $reportGroupCodes)) {
						// it belongs in our results
						$reportArray["attendees"]["totals"]["results"]++;

						// parse out OS and browser
						$osBrowserPieces = explode(",", $attendee["client_agent"]);

						$operating_system = $osBrowserPieces[0];
						$attendee["operating_system"] = $operating_system;
						$attendee["os_icon"] = $helper->displayOsIcon($operating_system);
						$attendee["os_type"] = strpos($attendee["client_agent"], "Standalone App") ? "mobile" : "desktop";

						if ($attendee["os_type"] == "mobile") {
						    $reportArray["attendees"]["totals"]["mobile_users"]++;
                        } else {
                            $reportArray["attendees"]["totals"]["desktop_users"]++;
                        }

						// mobile stats
						if (!isset($reportArray["attendees"]["statistics"][$orgShortCode]["mobile_users"])) {
							$reportArray["attendees"]["statistics"][$orgShortCode]["mobile_users"] = 1;
						} else {
							$reportArray["attendees"]["statistics"][$orgShortCode]["mobile_users"]++;
						}

						// desktop stats
						if (!isset($reportArray["attendees"]["statistics"][$orgShortCode]["desktop_users"])) {
							$reportArray["attendees"]["statistics"][$orgShortCode]["desktop_users"] = 1;
						} else {
							$reportArray["attendees"]["statistics"][$orgShortCode]["desktop_users"]++;
						}

						$browser = $osBrowserPieces[1];
						$attendee["browser"] = $browser;
						$attendee["browser_icon"] = $helper->displayBrowserIcon($browser);

						// unique attendees
						$attendeeEmail = $attendee["email"];
						if (!isset($attendeesUnique[$attendeeEmail])) {
							$attendeesUnique[$attendeeEmail] = 0;
						} else {
							$attendeesUnique[$attendeeEmail] += 1;
						}

						// index and add
						$attendee["orgShortCode"] = $orgShortCode;
						$reportArray["attendees"]["data"][$orgShortCode][$attendee["attendee_id"]] = $attendee;
					}
				}
			}
			$reportArray["attendees"]["totals"]["unique_attendees"] = count($attendeesUnique);
		}

		return $reportArray;
	}
}