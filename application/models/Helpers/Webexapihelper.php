<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/22/16
 * Time: 7:13 PM
 */

class Wst_Model_Helpers_Webexapihelper {
	/* ************ */
	/* TEST HELPERS */
	/* ************ */

	public function showRegistryVars() {
		$webexVars = Zend_Registry::get("webexApi");
		return $webexVars;
	}

	/* ************ */
	/* CORE HELPERS */
	/* ************ */

	// make a webex request with the given XML bodyContent.
	private function doWebexRequest($service, $request, $bodyContent, $environment = "webexApi", $asUser = null) {

		// prep
		$requestTarget = "$service.$request";
		// TODO - handle array input instead of raw XML

		$webexRequest = new Wst_Model_Helpers_Webexapicallhelper("/WBXService/XMLService", "post", $environment);
		$authInfo = $webexRequest->getAuthInfo(); // pull into a usable array

		$username = is_null($asUser) ? $authInfo["username"] : $asUser;

		if (!is_null($asUser)) {
			// check if user exists in user list (therefore can't get admins)
			$userListModel = new Wst_Model_Dbtable_Userlist();
			$user = $userListModel->getUserByWebExId($username);
			if (!$user) {
				throw new Exception("Error: user $username does not exist in local database.");
			}

			// assign temporary password
			$newPass = getNewSaltedPassword();
			$newRawPass = $newPass["rawPassword"];

			$changePassResult = $this->changeUserPassword($username, $newRawPass);
			if ($changePassResult["response"]["result"] != "SUCCESS") {
				throw new Exception("Error: could not change user password. Reason: " . $changePassResult["response"]["message"]);
			}

			// continue using the temporary password
			$password = $newRawPass;
		} else {
			$password = $authInfo["password"];
		}

		$xmlRequest = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<serv:message xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <header>
        <securityContext>
            <webExID>$username</webExID>
            <password>$password</password>
            <siteName>{$authInfo["siteName"]}</siteName>
            <partnerID>{$authInfo["partnerId"]}</partnerID>
        </securityContext>
    </header>
    <body>
	    <bodyContent xsi:type="java:com.webex.service.binding.$requestTarget">
	    $bodyContent
	    </bodyContent>
	</body>
</serv:message>
XML;

		// dumps the request XML
		//dump(htmlentities($xmlRequest), true);

		$webexRequest->setXml($xmlRequest);

		//dump(xml2array($xmlRequest), true);

		$result = $webexRequest->execute("xml");
		$xmlArray = xml2array($result);

		//return $xmlArray;

		// dumps the raw response XML
		//dump(htmlentities($result), true);

		// let's just parse the server response here, genius.
		$servResponse = $xmlArray["serv:message"]["serv:header"]["serv:response"];

		$servResult = $servResponse["serv:result"];
		if ($servResult == "SUCCESS") {
			$servMessage = "Successful API call";
			$servExceptionId = null;
		} else {
			$servMessage = isset($servResponse["serv:reason"]) ? $servResponse["serv:reason"] : "Unknown error.";
			$servExceptionId = isset($servResponse["serv:exceptionID"]) ? $servResponse["serv:exceptionID"] : "unknown exception ID";
		}

		//dump($webexRequest, true);

		return array(
			"response" => array(
				"endpoint"    => $requestTarget,
				"result"      => $servResult,
				"message"     => $servMessage,
				"exceptionId" => $servExceptionId,
				"httpCode"    => $webexRequest->getResponseCode()
			),
			"data"     => $xmlArray
		);
	}

	// gets a webex user list - optional addition of parameters. // TODO - consider making this into a params array?
//function getWebexUsersList($active = "all", $numberToGet = "all", $startFrom = 1, $sortBy = "UID", $sortDirection = "ASC", $raw = false) { // old way - we won't even bother including startFrom for our purposes.
	private function getWebexUsersList($active = "all", $numberToGet = "all", $sortBy = "UID", $sortDirection = "ASC", $raw = false) {
		// NOTES
		// if $startFrom is too high, it'll give FAILURE message "Sorry, no record found."
		// negative values passed into WebEx API will be set to defaults (startFrom to 1, maximumNum to default 500).

		// <orderBy> will take UID, FIRSTNAME, LASTNAME, EMAIL, REGDATE
		// <orderAD> will take ASC or DESC

		$webexUserRequestMaxSize = 500; // set by WebEx. Won't change unless their API changes.

		// $filter should be "active", "inactive"
		if ($active == "active") {
			$extraXML = "<active>ACTIVATED</active>";
		} else if ($active == "inactive") {
			$extraXML = "<active>DEACTIVATED</active>";
		} else {
			$extraXML = "";
		}

		// validate sorting
		$validSortCriteria = array("UID", "FIRSTNAME", "LASTNAME", "EMAIL", "REGDATE");
		if (!in_array($sortBy, $validSortCriteria)) {
			$sortBy = "UID";
		}

		$validSortDirections = array("ASC", "DESC");
		if (!in_array($sortDirection, $validSortDirections)) {
			$sortDirection = "ASC";
		}

		// initialize stuff for first call
		//$currentPosition = $startFrom; // old way!
		$currentPosition = 1;
		$fetchNumber = $webexUserRequestMaxSize; // actual fetch number - set here for initial call. only less than the max if specifically asked for less than the max.

		if (is_numeric($numberToGet)) {
			if ($numberToGet < $webexUserRequestMaxSize) {
				$fetchNumber = $numberToGet;
			}
			$stopAt = $numberToGet;
		} else {
			$numberToGet = 0; // will default to all.
			$stopAt = 0; // if not given a stop point, it'll be all results, which we can set at the same time as $resultSetSize.
		}

		$resultSetSize = 0; // to be set later once we know this value from the first API call.

		//$fetchNumber = 1; // testing

		// init result array
		$returnArray = array();

		// loop it!
		do {
			// set XML
			$bodyContent = <<<XML
	        <order>
				<orderBy>$sortBy</orderBy>
				<orderAD>$sortDirection</orderAD>
			</order>
			<listControl>
				<startFrom>$currentPosition</startFrom>
				<maximumNum>$fetchNumber</maximumNum>
				<listMethod>AND</listMethod>
			</listControl>
			$extraXML
XML;

			// execute call
			$result = $this->doWebexRequest("user", "LstsummaryUser", $bodyContent);

			$serverResponse = $result["response"];
			$data = $result["data"];

			// process
			if ($serverResponse["result"] == "SUCCESS") {
				// grab the users and append them to the returnArray.
				$usersReturned = $data["serv:message"]["serv:body"]["serv:bodyContent"]["use:user"];

				// if the call retrieves multiple records, use:user will be an array of arrays with user info.
				// if the call retrieves a single record, use:user will be only the array with user info.
				// we can get around this by looking at how many records webex tells us it's giving us.

				$numberOfMatchingRecords = 0;
				if ($serverResponse["result"] == "SUCCESS") {
					$numberOfMatchingRecords = $data["serv:message"]["serv:body"]["serv:bodyContent"]["use:matchingRecords"]["serv:returned"];
				}

				if ($numberOfMatchingRecords == 1) {
					// this is a singleton
					array_push($returnArray, $usersReturned);
					//dump($returnArray, true);
				} else {
					// grouping
					$returnArray = array_merge($returnArray, $usersReturned);
				}

				if ($resultSetSize == 0) {
					// if this hasn't been set yet, then set it now.
					$numberOfRecordsMatchingRequest = $data["serv:message"]["serv:body"]["serv:bodyContent"]["use:matchingRecords"]["serv:total"];
					$resultSetSize = $numberOfRecordsMatchingRequest;
					if ($stopAt == 0) {
						// set this also if it wasn't set before. this'll stop at the size of the resultset.
						$stopAt = $resultSetSize;
					}
				}
			} else {
				$stopAt = $currentPosition; // force the loop to stop.
			}

			$currentPosition += $fetchNumber; // in case pagination is needed. if it's not needed this won't matter anyway.

			$arrayCount = count($returnArray);
			if ($stopAt - $arrayCount < $webexUserRequestMaxSize) {
				$fetchNumber = $stopAt - $arrayCount;
			}

		} while($currentPosition < $stopAt);


		// append count of data to server response info
		$serverResponse["recordsFetched"] = count($returnArray);

		return array(
			"response" => $serverResponse,
			"data"     => $returnArray
		);

	}

	public function getMeetingTypeXmlFromRegistry() {
		// add privileges for which meeting types?
		$webexDetails = Zend_Registry::get("webexApi");
		$meetingTypes = explode(",", $webexDetails["newUserMeetingTypes"]);

		$meetingTypeXml = "";
		foreach ($meetingTypes as $mt) {
			$meetingTypeXml .= "<meetingType>$mt</meetingType>";
		}

		return $meetingTypeXml;
	}

	private function getRecordingsList($params = array()) {
		/*
		 * $params structure:
			"webExId"           => username of webex user to pull
			"maximumNum"        => max number of recordings to pull
			"createTimeStart"   => time created lower bound
			"createTimeEnd"     => time created upper bound
			"serviceTypes"      => array containing strings: MeetingCenter, EventCenter, SupportCenter, TrainingCenter
		*/

		// if set, go with the set value. otherwise default.
		$webExId = (isset($params["webExId"])) ? "<hostWebExID>{$params["webExId"]}</hostWebExID>" : "";
		$maximumNum = (isset($params["maximumNum"])) ? $params["maximumNum"] : 500; // 500 is webex max
		$confId = (isset($params["confId"])) ? "<confID>{$params["confId"]}</confID>" : "";

		$createTimeStart = (isset($params["createTimeStart"])) ? "<createTimeStart>{$params["createTimeStart"]}</createTimeStart>" : "";
		$createTimeEnd = (isset($params["createTimeEnd"])) ? "<createTimeEnd>{$params["createTimeEnd"]}</createTimeEnd>" : "";

		$createTimeScope = "";
		if (!empty($createTimeStart) || !empty($createTimeEnd)) {
			$createTimeScope = <<<XML
	<createTimeScope>
		$createTimeStart
		$createTimeEnd
	</createTimeScope>
XML;
		}

		if (isset($params["serviceTypes"]) && is_array($params["serviceTypes"])) {
			$serviceTypes = "<serviceTypes>";
			foreach ($params["serviceTypes"] as $s) {
				$serviceTypes .= "<serviceType>$s</serviceType>";
			}
			$serviceTypes .= "</serviceTypes>";
		} else {
			$serviceTypes = "";
		}

		$bodyContent = <<<XML
	<listControl>
		<startFrom>0</startFrom>
		<maximumNum>$maximumNum</maximumNum>
	</listControl>
	$createTimeScope
	$webExId
	$serviceTypes
	$confId
	<returnSessionDetails>TRUE</returnSessionDetails>
XML;

		$result = $this->doWebexRequest("ep", "LstRecording", $bodyContent);

		$serverResponse = $result["response"];
		$data = $result["data"]["serv:message"]["serv:body"]["serv:bodyContent"];

		return array(
			"response" => $serverResponse,
			"data"     => $data
		);

	}

	/* ******************************* */
	/* FUNCTIONS WE WILL CALL DIRECTLY */
	/* ******************************* */
	public function testProxy($url, $environment = "webexApi") {
		$webexRequest = new Wst_Model_Helpers_Webexapicallhelper("/WBXService/XMLService", "post", $environment);
		$webexRequest->overrideBaseUrl($url);

		$result = $webexRequest->execute("xml");
		$xmlArray = xml2array($result);

		//return $xmlArray;
		return $result != false;
	}


	public function addWebexUser($firstname, $lastname, $username, $email, $userTrackingCodes) {
		$webexDetails = Zend_Registry::get("webexApi");

		// get default password
		$defaultPassword = $webexDetails["newUserPlaceholderPassword"];

		$errorMessage = null;
		$isError = false;

		// validation?
		if (empty($firstname)) {
			$errorMessage = "Firstname not provided to addWebexUser() function.";
			$isError = true;
		} else if (empty($lastname)) {
			$errorMessage = "Lastname not provided to addWebexUser() function.";
			$isError = true;
		} else if (empty($username)) {
			$errorMessage = "Username not provided to addWebexUser() function.";
			$isError = true;
		} else if (empty($email)) {
			$errorMessage = "Email not provided to addWebexUser() function.";
			$isError = true;
		}

		if ($isError) {
			return array(
				"response" => "API call error: $errorMessage",
				"data"     => null
			);
		}

		$meetingTypeXml = $this->getMeetingTypeXmlFromRegistry();

		// get telephony options
		$configModel = new Wst_Model_Dbtable_Config();
		$defaultTelephonyRaw = $configModel->getVar("defaultTelephonyPrivileges");
		$defaultTelephonyPrivileges = explode(",", $defaultTelephonyRaw);

		$helper = new Wst_Model_Helpers_General();
		$telephonyXml = $helper->getWebexTelephonyTypeXml($defaultTelephonyPrivileges, true);

		$bodyContent = <<<XML
	<firstName>$firstname</firstName>
	<lastName>$lastname</lastName>
	<webExId>$username</webExId>
	<email>$email</email>
	<password>$defaultPassword</password>
	<active>ACTIVATED</active>
	<meetingTypes>
		$meetingTypeXml
	</meetingTypes>
	<privilege>
		<isEnableCET>true</isEnableCET>
		$telephonyXml
	</privilege>
	<sendWelcome>FALSE</sendWelcome>
	<myWebEx>
		<myFolders>FALSE</myFolders>
	</myWebEx>
	<phones>
		<phone>1,,5555555555,,,,</phone>
		<mobilePhone>1,,5555555555,,,,</mobilePhone>
	</phones>
XML;

		// TRACKING CODES!
		$trackingCodesModel = new Wst_Model_Dbtable_Trackingcodes();
		$trackingCodes = $trackingCodesModel->getTrackingCodesByUserListField();

		if (count($trackingCodes) > 0) {
			$bodyContent .= "<tracking>";
			foreach ($trackingCodes as $field => $code) {
				if (isset($userTrackingCodes[$field])) {
					$id = $code["tracking_code_id"];
					$value = $userTrackingCodes[$field];
					$bodyContent .= "<trackingCode$id>$value</trackingCode$id>";
				}
			}
			$bodyContent .= "</tracking>";
		}

		$result = $this->doWebexRequest("user", "CreateUser", $bodyContent);

		$serverResponse = $result["response"];
		$data = $result["data"]["serv:message"]["serv:body"]["serv:bodyContent"];

		return array(
			"response" => $serverResponse,
			"data"     => $data
		);
	}


	public function editWebexUser($username, $attributes = array(), $customXml = null) {

		// must include meeting types?
		$meetingTypeXml = $this->getMeetingTypeXmlFromRegistry();

		// init XML
		$bodyContent = "<webExId>$username</webExId>";
		$bodyContent .= "<active>ACTIVATED</active>";
		//$bodyContent .= "<meetingTypes>$meetingTypeXml</meetingTypes>";

		if (!is_null($customXml)) {
			// allow for custom XML
			$bodyContent .= $customXml;
		} else {
			$permittedAttributes = array("firstName", "lastName", "email", "newWebExId");
			foreach ($attributes as $attribute => $value) {
				// TODO: here.
				if (in_array($attribute, $permittedAttributes)) {
					$bodyContent .= "<$attribute>$value</$attribute>";
				}
			}
			// TRACKING CODES!
			$trackingCodesModel = new Wst_Model_Dbtable_Trackingcodes();
			$trackingCodes = $trackingCodesModel->getTrackingCodesByUserListField();

			if (count($trackingCodes) > 0) {
				$bodyContent .= "<tracking>";
				foreach ($trackingCodes as $field => $code) {
					if (isset($attributes[$field])) {
						$id = $code["tracking_code_id"];
						$value = $attributes[$field];
						$bodyContent .= "<trackingCode$id>$value</trackingCode$id>";
					}
				}
				$bodyContent .= "</tracking>";
			}
		}

		//dump(htmlentities($bodyContent), true);
		// TODO - can I abstract this whole process even further? Worth it?
		$result = $this->doWebexRequest("user", "SetUser", $bodyContent);

		//dump($result, true);

		$serverResponse = $result["response"];
		$data = $result["data"]["serv:message"]["serv:body"]["serv:bodyContent"];

		return array(
			"response" => $serverResponse,
			"data"     => $data
		);

	}


// returns null body - there is no body response from the server on this operation.
	public function deleteWebexUser($username) {
		// NOTE: this will only INACTIVATE the user. the user will still exist in the database and record will be returned with user.GetUser!
		// TODO: for rev2, put something in place to rename old users if someone new assumes the same username as an old user.
		$bodyContent = <<<XML
	<webExId>$username</webExId>
XML;

		$result = $this->doWebexRequest("user", "DelUser", $bodyContent);
		$serverResponse = $result["response"];

		return array(
			"response" => $serverResponse,
			"data"     => null
		);
	}

	// returns an array of user details
	public function getWebexUserDetails($username) {
		$bodyContent = <<<XML
			<webExId>$username</webExId>
XML;

		$result = $this->doWebexRequest("user", "GetUser", $bodyContent);

		$serverResponse = $result["response"];
		$data = $result["data"];

		//dump($data);

		$userInfo = $data["serv:message"]["serv:body"]["serv:bodyContent"];

		return array(
			"response" => $serverResponse,
			"data"     => $userInfo
		);
	}

	// sets a user's expiration
	public function setUserExpiration($webExId, $date) {
		// $date -> MM/DD/YYYY HH:MM:SS format

		$bodyContent = <<<XML
<webExId>$webExId</webExId>
<expirationDate>$date</expirationDate>
XML;

		// TODO - can I abstract this whole process even further? Worth it?
		$result = $this->doWebexRequest("user", "SetUser", $bodyContent);

		//dump($result, true);

		$serverResponse = $result["response"];
		$data = $result["data"]["serv:message"]["serv:body"]["serv:bodyContent"];

		return array(
			"response" => $serverResponse,
			"data"     => $data
		);

	}

	// gets all webex users
	public function getAllWebexUsers() {
		return $this->getWebexUsersList(); // no params needed, defaults will suffice.
	}

	public function getActiveWebexUsers() {
		// sorting by last name
		return $this->getWebexUsersList("active", "all", "LASTNAME");
	}

	public function getActiveWebexUsersByWebexId() {
		$result = $this->getActiveWebexUsers();
		$userList = false;

		if ($result["response"]["result"] == "SUCCESS") {
			$userList = $result["data"];

			$userListByWebexId = array();
			foreach ($userList as $u) {
				$webExId = $u["use:webExId"];
				$userListByWebexId[$webExId] = $u;
			}
		}

		return $userList;
	}

	// returns an array of booleans - user exists in webex, user is active in webex
	public function getWebexUserStatus($webExId) {

		$result = $this->getWebexUserDetails($webExId);

		// init
		$exists = false;
		$active = false;

		if ($result["response"]["result"] == "SUCCESS") {
			$exists = true;
			if ($result["data"]["use:active"] == "ACTIVATED") {
				$active = true;
			}
		}

		return array(
			"userExistsInWebEx" => $exists,
			"userActiveInWebEx" => $active
		);
	}

	// get basic site info
	public function getWebexSiteInfo() {
		return $this->doWebexRequest("site", "GetSite", "");
	}

	// get api version info
	public function getApiVersion() {
		return $this->doWebexRequest("ep", "GetAPIVersion", "");
	}

	public function getAllRecordings() {
		// TODO: pagination
		return $this->getRecordingsList();
	}

	public function getRecordingsForUser($webExId) {
		return $this->getRecordingsList(array(
			"webExId" => $webExId,
		));
	}

	function getLatestRecordingForUser($webExId) {
		$recordingsResult = $this->getRecordingsForUser($webExId);
		$recordingsForUser = $recordingsResult["data"]["ep:recording"];

		$threshold = 0;
		$recordingId = null;

		foreach ($recordingsForUser as $key => $rec) {
			$recTimestamp = strtotime($rec["ep:createTime"]);
			if ($recTimestamp > $threshold) {
				$threshold = $recTimestamp;
				$recordingId = $key;
			}
		}

		if (is_null($recordingId)) {
			return null;
		}

		return $recordingsForUser[$recordingId];
	}

// $startTimeString and $endTimeString need to be in MM/dd/yyy HH:mm:ss format
	function getRecordingsForUserBetweenTimestamps($webExId, $startTimeString, $endTimeString) {
		return $this->getRecordingsList(array(
			"webExId"         => $webExId,
			"createTimeStart" => $startTimeString,
			"createTimeEnd"   => $endTimeString
		));
	}

	function getRecordingsAfterTime($startTimeString) {

		//dump($startTimeString);

		return $this->getRecordingsList(array(
			"createTimeStart" => $startTimeString,
			"createTimeEnd"   => gmdate("m/d/Y H:i:s")
		));
	}

	function getRecordingDetails($recordingId) {
		$bodyContent = <<<XML
	<recordingID>$recordingId</recordingID>
XML;

		// because of the dumb API, we need to do THIS to look up by recording ID. then, we can use the session name it gives us to look up the ACTUAL information about the recording.
		$recInfoResult = $this->doWebexRequest("ep", "GetRecordingInfo", $bodyContent);

		//dump($recInfoResult, true);

		if ($recInfoResult["response"]["result"] == "SUCCESS") {
			$title = $recInfoResult["data"]["serv:message"]["serv:body"]["serv:bodyContent"]["ep:basic"]["ep:topic"];

			// encode the ampersands
			$title = str_replace("&", "&amp;", $title);
			//dump($title, true);

			$bodyContent = <<<XML
	<recordName>$title</recordName>
XML;
			$result = $this->doWebexRequest("ep", "LstRecording", $bodyContent);

			//dump($result, true);

			$serverResponse = $result["response"];
			$data = $result["data"]["serv:message"]["serv:body"]["serv:bodyContent"];


		} else {
			$serverResponse = $recInfoResult["response"];
			$data = null;
		}

		// because it's a fuzzy name search, we need to re-verify the recording ID if multiple records are returned so that we give back the right one. dumb, dumb API.
		if (isset($data["ep:recording"][0])) {
			// we need to parse these to find the right one - verify the recording ID
			$correctRecord = null;
			foreach ($data["ep:recording"] as $rec) {
				if ($rec["ep:recordingID"] == $recordingId) {
					$correctRecord = $rec;
				}
			}
			// modify the original call response
			$data["ep:recording"] = $correctRecord;
		}

		return array(
			"response" => $serverResponse,
			"data"     => $data
		);


	}

	function deleteRecording($recordingId) {
		$bodyContent = <<<XML
	<recordingID>$recordingId</recordingID>
XML;
		$result = $this->doWebexRequest("ep", "DelRecording", $bodyContent);

		//dump($result, true);

		$serverResponse = $result["response"];
		$data = $result["data"]["serv:message"]["serv:body"]["serv:bodyContent"];

		return array(
			"response" => $serverResponse,
			"data"     => $data
		);

	}

	/*********************
	 * REPORTING HELPERS *
	 *********************/

	public function listSessions($params = array()) {
		// dates formatted as MM/dd/yyyy HH:mm:ss format

		/*
		 * $params structure:
			"maximumNum"        => max number of meetings to pull
			"createTimeStart"   => time created lower bound
			"createTimeEnd"     => time created upper bound
			"webExId"           => username of webex user to pull
		*/

		$now = new Zend_Date();
		//$createTimeEnd = $now->get("MM/dd/yyyy HH:mm:ss");
		$nowTimestamp = $now->get();

		$oneMonthAgo = clone $now;
		$oneMonthAgo->add("-1", Zend_Date::MONTH);
		$oneMonthAgoTimestamp = $oneMonthAgo->get();

		// need them in GMT...*facepalm*
		$createTimeEnd = gmdate("m/d/Y H:i:s", $nowTimestamp);
		$createTimeStart = gmdate("m/d/Y H:i:s", $oneMonthAgoTimestamp);

		// TODO - handle user input on this. need to accept timestamps and convert to GMT.

		//dump($createTimeStart . ", " . $createTimeEnd, true);
		//dump($createTimeStart . ", " . $createTimeEnd);

		$defaults = array(
			"maximumNum"      => 500, // webex max value as of 4/2016
			"createTimeStart" => $createTimeStart,
			"createTimeEnd"   => $createTimeEnd,
			"webExId"         => null,
			"centers"         => array("mc", "tc", "ec", "sc")
		);

		// set params to defaults if we don't get them passed in.
		foreach ($defaults as $key => $val) {
			if (!isset($params[$key])) {
				$params[$key] = $val;
			}
		}

		//dump($params, true);

		$endpointsToQuery = array();

		foreach ($params["centers"] as $c) {
			switch ($c) {
				case "mc":
					$endpointsToQuery[$c] = "LstmeetingusageHistory";
					break;
				case "tc":
					$endpointsToQuery[$c] = "LsttrainingsessionHistory";
					break;
				case "sc":
					$endpointsToQuery[$c] = "LstsupportsessionHistory";
					break;
				case "ec":
					$endpointsToQuery[$c] = "LsteventsessionHistory";
					break;

			}
		}

		$data = array();

		$errorMessages = array();
		foreach ($endpointsToQuery as $c => $e) {

			$orderBy = "STARTTIME";
			if ($e == "LstsupportsessionHistory") {
				$orderBy = "SESSIONSTARTTIME";
			}

			$bodyContent = <<<XML
<listControl>
	<startFrom>0</startFrom>
	<maximumNum>{$params["maximumNum"]}</maximumNum>
</listControl>
<order>
	<orderBy>$orderBy</orderBy>
	<orderAD>DESC</orderAD>
</order>
<startTimeScope>
	<sessionStartTimeStart>{$params["createTimeStart"]}</sessionStartTimeStart>
	<sessionStartTimeEnd>{$params["createTimeEnd"]}</sessionStartTimeEnd>
</startTimeScope>
XML;

			$result = $this->doWebexRequest("history", $e, $bodyContent);

			$serverResponse = $result["response"]["result"];

			if ($serverResponse == "SUCCESS") {
				$data[$c] = $result["data"]["serv:message"]["serv:body"]["serv:bodyContent"];
			} else {
				$errorMessages[] = "error retrieving center history for endpoint $e. error: {$result["response"]["message"]}";
			}
		}

		return array(
			"response"      => "SUCCESS",
			"data"          => $data,
			"errorMessages" => $errorMessages
		);

	}

	public function getSessionDetails($confID, $center) {
		$endpoint = false;

		switch ($center) {
			case "mc":
				$endpoint = "LstmeetingattendeeHistory";
				$arrayKey = "history:meetingAttendeeHistory";
				break;
			case "tc":
				$endpoint = "LsttrainingattendeeHistory";
				$arrayKey = "history:trainingAttendeeHistory";
				break;
			case "ec":
				$endpoint = "LsteventattendeeHistory";
				$arrayKey = "history:eventAttendeeHistory";
				break;
			case "sc":
				$endpoint = "LstsupportattendeeHistory";
				$arrayKey = "history:supportAttendeeHistory";
				break;
			default:
				$center = false;
				$arrayKey = false;
				break;
		}

		if ($confID && $center) {
			// initialize stuff for first call
			$currentPosition = 1;
			$fetchNumber = 500; // actual fetch number - set here for initial call. only less than the max if specifically asked for less than the max.

			$stopAt = 0;

			$resultSetSize = 0; // to be set later once we know this value from the first API call.

			//$fetchNumber = 1; // testing

			// init result array
			$returnArray = array();

			// loop it!
			do {
				// set XML
				$bodyContent = <<<XML
            <confID>$confID</confID>
			<listControl>
				<startFrom>$currentPosition</startFrom>
				<maximumNum>$fetchNumber</maximumNum>
				<listMethod>AND</listMethod>
			</listControl>
XML;

				// execute call
				$result = $this->doWebexRequest('history', $endpoint, $bodyContent);
//                dd($result);
				$serverResponse = $result['response'];
				$body = $result['body'];

				// process
				if ($serverResponse['result'] == 'SUCCESS') {
					// grab the users and append them to the returnArray.
					$usersReturned = $body[$arrayKey];

					// if the call retrieves multiple records, use:user will be an array of arrays with user info.
					// if the call retrieves a single record, use:user will be only the array with user info.
					// we can get around this by looking at how many records webex tells us it's giving us.

					if ($body['history:matchingRecords']['serv:returned'] == 1) {
						// this is a singleton
						array_push($returnArray, $usersReturned);
					} else {
						// grouping
						$returnArray = array_merge($returnArray, $usersReturned);
					}

					if ($resultSetSize == 0) {
						// if this hasn't been set yet, then set it now.
						$resultSetSize = $body['history:matchingRecords']['serv:total'];
						if ($stopAt == 0) {
							// set this also if it wasn't set before. this'll stop at the size of the resultset.
							$stopAt = $resultSetSize;
						}
					}
				} else {
					$stopAt = $currentPosition; // force the loop to stop.
				}

				$currentPosition += $fetchNumber; // in case pagination is needed. if it's not needed this won't matter anyway.

				$arrayCount = count($returnArray);
				if ($stopAt - $arrayCount < $fetchNumber) {
					$fetchNumber = $stopAt - $arrayCount;
				}

			} while($currentPosition < $stopAt);


			// append count of data to server response info
			$serverResponse['recordsFetched'] = count($returnArray);

//            $result = $this->xmlApiCall("history", $endpoint, $bodyContent);

//            $serverResponse = $result["response"];
//            $data = $result["body"]["serv:message"]["serv:body"]["serv:bodyContent"];

			return array(
				"response" => $serverResponse,
				"data"     => $returnArray
			);
		}

		return array(
			"response" => "error: no meeting key provided, or invalid center value provided.",
			"data"     => null
		);
	}

	public function getSessionsInProgress() {
		$bodyContent = <<<XML
		<serviceType>EventCenter</serviceType>
		<serviceType>MeetingCenter</serviceType>
		<serviceType>TrainingCenter</serviceType>
		<serviceType>SupportCenter</serviceType>
XML;

		$result = $this->doWebexRequest("ep", "LstOpenSession", $bodyContent);

		$serverResponse = $result["response"];
		$data = $result["data"]["serv:message"]["serv:body"]["serv:bodyContent"];

		return array(
			"response" => $serverResponse,
			"data"     => $data
		);
	}

	public function getSessionAttendeeHistory($confID = false, $center = false) {
		$endpoint = false;

		switch ($center) {
			case "mc":
				$endpoint = "LstmeetingattendeeHistory";
				$arrayKey = "history:meetingAttendeeHistory";
				break;
			case "tc":
				$endpoint = "LsttrainingattendeeHistory";
				$arrayKey = "history:trainingAttendeeHistory";
				break;
			case "ec":
				$endpoint = "LsteventattendeeHistory";
				$arrayKey = "history:eventAttendeeHistory";
				break;
			case "sc":
				$endpoint = "LstsupportattendeeHistory";
				$arrayKey = "history:supportAttendeeHistory";
				break;
			default:
				$center = false;
				$arrayKey = false;
				break;
		}

		if ($confID && $center) {
			// initialize stuff for first call
			$currentPosition = 1;
			$fetchNumber = 500; // actual fetch number - set here for initial call. only less than the max if specifically asked for less than the max.

			$stopAt = 0;

			$resultSetSize = 0; // to be set later once we know this value from the first API call.

			//$fetchNumber = 1; // testing

			// init result array
			$returnArray = array();

			// loop it!
			do {
				// set XML
				$bodyContent = <<<XML
            <confID>$confID</confID>
			<listControl>
				<startFrom>$currentPosition</startFrom>
				<maximumNum>$fetchNumber</maximumNum>
				<listMethod>AND</listMethod>
			</listControl>
XML;

				// execute call
				$result = $this->doWebexRequest('history', $endpoint, $bodyContent);
				//dump($result, true);
				$serverResponse = $result['response'];
				$body = $result['data']['serv:message']['serv:body']['serv:bodyContent'];

				// process
				if ($serverResponse['result'] == 'SUCCESS') {
					// grab the history info and append to the returnArray.
					$historyEntries = $body[$arrayKey];
					//dump($historyEntries, true);

					// if the call retrieves multiple records, the $arrayKey index will be an array of arrays with history info.
					// if the call retrieves a single record, the $arrayKey index will be only the array with history info.
					// we can get around this by looking at how many records webex tells us it's giving us.

					if ($body['history:matchingRecords']['serv:returned'] == 1) {
						// this is a singleton
						array_push($returnArray, $historyEntries);
					} else {
						// grouping
						$returnArray = array_merge($returnArray, $historyEntries);
					}

					if ($resultSetSize == 0) {
						// if this hasn't been set yet, then set it now.
						$resultSetSize = $body['history:matchingRecords']['serv:total'];
						if ($stopAt == 0) {
							// set this also if it wasn't set before. this'll stop at the size of the resultset.
							$stopAt = $resultSetSize;
						}
					}
				} else {
					$stopAt = $currentPosition; // force the loop to stop.
				}

				$currentPosition += $fetchNumber; // in case pagination is needed. if it's not needed this won't matter anyway.

				$arrayCount = count($returnArray);
				if ($stopAt - $arrayCount < $fetchNumber) {
					$fetchNumber = $stopAt - $arrayCount;
				}

			} while($currentPosition < $stopAt);


			// append count of data to server response info
			$serverResponse['recordsFetched'] = count($returnArray);

//            $result = $this->xmlApiCall("history", $endpoint, $bodyContent);

//            $serverResponse = $result["response"];
//            $data = $result["body"]["serv:message"]["serv:body"]["serv:bodyContent"];

			return array(
				"response" => $serverResponse,
				"data"     => $returnArray
			);
		}

		return array(
			"response" => "error: no meeting key provided, or invalid center value provided.",
			"data"     => null
		);
	}


	/* RECORDING DOWNLOAD HELPERS */

	function downloadArfFile($recordingId) {
		// manual override for testing
		//$recordingId = "76358147";

		$webExApi = Zend_Registry::get('webexApi');

		//dump($webExApi, true);

		$siteId = $webExApi["siteId"];
		$username = $webExApi["username"];
		$password = $webExApi["password"];


		$host = $webExApi["nbrHost"];
		$endpoint = "/nbr/services/NBRFileOpenService?wsdl";

		$soapUrl = "http://" . $host . $endpoint;

		//dump($soapUrl, true);

		$xml_post_string = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
        <soapenv:Body>
                <ns1:downloadFile soapenv:encodingStyle= "http://schemas.xmlsoap.org/soap/encoding/" xmlns:ns1="nbrXmlService">
                    <siteId xsi:type="xsd:long">$siteId</siteId>
                    <userName xsi:type="xsd:string">$username</userName>
                    <password xsi:type="xsd:string">$password</password>
                    <recordId xsi:type="xsd:long">$recordingId</recordId>
                </ns1:downloadFile>
        </soapenv:Body>
</soapenv:Envelope>
XML;

		//dump($xml_post_string, true);

		$headers = array(
			"POST $endpoint",
			"Host: $host",
			"Content-Type: application/soap+xml; charset=utf-8",
			"Content-Length: " . strlen($xml_post_string),
			"SOAPAction: \"\""
		);

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $soapUrl);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);

		$proxyCreds = Zend_Registry::get('proxy');
		if (!empty($proxyCreds['url']) && !empty($proxyCreds['port'])) {
			curl_setopt($ch, CURLOPT_PROXY, $proxyCreds['url'] . ":" . $proxyCreds['port']);
		}

		$response = curl_exec($ch);

		// if this whole thing parses out as XML, then we probably have an error in there.
		if ($responseAsXml = xml2array($response)) {
			// doubly check to make sure that the soapenv:Fault info is there...
			if (isset($responseAsXml["soapenv:Envelope"]["soapenv:Body"]["soapenv:Fault"])) {
				$faultInfo = $responseAsXml["soapenv:Envelope"]["soapenv:Body"]["soapenv:Fault"];
				$faultCode = $faultInfo["faultcode"];
				$faultMessage = $faultInfo["faultstring"];

				return array(
					"success"  => false,
					"message"  => "SOAP Error: $faultCode; $faultMessage",
					"filename" => null,
					"fileInfo" => null
				);
			} else {
				return array(
					"success"  => false,
					"message"  => "SOAP Error: unexpected XML response from endpoint.",
					"filename" => null,
					"fileInfo" => null
				);
			}
		}

		// if it doesn't parse out as XML, then the file was found...so we're good to continue.


		$re = "/\\S+_Part_\\S+/";
		preg_match($re, $response, $matches);
		//dump($matches);

		$partString = $matches[0];
		$parts = explode($partString, $response);

		// xml response (part 1) will tell us cids.
		$xmlInfo = $parts[1];

		$re = "/cid:[0-9A-Z]+/";
		preg_match_all($re, $xmlInfo, $cids);

		//dump($cids);


		// part 2 is file info (name, size)
		$fileInfo = $parts[2];

		// part 3 is the file itself
		$arfFile = $parts[3];


		// hard coding...
		$cidToGrab = str_replace("cid:", "", $cids[0][1]);
		//dump($cidToGrab);

		// use that CID to help us figure out where to start grabbing...

		// more hard coding...
		$exploded = explode("Content-Id: <$cidToGrab>", $arfFile);
		$trimmed = trim($exploded[1]);

		//dump($trimmed);

		// write file to location using recId as filename.

		$filePath = APPLICATION_PATH . '/../arf-downloads/' . $recordingId . '.arf';
		$fh = fopen($filePath, "w+");
		fwrite($fh, $trimmed);

		$fileInfo = fstat($fh);

		// we're done here - close handles
		fclose($fh);
		curl_close($ch);

		return array(
			"success"  => true,
			"message"  => "Downloaded ARF file ID $recordingId successfully",
			"filename" => $filePath,
			"fileInfo" => $fileInfo
		);

	}

	// TRACKING CODES AND ALL THAT STUFF

	public function getSiteTrackingCodes() {
		$siteInfo = $this->getWebexSiteInfo();

		$trackingCodeInfo = $siteInfo["data"]["serv:message"]["serv:body"]["serv:bodyContent"]["ns1:siteInstance"]["ns1:trackingCodes"]["ns1:trackingCode"];

		$codes = array();
		foreach ($trackingCodeInfo as $code) {
			$name = $code["ns1:name"];
			$subcodes = array();

			if (isset($code["ns1:listValue"])) {
				foreach ($code["ns1:listValue"] as $subcode) {
					if ($subcode["ns1:active"] == "true") {
						$subcodes[] = $subcode["ns1:value"];
					}
				}
			}

			$codes[$name] = $subcodes;
		}

		return $codes;
	}

	public function addTrackingCodesToUser($username, $codes = array()) {
		// codes array should be formatted: [1] => value, [2] => value .... [10] => value
		// to remove a code entirely, put in an index and empty quotes. e.g. [1] => "" . codes not included in the array will simply be unchanged.

		$xmlArray = array();

		$validCodes = true;
		foreach ($codes as $code => $value) {
			$value = trim($value);

			if (is_numeric($code) && $code >= 1 && $code <= 10) {
				$xmlArray[] = "<trackingCode$code>$value</trackingCode$code>";
			} else {
				$validCodes = false;
				break;
			}
		}

		if (!$validCodes) {
			throw new Exception("Error: Tracking Code indexes are not valid - they must be numbers from 1-10.");
		}

		$customXml = "<tracking>";
		$customXml .= implode("", $xmlArray);
		$customXml .= "</tracking>";

		$result = $this->editWebexUser($username, null, $customXml);

		//dump($result, true);
		return $result;
	}


	// TELEPHONY

	public function getSiteSupportedTelephonyOptions() {
		$siteInfo = $this->getWebexSiteInfo();

		$helper = new Wst_Model_Helpers_General();
		$webexTelephonyKeys = $helper->telephonyKeys;

		if ($siteInfo["response"]["result"] == "SUCCESS") {
			$telephonyConfig = $siteInfo["data"]["serv:message"]["serv:body"]["serv:bodyContent"]["ns1:siteInstance"]["ns1:telephonyConfig"];

			$siteTelephony = array();
			foreach ($webexTelephonyKeys as $wstShortCode => $keys) {
				$siteXmlKey = $keys["webexSiteXmlKey"];
				$siteTelephony[$wstShortCode] = $telephonyConfig["ns1:$siteXmlKey"] == "true";
			}

			return array(
				"success"       => true,
				"message"       => "successfully fetched webex site telephony",
				"siteTelephony" => $siteTelephony
			);

		} else {
			return array(
				"success"       => false,
				"message"       => "could not get webex site information",
				"siteTelephony" => null
			);
		}
	}

	public function editTelephonyForUser($username, $telephonyPrivilegeArray, $override = false) {
		$helper = new Wst_Model_Helpers_General();
		$telephonyXml = $helper->getWebexTelephonyTypeXml($telephonyPrivilegeArray, $override);

		$editUserResult = $this->editWebexUser($username, null, $telephonyXml);
		return $editUserResult;
	}

	// RECORDING TRASH - new with API version 10.12

	public function getTrashedRecordings($asUser = null) {
		return $this->doWebexRequest("ep", "LstRecordingInRecycleBin", "", "webexApi", $asUser);
	}

	public function deleteRecordingFromTrash($recordingId, $ownerUsername = null) {

		if (is_null($ownerUsername)) {
			// try to figure out who owns it
			$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
			$recording = $recordingArchiveModel->getRecordingById($recordingId);
			if (is_null($recording)) {
				throw new Exception("Error: recording ID $recordingId does not exist in database. Unable to determine owner.");
			}

			$ownerUsername = $recording["host_webex_id"];
		}

		$xml = <<<XML
		<recordingID>$recordingId</recordingID>
XML;
		return $this->doWebexRequest("ep", "DelRecordingFromRecycleBin", $xml, "webexApi", $ownerUsername);
	}

	public function recoverRecordingFromTrash($recordingId, $ownerUsername = null) {

		if (is_null($ownerUsername)) {
			// try to figure out who owns it
			$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
			$recording = $recordingArchiveModel->getRecordingById($recordingId);
			if (is_null($recording)) {
				throw new Exception("Error: recording ID $recordingId does not exist in database. Unable to determine owner.");
			}

			$ownerUsername = $recording["host_webex_id"];
		}

		$xml = <<<XML
		<recordingID>$recordingId</recordingID>
XML;
		return $this->doWebexRequest("ep", "RecoverRecordingFromRecycleBin", $xml, "webexApi", $ownerUsername);
	}

	// other stuff

	public function getScheduledMeetings() {

		return $this->doWebexRequest("meeting", "LstsummaryMeeting", "");
	}

	public function changeUserPassword($username, $password) {
		$xml = <<<XML
<password>$password</password>
XML;
		return $this->editWebexUser($username, null, $xml);

	}


}