<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 5/9/16
 * Time: 10:36 AM
 */

class Wst_Model_Helpers_ArchiveSessionsHelper {
	public function archivePastSessions($sourceFunction, $startTimestamp = "", $endTimestamp = "") {

		// webex api helper
		$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();

		// log model
		$logModel = new Wst_Model_Dbtable_Log();

		// user list model, for lookups in Training Center
		$userListModel = new Wst_Model_Dbtable_Userlist();

		if ($startTimestamp == "") {
			$startTimestamp = strtotime("first day of this month -3 months 00:00:00 UTC");
		}

		if ($endTimestamp == "") {
			$endTimestamp = time();
		}

		// convert timestamps to what WebEx wants
		$createTimeStart = gmdate("m/d/Y H:i:s", $startTimestamp);
		$createTimeEnd = gmdate("m/d/Y H:i:s", $endTimestamp);

		$meetingsList = $webexApiHelper->listSessions(array(
			"createTimeStart" => $createTimeStart,
			"createTimeEnd"   => $createTimeEnd
		));

		$logModel->addLogMessage('Cronjob Processing', "Fetching sessions between $createTimeStart UTC and $createTimeEnd UTC", 1, 'INFO', 'cronjob', $sourceFunction);

		//dump($meetingsList, true);

		$masterList = array();

		$centerCrosswalk = array(
			"mc" => "Meeting Center",
			"tc" => "Training Center",
			"ec" => "Event Center",
			"sc" => "Support Center"
		);

		if ($meetingsList["response"] == "SUCCESS") {
			$logModel->addLogMessage('Cronjob Processing', "Fetched meetings list successfully.", 1, 'INFO', 'cronjob', $sourceFunction);
			//dump($meetingsList, true);

			// parse out by center now...

			$centerSessionNameCrosswalk = array(
				"mc" => "meetingUsageHistory",
				"tc" => "trainingSessionHistory",
				"ec" => "eventSessionHistory",
				"sc" => "supportSessionHistory"
			);

			foreach($meetingsList["data"] as $centerAbbrev => $list) {
				$centerSessionName = "history:" . $centerSessionNameCrosswalk[$centerAbbrev];

				// is it a single?
				if (isset($list[$centerSessionName]["history:confID"])) {
					$list[$centerSessionName] = array($list[$centerSessionName]);
				}

				//dump($list[$centerSessionName], true);

				foreach($list[$centerSessionName] as $k => $details) {

					//dump($details, true);

					$startTimeKey = (isset($details["history:meetingStartTime"])) ? "history:meetingStartTime" : "history:sessionStartTime";
					$endTimeKey = (isset($details["history:meetingEndTime"])) ? "history:meetingEndTime" : "history:sessionEndTime";

					// accounts for typo in webex api - training center data point misspells Toll as Tolll
					$tollFreeMinutesKey = (isset($details["history:totalCallInTolllfreeMinutes"])) ? "history:totalCallInTolllfreeMinutes" : "history:totalCallInTollfreeMinutes";

					// look up name if training center as the name is inexplicably not included in the API return
					$hostName = null;
					if (isset($details["history:hostName"])) {
						$hostName = $details["history:hostName"];
					} else {
						$userDetails = $userListModel->getUserByWebExId($details["history:hostWebExID"]);
						if ($userDetails) {
							$hostName = $userDetails["firstName"] . " " . $userDetails["lastName"];
						}
					}

					// tracking codes
					$trackingCodesRaw = $details["history:trackingCode"];

					$trackingCodes = array();
					foreach($trackingCodesRaw as $key => $val) {
						$keyAsNumber = str_replace("com:trackingCode", "", $key);
						$trackingCodes[$keyAsNumber] = $val;
					}

					// add to master list
					$masterList[] = array(
						'conf_id'                        => $details["history:confID"],
						'session_name'                   => $details["history:confName"],
						'session_key'                    => $details["history:sessionKey"],
						'center'                         => $centerCrosswalk[$centerAbbrev],
						'host_username'                  => strtolower($details["history:hostWebExID"]),
						'host_name'                      => $hostName,
						'start_time_utc'                 => strtotime($details[$startTimeKey] . " UTC"),
						'end_time_utc'                   => strtotime($details[$endTimeKey] . " UTC"),
						'timezone'                       => $details["history:timeZoneWithDST"],
						'duration'                       => $details["history:duration"],
						'registered_attendees'           => isset($details["history:totalRegistered"]) ? $details["history:totalRegistered"] : null,
						'invited_attendees'              => isset($details["history:totalInvited"]) ? $details["history:totalInvited"] : null,
						'total_attendees'                => isset($details["history:totalParticipants"]) ? $details["history:totalParticipants"] : null,
						'peak_attendee_count'            => $details["history:peakAttendee"],
						'call_in_participants'           => isset($details["history:totalParticipantsCallIn"]) ? $details["history:totalParticipantsCallIn"] : null,
						'call_out_participants'          => isset($details["history:totalParticipantsCallOut"]) ? $details["history:totalParticipantsCallOut"] : null,
						'voip_participants'              => isset($details["history:totalParticipantsVoip"]) ? $details["history:totalParticipantsVoip"] : null,
						'call_in_minutes'                => isset($details["history:totalCallInMinutes"]) ? $details["history:totalCallInMinutes"] : null,
						'call_in_tollfree_minutes'       => isset($details[$tollFreeMinutesKey]) ? $details[$tollFreeMinutesKey] : null,
						'call_out_domestic_minutes'      => isset($details["history:totalCallOutDomestic"]) ? $details["history:totalCallOutDomestic"] : null,
						'call_out_international_minutes' => isset($details["history:totalCallOutInternational"]) ? $details["history:totalCallOutInternational"] : null,
						'voip_minutes'                   => isset($details["history:totalParticipantsVoip"]) ? $details["history:totalParticipantsVoip"] : null,
						'people_minutes'                 => $details["history:totalPeopleMinutes"],
						'tracking_codes'                 => json_encode($trackingCodes),
						'raw_response'                   => json_encode($details)
					);

					//dump($masterList, true);
				}
			}
		}

		$logModel->addLogMessage('Cronjob Processing', "Finished building master list.", 1, 'INFO', 'cronjob', $sourceFunction);

		// session archives table model
		$archivesModel = new Wst_Model_Dbtable_Sessionarchive();
		$sessionArchiveMap = $archivesModel->getArchivesMap();

		$insertedIds = array();
		$skippedIds = array();

		foreach($masterList as $key => $insertData) {
			if (!isset($sessionArchiveMap[$insertData['conf_id']])) {
				// does not already exist in DB.
				$result = $archivesModel->insertArchive($insertData);
				$insertedIds[] = $insertData['conf_id'];
			} else {
				$skippedIds[] = $insertData['conf_id'];
			}
		}

		$logModel->addLogMessage('Cronjob Processing', "Inserted " . count($insertedIds) . pluralize($insertedIds, " record", "s") . " into session archive table. Skipped " . count($skippedIds) . pluralize($skippedIds, " record", "s") . " that already existed.", 1, 'INFO', 'cronjob', $sourceFunction);

		//$et = new Ot_Trigger_Dispatcher();
		//$et->setVariables(array(
		//	'subject'   => 'Apihealthcheck Cronjob Report',
		//	'summary'   => 'API Health Report (Apihealthcheck)',
		//	'details'   => "API failures found in the past $secondsToCheckForFailures seconds ($hoursToCheckForFailures hours): $numberOfFailuresInsideWindow",
		//	'timestamp' => date('Y-m-d @ H:i:s A')
		//));
		//$et->dispatch('Wst_System_Event');

		return array(
			"insertedIds" => $insertedIds,
			"skippedIds"  => $skippedIds
		);
	}
}