<?php

class Wst_Model_Helpers_Lookup {

    public function getUsernameFromLdap($username, $additionalFieldsArray = array())
    {
        $logModel = new Wst_Model_Dbtable_Log();
        $logModel->addLogMessage('Function Start', 'getUsernameFromLdap', 1, 'INFO', 'model', 'getUsernameFromLdap');
        
        ini_set('max_execution_time', 300);

        $ldapCreds = Zend_Registry::get('ldapreader');
        $connect = @ldap_connect($ldapCreds['server'], $ldapCreds['port']);
        ldap_set_option($connect, LDAP_OPT_TIMELIMIT, 5);
        ldap_set_option($connect, LDAP_OPT_NETWORK_TIMEOUT, 5);
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);

        if ($connect) {
            $logModel->addLogMessage('Function Processing', 'LDAP Connect True. Line: ' . __LINE__, 1, 'INFO', 'model', 'getUsernameFromLdap');
            $valid = @ldap_bind($connect, $ldapCreds['dn'], $ldapCreds['password']);
        }

        $returnArray = array();
        
        if($valid) {
            $logModel->addLogMessage('Function Processing', 'LDAP Bind Valid. Line: ' . __LINE__, 1, 'INFO', 'model', 'getUsernameFromLdap');
            $returnArray['bindStatus'] = true;
            
            $allValid = true;
            try {

                $srch = ldap_search($connect,
                    'ou=accounts, o=uncg',
                    'cn=' . strtolower($username)
                );

                $e = ldap_get_entries($connect, $srch);
 
                if ( $e["count"] > 0) {
                    
                    $logModel->addLogMessage('Function Processing', 'LDAP Count > 0. Line: ' . __LINE__ . ' Data: ' . json_encode($e), 1, 'INFO', 'model', 'getUsernameFromLdap');
                    
                    $returnArray['data'] = array(
                        'uidnumber' => $e[0]['uidnumber'][0],    
                        'uid' => $e[0]['uid'][0],
                        'fn' => (isset($e[0]['uncgpreferredgivenname'][0])) ? $e[0]['uncgpreferredgivenname'][0] : $e['givenname'][0],
                        'ln' => (isset($e[0]['uncgpreferredsurname'][0])) ? $e[0]['uncgpreferredsurname'][0] : $e['displayname'][0],
                        'uncgpreferredgivenname' => (isset($e[0]['uncgpreferredgivenname'][0])) ? $e[0]['uncgpreferredgivenname'][0] : NULL,
                        'uncgpreferredsurname' => (isset($e[0]['uncgpreferredsurname'][0])) ? $e[0]['uncgpreferredsurname'][0] : NULL,
                        'uncgemail' => (isset($e[0]['uncgemail'][0])) ? $e[0]['uncgemail'][0] : NULL,
                        'uncgaccounttype' => (isset($e[0]['uncgaccounttype'][0])) ? $e[0]['uncgaccounttype'][0] : NULL,
                        'departmentnumber' => (isset($e[0]['departmentnumber'][0])) ? $e[0]['departmentnumber'][0] : NULL,
                        'uncgorgnshortname' => (isset($e[0]['uncgorgnshortname'][0])) ? $e[0]['uncgorgnshortname'][0] : NULL,
                    );

                    if(!empty($additionalFieldsArray)) {
                        foreach($additionalFieldsArray as $additionalFieldName) {
                            $returnArray['data'][$additionalFieldName] = (isset($e[0][$additionalFieldName][0])) ? $e[0][$additionalFieldName][0] : NULL;
                        }
                    }

                } else {
                    
                    $logModel->addLogMessage('Function Processing', 'LDAP Count < 0. Line: ' . __LINE__, 1, 'INFO', 'model', 'getUsernameFromLdap');
                    
                    $returnArray['data'] = false;
                    $allValid = false;
                }

            } catch (Exception $e) {
                $logModel->addLogMessage('Function Processing', 'LDAP Exception. Line: ' . __LINE__. ' Data: ' . json_encode($e), 1, 'INFO', 'model', 'getUsernameFromLdap');
                
                $allValid = false;
                $returnArray['data'] = $e;
            }   

            $returnArray['status'] = $allValid;
            
            
        } else {
            
            $logModel->addLogMessage('Function Processing', 'LDAP Bind Invalid. Line: ' . __LINE__, 1, 'INFO', 'model', 'getUsernameFromLdap');
            
            $returnArray['status'] = false;
            $returnArray['data'] = false;
            $returnArray['bindStatus'] = false;
            
        }
        
        
        $logModel->addLogMessage('Function End', 'getUsernameFromLdap', 1, 'INFO', 'model', 'getUsernameFromLdap');
        return $returnArray;
        
    }


	/**
	 * @param $usernamesArray - array - the username(s) you wish to check. if not in array format, will be cast to array.
	 * @param string $source - "ldap" or "file" - if "ldap" will perform live LDAP lookup. if "file" will look for cached file.
	 * @param null $checkTeacherFeed - boolean - whether or not to perform the secondary instructor lookup in Canvas feed. if left null, will use system default from application.ini
	 * @param null $sourceArray - array (only used with $source="file") - user feed data already reindexed by username. recommmended if performing a large number of individual lookups, so that file does not need to be decoded and reindexed every time.
	 * @return array
	 */
	public function getUserEligibility($usernamesArray, $source = "ldap", $checkTeacherFeed = null, $sourceArray = null) {

	    // get started
	    $logModel = new Wst_Model_Dbtable_Log();
	    $localConfig = Zend_Registry::get('localConfig');

	    if (is_null($checkTeacherFeed)) {
			// if null, use system default
			$checkTeacherFeed = $localConfig["checkTeacherFeed"];
	    }

 	    // tweak input
	    if (!is_array($usernamesArray)) {
	    	$usernamesArray = array($usernamesArray);
	    }

	    // init results array
	    $resultsArray = array(); // will contain text strings
	    $resultsArray["source"] = $source;
		$resultsArray["checkTeacherFeed"] = $checkTeacherFeed;

	    $resultsArray["errors"] = array();

	    $eligibilityFields = explode(",", $localConfig["eligibilityList"]);
	    $resultsArray["eligibilityFields"] = $eligibilityFields;


	    // check source for lookup
	    if ($source == "ldap") {
		    foreach($usernamesArray as $username) {
			    // init vars
			    $eligibilityStatus = false;


			    $logModel->addLogMessage('Action Processing', "LDAP lookup for user $username started. Line: " . __LINE__, 1, 'INFO', 'controller', __CLASS__);

			    //lookup user in LDAP
			    $thisLdapUser = $this->getUsernameFromLdap($username, $eligibilityFields);

			    if (!$thisLdapUser['status'] || !$thisLdapUser['bindStatus']) {
				    $logModel->addLogMessage('Action Processing', "Bind or Status unsuccessful. Line: " . __LINE__, 1, 'INFO', 'controller', __CLASS__);
				    $resultsArray["errors"][] = "LDAP bind or status unsuccessful for user $username";
				    $resultsArray["data"][$username]["ldapSuccess"] = false;
			    } else {
				    $logModel->addLogMessage('Action Processing', "Bind and Status successful. Line: " . __LINE__, 1, 'INFO', 'controller', __CLASS__);

				    $resultsArray["data"][$username]["ldapSuccess"] = true;
				    $resultsArray["data"][$username]["rawData"] = $thisLdapUser;

				    $accountType = $thisLdapUser['data']['uncgaccounttype'];

				    if ($accountType == 'PRIMARY') {
					    $resultsArray["data"][$username]["isPrimary"] = true;
					    foreach ($eligibilityFields as $field) {
						    $thisField = trim($field);
						    $resultsArray["data"][$username]["qualifiesVia"] = "none";
						    if ($thisLdapUser['data'][$thisField] == "TRUE") {
							    $logModel->addLogMessage('Action Processing', "User $username qualifies via LDAP flag '$thisField'. Line: " . __LINE__, 1, 'INFO', 'controller', __CLASS__);
							    $eligibilityStatus = true;
							    $resultsArray["data"][$username]["qualifiesVia"] = "Has LDAP flag <em>$thisField</em>";
							    break;
						    }
					    }

					    if (!$eligibilityStatus) {
						    $logModel->addLogMessage('Action Processing', "User $username does not qualify via LDAP. Line: " . __LINE__, 1, 'INFO', 'controller', __CLASS__);
						    if ($checkTeacherFeed) {
							    $logModel->addLogMessage('Action Processing', "Checking Canvas Teacher Feed for $username. Line: " . __LINE__, 1, 'INFO', 'controller', __CLASS__);

							    // 2nd-tier check - CST feed
							    $canvasTeacherFeedHelper = new Wst_Model_Helpers_CanvasTeacherFeedHelper();
							    $feedCallResult = $canvasTeacherFeedHelper->isUserCurrentTeacherInCanvas($username);

							    if (!$feedCallResult["success"]) {
								    $logModel->addLogMessage('Action Processing', 'Canvas Teacher Feed API call failed. Message: ' . $feedCallResult["message"] . '; Line: ' . __LINE__, 2, 'WARNING', 'controller', __CLASS__);
								    $resultsArray["data"][$username]["errors"][] = "Canvas Teacher Feed API call failed.";
							    } else {
								    if ($feedCallResult["isTeacher"]) {
									    $logModel->addLogMessage('Action Processing', "User $username qualifies via Canvas Teacher Feed. Line: " . __LINE__, 1, 'INFO', 'controller', __CLASS__);
									    $eligibilityStatus = true;
									    $resultsArray["data"][$username]["qualifiesVia"] = "Instructor of Record";
								    }
							    }
						    } else {
							    $logModel->addLogMessage('Action Processing', "Bypassing Canvas Teacher Feed check. Line: " . __LINE__, 1, 'INFO', 'controller', __CLASS__);
						    }

					    }
				    } else {
					    $logModel->addLogMessage('Action Processing', "Account $username is not primary account. Line: " . __LINE__, 1, 'INFO', 'controller', __CLASS__);
					    $resultsArray["data"][$username]["isPrimary"] = false;
				    }

				    $logModel->addLogMessage('Action Processing', "LDAP lookup for user $username ended. Line: " . __LINE__, 1, 'INFO', 'controller', __CLASS__);

			    }
			    $resultsArray["data"][$username]["eligible"] = $eligibilityStatus;
		    }

	    } else if ($source == "file") {
		    // default
		    $continueProcessing = true;

	        // source was provided
		    if (!is_null($sourceArray)) {
			    $userDataArrayByUsername = $sourceArray;
		    } else {
			    // look up users in the cached LDAP file

			    $usernamesArray = array_map('strtolower', $usernamesArray);

			    $filePath = $localConfig["userDiffFilePath"];
			    $userDataArray = null;

			    $trigger = false;

			    //$usersStaged = 0;

			    if (file_exists($filePath)) {
				    // get file contents
				    $userDataJson = file_get_contents($filePath);
				    //$logModel->addLogMessage('Cronjob Processing', "Staging file found successfully at $filePath", 1, 'INFO', 'cronjob', 'Wst_Cronjob_Stageusers');

				    // check whether valid or missing
				    $userDataArray = @json_decode($userDataJson, true);

				    //dump($userDataArray, true);

				    if ($userDataArray === null) {
					    $continueProcessing = false;
					    $logSummary = "Error parsing users from local source";
					    if (json_last_error() !== JSON_ERROR_NONE) {
						    // there was an error parsing the JSON.
						    $logError = "Error parsing the LDAP source file JSON - invalid JSON detected";
						    $logModel->addLogMessage('Cronjob Processing', $logError, 2, 'WARNING', 'cronjob', __CLASS__);
						    $trigger = true;
					    } else {
						    // no data in the JSON file
						    $logError = "No data found in JSON file.";
						    $logModel->addLogMessage('Cronjob Processing', $logError, 1, 'INFO', 'cronjob', __CLASS__);
						    // no need to trigger for this one.
					    }
				    }

				    // reindex
				    $userDataArrayByUsername = array();
				    foreach($userDataArray as $index => $user) {
					    $userDataArrayByUsername[strtolower($user["uid"])] = $user;
				    }

			    } else {
				    $continueProcessing = false;
				    $logSummary = "Error parsing users from local source";
				    $logError = "LDAP data file NOT FOUND at $filePath.";
				    $logModel->addLogMessage('Cronjob Processing', $logError, 2, 'WARNING', 'cronjob', __CLASS__);
				    $trigger = true;
			    }
		    }

		    //dump($userDataArray, true);

		    if ($continueProcessing) {

			    foreach($usernamesArray as $uncgUsername) {
				    $uncgUsername = strtolower($uncgUsername);

				    $eligibleViaLdap = false;

				    $resultsArray["data"][$uncgUsername]["qualifiesVia"] = "none";

				    if (isset($userDataArrayByUsername[$uncgUsername])) {
				    	$user = $userDataArrayByUsername[$uncgUsername];

					    $resultsArray["data"][$uncgUsername]["rawData"] = $user;

					    if ($user["status"] != "not-in-ldap") {
						    foreach ($eligibilityFields as $field) {
							    if ($user[$field] == "TRUE") {
								    $eligibleViaLdap = true;
								    //$resultsArray["data"][$uncgUsername]["eligibleForWebEx"] = true;
								    $resultsArray["data"][$uncgUsername]["qualifiesVia"] = "Has LDAP flag <em>$field</em>";
								    break; // from foreach
							    }
						    }
					    }

					    $resultsArray["data"][$uncgUsername]["eligible"] = $eligibleViaLdap;

					    if (!$eligibleViaLdap) {
						    if ($checkTeacherFeed) {
							    // 2nd-tier check - CST feed
							    $canvasTeacherFeedHelper = new Wst_Model_Helpers_CanvasTeacherFeedHelper();
							    $feedCallResult = $canvasTeacherFeedHelper->isUserCurrentTeacherInCanvas($uncgUsername);

							    if (!$feedCallResult["success"]) {
								    $logModel->addLogMessage('Action Processing', 'Canvas Teacher Feed API call failed. Message: ' . $feedCallResult["message"] . '; Line: ' . __LINE__, 2, 'WARNING', 'controller', __CLASS__);
								    $resultsArray["errors"][] = "Canvas Teacher Feed API call failed for user $uncgUsername.";
							    } else {
								    if ($feedCallResult["isTeacher"]) {
									    $logModel->addLogMessage('Action Processing', "User $uncgUsername qualifies via Canvas Teacher Feed. Line: " . __LINE__, 1, 'INFO', 'controller', __CLASS__);
									    $resultsArray["data"][$uncgUsername]["eligible"] = true;
									    $resultsArray["data"][$uncgUsername]["qualifiesVia"] = "User qualifies via Instructor of Record";
								    }
							    }
						    } else {
							    $logModel->addLogMessage('Action Processing', "Bypassing Canvas Teacher Feed check. Line: " . __LINE__, 1, 'INFO', 'controller', __CLASS__);
						    }
					    }

				    } else {
					    $resultsArray["data"][$uncgUsername]["rawData"] = null;
					    $resultsArray["data"][$uncgUsername]["eligible"] = false;
				    }
			    }

		    } else {
			    // we didn't process for some reason.
			    $resultsArray["errors"][] = $logError;

			    if ($trigger) {
				    if (!isset($logSummary)) {
					    $logSummary = "Unknown error. Something went wrong.";
				    }

				    if (!isset($logError)) {
					    $logError = "Unknown error. Something went wrong.";
				    }

				    $et = new Ot_Trigger_Dispatcher();
				    $et->setVariables(array(
					    'subject'   => 'LDAP Eligibility Lookup WARNING',
					    'summary'   => $logSummary,
					    'details'   => $logError,
					    'timestamp' => date('Y-m-d @ H:i:s A')
				    ));

				    $et->dispatch('Wst_System_Event');
			    }
		    }
	    } else {
	        // lookup source isn't set properly
	        $resultsArray["source"] = false;
		    $resultsArray["errors"][] = "Invalid source provided: $source";
	    }

	    return $resultsArray;
    }
    
}
