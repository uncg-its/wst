<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 5/9/16
 * Time: 10:36 AM
 */

class Wst_Model_Helpers_ArchiveRecordingsHelper {
	public function archivePastRecordings($sourceFunction, $days = "") {

		// webex API helper
		$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();

		// log model
		$logModel = new Wst_Model_Dbtable_Log();

		if (!is_numeric($days)) {
			$days = "";
		}

		if ($days == "") {
			$fetchRecordingsResult = $webexApiHelper->getAllRecordings();
			$sinceString = "";
		} else {
			$now = time();
			$then = strtotime("-$days days", $now);
			$thenGmt = gmdate("m/d/Y 00:00:00", $then);

			$sinceString = " since $thenGmt UTC";

			$fetchRecordingsResult = $webexApiHelper->getRecordingsAfterTime($thenGmt);
		}

		$logModel->addLogMessage('Cronjob Processing', "Looking for WebEx recordings$sinceString", 1, 'INFO', 'cronjob', $sourceFunction);

		if ($fetchRecordingsResult["response"]["result"] == "SUCCESS") {
			// is it a single?
			$recordingsList = $fetchRecordingsResult["data"]["ep:recording"];
			if (isset($recordingsList["ep:recordingID"])) {
				$recordingsList = array($recordingsList);
			}

			$logModel->addLogMessage('Cronjob Processing', "Retrieved " . count($recordingsList) . pluralize($recordingsList, " recording", "s") . " from WebEx$sinceString", 1, 'INFO', 'cronjob', $sourceFunction);

			// get existing records map by recordingId
			$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
			$existingRecordings = $recordingArchiveModel->getRecordingArchiveMap();

			$logModel->addLogMessage('Cronjob Processing', "Retrieved " . count($existingRecordings) . pluralize($existingRecordings, " recording", "s") . " from local database.", 1, 'INFO', 'cronjob', $sourceFunction);

			// init tracking
			$insertedIds = array();
			$skippedIds = array();

			foreach($recordingsList as $key => $rec) {
				$id = $rec["ep:recordingID"];

				if (!isset($existingRecordings[$id])) {
					$insertData = array(
						'recording_id'      => $id,
						'conf_id'           => $rec["ep:confID"],
						'host_webex_id'     => strtolower($rec["ep:hostWebExID"]),
						'name'              => $rec["ep:name"],
						'create_time_utc'   => strtotime($rec["ep:createTime"]), // this one is local...no need to append UTC
						'size_mb'           => $rec["ep:size"],
						'duration_sec'      => $rec["ep:duration"],
						'stream_url'        => $rec["ep:streamURL"],
						'file_url'          => $rec["ep:fileURL"],
						'format'            => $rec["ep:format"]
					);

					$insertResult = $recordingArchiveModel->insertRecording($insertData);

					$insertedIds[] = $id;
				} else {
					$skippedIds[] = $id;
				}

			}

			$logModel->addLogMessage('Cronjob Processing', "Inserted " . count($insertedIds) . pluralize($insertedIds, " record", "s") . " into recordings archive table. Skipped " . count($skippedIds) . pluralize($skippedIds, " record", "s") . " that already existed.", 1, 'INFO', 'cronjob', $sourceFunction);

			return true;


		} else {
			$errorMessage = $fetchRecordingsResult["response"]["message"];

			// log the error
			$logModel->addLogMessage('Cronjob Processing', "API call returned error: $errorMessage", 1, 'INFO', 'cronjob', $sourceFunction);

			if ($fetchRecordingsResult["response"]["exceptionId"] == "000015") {
				// if it's not a simple "no records found" error - trigger it.
				// TODO - this.
			}

			return false;
		}


	}
}