<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 5/9/16
 * Time: 10:36 AM
 */

class Wst_Model_Helpers_MetricsHelper {
	public function getCommonMetricsBetweenTimestamps($start, $end) {

		// USERS
		$userListModel = new Wst_Model_Dbtable_Userlist();
		// total number of active users
		$activeUsers = $userListModel->getActiveUsersList();
		$numberOfActiveUsers = count($activeUsers);

		// RECORDINGS
		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$recordingsAgainstQuota = $recordingArchiveModel->getRecordingArchivesThatCountAgainstQuota();
		// recordings from this day
		$recordingsFromTimeframe = $recordingArchiveModel->getRecordingsBetweenTimestamps($start, $end);
		$numberOfRecordingsMade = count($recordingsFromTimeframe);
		// total recording storage used
		$recordingStorageUsed = 0;
		foreach($recordingsAgainstQuota as $r) {
			if ($r["create_time_utc"] <= $end) {
				$recordingStorageUsed += $r["size_mb"];
			}
		}

		// SESSIONS
		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();
		// sessions from previous day
		$sessions = $sessionArchiveModel->getArchivesBetweenTimestamps($start, $end);
		$numberOfSessions = count($sessions);
		// sessions by host / unique hosts
		$sessionsByHost = array();
		foreach ($sessions as $index => $session) {
			$hostUsername = $session["host_username"];
			if (!isset($sessionsByHost[$hostUsername])) {
				$sessionsByHost[$hostUsername] = 0;
			} else {
				$sessionsByHost[$hostUsername] += 1;
			}
		}
		$uniqueSessionHosts = count($sessionsByHost);

		// sessions with at least 1 recording from previous day
		$sessionsWithRecording = $recordingArchiveModel->getRecordedSessionsBetweenTimestamps($start, $end);
		$numberOfSessionsWithRecording = count($sessionsWithRecording);
		// session length & people minutes
		$peopleMinutes = 0;
		$metricsConfig = Zend_Registry::get("metricsDashboard");
		$sessionLengthThresholds = explode(",", $metricsConfig["sessionLengthThresholds"]);
		$sessionLengthThresholds = array_map("trim", $sessionLengthThresholds);

		// init array
		$sessionLengthCount = array();

		foreach ($sessionLengthThresholds as $index => $limit) {
			$upperBoundIndex = $index + 1;

			if (!isset($sessionLengthThresholds[$upperBoundIndex])) {
				$upperBound = 99999;
				$upperBoundText = "OrHigher";
			} else {
				$upperBound = $sessionLengthThresholds[$upperBoundIndex];
				$upperBoundText = "To" . ($upperBound - 1);
			}

			$lengthKey = "numberOfSessionsWithLength" . $limit . $upperBoundText;

			if (!isset($sessionLengthCount[$lengthKey])) {
				$sessionLengthCount[$lengthKey] = 0;
			}
		}

		// populate array
		foreach($sessions as $s) {
			// people minutes
			$peopleMinutes += $s["people_minutes"];

			// session length
			foreach($sessionLengthThresholds as $index => $limit) {
				$upperBoundIndex = $index + 1;

				if (!isset($sessionLengthThresholds[$upperBoundIndex])) {
					$upperBound = 99999;
					$upperBoundText = "OrHigher";
				} else {
					$upperBound = $sessionLengthThresholds[$upperBoundIndex];
					$upperBoundText = "To" . ($upperBound - 1);
				}

				$lengthKey = "numberOfSessionsWithLength" . $limit . $upperBoundText;

				if ($s["duration"] >= $limit && $s["duration"] < $upperBound) {
					$sessionLengthCount[$lengthKey]++;
				}
			}

			//// parse session length into usable data columns
			//foreach($sessionLengthCount as $key => $count) {
			//
			//}
		}


		// TELEPHONY
		// number of sessions with telephony
		$sessionsWithTelephony = $sessionArchiveModel->getTelephonyUsageBetweenTimestamps($start, $end);
		$numberOfSessionsWithTelephony = count($sessionsWithTelephony);
		// number of call-in and call-out users / minutes
		$callInUsers = 0;
		$callOutUsers = 0;
		$callInMinutes = 0;
		$callInTollFreeMinutes = 0;
		$callOutDomesticMinutes = 0;
		$callOutInternationalMinutes = 0;
		foreach($sessionsWithTelephony as $s) {
			$callInUsers += $s["call_in_participants"];
			$callOutUsers += $s["call_out_participants"];
			$callInMinutes += $s["call_in_minutes"];
			$callInTollFreeMinutes += $s["call_in_tollfree_minutes"];
			$callOutDomesticMinutes += $s["call_out_domestic_minutes"];
			$callOutInternationalMinutes += $s["call_out_international_minutes"];
		}

		// ATTENDEES
		// total attendees from previous day
		$attendeeArchiveModel = new Wst_Model_Dbtable_Attendeearchive();
		$attendees = $attendeeArchiveModel->getArchivesBetweenTimestamps($start, $end);
		$numberOfAttendees = count($attendees);
		// unique attendees from previous day
		$attendeesUnique = $attendeeArchiveModel->getUniqueAttendeesBetweenTimestamps($start, $end);
		$numberOfAttendeesUnique = count($attendeesUnique);
		// mobile users
		$numberOfMobileAppUsers = 0;
		foreach($attendees as $a) {
			if (strpos($a["client_agent"], "Standalone App")) {
				$numberOfMobileAppUsers++;
			}
		}

		// TRACKING CODE STUFF
		// # of departments with at least one user

		// # of departments with at least one host today

		// # of departments with at least one host in the last week

		// # of departments with at least one host in the last month

		// # of departments with at least one host in the last 3 months

		// # of departments with at least one attendee today

		// # of departments with at least one attendee in the last month

		// # of departments with at least one attendee in the last 3 months



		// WE'VE GOT THEM! NOW PUT THEM INTO AN ARRAY.

		$encodedFieldsArray = array(
			"numberOfActiveUsers" => $numberOfActiveUsers,
			"numberOfSessions"    => $numberOfSessions
		);

		$encodedFieldsArray = array_merge($encodedFieldsArray, $sessionLengthCount);

		$encodedFieldsArray = array_merge($encodedFieldsArray, array(
			"uniqueSessionHosts"            => $uniqueSessionHosts,
			"numberOfSessionsWithRecording" => $numberOfSessionsWithRecording,
			"numberOfSessionsWithTelephony" => $numberOfSessionsWithTelephony,
			"numberOfAttendees"             => $numberOfAttendees,
			"numberOfAttendeesUnique"       => $numberOfAttendeesUnique,
			"numberOfMobileAppUsers"        => $numberOfMobileAppUsers,
			"callInUsers"                   => $callInUsers,
			"callOutUsers"                  => $callOutUsers,
			"callInMinutes"                 => $callInMinutes,
			"callInTollFreeMinutes"         => $callInTollFreeMinutes,
			"callOutDomesticMinutes"        => $callOutDomesticMinutes,
			"callOutInternationalMinutes"   => $callOutInternationalMinutes,
			"peopleMinutes"                 => $peopleMinutes,
			"numberOfRecordingsMade"        => $numberOfRecordingsMade,
			"recordingStorageUsed"          => $recordingStorageUsed
		));

		$metricsArray = array(
			"date"    => date("Y-m-d", $start),
			"rawData" => json_encode($encodedFieldsArray)
		);

		//$metricsArray = array_merge($metricsArray, $sessionLengthCount);
		//
		//
		//$metricsArray = array_merge($metricsArray, array(
		//	"uniqueSessionHosts"            => $uniqueSessionHosts,
		//	"numberOfSessionsWithRecording" => $numberOfSessionsWithRecording,
		//	"numberOfSessionsWithTelephony" => $numberOfSessionsWithTelephony,
		//	"numberOfAttendees"             => $numberOfAttendees,
		//	"numberOfAttendeesUnique"       => $numberOfAttendeesUnique,
		//	"numberOfMobileAppUsers"        => $numberOfMobileAppUsers,
		//	"callInUsers"                   => $callInUsers,
		//	"callOutUsers"                  => $callOutUsers,
		//	"callInMinutes"                 => $callInMinutes,
		//	"callInTollFreeMinutes"         => $callInTollFreeMinutes,
		//	"callOutDomesticMinutes"        => $callOutDomesticMinutes,
		//	"callOutInternationalMinutes"   => $callOutInternationalMinutes,
		//	"peopleMinutes"                 => $peopleMinutes,
		//	"numberOfRecordingsMade"        => $numberOfRecordingsMade,
		//	"recordingStorageUsed"          => $recordingStorageUsed
		//));

		return $metricsArray;

	}

	public function sendMetrics($metricsJson, $date) {

	    $proxy = Zend_Registry::get('proxy');
		$metricsDashboardConfig = Zend_Registry::get('metricsDashboard');

		$endpoint = $metricsDashboardConfig['endpoint'];

		$client = new Zend_Http_Client($endpoint); // no proxy for this one

		$client->setMethod(Zend_Http_Client::POST);

		// add required params
		$serviceId = $metricsDashboardConfig["serviceId"];
        $timestamp = time();

        $sendData = array(
            "date" => $date,
            "serviceId" => $serviceId,
            "timestamp" => $timestamp,
            "rawData" => json_decode($metricsJson, true)
        );

		$client->setParameterPost(array(
			"key" => $metricsDashboardConfig["token"],
			"jsonData" => json_encode($sendData)
			//"jsonData" => json_encode(array("crap" => "crap"))
		));

//		dump(json_encode($sendData), true);

		//dump($client, true);

		try {
			$response = $client->request('POST');

			$version = $response->getVersion();
			$status = $response->getStatus();
			$message = $response->getMessage();
			$body = $response->getBody();

			$isSuccessful = $response->isSuccessful();

			return array(
				"success" => $isSuccessful,
				"version" => "HTTP/$version",
				"code"    => $status,
				"message" => $message,
				"body"    => $body
			);

		} catch(Exception $e) {
			// do stuff here
			return array(
				"success" => false,
				"version" => "",
				"code"    => "",
				"message" => $e->getMessage()
			);
		}
	}
}