<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 5/9/16
 * Time: 10:36 AM
 */

class Wst_Model_Helpers_CanvasTeacherFeedHelper {
	public function isUserCurrentTeacherInCanvas($username) {
		$proxy = Zend_Registry::get('proxy');
		$canvasTeacherFeedConfig = Zend_Registry::get('canvasTeacherFeed');

		$endpoint = $canvasTeacherFeedConfig['endpoint'];

		$client = new Zend_Http_Client($endpoint); // no proxy for this one

		$client->setMethod(Zend_Http_Client::POST);

		$client->setParameterPost(array(
			"key" => $canvasTeacherFeedConfig["apiKey"],
			"email" => $username . "@uncg.edu"
			//"email" => "notfound@uncg.edu"
		));

		try {
			$response = $client->request('POST');

			$version = $response->getVersion();
			$status = $response->getStatus();
			$message = $response->getMessage();
			$body = json_decode($response->getBody(), true);

			$isSuccessful = $response->isSuccessful();

			$isTeacher = $body["data"][0];

			return array(
				"success"   => $isSuccessful,
				"version"   => "HTTP/$version",
				"code"      => $status,
				"message"   => $message,
				"body"      => $body,
				"isTeacher" => $isTeacher
			);

		} catch(Exception $e) {
			// do stuff here
			return array(
				"success" => false,
				"version" => "",
				"code"    => "",
				"message" => $e->getMessage(),
			);
		}
	}

}