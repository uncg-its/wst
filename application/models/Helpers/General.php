<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/26/16
 * Time: 2:31 PM
 */

class Wst_Model_Helpers_General {

	// HARD DEFINED HERE.
	// order matters. wst short name is key.
	public $telephonyKeys = array(
		"toll-call-in"           => array(
			"webexSiteXmlKey" => "callInTeleconferencing",
			"webexUserXmlKey" => "teleConfCallIn",
			"wstShortName"    => "toll-call-in",
			"wstDisplayName"  => "Toll Call-In"
		),
		"toll-free-call-in"      => array(
			"webexSiteXmlKey" => "tollFreeCallinTeleconferencing",
			"webexUserXmlKey" => "teleConfTollFreeCallIn",
			"wstShortName"    => "toll-free-call-in",
			"wstDisplayName"  => "Toll-Free Call-In"
		),
		"call-out"               => array(
			"webexSiteXmlKey" => "callBackTeleconferencing",
			"webexUserXmlKey" => "teleConfCallOut",
			"wstShortName"    => "call-out",
			"wstDisplayName"  => "Call Out"
		),
		"call-in-international"  => array(
			"webexSiteXmlKey" => "intlCallInTeleconferencing",
			"webexUserXmlKey" => "teleConfCallInInternational",
			"wstShortName"    => "call-in-international",
			"wstDisplayName"  => "Call-In (international)"
		),
		"call-out-international" => array(
			"webexSiteXmlKey" => "callBackTeleconferencing",
			"webexUserXmlKey" => "teleConfCallOutInternational",
			"wstShortName"    => "call-out-international",
			"wstDisplayName"  => "Call-Out (International)"
		),
		"third-party"            => array(
			"webexSiteXmlKey" => "supportOtherTypeTeleconf",
			"webexUserXmlKey" => "otherTelephony",
			"wstShortName"    => "third-party",
			"wstDisplayName"  => "Third Party (Other)"
		)
	);

	// METHODS...

	public function endsWith($haystack, $needle, $case = true) {
		if ($case) {
			return (strcmp(substr($haystack, strlen($haystack) - strlen($needle)), $needle) === 0);

		}
		return (strcasecmp(substr($haystack, strlen($haystack) - strlen($needle)), $needle) === 0);
	}

	public function getResponseCode() {

		try {
            $webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
            $result = $webexApiHelper->getWebexSiteInfo();

            // we expect 'HTTP/1.1 200 Connection Established'
            $rawHttpCode = $result['response']['httpCode'];

            if ($rawHttpCode == 'HTTP/1.1 200 Connection Established') {
                $isSuccessful = true;
            } else {
                if (strpos($rawHttpCode, ' 200 ') !== false) {
                    $isSuccessful = true;
                }
                else {
                    $isSuccessful = false;
                }
            }

            preg_match('/HTTP\/[0-9].[0-9]/', $rawHttpCode, $version);
            $version = $version[0];

            preg_match('/ [0-9]{3} /', $rawHttpCode, $status);
            $status = trim($status[0]);

            $message = $result['response']['message'];

			return array(
				"success" => $isSuccessful,
				"version" => $version,
				"code"    => $status,
				"message" => $message
			);

		} catch(Exception $e) {
			// do stuff here
			return array(
				"success" => false,
				"version" => "",
				"code"    => "",
				"message" => $e->getMessage()
			);
		}
		//echo '<pre>'; print_r($response); die('public function getResponseCode $response');

	}

	public function reindexArray($array, $key) {
		// reindexes $array making values in field $key the new key.

		if (count($array) > 0) {
			$returnArray = array();
			foreach ($array as $r) {
				$returnArray[$r[$key]] = $r;
			}
			return $returnArray;
		} else {
			return false;
		}
	}

	public function displayOsIcon($osString) {
		switch (strtolower($osString)) {
			case "windows":
				$icon = "windows";
				break;
			case "mac":
				$icon = "apple";
				break;
			case "linux":
				$icon = "linux";
				break;
			case "iphone":
			case "android":
				$icon = "mobile";
				break;
			default:
				return "";
				break;
		}

		return "<i class='fa fa-$icon'></i> ";
	}

	public function displayBrowserIcon($browserString) {
		switch (strtolower($browserString)) {
			case "safari":
				$icon = "safari";
				break;
			case "firefox":
				$icon = "firefox";
				break;
			case "chrome":
				$icon = "chrome";
				break;
			case "ie":
				$icon = "internet-explorer";
				break;
			case "edge":
				$icon = "edge";
				break;
			case "opera":
				$icon = "opera";
				break;
			case "standalone app":
				$icon = "square";
				break;
			default:
				return "";
				break;
		}

		return "<i class='fa fa-$icon'></i> ";
	}

	public function convertGmtToLocal($gmtDateTimeString) {
		// $gmtDateTimeString should be formatted like in WebEx... MM/DD/YY HH:MM:SS
		// uses system timezone, which is set in Zend framework
		$gmtDateTimeString .= " UTC";
		$timestamp = strtotime($gmtDateTimeString);
		return date("m/d/Y H:i:s", $timestamp);
	}

	public function secondsToTime($original) {
		$hours = floor($original / (60 * 60));
		$minutes = floor(($original / 60) % 60);
		$seconds = floor($original % 60);

		return sprintf("%02d:%02d:%02d", $hours, $minutes, $seconds);
	}

	public function getWebexErrorMessageFromCode($ec) {
		// error codes and corresponding messages, from website: xxxxxxxx, last verified 6/1/2016
		$errorMessages = array(
			1  => "SSO protocol error",
			2  => "No user name found in SAML assertion",
			3  => "No user account found in the system",
			4  => "No X.509 certificate found in the system",
			5  => "Only POST request is supported",
			6  => "Incorrect SAML SSO POST data",
			7  => "The site is not allowed to use SSO",
			8  => "Incorrect X.509 certificate to validate SAML assertion",
			9  => "Loading configuration error",
			10 => "The value of NameQualifier does not match site URL",
			11 => "Unable to reach Assertion Party",
			12 => "Failed to resolve SAML Artifact",
			13 => "Invalid SAML assertion",
			14 => "Recipient does not match webex.com",
			15 => "X.509 certificate has expired",
			16 => "User account is locked",
			17 => "User account is expired",
			18 => "User account has been deactivated",
			19 => "SAML assertion is expired",
			20 => "SAML assertion is unsigned",
			21 => "User role is not allowed to login",
			22 => "Invalid RequestedSecurityToken",
			23 => "Invalid digital signature",
			24 => "Untrusted Issuer",
			25 => "Name Identifier format is incorrect",
			26 => "Unable to generate AuthnRequest",
			27 => "Unable to generate Logout Request",
			28 => "InResponseTo does not match the request ID",
			29 => "Invalid Response message",
			30 => "Invalid Request message",
			31 => "Auto Account Creation failed"
		);

		$message = "Unknown Error";

		if (isset($errorMessages[$ec])) {
			$message = $errorMessages[$ec];
		}

		return $message;
	}

	public function textareaToArray($data) {
		$separatorList = array(",", " ", ";", "\n");
		// strip out newlines, including multiples
		$dataNoNewlines = trim(preg_replace('/\s\s+/', ' ', $data));
		// convert all delimiters to this special one
		$replacedDelimiters = str_replace($separatorList, "@@@@@", $dataNoNewlines);
		// create array
		$returnArray = explode("@@@@@", $replacedDelimiters);
		// dump($returnArray, $die = true);

		return $returnArray;
	}

	public function incrementRecordingFailures($id, $action) {
		$validActions = array("download", "upload", "delete");
		if (!in_array($action, $validActions)) {
			throw new Exception("Error: action must be one of the following: " . implode(", ", $validActions));
		}

		$actionString = ucwords($action);
		$databaseField = $action . "_failures";
		$configVar = "arf" . $actionString . "Retries";

		$logModel = new Wst_Model_Dbtable_Log();
		$configModel = new Wst_Model_Dbtable_Config();
		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();

		// get existing
		$logModel->addLogMessage('Function Start', __CLASS__, 1, 'INFO', 'helper', __CLASS__);
		$logModel->addLogMessage('Function Processing', "Incrementing recording $actionString failure count for recording $id", 1, 'INFO', 'helper', __CLASS__);

		$recording = $recordingArchiveModel->find($id);
		if (!is_null($recording)) {
			$recording = $recording->toArray();
			$failures = $recording[$databaseField] + 1;

			$updateData = array(
				$databaseField => $failures
			);

			// are we over threshold?

			$allowedFailures = $configModel->getVar($configVar);

			if ($failures > $allowedFailures) {
				// YES!
				$triggerString = "Recording $id has failed to $actionString $failures time(s), exceeding the allowed limit of $allowedFailures. ";

				if ($action == "download") {
					$updateData["download_status"] = "failed";
					$triggerString .= "Manual intervention is required in the WST app.";
				} else if ($action == "upload") {
					$updateData["upload_status"] = "failed";
					$triggerString .= "Manual intervention is required in the WST app.";
				} else if ($action == "delete") {
					$updateData["status"] = "deleted-by-user"; // assuming deleted by user.
					$triggerString .= "Assuming recording has been deleted by user.";
				}

				$logModel->addLogMessage('Function Processing', $triggerString, 2, 'WARNING', 'helper', __CLASS__);

				$et = new Ot_Trigger_Dispatcher();
				$et->setVariables(array(
					'subject'   => "Recording $actionString Failure WARNING",
					'summary'   => "WARNING - Recording $actionString failures exceed allowed limit (" . __CLASS__ . ")",
					'details'   => $triggerString,
					'timestamp' => date('Y-m-d @ H:i:s A')
				));
				$et->dispatch('Wst_System_Event');

			}

			$where = $recordingArchiveModel->getAdapter()->quoteInto("recording_id = ?", $id);
			$updateResult = $recordingArchiveModel->update($updateData, $where);

			$logModel->addLogMessage('Function Result', "Recording $id information updated successfully. $actionString failures: $failures.", 1, 'INFO', 'helper', __CLASS__);

		} else {
			$logModel->addLogMessage('Function Result', "Recording $id not found. No changes made.", 1, 'INFO', 'helper', __CLASS__);
			$updateResult = false;
		}


		$logModel->addLogMessage('Function End', __CLASS__, 1, 'INFO', 'helper', __CLASS__);

		return $updateResult;

	}

	public function getModifiedDownloadLink($downloadLink) {
		$configModel = new Wst_Model_Dbtable_Config();
		$downloadLinkReplacement = $configModel->getVar('downloadLinkReplacement');

		if ($downloadLinkReplacement != "") {
			$urlBits = explode("?RCID=", $downloadLink);
			$downloadLink = $downloadLinkReplacement . "?RCID=" . $urlBits[1];
		}

		return $downloadLink;
	}

	public function generateLocalChecksum($webExId = null, $userDataArray = null) {

		if (!is_null($userDataArray)) {
			$sourceData = $userDataArray;
		} else if (!is_null($webExId)) {
			$userListModel = new Wst_Model_Dbtable_Userlist();
			$userInfo = $userListModel->getUserByWebExId($webExId);

			if (!$userInfo) {
				return false;
			}

			$sourceData = $userInfo;
		} else {
			return false;
		}

		$localConfig = Zend_Registry::get('localConfig');
		$localChecksumFields = explode(",", $localConfig["localChecksumFields"]);

		$checksumString = "";
		foreach ($localChecksumFields as $f) {
			if (!isset($sourceData[$f])) {
				return false;
			}
			$checksumString .= $sourceData[$f];
		}

		//dump($checksumString, true);
		$checksum = sha1($checksumString);
		return $checksum;
	}

	public function getTelephonyTypeMapping($abbrevs) {
		$helper = new Wst_Model_Helpers_General();
		$webexTelephonyKeys = $helper->telephonyKeys;

		if (!is_array($abbrevs)) {
			$abbrevs = array($abbrevs);
		}

		$mapped = array();
		foreach ($abbrevs as $abbrev) {
			$fullname = $webexTelephonyKeys[$abbrev]["wstDisplayName"];

			$mapped[$abbrev] = $fullname;
		}

		return $mapped;
	}

	// use override below if you want to make the user's telephony info match exclusively the items passed in. by default, it will change only what is passed in.
	public function getWebexTelephonyTypeXml($abbrevs, $override = false) {

		$helper = new Wst_Model_Helpers_General();
		$webexTelephonyKeys = $helper->telephonyKeys;

		if (!is_array($abbrevs)) {
			$abbrevs = array($abbrevs);
		}

		$xml = "";

		if ($override) {
			// figure out which features are enabled on the site
			$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
			$siteTelephonyResult = $webexApiHelper->getSiteSupportedTelephonyOptions();
			$siteTelephony = $siteTelephonyResult["siteTelephony"];

			foreach ($siteTelephony as $priv => $enabledOnSite) {
				if ($enabledOnSite) {
					$enableForUser = (in_array($priv, $abbrevs)) ? "TRUE" : "FALSE";
					$key = $webexTelephonyKeys[$priv]["webexUserXmlKey"];
					$xml .= "<$key>$enableForUser</$key>";
				}
			}

		} else {
			foreach ($abbrevs as $abbrev) {
				$key = $webexTelephonyKeys[$abbrev]["webexUserXmlKey"];
				if ($key != "ERROR") {
					$xml .= "<$key>TRUE</$key>";
				}
			}
		}

		return $xml;
	}

	public function isJSON($string) {
		return is_string($string) && is_array(json_decode($string, true)) ? true : false;
	}

	public static function parseCamelCaseToHumanReadable($string) {
		return preg_replace('/(?!^)[A-Z]{2,}(?=[A-Z][a-z])|[A-Z][a-z]|[0-9]{1,}/', ' $0', $string);
	}

	public function obfuscate($string, $endDigitsToShow, $character = '*') {
		if (is_string($string) && !is_null($string)) {
			$length = strlen($string);
			$shown = substr($string, (-1 * $endDigitsToShow), $endDigitsToShow);

			$obfuscated = str_repeat($character, $length - $endDigitsToShow) . $shown;

			return $obfuscated;
		}

		return $string;
	}
}