<?php

/**
 * Description of Box API Helper
 *
 * @author n_young_at_uncg_dot_edu
 */
class Wst_Model_Helpers_BoxApiHelper {

	public function getBoxTokenFromCode($code) {


		// endpoint sample:
		/*
		curl https://app.box.com/api/oauth2/token \
		-d 'grant_type=authorization_code&code={your_code}&client_id={your_client_id}&client_secret={your_client_secret}' \
		-X POST
		 * *
		 */

		$uri = 'https://app.box.com/api/oauth2/token';

		$proxyCreds = Zend_Registry::get('proxy');

		$config = array();

		if(!empty($proxyCreds['url']) && !empty($proxyCreds['port'])) {

			// re-set with proxy info
			$config = array(
				'adapter'    => 'Zend_Http_Client_Adapter_Proxy',
				'proxy_host' => $proxyCreds['url'],
				'proxy_port' => $proxyCreds['port'],
			);

		}

		$config['timeout'] = 30;

		$httpClient = new Zend_Http_Client($uri, $config);

		$boxCreds = Zend_Registry::get('boxApi');

		$parametersArray = array(
			'grant_type'    => 'authorization_code',
			'code'          => $code,
			'client_id'     => $boxCreds['clientId'],
			'client_secret' => $boxCreds['clientSecret']
		);

		$httpClient->setParameterPost($parametersArray);

		$result = $httpClient->request(Zend_Http_Client::POST);

		/*
		echo '<pre>';
		print_r($result);
		print_r($result->getBody());
		die('blah');
		 *
		 */

		// The expected result is 200: Created
		if ($result->getStatus() !== 200) {
			$returnArray['status'] = false;
			$returnArray['notes'] = 'Error making request. ' . $result->getStatus() . ': ' . $result->getMessage();
		}

		$body = json_decode($result->getBody());

		return $body;

	}

	public function refreshToken($refreshToken = false) {

		$logModel = new Wst_Model_Dbtable_Log();
		$configModel = new Wst_Model_Dbtable_Config();

		// endpoint sample:
		/*
		curl https://app.box.com/api/oauth2/token \ -d 'grant_type=refresh_token&refresh_token={valid refresh token}&client_id={your_client_id}&client_secret={your_client_secret}' \
		 *  -X POST
		 * *
		 */

		$logModel->addLogMessage('Function Processing', "Refreshing Box API token.", 1, 'INFO', 'function', __CLASS__);

		if(!$refreshToken) {
			$refreshToken = $configModel->getVar('refresh_token');
		}


		$uri = 'https://app.box.com/api/oauth2/token';

		$proxyCreds = Zend_Registry::get('proxy');

		if(!empty($proxyCreds['url']) && !empty($proxyCreds['port'])) {

			$config = array(
				'adapter'    => 'Zend_Http_Client_Adapter_Proxy',
				'proxy_host' => $proxyCreds['url'],
				'proxy_port' => $proxyCreds['port'],
			);

			$httpClient = new Zend_Http_Client($uri, $config);

		} else {

			$httpClient = new Zend_Http_Client($uri);

		}

		$boxCreds = Zend_Registry::get('boxApi');

		$parametersArray = array(
			'grant_type'    => 'refresh_token',
			'refresh_token' => $refreshToken,
			'client_id'     => $boxCreds['clientId'],
			'client_secret' => $boxCreds['clientSecret']
		);

		$httpClient->setParameterPost($parametersArray);

		$result = $httpClient->request(Zend_Http_Client::POST);

		/*
		echo '<pre>';
		print_r($result);
		print_r($result->getBody());
		die('blah');
		 *
		 */

		// The expected result is 200: Created
		if ($result->getStatus() !== 200) {
			$returnArray['status'] = false;
			$returnArray['notes'] = 'Error making request. ' . $result->getStatus() . ': ' . $result->getMessage();
		}

		$body = json_decode($result->getBody());

		// update records in DB
		$configModel->setVar("access_token", $body->access_token);
		$configModel->setVar("refresh_token", $body->refresh_token);

		$logModel->addLogMessage('Function Result', "Box API token refreshed succesfully.", 1, 'INFO', 'function', __CLASS__);

		return $body;

	}

	// CORE BOX API HELPER
	public function doBoxApiCall($endpoint, $method = "get", $fields = '', $fileToUpload = '') {
	//protected function doBoxApiCall($endpoint, $method = "get", $fields = '', $fileToUpload = '') {

		$logModel = new Wst_Model_Dbtable_Log();

		$returnArray = array();

		$boxCreds = Zend_Registry::get('boxApi');

		$params = array(); // empty by default.

		// take any existing url params and add them to the end
		if (strpos($endpoint, "?") !== false) {
			// there are params!
			$urlParams = explode("?", $endpoint);
			$endpoint = $urlParams[0];
			parse_str($urlParams[1], $rawParams);

			$params = array_merge($rawParams, $params);

		}

		// always add last; only to get requests
		if ($method == "get") {
			$limit = 1000;
			$params["limit"] = $limit;
		}

		if (empty($fileToUpload)) {
			$uri = $boxCreds["apiBaseUrl"] . $endpoint;
		} else {
			$uri = $boxCreds["apiBaseUploadUrl"] . $endpoint;
		}

		// add params
		if (!empty($params)) {
			$paramStrings = array();
			foreach ($params as $key => $value) {
				$paramStrings[] = $key . "=" . $value;
			}
			$uri .= "?" . implode("&amp;", $paramStrings);
		}

		// test uri
		//dump($uri, true);

		// do the API call
		try {
			$configModel = new Wst_Model_Dbtable_Config();
			$developerToken = $configModel->getVar('access_token');

			$proxyCreds = Zend_Registry::get('proxy');

			if(!empty($proxyCreds['url']) && !empty($proxyCreds['port'])) {

				$config = array(
					'adapter'    => 'Zend_Http_Client_Adapter_Proxy',
					'proxy_host' => $proxyCreds['url'],
					'proxy_port' => $proxyCreds['port'],
				);

				$httpClient = new Zend_Http_Client($uri, $config);

			} else {

				$httpClient = new Zend_Http_Client($uri);

			}

			$logModel->addLogMessage('Box API Call Processing', "Attempting Box API call - endpoint: $uri - method: " . strtoupper($method),  1, 'INFO', 'function', __CLASS__);

			if ($method == "get") {
				$httpClient->setMethod(Zend_Http_Client::GET);
			} else if ($method == "post") {
				$httpClient->setMethod(Zend_Http_Client::POST);
				if (!empty($fileToUpload)) {
					$httpClient->setParameterPost('attributes', json_encode($fields));
					$httpClient->setFileUpload($fileToUpload, 'file');
				} else {
					$httpClient->setRawData(json_encode($fields), 'application/json');
				}
			} else if ($method == "put") {
				$httpClient->setMethod(Zend_Http_Client::PUT);
				$httpClient->setRawData(json_encode($fields), 'application/json');
			}

			$httpClient->setHeaders(array(
				'Authorization: Bearer ' . $developerToken,
			));

			//dump($httpClient, true);

			$response = $httpClient->request(strtoupper($method));
			$lastResponse = $httpClient->getLastResponse();

			//dump($lastResponse, true);

			$body = json_decode($lastResponse->getBody());

			if (isset($body->errors)) {
				// TODO - figure out how to flesh this out a bit?
				throw new Exception("there were unexpected errors");
			}

			$returnArray = $body;

			//if (empty($returnArray)) {
			//
			//	// empty, so let's refresh the access token and see if that works
			//	$this->refreshToken();
			//	$this->doBoxApiCall($endpoint);
			//
			//}

			$logModel->addLogMessage('Box API Call Processing', "API call successful - response: " . $response->getStatus() . " " . $response->getMessage(), 1, 'INFO', 'function', __CLASS__);

			return $returnArray;

		} catch(Exception $e) {
			$logModel->addLogMessage('Box API Call Exception', "Exception caught: " . $e->getMessage(), 2, 'WARNING', 'function', __CLASS__);

			// log the essential info
			if ($method != "get") {
				$logModel->addLogMessage('Box API Call Exception', "Params: " . json_encode($fields), 1, 'INFO', 'function', __CLASS__);
			}

			$lastResultText = isset($lastResponse) ? "Raw Result: " . $lastResponse->getBody() : "No Raw Result data available";
			$logModel->addLogMessage('Box API Call Exception', "Raw Result: " . $lastResultText, 2, 'WARNING', 'function', __CLASS__);

			$result = isset($response) ? $response->getStatus() . " " . $response->getMessage() : 'no request data available';
			$logModel->addLogMessage('Box API Call Processing', "API call unsuccessful - code: $result", 1, 'INFO', 'function', __CLASS__);

			return $returnArray;
		}
	}




	// BOX API CALLS

	public function getAllUsers() {

		$this->refreshToken();

		$limit = 500;
		$offset = 0;
		$totalRetrieved = 0;

		$usersArray = array();

		do {

			$returnArray = $this->doBoxApiCall('/users?limit=' . $limit . '&offset=' . $offset);
			$usersArray = array_merge($returnArray, $returnArray->entries);

			$offset = $offset + $limit;
			$totalRetrieved = $totalRetrieved + count($returnArray->entries);

		} while ($totalRetrieved < $returnArray->total_count);

		return $usersArray;
	}

	public function getUser($boxUserIdNumber) {

		$this->refreshToken();

		return $this->doBoxApiCall('/users/' . $boxUserIdNumber);

	}

	public function getFolderContents($folderId = 0) {
		$this->refreshToken();
		return $this->doBoxApiCall('/folders/' . $folderId);
	}

	public function createFolderIfNotExists($parentId, $name) {
		$fetchContentsResult = $this->getFolderContents($parentId);
		$existingItems = $fetchContentsResult->item_collection->entries;

		$exists = false;
		foreach ($existingItems as $index => $item) {
			if ($item->type == "folder" && $item->name == $name) {
				$exists = true;
				$existingFolderId = $item->id;

				$result = array(
					"status" => "not-created",
					"message" => "folder '$name' already exists in parent folder (id $parentId)",
					"folderId" => $existingFolderId
				);
			}
		}

		if (!$exists) {
			// create it
			$postData = array(
				'name' => $name,
				'parent' => array(
					'id' => $parentId
				)
			);

			$createResult = $this->doBoxApiCall('/folders', 'post', $postData);
			if ($createResult->type != "error") {
				$folderId = $createResult->id;
				$result = array(
					"status" => "created",
					"message" => "folder '$name' (id $folderId) was created in parent folder (id $parentId)",
					"folderId" => $folderId
				);
			} else {
				$code = $createResult->status;
				$status = $createResult->code; // this is wonky, yes.

				$result = array(
					"status" => "failed",
					"message" => "API error: $code - $status",
					"folderId" => null
				);
			}


		}

		return $result;
	}

	public function createOrGetUploadDestination($username, $year) {
		$logModel = new Wst_Model_Dbtable_Log();

		$this->refreshToken();

		// get name
		$boxUploadsSettings = Zend_Registry::get("boxUploads");
		$baseFolderName = $boxUploadsSettings["recordingUploadBaseFolderName"];

		// responses array
		$responses = array();

		// 1. BASE FOLDER
		$baseFolderResult = $this->createFolderIfNotExists(0, $baseFolderName);
		$baseFolderId = $baseFolderResult["folderId"];
		$responses["base"] = $baseFolderResult;
		if (is_null($baseFolderId)) {
			return $responses;
		}

		$logModel->addLogMessage('Function Processing', "Created or Got base folder.", 1, 'INFO', 'function', __CLASS__);

		// 2. YEAR FOLDER
		$yearFolderResult = $this->createFolderIfNotExists($baseFolderId, $year);
		$yearFolderId = $yearFolderResult["folderId"];
		$responses["year"] = $yearFolderResult;
		if (is_null($yearFolderId)) {
			return $responses;
		}

		$logModel->addLogMessage('Function Processing', "Created or Got year folder.", 1, 'INFO', 'function', __CLASS__);

		// 3. USER FOLDER
		// set user folder based on app config
		$configModel = new Wst_Model_Dbtable_Config();
		$folderNameFormat = $configModel->getVar('recordingUploadFolderName');
		if ($folderNameFormat) {
			$varsToReplace = array(
				"username" => $username,
				"year"     => $year
			);

			foreach ($varsToReplace as $key => $value) {
				$folderNameFormat = str_replace("[[$key]]", $value, $folderNameFormat);
			}
			$folderName = $folderNameFormat;
		} else {
			// defaults to username if not set in app
			$folderName = $username;
		}

		$userFolderResult = $this->createFolderIfNotExists($yearFolderId, $folderName);
		$userFolderId = $userFolderResult["folderId"];
		$responses["user"] = $userFolderResult;
		if (is_null($userFolderId)) {
			return $responses;
		}

		$logModel->addLogMessage('Function Processing', "Created or Got user folder.", 1, 'INFO', 'function', __CLASS__);

		// return the responses so that we know what happened.
		return $responses;

	}

	public function uploadFileToBox($destinationFolder, $filePath, $newFilename) {
		/*
		 * Supported File Names:
		 *
		 * Box only supports file names of 255 characters or less. Names that will not be supported are those that contain non-printable ascii, / or \, names with trailing spaces, and the special names “.” and “..”.
		 *
		 * */

		//dump(func_get_args(), true);

		$this->refreshToken();

		$newFilename = trim(str_replace(array("/", "\\"), "-", $newFilename));
		$newFilename = substr($newFilename, 0, 250); // buffer just in case

		//$filePath = APPLICATION_PATH . '/../arf-downloads/154253847.arf'; // override for testing
		//$destinationFolder = "1132284687412312312313"; // also override for testing

		$postData = array(
			'name' => $newFilename . ".arf",
			'parent' => array(
				'id' => $destinationFolder
			)
		);

		$uploadResult = $this->doBoxApiCall('/files/content', 'post', $postData, $filePath);

		return $uploadResult;
	}

	public function createShareLinkForBoxFile($fileId) {

		$this->refreshToken();

		//$fileId = "95045533341"; // testing

		$postFields = array(
			"shared_link" => array(
				"access" => "company",
				"unshared_at" => null
			)
		);

		$result = $this->doBoxApiCall("/files/" . $fileId, 'put', $postFields);

		// URL will be at $result->shared_link->download_url

		return $result;



	}

	public function getFolderCollaborations($folderId) {
		$this->refreshToken();
		$result = $this->doBoxApiCall("/folders/$folderId/collaborations");

		return $result;
	}

	public function addFolderCollaboration($folderId, $username) {
		$localConfig = Zend_Registry::get('localConfig');
		$emailSuffix = $localConfig["emailSuffix"];

		$emailAddress = $username . $emailSuffix;

		$postData = array(
			'item' => array(
				'id' => $folderId,
				'type' => 'folder'
			),
			'accessible_by' => array(
				'login' => $emailAddress,
				'type' => 'user'
			),
			'role' => 'viewer'
		);

		$result = $this->doBoxApiCall('/collaborations?notify=false', 'post', $postData);

		return $result;
	}

	public function removeFolderCollaboration($collaborationId) {
		// TODO - not sure if we want to loop this in when we remove users.
	}


	public function getErrorMessagesFromResponse($response) {
		$errors = array();
		if ($response->type != "error") {
			return $errors;
		}

		$raw_errors = $response->context_info->errors;

		foreach($raw_errors as $index => $error) {
			$errors[] = $error->message;
		}

		return $errors;
	}

}