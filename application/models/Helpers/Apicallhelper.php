<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 1/22/16
 * Time: 3:33 PM
 */

// TODO - try-catch.

abstract class Wst_Model_Helpers_Apicallhelper {
	// ------ PARAMS -------

	// general configuration
	protected $method;
	protected $acceptedMethods = array("get", "post", "put", "delete");

	// URL configuration
	protected $baseUrl;
	protected $endpoint;
	protected $finalUrl;
	protected $params = array(); // if handled with inheriting class
	protected $additionalHeaders = array();

	// info to send - either post/get/put fields, or xml
	protected $fields;
	protected $encodedFields = null; // if sending json
	protected $xml = null; // if sending xml

	protected $dataToSend; // will be the same as one of the above. might need to refactor to adjust this

	// credentials - can be written to with the authenticate() method
	protected $authInfo = array();

	// curl
	protected $ch; // handle
	protected $curlOpts = array(
		// more added once type of request is specified.
		CURLOPT_HEADER => true,
		CURLOPT_RETURNTRANSFER => true
	);

	protected $response = array(
		"rawResponse" => "",
		"headers" => "",
		"body" => "",
		"code" => "",
		"errors" => null // needed?
	);
	protected $errors = array(
		"rawErrors" => array(),
		"rawErrorNumbers" => array()
	);

	// ------ METHODS ------
	public function __construct($endpoint = "", $method = "post", $host = null) {
		// set endpoint if good
		$this->setEndpoint($endpoint);
		// set method if good
		$method = strtolower($method);
		if (in_array($method, $this->acceptedMethods)) {
			$this->method = $method;
		} else {
			throw new Exception("Error: invalid method provided to " . __CLASS__ . "::__construct");
		}

		// authenticate to named host if provided
		if (isset($host)) {
			$this->authenticate($host);
		} else {
			// set basic authentication if "default" is still there
			if (array_key_exists("default", Config::$hosts)) {
				$this->authenticate("default");
			}
			// otherwise leave it to the user to call authenticate()
		}
	}

	public function overrideBaseUrl($baseUrl) {
		$this->baseUrl = $baseUrl;
	}

	public function setEndpoint($e) {
		//if ($e != "") {
		//	$this->endpoint = $e;
		//} else {
		//	throw new Exception("Error: invalid or empty endpoint value provided to API::setEndpoint");
		//}
		$this->endpoint = $e; // allows for blank
	}

	public function setMethod($method) {
		$method = strtolower($method);
		if (in_array($method, $this->acceptedMethods)) {
			$this->method = $method;
		} else {
			throw new Exception("Error: invalid method provided to " . __CLASS__ . "::setMethod");
		}
	}

	// show debug messages
	protected function showDebugMessage($msg) {
		// DISABLING FOR ZEND / OT

		//if (Config::debugModeOn()) {
		//	echo "$msg<br />";
		//}
	}

	protected function getNamedHostInfo($hostName) {
		// CHANGING THIS FOR ZEND / OT FRAMEWORK
		$registryVars = Zend_Registry::get($hostName);
		return $registryVars;

		//if (array_key_exists($hostName, Config::$hosts)) {
		//	return Config::$hosts[$hostName];
		//} else {
		//	throw new Exception("Error: " . __CLASS__ . "::getNamedHostInfo was unable to find a pre-named host named $hostName in the config file.");
		//}
	}

	// authenticates to named host (must be overridden in child classes)
	public function authenticate($hostName) {
		// child class will be able to handle this however they want to.
		throw new Exception("Error: " . __CLASS__ . "::authenticate() not overridden in child class!");
	}

	// sets parameters
	public function setParams($params) {
		$this->params = $params;
		$this->showDebugMessage("params set");
	}

	public function setFields($fields) {
		$this->fields = $fields;
		$this->showDebugMessage("fields set");
	}

	public function setXml($xml) {
		$this->xml = $xml;
		$this->showDebugMessage("xml set");
	}

	public function execute($dataFormat = "") {
		// what format is the source data? assuming normal unless we specify that we're sending JSON or XML. add headers and encode fields appropriately.
		$dataFormat = strtolower($dataFormat);

		if ($dataFormat == "json") {
			if (count($this->fields) > 0) {
				$this->encodedFields = json_encode($this->fields);
			}

			$this->additionalHeaders[] = "Content-Type: application/json";
			$this->additionalHeaders[] = "Content-length: " . strlen($this->encodedFields);

			$this->dataToSend = $this->encodedFields;

		} else if ($dataFormat == "xml") {
			// TODO - any additional structuring / validation needed for XML format?
			$this->additionalHeaders[] = "Content-Type: text/xml";
			$this->dataToSend= $this->xml;

		} else {
			//$this->additionalHeaders[] = "Content-Type: application/x-www-form-urlencoded"; // TODO: needed?
			$this->dataToSend = $this->fields;
		}

		// bind params (IF user has defined the function)
		if (count($this->params) > 0) {
			$this->bindParams();
			$this->showDebugMessage("params bound");
		}  else {
			$this->showDebugMessage("no params to bind");
		}

		// get the final URL put together
		$this->finalUrl = $this->baseUrl . $this->endpoint;
		$this->showDebugMessage("target url: $this->finalUrl");

		// start the curl request
		$this->ch = curl_init();

		//if ($this->debugMode) {
		//	$this->verbose = fopen('php://temp', 'w+');
		//	curl_setopt_array($this->ch, array(
		//		// debug
		//		CURLOPT_VERBOSE => true,
		//		CURLOPT_STDERR => $this->verbose
		//	));
		//}

		// Proxy
		$proxy = Zend_Registry::get('proxy');
		if (!empty($proxy['url'])) {
			$proxyWithPort = $proxy['url'];
			if (!empty($proxy['port'])) {
				$proxyWithPort .= ":" . $proxy['port'];
			}

			//die($proxyWithPort);

			curl_setopt($this->ch, CURLOPT_PROXY, $proxyWithPort);
		}

		// TODO: things to define in other methods:
		// CURLOPT_URL, CURLOPT_HTTPHEADER, CURLOPT_POST, CURLOPT_POSTFIELDS

		// the rest is handled in other functions depending on what needs to be done.
		$methodFunction = $this->method . "Request";
		//dump($this, true, "Ready to call method $methodFunction");
		$this->$methodFunction();

		curl_close($this->ch);

		//dump($this, true);

		if ($this->response["body"] !== false && $this->response["body"] !== null) {
			return $this->response["body"];
		} else {
			return false;
		}

	}

	protected function bindParams() {
		// user defined function to bind parameters to endpoint
	}

	// get headers into an array for us, and get the response out. the calling code will deal with the response (e.g. if it's json, xml, whatever).
	protected function parseResponse() {
		// grab headers
		$header_size = curl_getinfo($this->ch, CURLINFO_HEADER_SIZE);
		$header = substr($this->response["rawResponse"], 0, $header_size);
		$body = substr($this->response["rawResponse"], $header_size);

		// parse headers
		$headers = array();
		$header_text = substr($header, 0, strpos($header, "\r\n\r\n"));
		foreach (explode("\r\n", $header_text) as $i => $line) {
			if ($i === 0) {
				$headers['http_code'] = $line;
			}
			else {
				list ($key, $value) = explode(': ', $line);
				$headers[$key] = $value;
			}
		}

		$this->response["headers"] = $headers;
		$this->response["body"] = $body;

		// response code?
		$this->response["code"] = $this->response["headers"]["http_code"];
	}

	public function getResponseCode() {
		return $this->response["code"];
	}

	public function getAuthInfo() {
		return $this->authInfo;
	}

	protected function setCurlOptions() {
		// TODO: other validation?
		curl_setopt_array($this->ch, $this->curlOpts);
	}

	// ------ API TRANSACTION TYPE METHODS -----
	protected function postRequest() {
		// 1. we should already know what to send - $this->dataToSend
		//dump($this->dataToSend, true);

		// 2. set curl options
		$this->curlOpts += array(
			CURLOPT_URL => $this->finalUrl,
			CURLOPT_HTTPHEADER => $this->additionalHeaders,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $this->dataToSend
		);

		// 3. execute and get result into class vars
		$this->setCurlOptions();
		$this->response["rawResponse"] = curl_exec($this->ch);
		$this->parseResponse();

		//dump($this, true);
	}

	protected function getRequest() {
		// 1. get / assign getFields (make URL-safe)
		$urlParams = count($this->params) > 0 ? "?" . http_build_query($this->params) : "";
		$destination = $this->finalUrl . $urlParams;

		// 2. set additional curl options
		$this->curlOpts += array(
			CURLOPT_URL => $destination,
			CURLOPT_HTTPHEADER => $this->additionalHeaders
		);

		//dump($this->curlOpts, true);

		//dump($this, true);

		// 3. execute and get result into class vars
		$this->setCurlOptions();
		$this->response["rawResponse"] = curl_exec($this->ch);
		$this->parseResponse();

		//dump($this, true);

		// TODO: need to handle iteration / pagination
	}

	protected function putRequest() {
		// 1. we should already know what to send - $this->dataToSend
		//dump($this->dataToSend, true);

		// 2. set curl options
		$this->curlOpts += array(
			CURLOPT_URL => $this->finalUrl,
			CURLOPT_HTTPHEADER => $this->additionalHeaders,
			CURLOPT_CUSTOMREQUEST => "PUT",
			CURLOPT_POSTFIELDS => $this->dataToSend
		);

		// 3. execute and get result into class vars
		$this->setCurlOptions();
		$this->response["rawResponse"] = curl_exec($this->ch);
		$this->parseResponse();

		//dump($this, true);
	}

	protected function deleteRequest() {
		// 1. set curl options
		$this->curlOpts += array(
			CURLOPT_URL => $this->finalUrl,
			CURLOPT_HTTPHEADER => $this->additionalHeaders,
			CURLOPT_CUSTOMREQUEST => "DELETE",
		);

		// 2. execute and get result into class vars
		$this->setCurlOptions();
		$this->response["rawResponse"] = curl_exec($this->ch);
		$this->parseResponse();
	}
}

?>