<?php

class Wst_Model_Helpers_RecordingUploadHelper {

    public function uploadRecording($recordingId, $service) {

        $uploadFunction = 'uploadRecordingTo' . ucwords(strtolower($service));

        if (!method_exists($this, $uploadFunction)) {
            return array(
                "success"     => false,
                "message"     => "Service '$service' is not supported.",
                "fileId" => null
            );
        }

        $result = $this->$uploadFunction($recordingId);

        return $result;

    }



    public function uploadRecordingToBox($recordingId) {

        $boxApiHelper = new Wst_Model_Helpers_BoxApiHelper();
		$logModel = new Wst_Model_Dbtable_Log();

	    // set default return array.
	    $returnArray = array(
		    "success" => false,
		    "message" => null,
		    "fileId" => null,
		    "downloadLink" => null,
		    "parentFolderId" => null,
		    "collaborationResult" => null,
		    "collaborationId" => null
	    );

        // check to be sure file exists on server
        $filePath = APPLICATION_PATH . '/../arf-downloads/' . $recordingId . '.arf';

        if (!file_exists($filePath)) {
            $returnArray["message"] = "recording file $recordingId.arf not found on local server.";
	        return $returnArray;
        }

        // get file data from database (name, owner, timestamp)
        $recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive(); // TODO - better to do this, or to have it passed in?
        $recInfo = $recordingArchiveModel->getRecordingById($recordingId);

        if (is_null($recInfo)) {
	        $returnArray["message"] = "recording id $recordingId not found in local database.";
	        return $returnArray;
        }

        $ownerUsername = $recInfo["host_webex_id"];
        $actualName = $recInfo["name"];
        $createdTimestamp = $recInfo["create_time_utc"];

        $yearCreated = date("Y", $createdTimestamp);

        // create / get upload destination folder (get ID)
        $establishDestinationFolderResult = $boxApiHelper->createOrGetUploadDestination($ownerUsername, $yearCreated);

        if (!isset($establishDestinationFolderResult["year"]) || is_null($establishDestinationFolderResult["year"]["folderId"])) {
	        $returnArray["message"] = "could not create destination folder in Box.";
	        return $returnArray;
        }

        $destinationFolderId = $establishDestinationFolderResult["user"]["folderId"];
	    $logModel->addLogMessage('Function Processing', "Destination folder ID: " . $destinationFolderId, 1, 'INFO', 'function', __CLASS__);

        // upload file
        $uploadResult = $boxApiHelper->uploadFileToBox($destinationFolderId, $filePath, $actualName);

        $logInfo = (is_object($uploadResult) || is_array($uploadResult)) ? json_encode($uploadResult) : $uploadResult;
	    $logModel->addLogMessage('Function Raw Result', "Upload result raw info: " . $logInfo, 1, 'INFO', 'function', __CLASS__);

        if (isset($uploadResult->type) && $uploadResult->type == "error") {
            // get errors from Box
            $errors = $boxApiHelper->getErrorMessagesFromResponse($uploadResult);

	        $returnArray["message"] = "error uploading file to Box. API message(s) returned: " . implode("; ", $errors);
	        $returnArray["parentFolderId"] = $destinationFolderId;
	        return $returnArray;
        }

        if (!isset($uploadResult->entries[0]->id)) {
            $returnArray["message"] = "error uploading file to Box. No valid response object detected.";
            $returnArray["parentFolderId"] = $destinationFolderId;
            return $returnArray;
	    }

        $fileId = $uploadResult->entries[0]->id;
	    $logModel->addLogMessage('Function Processing', "Upload to Box successful, new File ID: $fileId", 1, 'INFO', 'function', __CLASS__);

        // assign share settings and get link

        $getShareLinkResult = $boxApiHelper->createShareLinkForBoxFile($fileId);

        if (isset($getShareLinkResult->type) && $getShareLinkResult->type == "error") {
            // get errors from Box
            $errors = $boxApiHelper->getErrorMessagesFromResponse($getShareLinkResult);

	        $returnArray["message"] = "error setting share link for file in Box. API message(s) returned: " . implode("; ", $errors);
	        $returnArray["fileId"] = $fileId;
	        $returnArray["parentFolderId"] = $destinationFolderId;
	        return $returnArray;
        }

        $downloadLink = $getShareLinkResult->shared_link->download_url;


	    // set collaboration with end-user - do this regardless of existing collaborations. if we already invited them, we'll get an error here, which is just fine

		$addCollaborationResult = $boxApiHelper->addFolderCollaboration($destinationFolderId, $ownerUsername);

	    $collaborationResult = null;
	    $collaborationId = null;
	    if (isset($addCollaborationResult->type) && $addCollaborationResult->type == "error") {
		    if ($addCollaborationResult->code != "user_already_collaborator") {
			    // there was an actual error.
			    $errors = $boxApiHelper->getErrorMessagesFromResponse($getShareLinkResult);

			    $returnArray["message"] = "unexpected error adding user collaboration for folder in Box. API message(s) returned: " . implode("; ", $errors);
			    $returnArray["fileId"] = $fileId;
			    $returnArray["parentFolderId"] = $destinationFolderId;
			    return $returnArray;
		    } else {
			    // collaboration already exists, no harm no foul.
			    $collaborationResult = "already-exists";
		    }
	    } else {
		    // it's a new one.
		    $collaborationResult = "new";

		    // get the new collaboration id
		    $collaborationId = $addCollaborationResult->id;

		    // trigger to alert users for new collaborations.

		    // do we only do this for users who don't already have Box accounts?

		    if (!is_null($addCollaborationResult->accessible_by)) {

			    // look up user to get name
			    $userListModel = new Wst_Model_Dbtable_Userlist();
			    $user = $userListModel->getUserByWebExId($ownerUsername);
			    $fullName = $user["firstName"] . " " . $user["lastName"];

			    // get shared folder name from the API call results
			    $folderName = $addCollaborationResult->item->name;
				$folderUrl = "https://uncg.app.box.com/files/0/f/$destinationFolderId";

			    // app info
			    $localConfig = Zend_Registry::get('localConfig');
			    $appUrl = $localConfig["appUrl"];

			    $configModel = new Wst_Model_Dbtable_Config();
			    $myRecordingsLink = $configModel->getVar('myRecordingsLinkReplacement');
			    if (!$myRecordingsLink) {
				    $myRecordingsLink = "{$appUrl}userservices/my-recordings";
			    }

			    // HTML EMAIL

			    $emailHelper = new Wst_Model_Helpers_HtmlEmail();
			    $htmlEmailBody = $emailHelper->head("UNCG WebEx Recordings - New Cloud Storage Share available");

			    $htmlBody = <<<HTML
<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hello $fullName,</p>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Your UNCG WebEx recordings (ARF format) are now accessible via the Box @ UNCG online file storage service. Any recordings you make in WebEx will be automatically downloaded from WebEx and uploaded to this folder for your convenience.</p>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">You may access this folder by signing into Box at <a href="http://box.uncg.edu">http://box.uncg.edu</a>, using your UNCG credentials. Your shared folder is titled <i>$folderName</i>.</p>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">You may also access the folder directly at the following URL, if you already have a UNCG Box account: <a href="$folderUrl">$folderUrl</a></p>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">For a list of your current WebEx Recordings, including links to the files and all shared folders in Box, please click the button below.</a></p>

<table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;" width="100%">
  <tbody>
    <tr>
      <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;" valign="top">
        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto; margin: 0 auto;">
          <tbody>
            <tr>
              <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; background-color: #3498db; border-radius: 5px; text-align: center;" valign="top" bgcolor="#3498db" align="center"> <a href="$myRecordingsLink" target="_blank" style="display: inline-block; color: #ffffff; background-color: #003366; border: solid 1px #003366; border-radius: 5px; box-sizing: border-box; cursor: pointer; text-decoration: none; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-transform: capitalize; border-color: #003366;">Manage My UNCG WebEx Recordings</a> </td>
            </tr>
          </tbody>
        </table>
      </td>
    </tr>
  </tbody>
</table>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Please visit <a href="http://go.uncg.edu/webex:cloudshareemail">http://go.uncg.edu/webex</a>, or contact 6-TECH (256-8324), if you have additional questions about this information.</p>

<hr>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">As always, ITS reminds you to confirm the safety of all links in electronic communications by hovering over them with your cursor and confirming legitimacy of the destination.</p>

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Never click on unverified website links. Report suspicious messages and links to 6-TECH at (336) 256-TECH (8324) or 6-TECH@uncg.edu.</p> 

<p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">This email is an official communication from the University of North Carolina at Greensboro. You may verify official university emails by checking the Verified Campus Communications Repository. If you have questions about the VCCR, or the authenticity of an email message you have received, please contact the sender of the message or search the UNCG website for "VCCR".</p>

HTML;

			    $htmlEmailBody .= $emailHelper->body("UNCG WebEx Recordings - New Cloud Storage Share available", $htmlBody);



			    // HTML version
			    $etHtml = new Ot_Trigger_Dispatcher();
			    $etHtml->setVariables(array(
				    'username'      => $ownerUsername,
				    'htmlEmailBody' => $htmlEmailBody
			    ));
			    $etHtml->dispatch('Wst_New_Cloud_Storage_Share_Html');


			    // NON-HTML EMAIL
			    //$et = new Ot_Trigger_Dispatcher();
			    //$et->setVariables(array(
				 //   'username' => $ownerUsername,
				 //   'fullName' => $fullName,
				 //   'serviceName' => "Box",
				 //   'serviceUrl' => "http://box.uncg.edu",
				 //   'folderUrl' => "https://uncg.app.box.com/files/0/f/$destinationFolderId",
				 //   'sharedFolderName' => $folderName
			    //));
			    //
			    //$et->dispatch('Wst_New_Cloud_Storage_Share');

			    // add all of this goodness to the Recording Folder Cloud Shares table.
			    $recordingCloudShareModel = new Wst_Model_Dbtable_RecordingFolderCloudShare();
			    $addShareResult = $recordingCloudShareModel->addShareForWebexId($ownerUsername, "box", $destinationFolderId, $collaborationId);
		    }


	    }

	    // all good - return the data set.
        $returnArray = array(
            "success" => true,
            "message" => "recording file $recordingId.arf successfully uploaded to Box.",
            "fileId" => $fileId,
            "downloadLink" => $downloadLink,
	        "parentFolderId" => $destinationFolderId,
            "collaborationResult" => $collaborationResult,
            "collaborationId" => $collaborationId
        );

	    return $returnArray;

    }

}