<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/23/16
 * Time: 10:42 AM
 */

class Wst_Model_Helpers_Webexapicallhelper extends Wst_Model_Helpers_Apicallhelper {
	public function authenticate($hostName) {
		$hostInfo = $this->getNamedHostInfo($hostName);

		$this->baseUrl = $hostInfo["xmlUrl"];
		$this->authInfo["username"] = $hostInfo["username"];
		$this->authInfo["password"] = $hostInfo["password"];
		$this->authInfo["partnerId"] = $hostInfo["partnerId"];
		$this->authInfo["siteId"] = $hostInfo["siteId"];
		$this->authInfo["siteName"] = $hostInfo["siteName"];

		$this->additionalHeaders[] = "POST /WBXService/XMLService HTTP/1.0";
		$this->additionalHeaders[] = "Host: {$hostInfo['siteName']}.webex.com";
		$this->additionalHeaders[] = "User-Agent: UNCG ITS App Admin script";

	}
}

?>