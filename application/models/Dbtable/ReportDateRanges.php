<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/19/16
 * Time: 1:05 PM
 */

class Wst_Model_Dbtable_ReportDateRanges extends Ot_Db_Table
{

	protected $_name = 'tbl_report_date_ranges';

	// helpers go here - specific operations for this table

	public function getRangesForUser($accountId) {
		$where = $this->getAdapter()->quoteInto("accountId = ?", $accountId);
		$ranges = $this->fetchAll($where)->toArray();

		return count($ranges) > 0 ? $ranges : false;
	}

	public function getRangesForLoggedInUser() {
		$identity = Zend_Auth::getInstance()->getIdentity();
		return $this->getRangesForUser($identity->accountId);
	}

	public function userOwnsRange($rangeId, $accountId = null) {

		if (is_null($accountId)) {
			$identity = Zend_Auth::getInstance()->getIdentity();
			$accountId = $identity->accountId;
		}

		$where = $this->getAdapter()->quoteInto("rangeId = ?", $rangeId);
		$where .= $this->getAdapter()->quoteInto(" AND accountId = ?", $accountId);

		$matchingRanges = $this->fetchAll($where)->toArray();

		return (count($matchingRanges) > 0);
	}
}