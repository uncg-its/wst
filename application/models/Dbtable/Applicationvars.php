<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/19/16
 * Time: 1:05 PM
 */

class Wst_Model_Dbtable_Applicationvars extends Ot_Db_Table
{

	protected $_name = 'tbl_application_vars';

	// helpers go here - specific operations for this table

	public function setVar($var_key, $var_value) {

		$data = array(
			"var_key" => $var_key,
			"var_value" => $var_value
		);

		// does key already exist?
		$queryResult = $this->getVar($var_key);

		if ($queryResult !== false) {
			$where = $this->getAdapter()->quoteInto("var_key = ?", $var_key);
			$this->update($data, $where);
		} else {
			$this->insert($data);
		}
	}

	public function getVar($key) {

		$where = $this->getAdapter()->quoteInto('var_key = ?', $key);
		$result = $this->fetchAll($where)->toArray();

		if (count($result) == 0) {
			return false;
		}

		return $result[0]["var_value"];
	}
}