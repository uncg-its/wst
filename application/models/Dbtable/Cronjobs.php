<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/24/16
 * Time: 4:49 PM
 */
class Wst_Model_Dbtable_Cronjobs extends Ot_Db_Table
{

	protected $_name = 'tbl_ot_cron_status';
	protected $_primary = 'jobKey';

	protected function changeCronjobStatus($id, $status) {
		$where = $this->getAdapter()->quoteInto("jobKey = ?", $id);
		return $this->update(array("status" => $status), $where);
	}

	protected function changeAllCronjobStatuses($status) {
		$where = $this->getAdapter()->quoteInto("jobKey IS NOT NULL", "");
		return $this->update(array("status" => $status), $where);
	}

	public function disableCronjob($id) {
		return $this->changeCronjobStatus($id, "disabled");
	}

	public function enableCronjob($id) {
		return $this->changeCronjobStatus($id, "enabled");
	}

	public function disableAllCronjobs() {
		return $this->changeAllCronjobStatuses("disabled");
	}

	public function enableAllCronjobs() {
		return $this->changeAllCronjobStatuses("enabled");
	}
	
	public function getCronjobsWithStatus($status) {
		$where = $this->getAdapter()->quoteInto("status = ?", $status);
		return $this->fetchAll($where)->toArray();
	}

	public function getCoreCronjobsWithStatus($status) {
		$cronjobsResult = $this->getCronjobsWithStatus($status);

		// get core cron jobs
		$localConfig = Zend_Registry::get('localConfig');
		$coreCronJobs = array_map("trim", explode(",", $localConfig["coreCronJobs"]));

		$jobsWithStatus = array();
		foreach($cronjobsResult as $c) {
			$jobsWithStatus[] = $c["jobKey"];
		}

		$jobsToIgnore = array_diff($jobsWithStatus, $coreCronJobs);

		return array_diff($jobsWithStatus, $jobsToIgnore);
	}

	public function getLastCronjobRuntime($jobKey) {
		$where = $this->getAdapter()->quoteInto("jobKey = ?", $jobKey);
		$result = $this->fetchAll($where)->toArray();

		if (count($result) > 0) {
			return $result[0]["lastRunDt"];
		}

		return false;
	}

}