<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/19/16
 * Time: 1:05 PM
 */

class Wst_Model_Dbtable_Sessionarchive extends Ot_Db_Table
{

	protected $_name = 'tbl_session_archive';
	protected $_primary = 'conf_id';

	// helpers go here - specific operations for this table
	public function getArchivesMap($where = false) {
		// returns an array with all confID values
		$select = $this->getAdapter()->select();
		$select->from($this->info('name'), array('conf_id', 'center'));

		if ($where) {
			$select->where($where);
		}

		$result = $select->query()->fetchAll();

		$sessionArchiveMap = array();
		foreach($result as $k => $v) {
			$confId = $v['conf_id'];
			$sessionArchiveMap[$confId] = $v;
		}

		return $sessionArchiveMap;
	}

	public function insertArchive($insertData) {
		// TODO - validate?
		try {
			$result = $this->insert($insertData);
			return $result;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function getSessionsWithUnparsedAttendees() {

		// returns array with all confID values of sessions that need attendee data pulled
		// TODO - reconsider how this is done?? can we do it without having to do one API call for each session?

		$where = $this->getAdapter()->quoteInto("parsed_attendees = ?", "n");
		return $this->getArchivesMap($where);

	}

	public function getSessionsWithUnparsedRecordings() {

		// returns array with all confID values of sessions that need recording data pulled
		// TODO - reconsider how this is done?? can we do it without having to do one API call for each session?

		$where = $this->getAdapter()->quoteInto("parsed_recordings= ?", "n");
		return $this->getArchivesMap($where);

	}

	public function setSessionAsParsed($confId) {
		$where = $this->getAdapter()->quoteInto("conf_id = ?", $confId);

		$updateData = array(
			"parsed_attendees" => "y"
		);

		return $this->update($updateData, $where);
	}

	public function getSessionsList($centersArray = array()) {
		if (count($centersArray) == 0) {
			return array(
				"result" => "failure",
				"data" => null,
				"message" => "error: must provide at least once center abbreviation to getSessionsList method."
			);
		}

		$centerCrosswalk = array(
			"mc" => "Meeting Center",
			"tc" => "Training Center",
			"ec" => "Event Center",
			"sc" => "Support Center"
		);

		$crosswalkKeys = array_keys($centerCrosswalk);
		sort($crosswalkKeys);
		sort($centersArray);

		$select = $this->getAdapter()->select();
		$select->from($this->info('name'));

		if ($crosswalkKeys != $centersArray) {
			$wherePieces = array();

			// build where statement ourselves, since input is controlled.
			foreach ($centersArray as $c) {
				if (isset($centerCrosswalk[$c])) {
					$wherePieces[] = "center = '{$centerCrosswalk[$c]}'";
				}
			}

			$whereString = implode(" OR ", $wherePieces);
			$select->where($whereString);
		}

		$select->order('start_time_utc DESC');

		return $select->query()->fetchAll();
		//return $this->fetchAll($whereString)->toArray();

	}

	public function getSessionByConfId($confId) {
		$result = $this->find($confId);

		if ($result) {
			return $result->toArray();
		}

		return $result;
	}


	public function fillInTrainingCenterNames() {
		// function we'll use only as needed as a backfill option.
		$where = "host_name IS NULL";
		$noNameSessions = $this->fetchAll($where)->toArray();

		if (count($noNameSessions) > 0) {

			$userListModel = new Wst_Model_Dbtable_Userlist();

			foreach($noNameSessions as $key => $info) {
				$usernameToLookup = $info["host_username"];
				$userDetails = $userListModel->getUserByWebExId($usernameToLookup);

				if ($userDetails)  {

					$updateData = array(
						"host_name" => $userDetails["firstName"] . " " . $userDetails["lastName"]
					);

					$where = $this->getAdapter()->quoteInto("conf_id = ?", $info["conf_id"]);

					$result = $this->update($updateData, $where);
				}
			}
		}
		
		return null;
	}

	public function getAllArchives() {
		return $this->getSessionsList(array("mc","tc","sc","ec"));
	}

	public function getArchivesBetweenTimestamps($start, $end) {
		$select = $this->getAdapter()->select();
		$select->from($this->info('name'));

		$where = $this->getAdapter()->quoteInto("start_time_utc BETWEEN ? ", $start);
		$where .= $this->getAdapter()->quoteInto("AND ?", $end);

		$select->where($where);

		return $select->query()->fetchAll();
	}

	public function getTelephonyUsageBetweenTimestamps($start, $end) {
		$select = $this->getAdapter()->select();
		$select->from($this->info('name'));

		$where = $this->getAdapter()->quoteInto("(start_time_utc BETWEEN ? ", $start);
		$where .= $this->getAdapter()->quoteInto("AND ?) ", $end);
		$where .= "AND (call_in_participants > 0 OR call_out_participants > 0)";

		$select->where($where);

		return $select->query()->fetchAll();
	}
}