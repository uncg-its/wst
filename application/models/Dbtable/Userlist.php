<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/22/16
 * Time: 1:15 PM
 */

class Wst_Model_Dbtable_Userlist extends Ot_Db_Table {

	protected $_name = 'tbl_user_list';
	protected $_primary = 'euid';

	// helpers go here - specific operations for this table
	public function getUserByEuid($euid) {
		$result = $this->find($euid); // can do this because euid is PK

		if ($result) {
			return $result->toArray();
		}

		return $result;

	}


	public function getUserEuidMap() {
		$result = $this->fetchAll()->toArray();

		if (count($result) > 0) {
			$returnArray = array();
			foreach($result as $r) {
				$returnArray[$r["euid"]] = $r;
			}
			return $returnArray;
		}

		return false;
	}


	public function getUserWebexIdMap() {
		$result = $this->fetchAll()->toArray();

		if (count($result) > 0) {
			$returnArray = array();
			foreach($result as $r) {
				$returnArray[$r["webExId"]] = $r;
			}
			return $returnArray;
		}

		return false;
	}

	public function getUserByWebExId($webExId) {
		$where = $this->getAdapter()->quoteInto("webExId = ?", $webExId);
		$result = $this->fetchAll($where)->toArray();

		if (count($result) > 0) {
			return $result[0];
		}

		return false;
	}

	public function addNewUserToUserList($euid, $webExId, $firstName, $lastName, $email, $deptNumber, $orgShortCode, $userId, $source = "feed") {
		//$checksum = generateChecksum($webExId, $firstName, $lastName, $email);

		$generalHelper = new Wst_Model_Helpers_General();
		$checksumData = array(
			"webExId"        => $webExId,
			"firstName"      => $firstName,
			"lastName"       => $lastName,
			"email"          => $email,
			"dept_number"    => $deptNumber,
			"org_short_code" => $orgShortCode
		);
		$checksum = $generalHelper->generateLocalChecksum(null, $checksumData);

		$insertData = array(
			"euid"           => $euid,
			"webExId"        => $webExId,
			"firstName"      => $firstName,
			"lastName"       => $lastName,
			"email"          => $email,
			"webExUserId"    => $userId,
			"checksum"       => $checksum,
			"status"         => "active",
			"source"         => $source,    // 'feed' is default. 'manual' if added or deleted via admin panel.
			"dept_number"    => $deptNumber,
			"org_short_code" => $orgShortCode
		);

		$result = $this->insert($insertData);
		return array(
			"status"    => "success",
			"message"   => "user $webExId added successfully."
		);
	}

	public function editUserInUserList($euid, $updateData) {

		$oldUserData = $this->getUserByEuid($euid);

		if ($oldUserData && isset($oldUserData["euid"])) {

			if (count($updateData) > 0) {
				// new checksum
				$checksumData = array();
				$checksumFields = array("webExId", "firstName", "lastName", "email", "dept_number", "org_short_code");
				foreach ($checksumFields as $key) {
					if (array_key_exists($key, $updateData)) {
						$checksumData[$key] = $updateData[$key];
					} else {
						$checksumData[$key] = $oldUserData[$key];
					}
				}

				$generalHelper = new Wst_Model_Helpers_General();
				$checksumData = array(
					"webExId"        => $checksumData["webExId"],
					"firstName"      => $checksumData["firstName"],
					"lastName"       => $checksumData["lastName"],
					"email"          => $checksumData["email"],
					"dept_number"    => $checksumData["dept_number"],
					"org_short_code" => $checksumData["org_short_code"]
				);
				$checksum = $generalHelper->generateLocalChecksum(null, $checksumData);

				//$checksum = generateChecksum($checksumData["webExId"], $checksumData["firstName"], $checksumData["lastName"], $checksumData["email"]);

				$updateData["checksum"] = $checksum;

				// perform insert
				$where = $this->getAdapter()->quoteInto("euid = ?", $euid);
				$recordsEdited = $this->update($updateData, $where);

				if (is_numeric($recordsEdited)) {
					return array(
						"status"            => "success",
						"editedRecordId"    => $euid,
						"message"           => "record edited successfully"
					);
				} else {
					return array(
						"status"        => "failure",
						"message"       => "updating record failed"
					);
				}
			}

		} else {
			return array(
				"status"    => "failure",
				"message"   => "user with euid = $euid was not found; cannot edit"
			);
		}

	}

	public function inactivateUserInUserList($euid, $manual = false) {
		return $this->editUserInUserList($euid, array(
			"status" => "inactive",
			"source" => $manual ? "manual" : "feed" // allow us to control the enrollment manually if needed.
		));
	}

	public function reactivateUserInUserList($euid, $manual = false) {
		$source = $manual ? "manual" : "feed";

		$userInfo = array(
			"status" => "active",
			"source" => $source
		);

		return $this->editUserInUserList($euid, $userInfo);
	}

	public function revertUserToFeedSource($euid) {
		$updateData = array(
			"source" => "feed"
		);

		$where = $this->getAdapter()->quoteInto("euid = ?", $euid);

		$result = $this->update($updateData, $where);

		return $result;
	}

	public function getActiveUsersList() {
		$where = $this->getAdapter()->quoteInto("status = ?", "active");
		return $this->fetchAll($where);
	}

	public function getUsersWithActiveRecordings() {
		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();

		$allRecordings = $recordingArchiveModel->getActiveRecordingsByUser();

		$allUsers = $this->getActiveUsersList()->toArray();

		// reindex by username
		$usersByUsername = array();
		foreach($allUsers as $index => $user) {
			$usersByUsername[$user["webExId"]] = $user;
		}

		$usersWithActiveRecordings = array();

		foreach ($allRecordings as $user => $recording) {
			if (isset($usersByUsername[$user])) {
				$usersWithActiveRecordings[$user] = $usersByUsername[$user];
			}
		}

		//$select = $this->getAdapter()->select();
		//$select->from(array('a' => $recordingArchiveModel->info('name')));
		//$select->joinRight(array('b' => $this->info('name')), 'a.host_webex_id = b.webExId');
		//
		//$where = $this->getAdapter()->quoteInto("a.status = ?", 'active');
		//$select->where($where);
		//
		//$result = $select->query()->fetchAll();

		return $usersWithActiveRecordings;
	}

	public function getUsersWithRecordings() {
		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();

		$allRecordings = $recordingArchiveModel->getAllRecordingsByUser();

		$allUsers = $this->getActiveUsersList()->toArray();

		// reindex by username
		$usersByUsername = array();
		foreach($allUsers as $index => $user) {
			$usersByUsername[$user["webExId"]] = $user;
		}

		$usersWithRecordings = array();

		foreach ($allRecordings as $user => $recording) {
			if (isset($usersByUsername[$user])) {
				$usersWithRecordings[$user] = $usersByUsername[$user];
			}
		}

		//$select = $this->getAdapter()->select();
		//$select->from(array('a' => $recordingArchiveModel->info('name')));
		//$select->joinRight(array('b' => $this->info('name')), 'a.host_webex_id = b.webExId');
		//
		//$where = $this->getAdapter()->quoteInto("a.status = ?", 'active');
		//$select->where($where);
		//
		//$result = $select->query()->fetchAll();

		return $usersWithRecordings;
	}

	public function getUsersWithTelephonyPrivileges() {
		$where = $this->getAdapter()->quoteInto("telephony_privileges IS NOT NULL", "");
		return $this->fetchAll($where)->toArray();
	}

	public function updateTelephonyForUser($username, $privsArray) {
		$privsImploded = implode(",", $privsArray);
		$where = $this->getAdapter()->quoteInto("webExId = ?", $username);
		$updateData = array(
			"telephony_privileges" => $privsImploded
		);

		return $this->update($updateData, $where);
	}

	public function updateCustomTrackingCode($username, $code) {

		$where = $this->getAdapter()->quoteInto("webExId = ?", $username);

		$code = trim($code);
		$codeJson = (empty($code)) ? NULL : json_encode(array("3" => $code));

		$updateData = array(
			"custom_tracking" => $codeJson
		);

		return $this->update($updateData, $where);
	}

	public function getUsersByTrackingCode($codeId) {
		$trackingCodeModel = new Wst_Model_Dbtable_Trackingcodes();
		$codeInfo = $trackingCodeModel->getCodeInformationById($codeId);
		$field = $codeInfo["user_list_field"];
		$isCustomCode = false;
		if (is_null($field)) {
			$isCustomCode = true;
		}

		$users = $this->getActiveUsersList()->toArray();

		$usersByTrackingCode = array();
		foreach ($users as $index => $user) {
			if (!$isCustomCode) {
				if (!empty($user[$field])) {
					$usersByTrackingCode[$user[$field]][] = $user;
				}
			} else {
				$customCodes = json_decode($user["custom_tracking"], true);
				if (isset($customCodes[$codeId])) {
					$usersByTrackingCode[$customCodes[$codeId]][] = $user;
				}
			}
		}

		return $usersByTrackingCode;
	}

	public function getUsersWithTrackingCode($codeId, $value = "") {
		// hard coded for now?
		if ($codeId == 1) {
			$field = "dept_number";
		} else if ($codeId == 2) {
			$field = "org_short_code";
		} else if ($codeId == 3) {
			$field = "custom_tracking";
		} else {
			throw new Exception("Error: " . __CLASS__ . "::" . __METHOD__ . " requires a valid codeId");
		}


		// TODO - apparently i never finished this method.
	}

	public function getDistinctOrgShortCodes() {
		$excludedCodes = ["undefined"];

	    $select = $this->getAdapter()->select();
	    $select->from($this->info('name'), 'org_short_code');
	    $select->distinct();

	    $codes = $select->query()->fetchAll();

		if (count($codes) == 0) {
			return false;
		}

		// need to reindex, now they are in [0 => ['org_short_code' => 'VALUE']] format. yuck.
		$codesReindexed = array();
		foreach ($codes as $codeArray) {
			$codeValue = $codeArray['org_short_code'];
			if (!in_array($codeValue, $excludedCodes)) {
				$codesReindexed[] = $codeValue;
			}
		}

		// now, sort.
		sort($codesReindexed);

		return $codesReindexed;
	}

	// defunct, we shouldn't ever need this.

	public function deleteUserFromUserList($euid) {
		$oldUserQuery = $this->getUserByWebExId($euid);
		if (count($oldUserQuery) > 0) {
			$where = $this->getAdapter()->quoteInto("euid = ?", $euid);
			$deletedRecordId = $this->delete($where);

			if (is_numeric($deletedRecordId)) {
				return array(
					"status"            => "success",
					"deletedRecordId"    => $deletedRecordId,
					"message"           => "record deleted successfully"
				);
			} else {
				return array(
					"status"        => "failure",
					"message"       => "deleting record failed"
				);
			}
		} else {
			return array(
				"status"    => "failure",
				"message"   => "user with euid = $euid was not found; cannot delete"
			);
		}
	}
}