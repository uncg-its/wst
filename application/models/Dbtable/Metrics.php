<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/19/16
 * Time: 1:05 PM
 */

class Wst_Model_Dbtable_Metrics extends Ot_Db_Table
{

	protected $_name = 'tbl_metrics';
	protected $_primary = 'metric_id';

	public function insertMetrics($metricsArray) {
		return $this->insert($metricsArray);
	}

	public function getMetricsMap($where = "") {
		$select = $this->getAdapter()->select();
		$select->from($this->info('name'));

		if ($where != "") {
			$select->where($where);
		}

		$select->order("date DESC");

		$metrics = $select->query()->fetchAll();

		$metricsByTimestamp = array();
		foreach($metrics as $m) {
			$timestamp = strtotime($m["date"] . "00:00:00");
			$metricsByTimestamp[$timestamp] = $m;

			// parse JSON
			$rawData = json_decode($m["rawData"], true);

			foreach($rawData as $key => $value) {
				$metricsByTimestamp[$timestamp][$key] = $value;
			}
		}

		return $metricsByTimestamp;
	}

	public function getMetricsWithStatus($status) {
		$where = $this->getAdapter()->quoteInto("status = ?", $status);
		return $this->getMetricsMap($where);
	}

	public function updateDailyMetric($updateData, $id) {
		$where = $this->getAdapter()->quoteInto("metric_id = ?", $id);
		return $this->update($updateData, $where);
	}

	public function getMetricsForMonth($month) {
		$pattern = '/[0-9]{4}-[0-1]{1}[1-9]{1}/';

		$validFormat = preg_match($pattern, $month) == 1;

		if (!$validFormat) {
			// try a strtotime
			$month = date("Y-m", strtotime($month));

			// still bad?
			if (preg_match($pattern, $month) != 1) {
				return false;
			}
		}

		// if we got here we have a good month format.
		$where = $this->getAdapter()->quoteInto("date LIKE ?", $month . "%");

		return $this->getMetricsMap($where);
	}

	public function getMetricById($id) {
		return $this->find($id)->toArray();
	}
}