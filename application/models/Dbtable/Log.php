<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/24/16
 * Time: 4:49 PM
 */
class Wst_Model_Dbtable_Log extends Ot_Db_Table
{

	protected $_name = 'tbl_log';
	protected $_primary = 'logId';


	public function addLogMessage($subject, $message, $priority, $priorityName, $attributeName, $attributeId)
	{

		$dba = $this->getAdapter();

		$dba->beginTransaction();

		$accountId = '0';
		$username = 'CRON-AUTO';
		$loggedInUser = Zend_Auth::getInstance()->getIdentity();
		if(!empty($loggedInUser)) {
			$accountId = Zend_Auth::getInstance()->getIdentity()->accountId;
			$username = Zend_Auth::getInstance()->getIdentity()->username;
		}

		$data = array(
			'accountId'     => $accountId,
			'username'      => $username,
			'timestamp'     => time(),
			'subject'       => $subject,
			'message'       => $message,
			'priority'      => $priority,
			'priorityName'  => $priorityName,
			'attributeId'   => $attributeId,
			'attributeName' => $attributeName,
		);

		try {
			$logId = $this->insert($data, null);
		} catch (Exception $e) {
			$dba->rollback();
			throw $e;
		}

		$dba->commit();

		return $logId;

	}

	public function getAllLogEntries() {
		$select = $this->getAdapter()->select();
		$select->from($this->info('name'));
		$select->order("logId DESC");

		return $select->query()->fetchAll();
	}

	public function getLogsWithSubject($subject) {
		$where = $this->getAdapter()->quoteInto("subject = ?", $subject);
		return $this->fetchAll($where)->toArray();
	}

	public function getLogsWithAttributeId($attributeId) {
		$where = $this->getAdapter()->quoteInto("attributeid = ?", $attributeId);
		return $this->fetchAll($where)->toArray();
	}

}