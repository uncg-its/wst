<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/19/16
 * Time: 1:05 PM
 */

class Wst_Model_Dbtable_Userstaging extends Ot_Db_Table
{

	protected $_name = 'tbl_user_staging';
	protected $_primary = 'id';

	// helpers go here - specific operations for this table


	public function stageUser() {
		// TODO - necessary?
	}


	public function deleteStagingEntry($id) {
		$where = $this->getAdapter()->quoteInto("id = ?", $id);
		$id = $this->delete($where);
		return $id;
	}
}