<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/22/16
 * Time: 1:15 PM
 */

class Wst_Model_Dbtable_Apiqueue extends Ot_Db_Table {

	protected $_name = 'tbl_api_queue';
	protected $_primary = 'q_id';

	// helpers go here - specific operations for this table


	protected function addQueueEntry($action, $euid, $webExId, $userData = array(), $source, $shouldUpdateUserList) {

		// TODO: validation for action and userData?

		// minimum first
		$insertData = array(
			"euid"                 => $euid,
			"webExId"              => $webExId,
			"status"               => "pending",
			"action"               => $action,
			"shouldUpdateUserList" => $shouldUpdateUserList,
			"source"               => $source
		);

		foreach ($userData as $key => $val) {
			$insertData[$key] = $val;
		}

		$lastInsertedRecordId = $this->insert($insertData);

		return array(
			"status"                    => "success",
			"lastQueuedId"              => $lastInsertedRecordId,
			"message"                   => "Successfully queued API call - type: $action; webExId: $webExId"
		);

	}

	public function queueAddAction($euid, $webExId, $userData = array(), $source, $shouldUpdateUserList = "y") {
		return $this->addQueueEntry("add", $euid, $webExId, $userData, $source, $shouldUpdateUserList);
	}
	public function queueEditAction($euid, $webExId, $userData = array(), $source, $shouldUpdateUserList = "y") {
		return $this->addQueueEntry("edit", $euid, $webExId, $userData, $source, $shouldUpdateUserList);
	}
	public function queueDeleteAction($euid, $webExId, $source, $shouldUpdateUserList = "y") {
		return $this->addQueueEntry("delete", $euid, $webExId, array(), $source, $shouldUpdateUserList);
	}

	public function getCallsWithStatus($status = array(), $action = "") {
		if (!is_array($status)) {
			$status = array($status);
		}

		$select = $this->getAdapter()->select();
		$select->from($this->info("name"));

		$wherePieces = array();

		foreach($status as $s) {
			$wherePieces[] = $this->getAdapter()->quoteInto("status = ?", $s);
		}

		$whereString = implode(" OR ", $wherePieces);

		if ($action != "") {
			$whereString .= $this->getAdapter()->quoteInto(" AND action = ?", $action);
		}

		//dump($whereString);

		$select->where($whereString);

		$result = $select->query()->fetchAll();

		if (count($result) > 0) {

			//$reindexed = array();
			//
			//foreach($result as $k => $v) {
			//	$reindexed[$v["euid"]] = $v;
			//}
			//
			//return $reindexed;

			return $result;

		}

		return false;
	}

	public function setCallStatus($id, $status) {
		$where = $this->getAdapter()->quoteInto("q_id = ?", $id);

		$updateData = array(
			"status" => $status
		);

		return $this->update($updateData, $where);
	}

    public function setCallAttemptNumber($id, $attemptNumber) {
        $where = $this->getAdapter()->quoteInto("q_id = ?", $id);

        $updateData = array(
            "attempt" => $attemptNumber
        );

        return $this->update($updateData, $where);
    }

	public function getCall($id) {
		return $this->find($id)->toArray();
	}
}