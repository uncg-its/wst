<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/19/16
 * Time: 1:05 PM
 */

class Wst_Model_Dbtable_Recordingarchive extends Ot_Db_Table
{

	protected $_name = 'tbl_recording_archive';
	protected $_primary = 'recording_id';

	public function getRecordingArchiveMap($where = false) {
		// returns an array indexed by recordingId
		$select = $this->getAdapter()->select();
		$select->from($this->info('name'));

		if ($where) {
			$select->where($where);
		}

		$result = $select->query()->fetchAll();

		$recordingArchiveMap = array();
		foreach($result as $k => $v) {
			$recId = $v['recording_id'];
			$recordingArchiveMap[$recId] = $v;
		}

		return $recordingArchiveMap;
	}

	public function insertRecording($insertData) {
		return $this->insert($insertData);
	}

	public function getRecordingArchives($where = "") {

		$select = $this->getAdapter()->select();
		$select->from($this->info('name'));
		$select->order("recording_id DESC");

		if ($where != "") {
			$select->where($where);
		}

		$result = $select->query()->fetchAll();

		return $result;
	}

	public function getRecordingArchivesThatCountAgainstQuota() {
		$where = $this->getAdapter()->quoteInto("status = ?", "active");
		$where .= $this->getAdapter()->quoteInto(" OR status = ?", "in-trash");

		return $this->getRecordingArchives($where);
	}

	public function getActiveRecordingArchives() {
		$where = $this->getAdapter()->quoteInto("status = ?", "active");
		return $this->getRecordingArchives($where);
	}

	public function getTrashedRecordingArchives() {
		$where = $this->getAdapter()->quoteInto("status = ?", "in-trash");
		return $this->getRecordingArchives($where);
	}

	public function getActiveRecordingsForWebExId($webExId) {
		$where = $this->getAdapter()->quoteInto("host_webex_id = ?", $webExId);
		$where .= $this->getAdapter()->quoteInto(" AND status = ?", "active");

		return $this->getRecordingArchives($where);
	}

	public function getAllRecordingsForWebExId($webExId) {
		$where = $this->getAdapter()->quoteInto("host_webex_id = ?", $webExId);

		return $this->getRecordingArchives($where);
	}

	public function getActiveRecordingsForConfId($confId) {
		$where = $this->getAdapter()->quoteInto("conf_id = ?", $confId);
		$where .= $this->getAdapter()->quoteInto(" AND status = ?", "active");

		return $this->getRecordingArchives($where);
	}

	public function getRecordingsBetweenTimestamps($start, $end) {
		$where = $this->getAdapter()->quoteInto("create_time_utc BETWEEN ? ", $start);
		$where .= $this->getAdapter()->quoteInto("AND ?", $end);

		return $this->getRecordingArchives($where);
	}

	public function getRecordedSessions($where = "") {
		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();

		$select = $this->getAdapter()->select();
		$select->from(array("a" => $this->info('name')));
		$select->joinLeft(array("b" => $sessionArchiveModel->info('name')), 'a.conf_id = b.conf_id');

		if ($where != "") {
			$select->where($where);
		}

		return $select->query()->fetchAll();

	}

	public function getRecordedSessionsBetweenTimestamps($start, $end) {
		$where = $this->getAdapter()->quoteInto("b.start_time_utc BETWEEN ? ", $start);
		$where .= $this->getAdapter()->quoteInto("AND ?", $end);

		return $this->getRecordedSessions($where);
	}

	public function getRecordingById($id) {

		$result = $this->find($id);

		if ($result) {
			return $result->toArray();
		}

		return $result;
	}

	public function updateRecording($updateData, $id) {
		$where = $this->getAdapter()->quoteInto("recording_id = ?", $id);
		return $this->update($updateData, $where);
	}

	public function changeRecordingStatus($id, $status) {
		$updateData = array("status" => $status);
		return $this->updateRecording($updateData, $id);
	}

	public function getCurrentRecordingStorageUsage() {
		$select = $this->getAdapter()->select();
		$select->from($this->info("name"), "SUM(size_mb) as recording_usage");
		$where = $this->getAdapter()->quoteInto("status = ?", "active");
		$where .= $this->getAdapter()->quoteInto(" OR status = ?", "in-trash");
		$select->where($where);

		return $select->query()->fetchAll();

	}

	public function trashRecording($id) {
		// status change + timestamp
		$trashTimestamp = time();
		$updateData = array(
			"status" => "in-trash",
			"trashed_timestamp" => $trashTimestamp
		);

		$where = $this->getAdapter()->quoteInto("recording_id = ?", $id);

		return $this->update($updateData, $where);
	}

	public function markRecordingAsDeleted($id, $status = "deleted-by-user") {
		// status change + timestamp
		$deletedTimestamp = time();
		$updateData = array(
			"status" => $status,
			"deleted_timestamp" => $deletedTimestamp
		);

		$where = $this->getAdapter()->quoteInto("recording_id = ?", $id);

		return $this->update($updateData, $where);
	}

	public function getRecordingsToBeLifeCycled() {

		$configModel = new Wst_Model_Dbtable_Config();
		$lifeCycleDays = $configModel->getVar('recordingDeletionWindow');

		$deleteBeforeTimestamp = strtotime("-$lifeCycleDays days");

		$where = $this->getAdapter()->quoteInto("create_time_utc < ?", $deleteBeforeTimestamp);
		$where .= $this->getAdapter()->quoteInto(" AND status = ?", "active");

		$recordingsThatShouldBeLifeCycled = $this->fetchAll($where)->toArray();

		return $recordingsThatShouldBeLifeCycled;
	}

	public function markExpiredRecordingsForDownload() {
		$configModel = new Wst_Model_Dbtable_Config();
		$lifeCycleDays = $configModel->getVar('recordingDeletionWindow');

		$deleteBeforeTimestamp = strtotime("-$lifeCycleDays days");

		$where = $this->getAdapter()->quoteInto("create_time_utc < ?", $deleteBeforeTimestamp);
		$where .= $this->getAdapter()->quoteInto(" AND status = ?", "active");
		$where .= $this->getAdapter()->quoteInto(" AND download_status = ?", "not-downloaded");

		$updatedRecordings = $this->update(array("download_status" => "pending"), $where);
		return $updatedRecordings;
	}

	public function getRecordingsToDownload() {
		$where = $this->getAdapter()->quoteInto("download_status = ?", "pending");

		return $this->fetchAll($where)->toArray();
	}

	public function getNextRecordingToDownload($status = "pending") {
		$select = $this->getAdapter()->select();
		$select->from($this->info('name'));

		$where = $this->getAdapter()->quoteInto("download_status = ?", $status);
		$select->where($where);

		$select->order("create_time_utc ASC");

		$select->limit(1);

		$result = $select->query()->fetchAll();

		return $result;
	}

	public function getNextRecordingToUpload($status = "pending") {
		$select = $this->getAdapter()->select();
		$select->from($this->info('name'));

		$where = $this->getAdapter()->quoteInto("upload_status = ?", $status);
		$select->where($where);

		$select->order("create_time_utc ASC");

		$select->limit(1);

		$result = $select->query()->fetchAll();

		//dump($result, true);

		return $result;
	}

	public function changeDownloadStatus($id, $status) {
		$where = $this->getAdapter()->quoteInto("recording_id = ?", $id);
		$updateData = array(
			"download_status" => $status
		);

		return $this->update($updateData, $where);
	}

	public function changeUploadStatus($id, $status) {
		$where = $this->getAdapter()->quoteInto("recording_id = ?", $id);
		$updateData = array(
			"upload_status" => $status
		);

		return $this->update($updateData, $where);
	}

	//public function setFailures($id, $failures) {
	//	if (!is_numeric($failures)) {
	//		throw new Exception("Error: number of failures must be numeric. Value provided: $failures");
	//	} else if ($failures < 0) {
	//		throw new Exception("Error: number of failures must be positive. Value provided: $failures");
	//	}
	//
	//	$where = $this->getAdapter()->quoteInto("recording_id = ?", $id);
	//	$updateData = array(
	//		"download_failures" => $failures
	//	);
	//
	//	return $this->update($updateData, $where);
	//
	//}

	public function getDownloadedRecordingsToBeDeletedFromWebex() {
		$where = $this->getAdapter()->quoteInto("status = ?", "active");
		$where .= $this->getAdapter()->quoteInto(" AND download_status = ?", "downloaded");

		return $this->fetchAll($where)->toArray();
	}

    public function setDownloadedFileSize($recId, $sizeInMegabytes) {
        $where = $this->getAdapter()->quoteInto("recording_id = ?", $recId);
        $updateData = array(
            "downloaded_file_size_mb" => $sizeInMegabytes
        );

        return $this->update($updateData, $where);
    }

    public function setCloudStorageInfo($recId, $cloudStorageInfo) {
        $where = $this->getAdapter()->quoteInto("recording_id = ?", $recId);
        $updateData = array(
            "cloud_storage_info" => $cloudStorageInfo
        );

        return $this->update($updateData, $where);
    }

    public function getCloudStorageInfo($recId) {
	    $where = $this->getAdapter()->quoteInto("recording_id = ?", $recId);
	    $recInfo = $this->fetchAll($where)->toArray();

	    if (count($recInfo) > 0) {
	    	return json_decode($recInfo[0]["cloud_storage_info"], true);
	    }

	    return false;
    }

    public function getActiveRecordingsByUser() {
	    $activeRecordings = $this->getActiveRecordingArchives();
	    $recordingsByUser = array();

	    foreach ($activeRecordings as $index => $recInfo) {
		    $recordingsByUser[$recInfo["host_webex_id"]][] = $recInfo;
	    }

	    return $recordingsByUser;
    }
    public function getAllRecordingsByUser() {
	    $recordings = $this->getRecordingArchives();
	    $recordingsByUser = array();

	    foreach ($recordings as $index => $recInfo) {
		    $recordingsByUser[$recInfo["host_webex_id"]][] = $recInfo;
	    }

	    return $recordingsByUser;
    }

	public function getRecordingsWithTransferStatus($status) {
		$where = $this->getAdapter()->quoteInto("upload_status = ?", $status);
		$where .= $this->getAdapter()->quoteInto(" OR download_status = ?", $status);
		$recs = $this->fetchAll($where)->toArray();

		if (count($recs) > 0) {
			return $recs;
		}

		return false;
	}

    public function getRecordingsWithFailedTransfers() {
	    $configModel = new Wst_Model_Dbtable_Config();
	    $maxDeleteFailures = $configModel->getVar('arfDeleteRetries');
		if (!$maxDeleteFailures) {
			throw new Ot_Exception("Error: app variable 'arfDeleteRetries' not set.");
		}

		// can't really do anything about the delete failures...so...ignore.
	    //$where = "upload_status = 'failed' OR download_status = 'failed' OR delete_failures > $maxDeleteFailures";
	    return $this->getRecordingsWithTransferStatus("failed");
    }

    public function getRecordingsWithIgnoredTransfers() {
	    return $this->getRecordingsWithTransferStatus("ignored");
    }
}