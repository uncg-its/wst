<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/19/16
 * Time: 1:05 PM
 */

class Wst_Model_Dbtable_ReportGroups extends Ot_Db_Table
{

	protected $_name = 'tbl_report_groups';

	// helpers go here - specific operations for this table

	public function getReportGroupsForUser($accountId) {
		$where = $this->getAdapter()->quoteInto("accountId = ?", $accountId);
		$groups = $this->fetchAll($where)->toArray();

		return count($groups) > 0 ? $groups : false;
	}

	public function getReportGroupsForLoggedInUser() {
		$identity = Zend_Auth::getInstance()->getIdentity();
		return $this->getReportGroupsForUser($identity->accountId);
	}

	public function userOwnsReportGroup($reportGroupId, $accountId = null) {

		if (is_null($accountId)) {
			$identity = Zend_Auth::getInstance()->getIdentity();
			$accountId = $identity->accountId;
		}

		$where = $this->getAdapter()->quoteInto("reportGroupId = ?", $reportGroupId);
		$where .= $this->getAdapter()->quoteInto(" AND accountId = ?", $accountId);

		$matchingGroups = $this->fetchAll($where)->toArray();

		return (count($matchingGroups) > 0);
	}
}