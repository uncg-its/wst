<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/22/16
 * Time: 1:15 PM
 */

class Wst_Model_Dbtable_Useroverrides extends Ot_Db_Table {

	protected $_name = 'tbl_user_overrides';
	protected $_primary = 'o_id';

	// helpers go here - specific operations for this table

	public function getOverride($o_id) {
		$result = $this->find($o_id); // can do this because o_id is PK

		if ($result) {
			return $result->toArray();
		}

		return $result;
	}

	public function cancelOverride($id) {
		// get override data
		$override = $this->getOverride($id);

		if ($override) {
			// cancel the override
			$updateData = array(
				"cancelled_timestamp" => time()
			);

			$where = $this->getAdapter()->quoteInto("o_id = ?", $id);
			$updateResult = $this->update($updateData, $where);

			// now revert the user
			$userListModel = new Wst_Model_Dbtable_Userlist();
			$revertResult = $userListModel->revertUserToFeedSource($override["euid"]);

			return array(
				"updateResult" => $updateResult,
				"revertResult" => $revertResult
			);
		}

		return false;
	}

	public function getAllOverrides($paginator = false) {
		$helper = new Wst_Model_Helpers_General();

		$result = $this->fetchJoinedData($paginator);

		//if (!$paginator) {
		//	return $helper->reindexArray($result, "euid");
		//}

		return $result;
	}

	public function getInEffectOverrides($paginator = false) {
		$helper = new Wst_Model_Helpers_General();

		$currentTime = time();

		$where = $this->getAdapter()->quoteInto("start_timestamp < ?", $currentTime);
		$where .= $this->getAdapter()->quoteInto(" AND end_timestamp > ?", $currentTime);
		$where .= " AND cancelled_timestamp IS NULL";

		$result = $this->fetchJoinedData($paginator, $where);

		//if (!$paginator) {
		//	return $helper->reindexArray($result, "euid");
		//}

		return $result;
	}

	public function getInEffectOverridesForEuid($euid) {
		$helper = new Wst_Model_Helpers_General();

		$currentTime = time();

		$where = $this->getAdapter()->quoteInto("start_timestamp < ?", $currentTime);
		$where .= $this->getAdapter()->quoteInto(" AND end_timestamp > ?", $currentTime);
		$where .= " AND cancelled_timestamp IS NULL";
		$where .= $this->getAdapter()->quoteInto(" AND euid = ?", $euid);

		$result = $this->fetchAll($where);

		if ($result) {
			return $result->toArray();
		}

		return false;
	}

	public function getAllOverridesForEuid($euid) {
		$where = $this->getAdapter()->quoteInto("euid = ?", $euid);

		$result = $this->fetchAll($where);

		if ($result) {
			return $result->toArray();
		}

		return false;
	}

	public function getExpiringOverrides() {
		$currentTime = time();

		$userListModel = new Wst_Model_Dbtable_Userlist();

		$select = $this->getAdapter()->select();
		$select->from(array("a" => $this->info('name')));
		$select->joinLeft(array("b" => $userListModel->info('name')), 'a.euid = b.euid');


		$where = $this->getAdapter()->quoteInto("end_timestamp < ?", $currentTime);
		$where .= " AND cancelled_timestamp IS NULL";
		$where .= " AND user_reverted_timestamp IS NULL";
		$select->where($where);

		$result = $select->query()->fetchAll();

		if ($result) {
			//return $result->toArray();
			return $result;
		}
		
		return false;
	}

	// SCRAP THIS FOR NOW
	//public function getPendingOverrides() {
	//	$where = $this->getAdapter()->quoteInto("status = ?", "pending");
	//	$result = $this->fetchAll($where);
	//
	//	if ($result) {
	//		return $result->toArray();
	//	}
	//
	//	return false;
	//}

	public function addOverride($euid, $webExId, $reason, $contactEmail, $startTimestamp, $endTimestamp, $userStatus) {
		$insertData = array(
			"euid"            => $euid,
			"reason"          => $reason,
			"contact_email"   => $contactEmail,
			"start_timestamp" => $startTimestamp,
			"end_timestamp"   => $endTimestamp,
			"user_status"     => $userStatus
		);

		$result = $this->insert($insertData);

		// notification
		if (is_null($webExId)) {
			// find it from list.
			$userListModel = new Wst_Model_Dbtable_Userlist();
			$user = $userListModel->getUserByEuid($euid);

			if ($user) {
				$webExId = $user["webExId"];
			} else {
				// we could not find an existing user! what do we do?
			}
		}

		$htmlEmailHelper = new Wst_Model_Helpers_HtmlEmail();

		$htmlEmailBody = $htmlEmailHelper->head("UNCG WebEx - User Override for $webExId");

		$htmlBody = $htmlEmailHelper->overrideEmail($webExId, $userStatus, date("m/d/Y h:i:s a", $startTimestamp), date("m/d/Y h:i:s a", $endTimestamp));

		$htmlEmailBody .= $htmlEmailHelper->body("UNCG WebEx - User Override for $webExId", $htmlBody);

		// trigger email
		$et = new Ot_Trigger_Dispatcher();
		$et->setVariables(array(
			'emailAddress'  => $contactEmail,
			'username'      => $webExId,
			'htmlEmailBody' => $htmlEmailBody
		));
		$et->dispatch('Wst_Override_Expiration_Warning_Html');



		return array(
			"status"    => "success",
			"message"   => "override for user with euid=$euid added successfully"
		);
	}

	public function changeOverrideStatus($o_id, $status) {
		$allowedStatuses = array(
			"pending",
			"in-effect",
			"expired",
			"cancelled"
		);

		if (in_array($status, $allowedStatuses)) {
			$updateResult = $this->editOverride($o_id, array("processed_status" => $status));
			return $updateResult;
		}

		return array(
			"status" => "failure",
			"message" => "status '$status' is not an allowed value for the processed_status field."
		);
	}

	public function editOverride($o_id, $updateData) {
		if (count($updateData) > 0) {
			$existingRecord = $this->getOverride($o_id);
			if (!$existingRecord) {
				return array(
					"status"  => "failure",
					"message" => "record with o_id = $o_id not found in overrides table"
				);
			}

			$where = $this->getAdapter()->quoteInto("o_id = ?", $o_id);
			$recordsEdited = $this->update($updateData, $where);

			if (is_numeric($recordsEdited)) {
				return array(
					"status"         => "success",
					"editedRecordId" => $o_id,
					"message"        => "record edited successfully"
				);
			} else {
				return array(
					"status"  => "failure",
					"message" => "updating record failed"
				);
			}
		} else {
			return array(
				"status"    => "failure",
				"message"   => "update data not provided"
			);
		}
	}

	protected function fetchJoinedData($paginator = false, $where = "") {
		$userListModel = new Wst_Model_Dbtable_Userlist();

		$select = $this->getAdapter()->select();
		$select->from(array('a' => $this->info('name')));
		$select->join(array('b' => $userListModel->info('name')), 'a.euid = b.euid');
		//$select->order('a.euid ')

		if (!empty($where)) {
			$select->where($where);
		}

		if ($paginator) {
			$adapter = new Zend_Paginator_Adapter_DbSelect($select);
			$paginatorObject = new Zend_Paginator($adapter);

			return $paginatorObject;
		}

		return $select->query()->fetchAll();
	}

	public function cancelExistingUserOverrides($euid) {
		$time = time();

		$updateData = array(
			"cancelled_timestamp" => $time
		);

		$where = $this->getAdapter()->quoteInto("euid = ?", $euid);
		$where .= " AND cancelled_timestamp IS NULL";
		$where .= " AND user_reverted_timestamp IS NULL";

		$result = $this->update($updateData, $where);

		return $result;
	}

	public function setRevertedTimestampForOverride($id, $time = null) {

		if (is_null($time)) {
			$time = time();
		}

		$updateData = array(
			"user_reverted_timestamp" => $time
		);

		$where = $this->getAdapter()->quoteInto("o_id = ?", $id);

		$result = $this->update($updateData, $where);

		return $result;
	}

	public function updateNotifications($id, $newNotification) {
		$existing = $this->getOverride($id);

		$notifications = is_null($existing["notifications"]) ? array() : explode(",", $existing["notifications"]);
		$notifications[] = $newNotification;

		$where = $this->getAdapter()->quoteInto("o_id = ?", $id);
		$updateData = array(
			"notifications" => implode(",", $notifications)
		);

		return $this->update($updateData, $where);
	}
}