<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/19/16
 * Time: 1:05 PM
 */

class Wst_Model_Dbtable_Trackingcodes extends Ot_Db_Table
{

	protected $_name = 'tbl_tracking_codes';

	// helpers go here - specific operations for this table

	public function getTrackingCodesById() {
		$codes = $this->fetchAll()->toArray();

		$codesById = array();
		foreach ($codes as $index => $code) {
			$codesById[$code["tracking_code_id"]] = $code;
		}

		return $codesById;
	}

	public function getTrackingCodesByUserListField() {
		$where = "user_list_field IS NOT NULL";

		$codes = $this->fetchAll($where)->toArray();

		$codesById = array();
		foreach ($codes as $index => $code) {
			if ($code["user_list_field"])
			$codesById[$code["user_list_field"]] = $code;
		}

		return $codesById;
	}

	public function getCodeInformationById($id) {
		return $this->find($id)->toArray();
	}
}