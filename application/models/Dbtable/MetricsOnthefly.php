<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/19/16
 * Time: 1:05 PM
 */

class Wst_Model_Dbtable_MetricsOnthefly extends Ot_Db_Table
{

	protected $_name = 'tbl_metrics_fly';
	protected $_primary = 'metric_id';

	public function clearTable() {
		$logModel = new Wst_Model_Dbtable_Log();

		// clear table metadata
		$this->getDefaultMetadataCache()->clean();

		// which cols to get rid of?
		$protectedCols = array("metric_id", "date");
		$cols = $this->info(Zend_Db_Table_Abstract::COLS);
		$logModel->addLogMessage('Function Processing', "Columns found: " . implode(",", $cols), 1, 'INFO', 'helper', __CLASS__);

		$toDelete = array_diff($cols, $protectedCols);

		$logModel->addLogMessage('Function Processing', "Columns to drop: " . implode(",", $toDelete), 1, 'INFO', 'helper', __CLASS__);

		foreach ($toDelete as $index => $col) {
			$query = "ALTER TABLE {$this->info('name')} DROP COLUMN $col";
			$result = $this->getAdapter()->query($query);
		}

		$truncateResult = $this->getAdapter()->query("TRUNCATE TABLE " . $this->info('name'));
		return;
	}


	/**
	 * @param $cols - array of column names to create. all INT datatype for this cause.
	 */
	public function addDbColumns($cols, $dataTypeOverrides = null) {
		$logModel = new Wst_Model_Dbtable_Log();

		if (!is_array($cols)) {
			$cols = array($cols);
		}

		foreach ($cols as $col) {
			$dataType = isset($dataTypeOverrides[$col]) ? $dataTypeOverrides[$col] : "INT(11) unsigned";

			$query = "ALTER TABLE {$this->info('name')} ADD COLUMN $col $dataType";
			$result = $this->getAdapter()->query($query);

			$logModel->addLogMessage('Function Processing', "Added column $col, type $dataType.", 1, 'INFO', 'helper', __CLASS__);
		}

		return;

	}

	public function populateDbColumns($metricsArray) {
		$rows = count($metricsArray);

		$successfulInserts = 0;
		foreach ($metricsArray as $metric) {
			$insertResult = $this->insert($metric);
			if ($insertResult > 0) {
				$successfulInserts++;
			}
		}

		$failedInserts = $rows - $successfulInserts;

		return array(
			"rows"              => $rows,
			"successfulInserts" => $successfulInserts,
			"failedInserts"     => $failedInserts
		);
	}



	public function insertMetrics($metricsArray) {
		return $this->insert($metricsArray);
	}

	public function getMetricsMap($where = "") {
		$select = $this->getAdapter()->select();
		$select->from($this->info('name'));

		if ($where != "") {
			$select->where($where);
		}

		$select->order("date DESC");

		$metrics = $select->query()->fetchAll();

		$metricsByTimestamp = array();
		foreach($metrics as $m) {
			$timestamp = strtotime($m["date"] . "00:00:00");
			$metricsByTimestamp[$timestamp] = $m;
		}

		return $metricsByTimestamp;
	}

	public function getMetricsWithStatus($status) {
		$where = $this->getAdapter()->quoteInto("status = ?", $status);
		return $this->getMetricsMap($where);
	}

	public function updateDailyMetric($updateData, $id) {
		$where = $this->getAdapter()->quoteInto("metric_id = ?", $id);
		return $this->update($updateData, $where);
	}

	public function getMetricsForMonth($month) {
		$pattern = '/[0-9]{4}-[0-1]{1}[1-9]{1}/';

		$validFormat = preg_match($pattern, $month) == 1;

		if (!$validFormat) {
			// try a strtotime
			$month = date("Y-m", strtotime($month));

			// still bad?
			if (preg_match($pattern, $month) != 1) {
				return false;
			}
		}

		// if we got here we have a good month format.
		$where = $this->getAdapter()->quoteInto("date LIKE ?", $month . "%");

		return $this->getMetricsMap($where);
	}

	public function getMetricById($id) {
		return $this->find($id)->toArray();
	}
}