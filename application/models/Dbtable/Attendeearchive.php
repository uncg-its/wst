<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/19/16
 * Time: 1:05 PM
 */

class Wst_Model_Dbtable_Attendeearchive extends Ot_Db_Table
{

	protected $_name = 'tbl_attendee_archive';
	protected $_primary = 'attendee_id';

	// helpers go here - specific operations for this table
	//public function getArchivesMap() {
	//	// returns an array with all confID values
	//	$select = $this->getAdapter()->select();
	//	$select->from($this->info('name'), 'conf_id');
	//
	//	$result = $select->query()->fetchAll();
	//
	//	$sessionArchiveMap = array();
	//	foreach($result as $k => $v) {
	//		$confId = $v['conf_id'];
	//		$sessionArchiveMap[$confId] = $v;
	//	}
	//
	//	return $sessionArchiveMap;
	//}

	public function insertArchive($insertData) {
		// TODO - validate?
		try {
			$result = $this->insert($insertData);
			return $result;
		} catch (Exception $e) {
			return $e->getMessage();
		}
	}

	public function getAttendeesForSessionByConfId($confId) {

		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();

		$select = $this->getAdapter()->select();
		$select->from(array('a' => $this->info('name')));
		$select->join(array('b' => $sessionArchiveModel->info('name')), 'a.conf_id = b.conf_id', array("session_name", "center"));

		$where = $this->getAdapter()->quoteInto("a.conf_id = ?", $confId);

		$select->where($where);

		return $select->query()->fetchAll();

	}

	public function getAllArchives() {
		return $this->fetchAll()->toArray();
	}

	public function getArchivesBetweenTimestamps($start, $end) {
		$select = $this->getAdapter()->select();
		$select->from($this->info('name'));

		$where = $this->getAdapter()->quoteInto("join_time_utc BETWEEN ? ", $start);
		$where .= $this->getAdapter()->quoteInto("AND ?", $end);

		$select->where($where);

		return $select->query()->fetchAll();
	}

	public function getUniqueAttendeesBetweenTimestamps($start, $end) {
		$select = $this->getAdapter()->select();
		$select->from($this->info('name'), 'email');

		$where = $this->getAdapter()->quoteInto("join_time_utc BETWEEN ? ", $start);
		$where .= $this->getAdapter()->quoteInto("AND ?", $end);

		$select->where($where);

		$select->distinct();

		return $select->query()->fetchAll();
	}
}