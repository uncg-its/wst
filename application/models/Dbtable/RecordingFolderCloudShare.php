<?php
/**
 * Created by PhpStorm.
 * User: mattlibera
 * Date: 2/19/16
 * Time: 1:05 PM
 */

class Wst_Model_Dbtable_RecordingFolderCloudShare extends Ot_Db_Table
{

	protected $_name = 'tbl_recording_folder_cloud_shares';

	// helpers go here - specific operations for this table

	public function addShareForEuid($euid, $serviceName, $folderId, $transactionId) {

		$insertData = array(
			"euid" => $euid,
			"transaction_id" => $transactionId,
			"folder_id" => $folderId,
			"service_name" => $serviceName
		);

		$insertResult = $this->insert($insertData);

		return $insertResult;
	}

	public function addShareForWebexId($webExId, $service, $folderId, $transactionId) {
		$userListModel = new Wst_Model_Dbtable_Userlist();
		$userInfo = $userListModel->getUserByWebExId($webExId);

		if (!$userInfo) {
			return false;
		}

		$euid = $userInfo["euid"];
		return $this->addShareForEuid($euid, $service, $folderId, $transactionId);
	}

	public function getSharesForWebexId($webExId) {
		$userListModel = new Wst_Model_Dbtable_Userlist();

		$select = $this->getAdapter()->select();
		$select->from(array('a' => $this->info('name')));
		$select->joinLeft(array('b' => $userListModel->info('name')), 'a.euid = b.euid');

		$where = $this->getAdapter()->quoteInto("b.webExId = ?", $webExId);
		$select->where($where);

		$result = $select->query()->fetchAll();

		return $result;
	}
}