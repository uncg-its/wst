<?php
class Wst_Form_Addusertolocal extends Twitter_Bootstrap_Form_Horizontal {
	public function init() {
		$this->setAttrib('id', 'addUserToLocal');

		$webExId = $this->createElement('text', 'webExId', array('label' => "WebEx ID (username)"));
		$webExId->setRequired(true);

		$this->addElements(array($webExId));

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Submit'));
		$submit->setAttrib('class', 'btn');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
		));

		$this->addElements(array($submit));
	}
}

?>

