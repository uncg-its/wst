<?php
class Wst_Form_Manuallydeactivateuser extends Twitter_Bootstrap_Form_Horizontal {
	public function init() {
		$this->setAttrib('id', 'manuallyDeactivateUser');

		$get = Zend_Registry::get('getFilter');

		$euid = $this->createElement('hidden', 'euid');
		$euid->setValue($get->euid);
		$euid->setRequired(true);

		// TODO - how to abstract this?
		$reason = $this->createElement('text', 'reason', array('label' => "Reason for Override"));
		$reason->setRequired(true);
		$reason->setDescription("Please provide a reason for adding this override.");

		$contactEmail = $this->createElement('text', 'contact_email', array('label' => "Contact e-mail address"));
		$contactEmail->setRequired(true);
		$contactEmail->setDescription("Please provide the primary contact person responsible for managing this manual action. This person will receive periodic notifications about the manual action.");
		$contactEmail->addValidator(new Zend_Validate_EmailAddress());

		//$startDate = $this->createElement('text', 'start_date', array('label' => "Start Date"));
		//$startDate->setDescription("Date the override should start (default is today)");
		//$startDate->addValidator(new Zend_Validate_Date(array('format' => 'MM/dd/yyyy')));

		$endDate = $this->createElement('text', 'end_date', array('label' => "End Date"));
		$endDate->setRequired(true);
		$endDate->setDescription("Date the override should end");
		$endDate->addValidator(new Zend_Validate_Date(array('format' => 'MM/dd/yyyy')));
		$endDate->addValidator(new Custom_Validate_ValidEndDate(true, array(
			//'start_date' => $startDate->getValue(),
			'start_date' => "",
			'end_date' => $endDate->getValue()
		)));

		// REFERENCE - datepicker
		//$dateInput = $this->createElement('text', 'date');
		//$dateInput->addValidator(new Zend_Validate_Date(array('format' => 'MM/dd/yyyy')));
		//$dateInput->setValue(Zend_Date::now()->toString('MM/dd/yyyy'));

		//$this->addElements(array($euid, $reason, $contactEmail, $startDate, $endDate));
		$this->addElements(array($euid, $reason, $contactEmail, $endDate));

		$this->addDisplayGroup(
		//array('reason','contact_email','start_date','end_date'),
			array('reason','contact_email','end_date'),
			'override-information',
			array(
				'disableLoadDefaultDecorators' => true,
				'decorators' => array('FormElements', 'Fieldset'),
				'legend' => "Override Information"
			)
		);

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Submit'));
		$submit->setAttrib('class', 'btn');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
		));

		$this->addElements(array($submit));
	}
}

?>

