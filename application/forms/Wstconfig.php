<?php

class Wst_Form_Wstconfig extends Twitter_Bootstrap_Form_Horizontal
{
	public function init()
	{
		$this->setAttrib('id', 'configForm');

		$this->setElementsBelongTo('bootstrap');

		$recordingDeletionWindow = $this->createElement('text', 'recordingDeletionWindow', array('label' => 'Recording Deletion Window (days)', 'class' => 'input-small'));
		$recordingDeletionWindow->setDescription('Number of days that recordings will persist on the WebEx server before being auto-deleted by WST');
		$recordingDeletionWindow->addValidator(new Zend_Validate_Int());

		$localRecordingCacheCleaningWindow = $this->createElement('text', 'localRecordingCacheCleaningWindow', array('label' => 'Local Cache Storage Window (days)', 'class' => 'input-small'));
		$localRecordingCacheCleaningWindow->setDescription('Number of days that downloaded recordings will remain locally on the WST server before being removed by WST (if successfully uploaded to cloud storage)');
		$localRecordingCacheCleaningWindow->addValidator(new Zend_Validate_Int());

		$arfSizeDifferenceThresholdMegabytes = $this->createElement('text', 'arfSizeDifferenceThresholdMegabytes', array('label' => "ARF File Size Difference Threshold (MB)", 'class' => 'input-small'));
		$arfSizeDifferenceThresholdMegabytes->setDescription("Allowable difference in size (megabytes) in an ARF file's reported size (from WebEx) and actual size (after downloading via API), checked when downloading an ARF file to WST. If exceeded, WST will trigger an email alert and suspend deletion of the original file via API.");
		$arfSizeDifferenceThresholdMegabytes->addValidator(new Zend_Validate_Float());

		$arfDownloadRetries = $this->createElement('text', 'arfDownloadRetries', array('label' => 'Times to retry ARF download', 'class' => 'input-small'));
		$arfDownloadRetries->setDescription("Number of times to retry ARF download if an error occurred. If exceeded then the system will ignore the recording and move on to the next one.");
		$arfDownloadRetries->addValidator(new Zend_Validate_Int());

		$arfUploadRetries = $this->createElement('text', 'arfUploadRetries', array('label' => 'Times to retry ARF upload', 'class' => 'input-small'));
		$arfUploadRetries->setDescription("Number of times to retry ARF upload if an error occurred. If exceeded then the system will ignore the recording and move on to the next one.");
		$arfUploadRetries->addValidator(new Zend_Validate_Int());

		$arfDeleteRetries = $this->createElement('text', 'arfDeleteRetries', array('label' => 'Times to retry ARF delete', 'class' => 'input-small'));
		$arfDeleteRetries->setDescription("Number of times to retry ARF deletion if an error occurred. If exceeded then the system will ignore the recording and move on to the next one.");
		$arfDeleteRetries->addValidator(new Zend_Validate_Int());

		$recordingUploadFolderName = $this->createElement('text', 'recordingUploadFolderName', array('label' => 'Recording Upload Folder Name', 'class' => 'input-xxlarge'));
		$recordingUploadFolderName->setDescription('Format for recording uploads folder name (cloud storage). Use [[username]] and [[year]] to insert those variables into the string');

		$downloadLinkReplacement = $this->createElement('text', 'downloadLinkReplacement', array('label' => 'Download Link Replacement URL', 'class' => 'input-xxlarge'));
		$downloadLinkReplacement->setDescription('Replaces the default download link (keeping the RCID parameter) with this link in all end-user communications');
		$downloadLinkReplacement->addValidator(new Custom_Validate_ValidUrl(null, true));

		$myRecordingsLinkReplacement = $this->createElement('text', 'myRecordingsLinkReplacement', array('label' => 'My Recordings Replacement URL', 'class' => 'input-xxlarge'));
		$myRecordingsLinkReplacement->setDescription('Replaces the default my-recordings link (e.g. if using a URL shortener)');
		$myRecordingsLinkReplacement->addValidator(new Custom_Validate_ValidUrl(null, true));

		$earliestDeleteTimestamp = $this->createElement('text', 'earliestDeleteTimestamp', array('label' => 'Earliest Delete Timestamp'));
		$earliestDeleteTimestamp->setDescription('If present, this value will override the expiration date that is sent to users for their ARF files. This is useful if giving users extra time until their recordings are deleted.');
		$earliestDeleteTimestamp->addValidator(new Zend_Validate_Int());

		$tollMinuteCost = $this->createElement('text', 'tollMinuteCost', array('label' => 'Cost per Toll Minute of telephony', 'class' => 'input-small', 'prepend' => '$'));
		$tollMinuteCost->setDescription("Amount charged to account per minute of toll telephony (in dollars)");
		$tollMinuteCost->addValidator(new Zend_Validate_Float());

		$monthlyTelephonyBudget = $this->createElement('text', 'monthlyTelephonyBudget', array('label' => 'Monthly Telephony Budget', 'class' => 'input-small', 'prepend' => '$'));
		$monthlyTelephonyBudget->setDescription("Amount of money allotted each month for telephony (in dollars)");
		$monthlyTelephonyBudget->addValidator(new Zend_Validate_Float());

		$telephonyResetDay = $this->createElement('text', 'telephonyResetDay', array('label' => 'Telephony Reset Day', 'class' => 'input-small'));
		$telephonyResetDay->setDescription("Day of month that Telephony resets. Defaults to 1st of the month. Clear field or set to 0 or 1 to use default value.");
		$telephonyResetDay->addValidator(new Custom_Validate_ValidDayOfMonth());

		// get telephony options from site
		$webexApiHelper = new Wst_Model_Helpers_Webexapihelper();
		$fetchTelephonyResult = $webexApiHelper->getSiteSupportedTelephonyOptions();

		$telephonyPrivilegeOptionsRaw = array();
		foreach ($fetchTelephonyResult["siteTelephony"] as $priv => $enabled) {
			if ($enabled) {
				$telephonyPrivilegeOptionsRaw[] = $priv;
			}
		}
		$helper = new Wst_Model_Helpers_General();
		$telephonyPrivilegeOptionsArray = $helper->getTelephonyTypeMapping($telephonyPrivilegeOptionsRaw);

		// default telephony
		$defaultTelephonyPrivileges = $this->createElement('multiCheckbox', 'defaultTelephonyPrivileges', array(
			'label' => 'Default Telephony Privileges',
			'multiOptions' => $telephonyPrivilegeOptionsArray
		));
		$defaultTelephonyPrivileges->setDescription('The default telephony options for the site (for new users).');

		// admin-add telephony
		$telephonyPrivilegeOptions = $this->createElement('multiCheckbox', 'telephonyPrivilegeOptions', array(
			'label' => 'Added Telephony Privilege Options',
			'multiOptions' => $telephonyPrivilegeOptionsArray
		));
		$telephonyPrivilegeOptions->setDescription('Select one or more options to offer to admins when giving Telephony privileges to users.');

		// thresholds
		$telephonyUsageThreshold = $this->createElement('text', 'telephonyUsageThreshold', array('label' => 'Telephony % Usage Threshold', 'class' => 'input-small'));
		$telephonyUsageThreshold->setDescription("Notifications will be sent to admins if usage exceeds this amount in the current billing period.");
		$telephonyUsageThreshold->addValidator(new Zend_Validate_Int());

		$storageUsageThreshold = $this->createElement('text', 'storageUsageThreshold', array('label' => 'Rec. Storage % Usage Threshold', 'class' => 'input-small'));;
		$storageUsageThreshold->setDescription("Notifications will be sent to admins if recording storage exceeds this amount.");
		$storageUsageThreshold->addValidator(new Zend_Validate_Int());

		$overrideEmailIntervals = $this->createElement('text', 'overrideEmailIntervals', array('label' => 'Override Expiration Email Intervals', 'class' => 'input-medium'));
		$overrideEmailIntervals->setDescription("Notifications intervals (in days) at which to send notice to the responsible party when a WebEx Override is going to expire. Separate with commas (e.g. 1,3,5,10)");

		//$splunkSearchQueryOneshot = $this->createElement('text', 'splunkSearchQueryOneshot', array('label' => 'Splunk IDP Query'));
		//$splunkSearchQueryOneshot->setDescription('One-Shot Search Query for Splunk IDP Auth activity. Must start with "search"');
		//$splunkSearchQueryOneshot->setAttrib('style', 'width: 80%;');
		//
		//$splunkDuoSearchQueryOneshot = $this->createElement('text', 'splunkDuoSearchQueryOneshot', array('label' => 'Splunk Duo Query'));
		//$splunkDuoSearchQueryOneshot->setDescription('One-Shot Search Query for Splunk Duo Auth Activity. Must start with "search"');
		//$splunkDuoSearchQueryOneshot->setAttrib('style', 'width: 80%;');
		//
		//$splunkCitrixSearchQueryOneshot = $this->createElement('text', 'splunkCitrixSearchQueryOneshot', array('label' => 'Splunk Citrix Query'));
		//$splunkCitrixSearchQueryOneshot->setDescription('One-Shot Search Query for Splunk Citrix Auth Activity. Must start with "search"');
		//$splunkCitrixSearchQueryOneshot->setAttrib('style', 'width: 80%;');
		//
		//$statusArray = array(
		//	'1' => 'Enabled',
		//	'0' => 'Disabled',
		//);
		//
		//$disableCompromisedAccountsInCsam = $this->createElement('select', 'disableCompromisedAccountsInCsam', array('label' => 'Disable Compromises in CSAM:'));
		//$disableCompromisedAccountsInCsam->setMultiOptions($statusArray);
		//$disableCompromisedAccountsInCsam->setRequired(true);
		//
		//$disableCompromisedAccountsInGoogle = $this->createElement('select', 'disableCompromisedAccountsInGoogle', array('label' => 'Disable Compromises in Google:'));
		//$disableCompromisedAccountsInGoogle->setMultiOptions($statusArray);
		//$disableCompromisedAccountsInGoogle->setRequired(true);
		//
		//$thresholdRange = range(1,100);
		//$thresholdArray = array();
		//foreach($thresholdRange as $t) {
		//	$thresholdArray[$t] = $t;
		//}
		//
		//$thresholdNumberOfAccountsToProcessAsCompromised = $this->createElement('select', 'thresholdNumberOfAccountsToProcessAsCompromised', array('label' => 'Threshold for Compromised Accounts:'));
		//$thresholdNumberOfAccountsToProcessAsCompromised->setMultiOptions($thresholdArray);
		//$thresholdNumberOfAccountsToProcessAsCompromised->setRequired(true);
		//$thresholdNumberOfAccountsToProcessAsCompromised->setDescription('If the compromise workflow finds this many to process in one run, pause cron jobs until admin review.');
		//
		//$dayRange = range(1, 90);
		//$dayRangeArray = array();
		//foreach($dayRange as $day) {
		//	$dayRangeArray[$day] = $day;
		//}
		//
		//$daysToSendNotificationsForExceptions = $this->createElement('multiselect', 'daysToSendNotificationsForExceptions', array('label' => 'Days to send Exception Expiration Notifications'));
		//$daysToSendNotificationsForExceptions->setMultiOptions($dayRangeArray);
		//$daysToSendNotificationsForExceptions->setRequired(true);
		//$daysToSendNotificationsForExceptions->setDescription('Select one or more days that notifications will be sent to the owner of a current Compromise Exception record. IE: 30 days until the exception ends, 15 days until it ends etc.');
		//
		//
		//$dmarcEmailAddress = $this->createElement('text', 'dmarcEmailAddress', array('label' => 'DMARC Email Address'));
		//$dmarcEmailAddress->setDescription('Full email address of the account that will be searched for DMARC attachment/reports');
		//$dmarcEmailAddress->setAttrib('style', 'width: 80%;');
		//
		//$dmarcSearchQuery = $this->createElement('text', 'dmarcSearchQuery', array('label' => 'DMARC Search Query'));
		//$dmarcSearchQuery->setDescription('IE: "has:attachment filename:zip label:app-notifications-dmarc-reports". GMAIL search query to use on the DMARC account. Will be appended by BEFORE/AFTER argument based on cron schedule.');
		//$dmarcSearchQuery->setAttrib('style', 'width: 80%;');
		//
		//$dmarcOptionsArray = array(
		//	'all' => 'All matching emails',
		//	'1' => 'Previous day',
		//	'7' => 'Previous 7 days',
		//	'30' => 'Previous 30 days',
		//	'60' => 'Previous 60 days',
		//	'90' => 'Previous 90 days',
		//);
		//
		//$dmarcSearchRange = $this->createElement('select', 'dmarcSearchRange', array('label' => 'DMARC Search Range:'));
		//$dmarcSearchRange->setMultiOptions($dmarcOptionsArray);
		//$dmarcSearchRange->setRequired(true);
		//
		//$metricsKey = $this->createElement('text', 'metricsKey', array('label' => 'Metrics Key'));
		//$metricsKey->setDescription('Key for the Metrics Dashboard, to differentiate this instance of GST from others that may also push metrics to the dashboard.');
		//$metricsKey->setAttrib('style', 'width: 80%;');
		//
		//$metricsCompromiseKey = $this->createElement('text', 'metricsCompromiseKey', array('label' => 'Metrics - Compromise Key'));
		//$metricsCompromiseKey->setDescription('Key for the Metrics Dashboard for Compromised Accounts, to differentiate this instance of GST from others that may also push metrics to the dashboard.');
		//$metricsCompromiseKey->setAttrib('style', 'width: 80%;');
		//
		//$this->addElements(array($splunkSearchQueryOneshot, $splunkDuoSearchQueryOneshot, $splunkCitrixSearchQueryOneshot, $disableCompromisedAccountsInCsam, $disableCompromisedAccountsInGoogle, $thresholdNumberOfAccountsToProcessAsCompromised, $daysToSendNotificationsForExceptions, $dmarcEmailAddress, $dmarcSearchQuery, $dmarcSearchRange, $metricsKey, $metricsCompromiseKey));
		//

		$this->addElements(array($recordingDeletionWindow, $localRecordingCacheCleaningWindow, $arfSizeDifferenceThresholdMegabytes, $arfDownloadRetries, $arfUploadRetries, $arfDeleteRetries, $recordingUploadFolderName, $downloadLinkReplacement, $earliestDeleteTimestamp, $tollMinuteCost, $monthlyTelephonyBudget, $telephonyResetDay, $defaultTelephonyPrivileges, $telephonyPrivilegeOptions, $telephonyUsageThreshold, $storageUsageThreshold, $myRecordingsLinkReplacement, $overrideEmailIntervals));


		$this->addDisplayGroup(
			array('recordingDeletionWindow','localRecordingCacheCleaningWindow','arfSizeDifferenceThresholdMegabytes','arfDownloadRetries','arfUploadRetries','arfDeleteRetries','recordingUploadFolderName','downloadLinkReplacement','myRecordingsLinkReplacement','earliestDeleteTimestamp'),
			'recordings',
			array(
				'disableLoadDefaultDecorators' => true,
				'decorators' => array('FormElements','Fieldset'),
				'legend' => "<i class='fa fa-film'></i> Recording Download Management",
				'escape' => false // for font-awesome
			)
		);

		$this->addDisplayGroup(
			array('tollMinuteCost','monthlyTelephonyBudget','telephonyResetDay','defaultTelephonyPrivileges', 'telephonyPrivilegeOptions'),
			'telephony',
			array(
				'disableLoadDefaultDecorators' => true,
				'decorators' => array('FormElements','Fieldset'),
				'legend' => "<i class='fa fa-phone'></i> Telephony Management",
				'escape' => false // for font-awesome
			)
		);

		$this->addDisplayGroup(
			array('telephonyUsageThreshold','storageUsageThreshold','overrideEmailIntervals'),
			'notifications',
			array(
				'disableLoadDefaultDecorators' => true,
				'decorators' => array('FormElements','Fieldset'),
				'legend' => "<i class='fa fa-warning'></i> Usage / Notification Thresholds",
				'escape' => false // for font-awesome
			)
		);



		$this->addElement('submit', 'submit', array(
			'buttonType' => Twitter_Bootstrap_Form_Element_Submit::BUTTON_PRIMARY,
			'label'      => 'Save/Confirm Configuration Now'
		));



		$this->addElement('button', 'cancel', array(
			'label'         => 'form-button-cancel',
			'type'          => 'button'
		));

		$this->addDisplayGroup(
			array('submit', 'cancel'),
			'actions',
			array(
				'disableLoadDefaultDecorators' => true,
				'decorators' => array('Actions')
			)
		);

	}

}