<?php
class Wst_Form_Searchrecordings extends Twitter_Bootstrap_Form_Inline
{
	public function init()
	{
		$this->setAttrib('id', 'searchForm')
			->setDecorators(array(
				'FormElements',
				array('HtmlTag', array('tag' => 'div', 'class' => 'panel panel-gray-bg')),
				'Form',
			))
			->setMethod(Zend_Form::METHOD_GET);

		$host_webex_id = $this->createElement('text', 'host_webex_id', array('label' => 'Host Username'));
		$host_webex_id->setAttrib('class', 'input-small');

		$recording_name = $this->createElement('text', 'session_name', array('label' => 'Recording Name'));
		$recording_name->setAttrib('class', 'input-small');

		$create_time_start = $this->createElement('text', 'create_time_start', array('label' => 'Create Time after'));
		$create_time_start->setAttrib('class', 'input-small');

		$create_time_end = $this->createElement('text', 'create_time_end', array('label' => 'Create Time before'));
		$create_time_end->setAttrib('class', 'input-small');

		$duration_min = $this->createElement('text', 'duration_min', array('label' => 'Duration Minutes (min)'));
		$duration_min->setAttrib('class', 'input-mini');

		$duration_max = $this->createElement('text', 'duration_max', array('label' => 'Duration Minutes (max)'));
		$duration_max->setAttrib('class', 'input-mini');

		$size_min = $this->createElement('text', 'size_min', array('label' => 'Size (min)'));
		$size_min->setAttrib('class', 'input-mini');

		$size_max = $this->createElement('text', 'size_max', array('label' => 'Size (max)'));
		$size_max->setAttrib('class', 'input-mini');

		$recordingArchiveModel = new Wst_Model_Dbtable_Recordingarchive();
		$select = $recordingArchiveModel->getAdapter()->select();
		$select->from($recordingArchiveModel->info('name'), 'status');
		$select->distinct();
		$result = $select->query()->fetchAll();

		$statusOptions = array("" => "");

		foreach($result as $k => $v) {
			$statusOptions[$v['status']] = $v['status'];
		}

		$status = $this->createElement('select', 'status', array('label' => 'Status'));
		$status->setMultiOptions($statusOptions);
		$status->setAttrib('class', 'input-small');

		$this->addElements(array($host_webex_id, $recording_name, $create_time_start, $create_time_end, $duration_min, $duration_max, $size_min, $size_max, $status));

		$sort = $this->createElement('hidden', 'sort');
		$sort->setDecorators(array('ViewHelper'));

		$direction = $this->createElement('hidden', 'direction');
		$direction->setDecorators(array('ViewHelper'));

		$this->setElementDecorators(array(
			'ViewHelper',
			//array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'elm')),
			array('Errors', array('placement' => 'append')),
			array('Label', array('placement' => 'prepend')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'criteria')),
		));

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Apply Filter'));
		$submit->setAttrib('class', 'btn btn-primary');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'submit')),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'ui-helper-clearfix')),
		));

		$reset = $this->createElement('reset', 'resetButton', array('label' => 'Reset'));
		$reset->setAttrib('class', 'btn btn-danger');
		$reset->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'reset')),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'ui-helper-clearfix')),
		));

		$this->addElements(array($submit, $reset, $sort, $direction));



	}
}
