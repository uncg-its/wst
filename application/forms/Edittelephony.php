<?php
class Wst_Form_Edittelephony extends Twitter_Bootstrap_Form_Horizontal {
	public function init() {
		$this->setAttrib('id', 'editTelephony');

		//$webExId = $this->createElement('text', 'webExId', array('label' => "WebEx ID (username)"));
		//$webExId->setDescription('Username of user. Must be existing WebEx user.');
		//$webExId->addValidator(new Custom_Validate_ExistingWebexUser);
		//$webExId->addValidator(new Custom_Validate_UserDoesNotHaveTelephony());
		//$webExId->setRequired(true);

		$configModel = new Wst_Model_Dbtable_Config();
		$telephonyPrivilegeOptions = explode(",", $configModel->getVar('telephonyPrivilegeOptions'));
		$helper = new Wst_Model_Helpers_General();
		$telephonyPrivilegeOptions = $helper->getTelephonyTypeMapping($telephonyPrivilegeOptions);

		$telephonyPrivileges = $this->createElement('multiCheckbox', 'telephonyPrivileges', array('label' => 'Telephony Privileges'));
		$telephonyPrivileges->setMultiOptions($telephonyPrivilegeOptions);
		$telephonyPrivileges->setDescription('Select which privileges to give this user');

		$customTrackingCode = $this->createElement('text', 'customTrackingCode', array('label' => "Custom Tracking Code"));
		$customTrackingCode->setDescription("Enter a custom tracking code value here, to be stored in the 'Custom 1' field");

		$this->addElements(array($telephonyPrivileges, $customTrackingCode));

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Submit'));
		$submit->setAttrib('class', 'btn');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
		));

		$this->addElements(array($submit));
	}
}

?>

