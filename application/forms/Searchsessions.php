<?php
class Wst_Form_Searchsessions extends Twitter_Bootstrap_Form_Inline
{
	public function init()
	{
		$this->setAttrib('id', 'searchForm')
			->setDecorators(array(
				'FormElements',
				array('HtmlTag', array('tag' => 'div', 'class' => 'panel panel-gray-bg')),
				'Form',
			))
			->setMethod(Zend_Form::METHOD_GET);

		$sessionArchiveModel = new Wst_Model_Dbtable_Sessionarchive();
		$select = $sessionArchiveModel->getAdapter()->select();
		$select->from($sessionArchiveModel->info('name'), 'center');
		$select->distinct();

		$result = $select->query()->fetchAll();

		$centerOptions = array("" => "");

		foreach($result as $k => $v) {
			$centerOptions[$v['center']] = $v['center'];
		}

		$center = $this->createElement('select', 'center', array('label' => 'Center'));
		$center->setMultiOptions($centerOptions);
		$center->setAttrib('class', 'input-medium');

		$host_username = $this->createElement('text', 'host_username', array('label' => 'Host Username'));
		$host_username->setAttrib('class', 'input-small');

		$session_name = $this->createElement('text', 'session_name', array('label' => 'Session Name'));
		$session_name->setAttrib('class', 'input-small');

		$start_time_start = $this->createElement('text', 'start_time_start', array('label' => 'Start time after'));
		$start_time_start->setAttrib('class', 'input-small');

		$start_time_end = $this->createElement('text', 'start_time_end', array('label' => 'Start time before'));
		$start_time_end->setAttrib('class', 'input-small');

		$duration_min = $this->createElement('text', 'duration_min', array('label' => 'Duration (min)'));
		$duration_min->setAttrib('class', 'input-mini');

		$duration_max = $this->createElement('text', 'duration_max', array('label' => 'Duration (max)'));
		$duration_max->setAttrib('class', 'input-mini');

		$this->addElements(array($center, $session_name, $host_username, $start_time_start, $start_time_end, $duration_min, $duration_max));

		$sort = $this->createElement('hidden', 'sort');
		$sort->setDecorators(array('ViewHelper'));

		$direction = $this->createElement('hidden', 'direction');
		$direction->setDecorators(array('ViewHelper'));

		$this->setElementDecorators(array(
			'ViewHelper',
			//array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'elm')),
			array('Errors', array('placement' => 'append')),
			array('Label', array('placement' => 'prepend')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'criteria')),
		));

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Apply Filter'));
		$submit->setAttrib('class', 'btn btn-primary');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'submit')),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'ui-helper-clearfix')),
		));

		$reset = $this->createElement('reset', 'resetButton', array('label' => 'Reset'));
		$reset->setAttrib('class', 'btn btn-danger');
		$reset->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'reset')),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'ui-helper-clearfix')),
		));

		$this->addElements(array($submit, $reset, $sort, $direction));



	}
}
