<?php
class Wst_Form_Checkusereligibility extends Twitter_Bootstrap_Form_Horizontal {
	public function init() {
		$this->setAttrib('id', 'checkUserEligibility');

		$checkTeacherFeed = $this->createElement('select', 'checkTeacherFeed', array('label' => "Check Canvas Teacher Feed?"));
		$checkTeacherFeed->setRequired(true);
		$checkTeacherFeed->setMultiOptions(array(
			'false' => 'No',
			'true' => 'Yes',
		));

		$userSourceType = $this->createElement('select', 'userSourceType', array('label' => "User Source"));
		$userSourceType->setRequired(true);
		$userSourceType->setMultiOptions(array(
			'file' => 'User Feed (cached)',
			'ldap' => 'LDAP (live)',
		));

		$usernames = $this->createElement('textarea', 'usernames', array('label' => "Usernames (one per line)"));
		$usernames->setRequired(true);
		$usernames->setAttrib('class', 'input-xxlarge');

		$this->addElements(array($checkTeacherFeed, $userSourceType, $usernames));

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Submit'));
		$submit->setAttrib('class', 'btn');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
		));

		$this->addElements(array($submit));
	}
}

?>