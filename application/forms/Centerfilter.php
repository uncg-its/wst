<?php
class Wst_Form_Centerfilter extends Twitter_Bootstrap_Form_Horizontal {
	// TODO - this doesn't work yet.

	public function init() {
		$this->setAttrib('id', 'centerFilter');
		$this->setMethod("get");

		$mc = $this->createElement('checkbox', 'mc', array('label' => 'Meeting Center'));
		$tc = $this->createElement('checkbox', 'tc', array('label' => 'Training Center'));
		$ec = $this->createElement('checkbox', 'ec', array('label' => 'Event Center'));
		$sc = $this->createElement('checkbox', 'sc', array('label' => 'Support Center'));

		$this->addElements(array($mc, $tc, $ec, $sc));

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Submit'));
		$submit->setAttrib('class', 'btn');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
		));

		$this->addElements(array($submit));
	}
}

?>

