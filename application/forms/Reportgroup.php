<?php

class Wst_Form_Reportgroup extends Twitter_Bootstrap_Form_Horizontal
{
	public function init()
	{
		$this->setAttrib('id', 'reportGroup');

		$this->setElementsBelongTo('bootstrap');

		// name of group
		$reportGroupName = $this->createElement('text', 'reportGroupName', array('label' => 'Name of group'));
		$reportGroupName->setDescription('Unique name for this group to help you identify it (e.g. CVPA all, Music Departments, etc.)');


		// get short codes
		$userListModel = new Wst_Model_Dbtable_Userlist();
		$orgShortCodeList = $userListModel->getDistinctOrgShortCodes();

		// reindex so that it will work with Zend
		$orgShortCodeListReindexed = array();
		foreach($orgShortCodeList as $code) {
			$orgShortCodeListReindexed[$code] = $code;
		}

		$orgShortCodes = $this->createElement('multiCheckbox', 'orgShortCodes', array(
			'label' => 'Department Codes to include',
			'multiOptions' => $orgShortCodeListReindexed
		));

		$this->addElements(array($reportGroupName, $orgShortCodes));


		$this->addDisplayGroup(
			array('reportGroupName', 'orgShortCodes'),
			'reportGroupDetails',
			array(
				'disableLoadDefaultDecorators' => true,
				'decorators' => array('FormElements','Fieldset'),
				'legend' => "<i class='fa fa-list'></i> Report Group Details",
				'escape' => false // for font-awesome
			)
		);



		$this->addElement('submit', 'submit', array(
			'buttonType' => Twitter_Bootstrap_Form_Element_Submit::BUTTON_PRIMARY,
			'label'      => 'Save Report Group'
		));



		$this->addElement('button', 'cancel', array(
			'label'         => 'form-button-cancel',
			'type'          => 'button'
		));

		$this->addDisplayGroup(
			array('submit', 'cancel'),
			'actions',
			array(
				'disableLoadDefaultDecorators' => true,
				'decorators' => array('Actions')
			)
		);

	}

}