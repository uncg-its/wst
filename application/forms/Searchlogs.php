<?php
class Wst_Form_Searchlogs extends Twitter_Bootstrap_Form_Inline
{
	public function init()
	{
		$this->setAttrib('id', 'searchForm')
			->setDecorators(array(
				'FormElements',
				array('HtmlTag', array('tag' => 'div', 'class' => 'panel panel-gray-bg')),
				'Form',
			))
			->setMethod(Zend_Form::METHOD_GET);

		//'username'    => trim($this->_getParam('username')),
		//	'message'     => trim($this->getParam('message')),
		//	'priority'    => trim($this->getParam('priority')),
		//	'attributeId' => trim($this->getParam('attributeId')),

		$username = $this->createElement('text', 'username', array('label' => 'Username'));
		$username->setAttrib('class', 'input-small');

		$message = $this->createElement('text', 'message', array('label' => 'Message'));
		$message->setAttrib('class', 'input-small');

		$priorityList = array(
			'' => '',
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4'
		);

		$priority = $this->createElement('select', 'priority', array('label' => 'Priority'));
		$priority->setMultiOptions($priorityList);
		$priority->setAttrib('class', 'input-mini');

		$attributeId = $this->createElement('text', 'attributeId', array('label' => 'Attribute ID'));
		$attributeId->setAttrib('class', 'input-small');


		$this->addElements(array($username, $message, $priority, $attributeId));

		$sort = $this->createElement('hidden', 'sort');
		$sort->setDecorators(array('ViewHelper'));

		$direction = $this->createElement('hidden', 'direction');
		$direction->setDecorators(array('ViewHelper'));

		$this->setElementDecorators(array(
			'ViewHelper',
			//array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'elm')),
			array('Errors', array('placement' => 'append')),
			array('Label', array('placement' => 'prepend')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'criteria')),
		));

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Apply Filter'));
		$submit->setAttrib('class', 'btn btn-primary');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'submit')),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'ui-helper-clearfix')),
		));

		$reset = $this->createElement('reset', 'resetButton', array('label' => 'Reset'));
		$reset->setAttrib('class', 'btn btn-danger');
		$reset->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'reset')),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'ui-helper-clearfix')),
		));

		$this->addElements(array($submit, $reset, $sort, $direction));



	}
}
