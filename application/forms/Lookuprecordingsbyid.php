<?php
class Wst_Form_Lookuprecordingsbyid extends Twitter_Bootstrap_Form_Horizontal {
	public function init() {
		$this->setAttrib('id', 'lookupRecordingsById');

		$recordingIds = $this->createElement('textarea', 'recordingIds', array('label' => "Recording IDs (one per line)"));
		$recordingIds->setRequired(true);
		$recordingIds->setAttrib('class', 'input-xxlarge');

		$this->addElements(array($recordingIds));

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Submit'));
		$submit->setAttrib('class', 'btn');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
		));

		$this->addElements(array($submit));
	}
}

?>