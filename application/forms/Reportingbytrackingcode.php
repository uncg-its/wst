<?php
class Wst_Form_Reportingbytrackingcode extends Twitter_Bootstrap_Form_Inline
{
	public function init()
	{
		$this->setAttrib('id', 'filterForm')
			->setDecorators(array(
				'FormElements',
				array('HtmlTag', array('tag' => 'div', 'class' => 'panel panel-gray-bg')),
				'Form',
			))
			->setMethod(Zend_Form::METHOD_GET);


		$trackingCodeModel = new Wst_Model_Dbtable_Trackingcodes();
		$trackingCodes = $trackingCodeModel->getTrackingCodesById();

		$trackingCodesSelect = array("" => "");
		foreach ($trackingCodes as $id => $codeInfo) {
			$trackingCodesSelect[$id] = $codeInfo["display_name"];
		}

		$trackingCodeCategory = $this->createElement('select', 'trackingCodeCategory', array('label' => 'Tracking Code Category'));
		$trackingCodeCategory->setMultiOptions($trackingCodesSelect);
		$trackingCodeCategory->setAttrib('class', 'input-medium');

		$start_time_start = $this->createElement('text', 'start_time_start', array('label' => 'Start time after'));
		$start_time_start->addValidator(new Zend_Validate_Date(array('format' => 'MM/dd/yyyy')));
		$start_time_start->setAttrib('class', 'input-small');

		$start_time_end = $this->createElement('text', 'start_time_end', array('label' => 'Start time before'));
		$start_time_end->addValidator(new Zend_Validate_Date(array('format' => 'MM/dd/yyyy')));
		$start_time_end->setAttrib('class', 'input-small');

		$this->addElements(array($trackingCodeCategory, $start_time_start, $start_time_end));

		//$sort = $this->createElement('hidden', 'sort');
		//$sort->setDecorators(array('ViewHelper'));
		//
		//$direction = $this->createElement('hidden', 'direction');
		//$direction->setDecorators(array('ViewHelper'));

		$this->setElementDecorators(array(
			'ViewHelper',
			//array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'elm')),
			array('Errors', array('placement' => 'append')),
			array('Label', array('placement' => 'prepend')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'criteria')),
		));

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Apply Filter'));
		$submit->setAttrib('class', 'btn btn-primary');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'submit')),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'ui-helper-clearfix')),
		));

		$reset = $this->createElement('reset', 'resetButton', array('label' => 'Reset'));
		$reset->setAttrib('class', 'btn btn-danger');
		$reset->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'reset')),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'ui-helper-clearfix')),
		));

		$this->addElements(array($submit, $reset));



	}
}
