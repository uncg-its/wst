<?php
class Wst_Form_Searchapiqueue extends Twitter_Bootstrap_Form_Inline
{
	public function init()
	{
		$this->setAttrib('id', 'searchForm')
			->setDecorators(array(
				'FormElements',
				array('HtmlTag', array('tag' => 'div', 'class' => 'panel panel-gray-bg')),
				'Form',
			))
			->setMethod(Zend_Form::METHOD_GET);



		$euid = $this->createElement('text', 'euid', array('label' => 'EUID'));
		$euid->setAttrib('class', 'input-small');

		$webExId = $this->createElement('text', 'webExId', array('label' => 'WebEx ID'));
		$webExId->setAttrib('class', 'input-small');

		$statusList = array(
			'' => '',
			'pending' => 'pending',
			'in-progress' => 'in-progress',
			'complete' => 'complete',
			'error' => 'error',
			'ignored' => 'ignored'
		);

		$status = $this->createElement('select', 'status', array('label' => 'Status'));
		$status->setMultiOptions($statusList);
		$status->setAttrib('class', 'input-small');

		$actionList = array(
			'' => '',
			'add' => 'add',
			'edit' => 'edit',
			'delete' => 'delete'
		);

		$action = $this->createElement('select', 'queue_action', array('label' => 'Action'));
		$action->setMultiOptions($actionList);
		$action->setAttrib('class', 'input-small');

		$message = $this->createElement('text', 'message', array('label' => 'Message'));
		$message->setAttrib('class', 'input-small');


		$this->addElements(array($euid, $webExId, $status, $action, $message));

		$sort = $this->createElement('hidden', 'sort');
		$sort->setDecorators(array('ViewHelper'));

		$direction = $this->createElement('hidden', 'direction');
		$direction->setDecorators(array('ViewHelper'));

		$this->setElementDecorators(array(
			'ViewHelper',
			//array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'elm')),
			array('Errors', array('placement' => 'append')),
			array('Label', array('placement' => 'prepend')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'criteria')),
		));

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Apply Filter'));
		$submit->setAttrib('class', 'btn btn-primary');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'submit')),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'ui-helper-clearfix')),
		));

		$reset = $this->createElement('reset', 'resetButton', array('label' => 'Reset'));
		$reset->setAttrib('class', 'btn btn-danger');
		$reset->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'reset')),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'ui-helper-clearfix')),
		));

		$this->addElements(array($submit, $reset, $sort, $direction));



	}
}
