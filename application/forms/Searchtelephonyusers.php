<?php
class Wst_Form_Searchtelephonyusers extends Twitter_Bootstrap_Form_Inline
{
	public function init()
	{
		$this->setAttrib('id', 'searchForm')
			->setDecorators(array(
				'FormElements',
				array('HtmlTag', array('tag' => 'div', 'class' => 'panel panel-gray-bg')),
				'Form',
			))
			->setMethod(Zend_Form::METHOD_GET);

		$helper = new Wst_Model_Helpers_General();
		$telephonyOptions = array("" => "");

		foreach($helper->telephonyKeys as $wstShortName => $keys) {
			$telephonyOptions[$wstShortName] = $keys["wstDisplayName"];
		}

		$telephony = $this->createElement('select', 'telephony_privileges', array('label' => 'Telephony'));
		$telephony->setMultiOptions($telephonyOptions);
		$telephony->setAttrib('class', 'input-medium');

		$webExId = $this->createElement('text', 'webExId', array('label' => 'WebEx ID'));
		$webExId->setAttrib('class', 'input-small');

		$lastName = $this->createElement('text', 'lastName', array('label' => 'Last Name'));
		$lastName->setAttrib('class', 'input-small');

		$firstName = $this->createElement('text', 'firstName', array('label' => 'First Name'));
		$firstName->setAttrib('class', 'input-small');


		$this->addElements(array($webExId, $lastName, $firstName, $telephony));

		$sort = $this->createElement('hidden', 'sort');
		$sort->setDecorators(array('ViewHelper'));

		$direction = $this->createElement('hidden', 'direction');
		$direction->setDecorators(array('ViewHelper'));

		$this->setElementDecorators(array(
			'ViewHelper',
			//array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'elm')),
			array('Errors', array('placement' => 'append')),
			array('Label', array('placement' => 'prepend')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'criteria')),
		));

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Apply Filter'));
		$submit->setAttrib('class', 'btn btn-primary');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'submit')),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'ui-helper-clearfix')),
		));

		$reset = $this->createElement('reset', 'resetButton', array('label' => 'Reset'));
		$reset->setAttrib('class', 'btn btn-danger');
		$reset->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'reset')),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'ui-helper-clearfix')),
		));

		$this->addElements(array($submit, $reset, $sort, $direction));



	}
}
