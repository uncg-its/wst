<?php
class Wst_Form_Searchmetrics extends Twitter_Bootstrap_Form_Inline
{
	public function init()
	{
		$this->setAttrib('id', 'searchForm')
			->setDecorators(array(
				'FormElements',
				array('HtmlTag', array('tag' => 'div', 'class' => 'panel panel-gray-bg')),
				'Form',
			))
			->setMethod(Zend_Form::METHOD_GET);

		$date = $this->createElement('text', 'date', array('label' => 'Date'));
		$date->setAttrib('class', 'input-small');

		$metricsModel = new Wst_Model_Dbtable_Metrics();
		$select = $metricsModel->getAdapter()->select();
		$select->from($metricsModel->info('name'), 'status');
		$select->distinct();

		$result = $select->query()->fetchAll();

		$statusOptions = array("" => "");

		foreach($result as $k => $v) {
			$statusOptions[$v['status']] = $v['status'];
		}

		$status = $this->createElement('select', 'status', array('label' => 'Status'));
		$status->setMultiOptions($statusOptions);
		$status->setAttrib('class', 'input-medium');

		$this->addElements(array($date, $status));

		$sort = $this->createElement('hidden', 'sort');
		$sort->setDecorators(array('ViewHelper'));

		$direction = $this->createElement('hidden', 'direction');
		$direction->setDecorators(array('ViewHelper'));

		$this->setElementDecorators(array(
			'ViewHelper',
			//array(array('wrapperField' => 'HtmlTag'), array('tag' => 'div', 'class' => 'elm')),
			array('Errors', array('placement' => 'append')),
			array('Label', array('placement' => 'prepend')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'criteria')),
		));

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Apply Filter'));
		$submit->setAttrib('class', 'btn btn-primary');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'submit')),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'ui-helper-clearfix')),
		));

		$reset = $this->createElement('reset', 'resetButton', array('label' => 'Reset'));
		$reset->setAttrib('class', 'btn btn-danger');
		$reset->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
			//array(array('wrapperAll' => 'HtmlTag'), array('tag' => 'div', 'class' => 'reset')),
			//array('HtmlTag', array('tag' => 'div', 'class' => 'ui-helper-clearfix')),
		));

		$this->addElements(array($submit, $reset, $sort, $direction));



	}
}
