<?php

class Wst_Form_Reportdaterange extends Twitter_Bootstrap_Form_Horizontal
{
	public function init()
	{
		$this->setAttrib('id', 'reportDateRange');

//		$this->setElementsBelongTo('bootstrap');

        $startDate = $this->createElement('text', 'start_date', array('label' => "Start Date"));
        $startDate->setRequired(true);
        $startDate->setDescription("Earliest date for this range (inclusive)");
        $startDate->addValidator(new Zend_Validate_Date(array('format' => 'MM/dd/yyyy')));

        $endDate = $this->createElement('text', 'end_date', array('label' => "End Date"));
        $endDate->setRequired(true);
        $endDate->setDescription("Latest date for this range (inclusive)");
        $endDate->addValidator(new Zend_Validate_Date(array('format' => 'MM/dd/yyyy')));
        $endDate->addValidator(new Custom_Validate_EndDateIsAfterStartDate(true, array(
            'start_date' => $startDate->getValue(),
            'end_date' => $endDate->getValue()
        )));

		$this->addElements(array($startDate, $endDate));


		$this->addDisplayGroup(
			array('start_date', 'end_date'),
			'reportGroupDetails',
			array(
				'disableLoadDefaultDecorators' => true,
				'decorators' => array('FormElements','Fieldset'),
				'legend' => "<i class='fa fa-calendar'></i> Date Range Definition",
				'escape' => false // for font-awesome
			)
		);



		$this->addElement('submit', 'submit', array(
			'buttonType' => Twitter_Bootstrap_Form_Element_Submit::BUTTON_PRIMARY,
			'label'      => 'Save Date Range'
		));



		$this->addElement('button', 'cancel', array(
			'label'         => 'form-button-cancel',
			'type'          => 'button'
		));

		$this->addDisplayGroup(
			array('submit', 'cancel'),
			'actions',
			array(
				'disableLoadDefaultDecorators' => true,
				'decorators' => array('Actions')
			)
		);

	}

}