<?php
class Wst_Form_Manuallyadduser extends Twitter_Bootstrap_Form_Horizontal {
	public function init() {
		$this->setAttrib('id', 'manuallyAddUser');

		$euid = $this->createElement('text', 'euid', array('label' => "EUID"));
		$euid->setRequired(true);
		$euid->addValidator(new Zend_Validate_Digits());

		$webExId = $this->createElement('text', 'webExId', array('label' => "WebEx ID (username)"));
		$webExId->setRequired(true);

		$firstName = $this->createElement('text', 'firstName', array('label' => "First Name"));
		$firstName->setRequired(true);

		$lastName = $this->createElement('text', 'lastName', array('label' => "Last Name"));
		$lastName->setRequired(true);

		$email = $this->createElement('text', 'email', array('label' => "Email Address"));
		$email->setRequired(true);
		$email->addValidator(new Zend_Validate_EmailAddress());

		$reason = $this->createElement('text', 'reason', array('label' => "Reason for Manual Add"));
		$reason->setRequired(true);
		$reason->setDescription("Please provide a reason for adding this user manually to WebEx.");

		$contactEmail = $this->createElement('text', 'contact_email', array('label' => "Contact e-mail address"));
		$contactEmail->setRequired(true);
		$contactEmail->setDescription("Please provide the primary contact person responsible for managing this manual add. This person will receive periodic notifications about the manual add.");
		$contactEmail->addValidator(new Zend_Validate_EmailAddress());

		//$startDate = $this->createElement('text', 'start_date', array('label' => "Start Date"));
		//$startDate->setDescription("Date the override should start (default is today)");
		//$startDate->addValidator(new Zend_Validate_Date(array('format' => 'MM/dd/yyyy')));

		$endDate = $this->createElement('text', 'end_date', array('label' => "End Date"));
		$endDate->setRequired(true);
		$endDate->setDescription("Date the override should end");
		$endDate->addValidator(new Zend_Validate_Date(array('format' => 'MM/dd/yyyy')));
		$endDate->addValidator(new Custom_Validate_ValidEndDate(true, array(
			//'start_date' => $startDate->getValue(),
			'start_date' => "",
			'end_date' => $endDate->getValue()
		)));

		// REFERENCE - datepicker
		//$dateInput = $this->createElement('text', 'date');
		//$dateInput->addValidator(new Zend_Validate_Date(array('format' => 'MM/dd/yyyy')));
		//$dateInput->setValue(Zend_Date::now()->toString('MM/dd/yyyy'));

		$this->addElements(array($euid, $webExId, $firstName, $lastName, $email, $reason, $contactEmail, $endDate));

		$this->addDisplayGroup(
			array('euid','webExId','firstName','lastName','email'),
			'user-information',
			array(
				'disableLoadDefaultDecorators' => true,
				'decorators' => array('FormElements','Fieldset'),
				'legend' => "User Information"
			)
		);

		$this->addDisplayGroup(
			array('reason','contact_email','end_date'),
			'override-information',
			array(
				'disableLoadDefaultDecorators' => true,
				'decorators' => array('FormElements', 'Fieldset'),
				'legend' => "Override Information"
			)
		);

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Submit'));
		$submit->setAttrib('class', 'btn');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
		));

		$this->addElements(array($submit));
	}
}

?>

