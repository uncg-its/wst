<?php
class Wst_Form_Lookupuserrecordings extends Twitter_Bootstrap_Form_Horizontal {
	public function init() {
		$this->setAttrib('id', 'lookupUserRecordings');

		$username = $this->createElement('text', 'username', array('label' => "Username"));
		$username->setRequired(true);
		$username->setAttrib('class', 'input-large');

		$this->addElements(array($username));

		$submit = $this->createElement('submit', 'submitButton', array('label' => 'Submit'));
		$submit->setAttrib('class', 'btn');
		$submit->setDecorators(array(
			array('ViewHelper', array('helper' => 'formSubmit')),
		));

		$this->addElements(array($submit));
	}
}

?>