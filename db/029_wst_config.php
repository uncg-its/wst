<?php

class Db_029_wst_config extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_wst_config` (
  `var_key` varchar(255) NOT NULL DEFAULT '',
  `var_value` text,
  PRIMARY KEY (`var_key`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "DROP TABLE
            `" . $this->tablePrefix . "tbl_wst_config`;";

        $dba->query($query);

    }
   
}