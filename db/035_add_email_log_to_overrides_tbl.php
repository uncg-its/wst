<?php

class Db_035_add_email_log_to_overrides_tbl extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            ALTER TABLE `" . $this->tablePrefix ."tbl_user_overrides`
                ADD COLUMN `notifications` TEXT DEFAULT NULL AFTER `user_reverted_timestamp`
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "
			ALTER TABLE `" . $this->tablePrefix . "tbl_user_overrides`
                DROP COLUMN `notifications`
        ;";

        $dba->query($query);

    }
   
}