<?php

class Db_027_wst_recording_archive extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_recording_archive` (
  `recording_id` bigint(11) unsigned NOT NULL,
  `conf_id` bigint(11) NOT NULL,
  `host_webex_id` varchar(50) NOT NULL DEFAULT '',
  `name` text NOT NULL,
  `create_time_utc` int(11) NOT NULL,
  `size_mb` float NOT NULL,
  `duration_sec` int(11) NOT NULL,
  `stream_url` text,
  `file_url` text,
  `format` varchar(10) NOT NULL DEFAULT '',
  `status` enum('active','deleted-by-user','deleted-by-admin','deleted-by-wst','in-trash') NOT NULL DEFAULT 'active',
  `trashed_timestamp` int(11) DEFAULT NULL,
  `deleted_timestamp` int(11) DEFAULT NULL,
  `cloud_storage_info` text,
  `upload_status` enum('not-uploaded','pending','in-progress','uploaded','uploaded-with-errors','error','failed') NOT NULL DEFAULT 'not-uploaded',
  `download_status` enum('not-downloaded','pending','in-progress','downloaded','error','failed') NOT NULL DEFAULT 'not-downloaded',
  `downloaded_file_size_mb` float DEFAULT NULL,
  `download_failures` int(11) NOT NULL DEFAULT '0',
  `upload_failures` int(11) NOT NULL DEFAULT '0',
  `delete_failures` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`recording_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "DROP TABLE
            `" . $this->tablePrefix . "tbl_recording_archive`;";

        $dba->query($query);

    }
   
}