<?php

/**
 * Changes the how the API works
 */

class Db_021_wst_application_vars extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_application_vars` (
              `var_key` varchar(100) NOT NULL DEFAULT '',
              `var_value` varchar(100) DEFAULT NULL,
              PRIMARY KEY (`var_key`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ";

        $dba->query($query);

        $query = "
            INSERT INTO `" . $this->tablePrefix . "tbl_application_vars` (`var_key`,`var_value`) VALUES('lastFeedTimestamp','0'), ('webexApiStatus','up'),('appStatus','up');
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "DROP TABLE
            `" . $this->tablePrefix . "tbl_application_vars`;";

        $dba->query($query);

    }
   
}