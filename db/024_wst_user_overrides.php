<?php

/**
 * Changes the how the API works
 */

class Db_024_wst_user_overrides extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_user_overrides` (
              `o_id` int(11) NOT NULL AUTO_INCREMENT,
              `euid` varchar(25) NOT NULL DEFAULT '',
              `reason` text NOT NULL,
              `contact_email` tinytext NOT NULL,
              `start_timestamp` int(11) NOT NULL,
              `end_timestamp` int(11) NOT NULL,
              `user_status` enum('active','inactive') NOT NULL DEFAULT 'inactive',
              `cancelled_timestamp` int(11) DEFAULT NULL,
              `user_reverted_timestamp` int(11) DEFAULT NULL,
              PRIMARY KEY (`o_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "DROP TABLE
            `" . $this->tablePrefix . "tbl_user_overrides`;";

        $dba->query($query);

    }
   
}