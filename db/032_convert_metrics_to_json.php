<?php

class Db_032_convert_metrics_to_json extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        //$query = "ALTER TABLE `" . $this->tablePrefix . "tbl_recording_archive`
        //    MODIFY COLUMN  upload_status enum('not-uploaded','pending','in-progress','uploaded','uploaded-with-errors','error','failed','ignored'),
        //    MODIFY COLUMN  download_status enum('not-downloaded','pending','in-progress','downloaded','error','failed','ignored')";
        //
        //$dba->query($query);

	    // get data from metrics table
	    $metricsModel = new Wst_Model_Dbtable_Metrics();
	    $metrics = $metricsModel->fetchAll()->toArray();

	    // duplicate
	    $query1 = "CREATE TABLE " . $this->tablePrefix . "tbl_metrics_old LIKE " . $this->tablePrefix . "tbl_metrics";
		$query2 = "INSERT " . $this->tablePrefix . "tbl_metrics_old SELECT * FROM " . $this->tablePrefix . "tbl_metrics";

	    $dba->query($query1);
	    $dba->query($query2);

	    // get rid of old table, make new one
	    $query3 = "DROP TABLE " . $this->tablePrefix . "tbl_metrics";

	    $dba->query($query3);

	    $query4 = "CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_metrics` (
  `metric_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` tinytext NOT NULL,
  `rawData` text NOT NULL,
  `timestamp` int(11) NOT NULL,
  `status` enum('pending-verification','verified','sending','complete','error') NOT NULL DEFAULT 'pending-verification',
  `error_data` text,
  PRIMARY KEY (`metric_id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;
	    ";

	    $dba->query($query4);

		// migrate
	    $staticFields = array("metric_id", "date", "timestamp", "status", "error_data");

	    $inserts = array();
	    foreach ($metrics as $metric) {
		    $encodedFields = array();
		    foreach($metric as $k => $m) {
			    if (!in_array($k, $staticFields)) {
				    $encodedFields[$k] = $m;
			    }
		    }

		    $insertData = array();
		    foreach($staticFields as $f) {
			    $insertData[$f] = $metric[$f];
		    }

		    $rawData = json_encode($encodedFields);
		    $insertData["rawData"] = $rawData;

		    $inserts[] = $insertData;
	    }

	    $metricsModel = new Wst_Model_Dbtable_Metrics();
	    foreach($inserts as $i) {
		    $metricsModel->insert($i);
	    }

	    // drop old table
	    $query5 = "DROP TABLE " . $this->tablePrefix . "tbl_metrics_old";

	    $dba->query($query5);
    }
    
    public function down($dba)
    {
	    // get current metrics in json
	    $metricsModel = new Wst_Model_Dbtable_Metrics();
	    $metrics = $metricsModel->fetchAll()->toArray();

	    // move the table to a temp table
	    $query1 = "CREATE TABLE " . $this->tablePrefix . "tbl_metrics_old LIKE " . $this->tablePrefix . "tbl_metrics";
	    $query2 = "INSERT " . $this->tablePrefix . "tbl_metrics_old SELECT * FROM " . $this->tablePrefix . "tbl_metrics";

	    $dba->query($query1);
	    $dba->query($query2);

	    // get rid of old table, make new one
	    $query3 = "DROP TABLE " . $this->tablePrefix . "tbl_metrics";

	    $dba->query($query3);

	    $query4 = "CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_metrics` (
  `metric_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` tinytext NOT NULL,
  `numberOfActiveUsers` int(11) NOT NULL DEFAULT 999,
  `numberOfSessions` int(11) NOT NULL,
  `numberOfSessionsWithLength0To4` int(11) NOT NULL,
  `numberOfSessionsWithLength5To29` int(11) NOT NULL,
  `numberOfSessionsWithLength30To59` int(11) NOT NULL,
  `numberOfSessionsWithLength60To119` int(11) NOT NULL,
  `numberOfSessionsWithLength120OrHigher` int(11) NOT NULL,
  `numberOfSessionsWithRecording` int(11) NOT NULL,
  `numberOfSessionsWithTelephony` int(11) NOT NULL,
  `numberOfAttendees` int(11) NOT NULL,
  `numberOfAttendeesUnique` int(11) NOT NULL,
  `numberOfMobileAppUsers` int(11) NOT NULL,
  `callInUsers` int(11) NOT NULL,
  `callOutUsers` int(11) NOT NULL,
  `callInMinutes` int(11) NOT NULL,
  `callInTollFreeMinutes` int(11) NOT NULL,
  `callOutDomesticMinutes` int(11) NOT NULL,
  `callOutInternationalMinutes` int(11) NOT NULL,
  `peopleMinutes` int(11) NOT NULL,
  `numberOfRecordingsMade` int(11) NOT NULL,
  `recordingStorageUsed` float NOT NULL,
  `timestamp` int(11) NOT NULL,
  `status` enum('pending-verification','verified','sending','complete','error') NOT NULL DEFAULT 'pending-verification',
  `error_data` text,
  PRIMARY KEY (`metric_id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;";

	    $dba->query($query4);

	    // migrate

	    $metricsModel = new Wst_Model_Dbtable_Metrics(); // re-grab
	    $cols = $metricsModel->info(Zend_Db_Table_Abstract::COLS); // to refer to later

	    $inserts = array();

	    foreach ($metrics as $k => $metric) {
		    $rawData = json_decode($metric["rawData"], true);

		    unset($metric["rawData"]);

		    foreach($rawData as $key => $value) {
			    if (!in_array($key, $cols)) {
				    unset($rawData[$key]);
			    }
		    }

		    $insertData = array_merge($metric, $rawData);

		    //dump($insertData, true);

		    $inserts[] = $insertData;
	    }

	    foreach($inserts as $i) {
		    $metricsModel->insert($i);
	    }

	    // drop old table
	    $query5 = "DROP TABLE " . $this->tablePrefix . "tbl_metrics_old";

	    $dba->query($query5);

    }
   
}