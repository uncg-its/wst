<?php

class Db_036_increase_confid_field_length extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            ALTER TABLE `" . $this->tablePrefix ."tbl_session_archive`
                MODIFY COLUMN `conf_id` BIGINT(20)
        ";

        $dba->query($query);

        $query = "
            ALTER TABLE `" . $this->tablePrefix ."tbl_recording_archive`
                MODIFY COLUMN `conf_id` BIGINT(20)
        ";

        $dba->query($query);

        $query = "
            ALTER TABLE `" . $this->tablePrefix ."tbl_attendee_archive`
                MODIFY COLUMN `conf_id` BIGINT(20)
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "
            ALTER TABLE `" . $this->tablePrefix ."tbl_session_archive`
                MODIFY COLUMN `conf_id` BIGINT(11)
        ";

        $query = "
            ALTER TABLE `" . $this->tablePrefix ."tbl_recording_archive`
                MODIFY COLUMN `conf_id` BIGINT(11)
        ";

        $query = "
            ALTER TABLE `" . $this->tablePrefix ."tbl_attendee_archive`
                MODIFY COLUMN `conf_id` BIGINT(11)
        ";

        $dba->query($query);

    }
   
}