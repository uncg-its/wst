<?php

class Db_026_wst_attendee_archive extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_attendee_archive` (
  `attendee_id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `conf_id` bigint(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `join_time_utc` int(11) NOT NULL,
  `leave_time_utc` int(11) NOT NULL,
  `duration` smallint(11) NOT NULL,
  `participant_type` varchar(25) NOT NULL DEFAULT '',
  `ip_address` varchar(25) NOT NULL DEFAULT '',
  `client_agent` varchar(50) NOT NULL DEFAULT '',
  `registered` varchar(10) DEFAULT NULL,
  `invited` varchar(10) DEFAULT NULL,
  `voip_duration` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`attendee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "DROP TABLE
            `" . $this->tablePrefix . "tbl_attendee_archive`;";

        $dba->query($query);

    }
   
}