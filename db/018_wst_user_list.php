<?php

/**
 * Changes the how the API works
 */

class Db_018_wst_user_list extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_user_list` (
              `euid` varchar(25) NOT NULL DEFAULT '',
  `webExId` varchar(50) NOT NULL DEFAULT '',
  `firstName` varchar(100) NOT NULL DEFAULT '',
  `lastName` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `webExUserId` int(25) DEFAULT NULL,
  `checksum` varchar(50) NOT NULL,
  `status` enum('active','inactive','pending') NOT NULL DEFAULT 'inactive',
  `action` enum('add','edit','delete') DEFAULT NULL,
  `source` enum('feed','manual') NOT NULL DEFAULT 'feed',
  `dept_number` varchar(10) NOT NULL DEFAULT '0',
  `org_short_code` varchar(25) NOT NULL DEFAULT 'undefined',
  `custom_tracking` text,
  `telephony_privileges` text,
  PRIMARY KEY (`euid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "DROP TABLE
            `" . $this->tablePrefix . "tbl_user_list`;";

        $dba->query($query);

    }
   
}