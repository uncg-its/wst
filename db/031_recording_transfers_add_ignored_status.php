<?php

class Db_031_recording_transfers_add_ignored_status extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "ALTER TABLE `" . $this->tablePrefix . "tbl_recording_archive`
            MODIFY COLUMN  upload_status enum('not-uploaded','pending','in-progress','uploaded','uploaded-with-errors','error','failed','ignored') DEFAULT 'not-uploaded',
            MODIFY COLUMN  download_status enum('not-downloaded','pending','in-progress','downloaded','error','failed','ignored') DEFAULT 'not-downloaded'";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "ALTER TABLE `" . $this->tablePrefix . "tbl_recording_archive`
            MODIFY COLUMN  upload_status enum('not-uploaded','pending','in-progress','uploaded','uploaded-with-errors','error','failed') DEFAULT 'not-uploaded',
            MODIFY COLUMN  download_status enum('not-downloaded','pending','in-progress','downloaded','error','failed') DEFAULT 'not-downloaded'";

        $dba->query($query);

    }
   
}