<?php

class Db_034_create_recording_folder_cloud_shares_tbl extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_recording_folder_cloud_shares` (
  `share_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `euid` varchar(25) NOT NULL DEFAULT '',
  `transaction_id` varchar(150) NOT NULL DEFAULT '',
  `folder_id` varchar(150) NOT NULL DEFAULT '',
  `service_name` tinytext NOT NULL,
  PRIMARY KEY (`share_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "DROP TABLE
            `" . $this->tablePrefix . "tbl_recording_folder_cloud_shares`;";

        $dba->query($query);

    }
   
}