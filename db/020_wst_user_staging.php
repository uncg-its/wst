<?php

/**
 * Changes the how the API works
 */

class Db_020_wst_user_staging extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_user_staging` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
  `euid` varchar(25) NOT NULL,
  `webExId` varchar(50) NOT NULL DEFAULT '',
  `firstName` varchar(100) NOT NULL DEFAULT '',
  `lastName` varchar(100) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `ldap_status` varchar(25) NOT NULL DEFAULT '',
  `eligibility_for_webex` enum('eligible','ineligible') NOT NULL DEFAULT 'ineligible',
  `record_change_timestamp` int(11) NOT NULL,
  `checksum` varchar(50) NOT NULL DEFAULT '',
  `timestamp` int(11) DEFAULT NULL,
  `dept_number` varchar(10) NOT NULL DEFAULT '0',
  `org_short_code` varchar(25) NOT NULL DEFAULT 'undefined',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "DROP TABLE
            `" . $this->tablePrefix . "tbl_user_staging`;";

        $dba->query($query);

    }
   
}