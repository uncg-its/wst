<?php

class Db_038_create_report_groups_table extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE `" . $this->tablePrefix ."tbl_report_groups` (
  `reportGroupId` int(11) NOT NULL AUTO_INCREMENT,
  `accountId` int(10) unsigned NOT NULL DEFAULT '0',
  `displayName` text NOT NULL,
  `orgShortCodeList` text NOT NULL,
  `isDefault` enum('y','n') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`reportGroupId`)
)
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "
            DROP TABLE `" . $this->tablePrefix ."tbl_report_groups`
        ";

        $dba->query($query);

    }
   
}