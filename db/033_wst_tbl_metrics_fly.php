<?php

class Db_033_wst_tbl_metrics_fly extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_metrics_fly` (
  `metric_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` tinytext NOT NULL,
  `rawData` text,
  PRIMARY KEY (`metric_id`)
) ENGINE=InnoDB AUTO_INCREMENT=442 DEFAULT CHARSET=utf8;
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "DROP TABLE
            `" . $this->tablePrefix . "tbl_metrics_fly`;";

        $dba->query($query);

    }
   
}