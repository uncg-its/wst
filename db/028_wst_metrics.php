<?php

class Db_028_wst_metrics extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_metrics` (
  `metric_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `date` tinytext NOT NULL,
  `numberOfActiveUsers` int(11) NOT NULL,
  `numberOfSessions` int(11) NOT NULL,
  `numberOfSessionsWithLength0To4` int(11) NOT NULL,
  `numberOfSessionsWithLength5To29` int(11) NOT NULL,
  `numberOfSessionsWithLength30To59` int(11) NOT NULL,
  `numberOfSessionsWithLength60To119` int(11) NOT NULL,
  `numberOfSessionsWithLength120OrHigher` int(11) NOT NULL,
  `numberOfSessionsWithRecording` int(11) NOT NULL,
  `numberOfSessionsWithTelephony` int(11) NOT NULL,
  `numberOfAttendees` int(11) NOT NULL,
  `numberOfAttendeesUnique` int(11) NOT NULL,
  `numberOfMobileAppUsers` int(11) NOT NULL,
  `callInUsers` int(11) NOT NULL,
  `callOutUsers` int(11) NOT NULL,
  `callInMinutes` int(11) NOT NULL,
  `callInTollFreeMinutes` int(11) NOT NULL,
  `callOutDomesticMinutes` int(11) NOT NULL,
  `callOutInternationalMinutes` int(11) NOT NULL,
  `peopleMinutes` int(11) NOT NULL,
  `numberOfRecordingsMade` int(11) NOT NULL,
  `recordingStorageUsed` float NOT NULL,
  `timestamp` int(11) NOT NULL,
  `status` enum('pending-verification','verified','sending','complete','error') NOT NULL DEFAULT 'pending-verification',
  `error_data` text,
  PRIMARY KEY (`metric_id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "DROP TABLE
            `" . $this->tablePrefix . "tbl_metrics`;";

        $dba->query($query);

    }
   
}