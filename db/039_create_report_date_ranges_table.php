<?php

class Db_039_create_report_date_ranges_table extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE `" . $this->tablePrefix ."tbl_report_date_ranges` (
  `rangeId` int(11) NOT NULL AUTO_INCREMENT,
  `accountId` int(10) unsigned DEFAULT '0',
  `startTimestamp` int(10) unsigned DEFAULT '0',
  `endTimestamp` int(10) unsigned DEFAULT '0',
  `isActive` enum('y','n') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`rangeId`)
)
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "
            DROP TABLE `" . $this->tablePrefix ."tbl_report_date_ranges`
        ";

        $dba->query($query);

    }
   
}