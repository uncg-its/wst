<?php

class Db_037_add_attempt_to_queue_tbl extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            ALTER TABLE `" . $this->tablePrefix ."tbl_api_queue`
                ADD COLUMN `attempt` INT DEFAULT NULL AFTER `q_id`
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "
			ALTER TABLE `" . $this->tablePrefix . "tbl_api_queue`
                DROP COLUMN `attempt`
        ;";

        $dba->query($query);

    }
   
}