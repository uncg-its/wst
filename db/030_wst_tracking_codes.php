<?php

class Db_030_wst_tracking_codes extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_tracking_codes` (
  `tracking_code_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `display_name` varchar(100) NOT NULL DEFAULT '',
  `user_list_field` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`tracking_code_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "DROP TABLE
            `" . $this->tablePrefix . "tbl_tracking_codes`;";

        $dba->query($query);

    }
   
}