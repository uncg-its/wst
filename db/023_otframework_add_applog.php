<?php

/**
 * Add App Log Table
 * 
 */

class Db_023_otframework_add_applog extends Ot_Migrate_Migration_Abstract
    {
        public function up($dba)
    {
        
        $query = "CREATE TABLE `" . $this->tablePrefix . "tbl_ot_applog` (
            `logId` int(10) unsigned NOT NULL AUTO_INCREMENT,
            `accountId` int(10) unsigned NOT NULL DEFAULT '0',
            `username` varchar(128) NOT NULL DEFAULT '',
            `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
            `subject` varchar(255) NOT NULL DEFAULT '',            
            `message` LONGTEXT NOT NULL DEFAULT '',
            `priority` int(10) unsigned NULL DEFAULT '0',
            `priorityName` varchar(64) NULL DEFAULT '',
            `attributeName` varchar(128) NULL DEFAULT '',
            `attributeId` varchar(64) NULL DEFAULT '',
            PRIMARY KEY (`logId`)
          ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;";
        
        $dba->query($query);
        
    }
    
    public function down($dba)
    {
        $query = "DROP TABLE `" . $this->tablePrefix . "tbl_ot_applog`";
        
        $dba->query($query);
        
    }

    }