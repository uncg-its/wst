<?php

class Db_025_wst_session_archive extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_session_archive` (
  `conf_id` bigint(11) unsigned NOT NULL,
  `session_name` text NOT NULL,
  `session_key` int(11) NOT NULL,
  `center` varchar(25) NOT NULL DEFAULT '',
  `host_username` varchar(20) NOT NULL DEFAULT '',
  `host_name` varchar(100) DEFAULT NULL,
  `start_time_utc` int(11) NOT NULL,
  `end_time_utc` int(11) NOT NULL,
  `timezone` varchar(50) NOT NULL DEFAULT '',
  `duration` smallint(11) NOT NULL,
  `registered_attendees` smallint(11) DEFAULT NULL,
  `invited_attendees` smallint(6) DEFAULT NULL,
  `total_attendees` smallint(6) DEFAULT NULL,
  `peak_attendee_count` smallint(11) NOT NULL,
  `call_in_participants` smallint(11) DEFAULT NULL,
  `call_out_participants` smallint(11) DEFAULT NULL,
  `voip_participants` smallint(6) DEFAULT NULL,
  `call_in_minutes` smallint(11) DEFAULT NULL,
  `call_in_tollfree_minutes` smallint(11) DEFAULT NULL,
  `call_out_domestic_minutes` smallint(11) DEFAULT NULL,
  `call_out_international_minutes` smallint(11) DEFAULT NULL,
  `voip_minutes` smallint(11) DEFAULT NULL,
  `people_minutes` mediumint(11) NOT NULL,
  `tracking_codes` text,
  `raw_response` blob NOT NULL,
  `parsed_attendees` enum('n','y') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`conf_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "DROP TABLE
            `" . $this->tablePrefix . "tbl_session_archive`;";

        $dba->query($query);

    }
   
}