<?php

/**
 * Changes the how the API works
 */

class Db_019_wst_api_queue extends Ot_Migrate_Migration_Abstract
{
    public function up($dba)
    {
        $query = "
            CREATE TABLE IF NOT EXISTS  `" . $this->tablePrefix ."tbl_api_queue` (
                `q_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `euid` varchar(25) NOT NULL,
  `webExId` varchar(25) NOT NULL DEFAULT '',
  `email` varchar(50) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `newWebExId` varchar(25) DEFAULT NULL,
  `dept_number` varchar(10) DEFAULT NULL,
  `org_short_code` varchar(25) DEFAULT NULL,
  `status` enum('pending','in-progress','complete','error','ignored') NOT NULL DEFAULT 'pending',
  `action` enum('add','edit','delete') NOT NULL DEFAULT 'edit',
  `message` text,
  `timestampAttempted` int(11) DEFAULT NULL,
  `shouldUpdateUserList` enum('y','n') NOT NULL DEFAULT 'n',
  `source` tinytext,
  PRIMARY KEY (`q_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
        ";

        $dba->query($query);

    }
    
    public function down($dba)
    {
        $query = "DROP TABLE
            `" . $this->tablePrefix . "tbl_api_queue`;";

        $dba->query($query);

    }
   
}